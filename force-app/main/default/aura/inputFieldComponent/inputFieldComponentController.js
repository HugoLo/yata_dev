({
	doInit : function(cmp, event, helper) {
        $A.createComponent(
            "lightning:recordForm",
            {
                "objectApiName": cmp.get("v.objectApiName"),
                "fields": cmp.get("v.fields"),
                "mode": "view",
                "onsubmit": cmp.getReference("c.handlePress"),
                "aura:id": "inputValue"
            },
            function(newButton, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    var body = cmp.get("v.body");
                    body.push(newButton);
                    newButton.set("v.body", body);
                    var inputTargetingMap = cmp.get("v.inputTargetingMap");
                    var fieldValueString = inputTargetingMap.fieldValueString;
                    cmp.find("inputValue").set("v.value", fieldValueString);
                }
                else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                    // Show offline error
                }
                else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
	}, 
    handlePress: function(cmp, event, helper) {
        alert("testing");
    }
})