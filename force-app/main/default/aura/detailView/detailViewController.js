({
	init : function(cmp, event, helper) {
		var defaultRulesSetting = cmp.get("v.defaultRulesSetting");
        var data = cmp.get("v.data");
        
        var rulesSettingName = data.rulesSettingName;
        //var rulesSetting = result1Dict[rulesSettingName].rulesSetting;
        //defaultTargetingDetail[i].ObjectName = rulesSetting.Name;
        var fieldSet = defaultRulesSetting[rulesSettingName].fieldSet;
        
        
        var fieldSetMap = {}
        for(var j=0; j<fieldSet.length; j++){
            fieldSetMap[fieldSet[j].fieldPath] = fieldSet[j];
        }
        var FieldName = data.FieldName;
        var targetFieldName = fieldSetMap[FieldName];
        data.FieldName = targetFieldName.label;
        var FieldValueString = data.FieldValueString;
        var objectAPINameMap = defaultRulesSetting[rulesSettingName].objectAPIName;
        if(objectAPINameMap){
            var objectAPIName = objectAPINameMap[FieldName];
            if(objectAPIName && FieldValueString){
                var exeAction1 = cmp.get("c.apex_getRecordById");
                //alert("objectAPIName: "+objectAPIName+'\n recordId: '+FieldValueString);
                exeAction1.setParams({ 
                    "objectName": objectAPIName,
                    "recordId": FieldValueString
                });
                
                Promise.all([
                    helper.serverSideCall(cmp, exeAction1)
                ]).then(
                    function(response) {
                        
                        var returnValue = response[0];
                        data.FieldValueString = returnValue.Name;
                        cmp.set("v.data", data);
                    }
                ).catch(
                    function(error) {
                        cmp.set("v.data", data);
                    }
                );
            }
            else{
                cmp.set("v.data", data);
            }
        }
        else{
        	cmp.set("v.data", data);   
        }
	}
})