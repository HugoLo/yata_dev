({
    loadInitData : function(cmp, event, helper) {
        
        cmp.set("v.Spinner", true);
		cmp.set("v.haveRecord", false);        
        var previewLimit = cmp.get("v.previewLimit");
        
        var parameter = {}
		var exeAction1 = cmp.get("c.apex_getRedemptionList");
        parameter["MemberSFId"] = cmp.get("v.recordId");
        parameter["Status"] = null;
        parameter["RedeemDateFrom"] = null;
        parameter["RedeemDateTo"] = null;
        parameter["CouponValidDate"] = null;
        parameter["CouponCode"] = null;
        parameter['limit'] = previewLimit.toString();
        exeAction1.setParams({ 
            parameter: parameter
        });
        
        var exeAction2 = cmp.get("c.apex_getFieldset");
        exeAction2.setParams({
            objectName : 'Redemption__c',
            fieldSetName : 'RedemptionFieldSetPreview'
        }); 
        
        
        Promise.all([
            helper.serverSideCall(cmp, exeAction1),
            helper.serverSideCall(cmp, exeAction2)
        ]).then(
            function(response) {
                
                var result2 = response[2];
                console.log("result2: "+result2);
                
                var result1 = response[1];
                var result1Arr = JSON.parse(result1);
                
                var result = response[0];
                var resultDict = JSON.parse(result);
                var ReturnCode = resultDict.ReturnCode;
                var ReturnMessage = resultDict.ReturnMessage;
                
                
                if(ReturnCode == "1"){
                    var Redemptions = resultDict.Redemptions;
						
                    if(Redemptions.length > 0){
                        cmp.set("v.haveRecord", true);
                    }                    
                   
                    var mycolumns = [];
                    for(var i=0; i<result1Arr.length; i++){
                        var theResult = result1Arr[i];
                        var fieldPath = theResult.fieldPath;
                        fieldPath = fieldPath.toLowerCase();
                        
                        if(fieldPath == 'pgid__c'){
                            mycolumns.push({ label:theResult.label, type: 'button', typeAttributes: { label: { fieldName: fieldPath }, variant:'base', title: 'Click to View Details'}});
                        }
                        else{
                         	if(theResult.type == 'double'){
                                mycolumns.push({ label: theResult.label, fieldName: fieldPath, type: 'number'});
                            }
                            else if(theResult.type == 'datetime'){
                                mycolumns.push({ label: theResult.label, fieldName: fieldPath, type: 'date', sortable: true, typeAttributes:{ year: 'numeric', month: '2-digit', day: 'numeric', hour: '2-digit', minute: '2-digit', second: '2-digit', hour12: false}});
                            }
                            else if(theResult.type == 'date'){
                                mycolumns.push({ label: theResult.label, fieldName: fieldPath, type: 'date', sortable: true, typeAttributes:{ year: 'numeric', month: '2-digit', day: 'numeric'}});
                            }
                            else if(theResult.type == 'reference'){
                                mycolumns.push({label: theResult.label, fieldName: fieldPath, type: 'url', typeAttributes: { label: { fieldName: fieldPath+'_name' } }});
                            }
                            else{
                                mycolumns.push({ label: theResult.label, fieldName: fieldPath, type: theResult.type});
                            }   
                        }
                    }
                    
                    cmp.set("v.previewcolumns", mycolumns);
                    cmp.set("v.previewdata", Redemptions);
                }
                else{
                	
                    
                }
            	
                cmp.set("v.Spinner", false);
                /*
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": " Your record have been loaded successfully."
                });
                toastEvent.fire();
                */
            }
        ).catch(
            function(error) {
                cmp.set("v.Spinner", false);
            }
        ); 
        
	},
    view_all_member_redemption : function(cmp, event, helper) {
        cmp.set("v.inputPGID", null);
        helper.view_all_member_redemption(cmp, event);
    },
    hideExampleModal : function(cmp) {
        var modal = cmp.find("campaignModal");
        var modalBackdrop = cmp.find("myModal-Back");
        $A.util.removeClass(modal, 'slds-fade-in-open');
    	$A.util.removeClass(modalBackdrop, 'slds-backdrop_open');
    },
    handleRowAction: function (cmp, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        switch (action.name) {
            case 'view_details':
                //alert(row.length);
                var mydata = cmp.get("v.mydata");
                var index = mydata.indexOf(row);
                //alert(index);
                var result2Arr = cmp.get("v.result2Arr");
                
                var redemption = mydata[index];
                
                for(var i=0; i<result2Arr.length; i++){
                    var result = result2Arr[i];
                    var fieldPath = result.fieldPath;
                    fieldPath = fieldPath.toLowerCase();
                    var type = result.type;
                    var value = '';
                    if(type == 'reference'){
                        var fieldPathName = fieldPath+'_name';
                        value = '<a href="'+redemption[fieldPath]+'" >'+redemption[fieldPathName]+'</a>'
                    }
                    else{
                    	value = redemption[fieldPath];    
                    }
                    result["value"] = value;
                }
                
                //alert(JSON.stringify(resultArr));
                cmp.set("v.result2Arr", result2Arr);
                cmp.set("v.show_detail", true)
                
                
                break;
            case 'edit_status':
                //helper.editRowStatus(cmp, row, action);
                break;
            default:
                //helper.showRowDetails(row);
                break;
        }
    },
    updateColumnSorting: function (cmp, event, helper) {
        cmp.set('v.isLoading', true);
        // We use the setTimeout method here to simulate the async
        // process of the sorting data, so that user will see the
        // spinner loading when the data is being sorted.
        setTimeout(function() {
            var fieldName = event.getParam('fieldName');
            var sortDirection = event.getParam('sortDirection');
            cmp.set("v.sortedBy", fieldName);
            cmp.set("v.sortedDirection", sortDirection);
            helper.sortData(cmp, fieldName, sortDirection);
            cmp.set('v.isLoading', false);
        }, 0);
    },
    handleFilterRedemption: function (cmp, event, helper) {
        helper.loadData(cmp, event, 0); 
    },
    handlePerviewRowAction: function (cmp, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        var pgid = row.pgid__c;
        var inputPGID = cmp.find("inputPGID");
        inputPGID.set("v.value", pgid.toString());
        helper.view_all_member_redemption(cmp, event);
    },
    loadMoreData: function(cmp, event, helper){
        
        //event.getSource().set("v.Spinner_view_all", true);
        //alert('loadMoreData');
        //helper.view_all_member_redemption(cmp, event);
        var mydata = cmp.get("v.mydata");
        var dataLength = mydata.length;
        var totalNumberOfRows = 5;
        //if(dataLength >= totalNumberOfRows){
            //alert('loadMoreData: '+mydata.length);
            //helper.loadData(cmp, event);
        //}
    },
    comboboxHandleChange: function(cmp, event, helper){
        var selectedOptionValue = event.getParam("value");
        cmp.set("v.optionValue", selectedOptionValue);
        var limit = cmp.get("v.limit");
        var offset = (selectedOptionValue - 1) * limit;
        helper.loadData(cmp, event, offset);
    }
})