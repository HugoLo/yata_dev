({
    serverSideCall : function(component,action) {
        return new Promise(function(resolve, reject) { 
            action.setCallback(this, 
                function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                       	resolve(response.getReturnValue());
                    } else {
                       reject(new Error(response.getError()));
                    }
                }); 
            $A.enqueueAction(action);
        });
    },
    view_all_member_redemption : function(cmp, event) {
        var modal = cmp.find("campaignModal");
        var modalBackdrop = cmp.find("myModal-Back");
        $A.util.addClass(modal, 'slds-fade-in-open');
        $A.util.addClass(modalBackdrop, 'slds-backdrop_open');
  		var self = this;
        
        cmp.set("v.show_detail", false);
        cmp.set("v.Spinner_view_all", true);
        cmp.set("v.optionValue", "1");
        
     	var exeAction1 = cmp.get("c.apex_getFieldset");
        //Set the Object parameters and Field Set name
        exeAction1.setParams({
            objectName : 'Redemption__c',
            fieldSetName : 'RedemptionFieldSet1'
        });
        
        var exeAction2 = cmp.get("c.apex_getFieldset");
        //Set the Object parameters and Field Set name
        exeAction2.setParams({
            objectName : 'Redemption__c',
            fieldSetName : 'RedemptionFieldSet2'
        });
        
        var exeAction3 = cmp.get("c.apex_getNoRecordDisplay");
        //Set the Object parameters and Field Set name
        exeAction3.setParams({
            name : 'NoRecordDisplay_Txn'
        });
        
        var self = this;
        
        Promise.all([
            self.serverSideCall(cmp, exeAction1),
            self.serverSideCall(cmp, exeAction2),
            self.serverSideCall(cmp, exeAction3)
        ]).then(
            function(response) {
                
                var result = response[0];               
                var resultArr = JSON.parse(result);
                
                var result1 = response[1];                
                var result1Arr = JSON.parse(result1);
                
                var result2 = response[2]; 
                cmp.set("v.limit", result2);
                
                var mycolumns = [];
                mycolumns.push({label: 'View', type: 'button', initialWidth: 135, typeAttributes: { label: 'View Details', name: 'view_details', title: 'Click to View Details'}});
                for(var i=0; i<resultArr.length; i++){
                    var theResult = resultArr[i];
                    var fieldPath = theResult.fieldPath;
                    fieldPath = fieldPath.toLowerCase();
                    
                    if(theResult.type == 'double'){
                        mycolumns.push({ label: theResult.label, fieldName: fieldPath, type: 'number', sortable: true});
                    }
                    else if(theResult.type == 'datetime'){
                        mycolumns.push({ label: theResult.label, fieldName: fieldPath, type: 'date', sortable: true, typeAttributes:{ year: 'numeric', month: '2-digit', day: 'numeric', hour: '2-digit', minute: '2-digit', second: '2-digit', hour12: false}});
                    }
                    else if(theResult.type == 'date'){
                        mycolumns.push({ label: theResult.label, fieldName: fieldPath, type: 'date', sortable: true, typeAttributes:{ year: 'numeric', month: '2-digit', day: 'numeric'}});
                    }
                    else if(theResult.type == 'reference'){
                        mycolumns.push({label: theResult.label, fieldName: fieldPath, type: 'url', typeAttributes: { label: { fieldName: fieldPath+'_name' } }});
                    }
                    else{
                        mycolumns.push({ label: theResult.label, fieldName: fieldPath, type: theResult.type, sortable: true});
                    }
                }
                
              
                cmp.set("v.result2Arr", result1Arr);
                cmp.set("v.mycolumns", mycolumns);
                self.loadData(cmp, event, 0);
                
            }
        ).catch(
            function(error) {
                cmp.set("v.show_detail", false);
                cmp.set("v.Spinner_view_all", false);
            }
        );
    },
    loadData : function(cmp, event, offset) {
     
        cmp.set("v.show_detail", false);
        cmp.set("v.Spinner_view_all", true);
        
        var inputSelectStatus = cmp.find("inputSelectStatus");
        var inputSelectStatusValue = inputSelectStatus.get("v.value") ?inputSelectStatus.get("v.value") : null;
        var inputCouponCode = cmp.find("inputCouponCode");
        var inputCouponCodeValue = inputCouponCode.get("v.value") ? inputCouponCode.get("v.value") : null;
        var inputRedeemDateFrom = cmp.find("inputRedeemDateFrom");
        var inputRedeemDateFromValue = inputRedeemDateFrom.get("v.value") ? inputRedeemDateFrom.get("v.value") : null;
        var inputRedeemDateTo = cmp.find("inputRedeemDateTo");
        var inputRedeemDateToValue = inputRedeemDateTo.get("v.value") ? inputRedeemDateTo.get("v.value") : null;
        var inputCouponValidDate = cmp.find("inputCouponValidDate");
        var inputCouponValidDateValue = inputCouponValidDate.get("v.value") ? inputCouponValidDate.get("v.value") : null;
        var inputPGID = cmp.find("inputPGID");
        var inputPGIDValue = inputPGID.get("v.value") ? inputPGID.get("v.value") : null;
       	var limit = cmp.get("v.limit");
        
        var parameter = {}
		var exeAction1 = cmp.get("c.apex_getRedemptionList");
        parameter["MemberSFId"] = cmp.get("v.recordId");
        parameter["RedemptionPGId"] = inputPGIDValue;
        parameter["Status"] = inputSelectStatusValue;
        parameter["RedeemDateFrom"] = inputRedeemDateFromValue;
        parameter["RedeemDateTo"] = inputRedeemDateToValue;
        parameter["CouponValidDate"] = inputCouponValidDateValue;
        parameter["CouponCode"] = inputCouponCodeValue;
        parameter["offset"] = offset.toString();
        parameter["limit"] = limit.toString();
        exeAction1.setParams({ 
            parameter: parameter
        });
        
        var self = this;
        
        Promise.all([
            self.serverSideCall(cmp, exeAction1)
        ]).then(
            function(response) {

                var result = response[0];
                var resultDict = JSON.parse(result);
                var ReturnCode = resultDict.ReturnCode;
                var ReturnMessage = resultDict.ReturnMessage;
                
                var mydata = [];
                if(ReturnCode == "1"){
                    var TotalCount = resultDict.TotalCount;
                    var page = Math.ceil(TotalCount/limit);
                    var options = [];
                    for(var i=0; i<page; i++){
                    	var pageDict = {'label': (i+1).toString(), 'value': (i+1).toString()}
                        options.push(pageDict);   
                    }
                    cmp.set("v.options", options);
                    var Redemptions = resultDict.Redemptions;
                    cmp.set("v.mydata", Redemptions)
                    cmp.set("v.show_detail", false)
					                    
                }
                else{
                    
                    
                }
                
                cmp.set("v.Spinner_view_all", false);
            }
        ).catch(
            function(error) {
                cmp.set("v.show_detail", false);
                cmp.set("v.Spinner_view_all", false);
            }
        );
    },
    sortData: function (cmp, fieldName, sortDirection) {
        var data = cmp.get("v.mydata");
        var reverse = sortDirection !== 'asc';

        data = Object.assign([],
            data.sort(this.sortBy(fieldName, reverse ? -1 : 1))
        );
        cmp.set("v.mydata", data);
    },
    sortBy: function (field, reverse, primer) {
        var key = primer
            ? function(x) { return primer(x[field]) }
            : function(x) { return x[field] };

        return function (a, b) {
            var A = key(a);
            var B = key(b);
            return reverse * ((A > B) - (B > A));
        };
    }
})