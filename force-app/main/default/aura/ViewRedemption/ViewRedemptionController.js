({
	loadInitData : function(cmp, event, helper) {

        var exeAction1 = cmp.get("c.apex_getRedemption");
        exeAction1.setParams({ 
            "RedemptionPGId": cmp.get("v.RedemptionPGId")
        });
        
        
        var exeAction2 = cmp.get("c.apex_getFieldset");
        exeAction2.setParams({ 
            "objectName": "Redemption__c",
            "fieldSetName": "RedemptionFieldSet1"
        });
        
        Promise.all([
            helper.serverSideCall(cmp, exeAction1),
            helper.serverSideCall(cmp, exeAction2)
        ]).then(
            function(response) {
                
                var result = response[0];
                var resultDict = JSON.parse(result);
                var ReturnCode = resultDict.ReturnCode;
                var ReturnMessage = resultDict.ReturnMessage;
                
                if(ReturnCode == "1"){
                    var fieldNameArr = [];
                    var Redemptions = resultDict.Redemptions;
                    var Redemption = Redemptions[0];
                    for (var key in Redemption) {
                        fieldNameArr.push(key);
                    }
                    cmp.set("v.fieldNameArr", fieldNameArr);
                    var result1 = response[1];
                    var result1Arr = JSON.parse(result1);
                    console.log(result1);
                    
                    for(var i=0; i<result1Arr.length; i++){
                        var result = result1Arr[i];
                        var fieldPath = result.fieldPath;
                        var lowerCaseFieldPath = fieldPath.toLowerCase();
                        var value = Redemption[lowerCaseFieldPath];
                        result["value"] = value;
                    }
                    
                    
                    console.log(JSON.stringify(result1Arr));
                    cmp.set("v.result1Arr", result1Arr);
                }
                else{
                	
                    
                }
                
                
                
                
            }
        ).catch(
            function(error) {
                
            }
        );
        
        
                
	},
    onload : function(cmp, event, helper) {
        //var objectInfo = event.getParams().objectInfos;
        //console.log(JSON.stringify(objectInfo));
        //objectInfo["Redemption__c"].fields['RedeemPoint__c'] = '1000';
        
    }, 
    edit : function(cmp, event, helper) {
        cmp.set("v.editMode", "true");
        /*
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
        	"url": '/lightning/n/ViewRedemption?memberId=123'
        });
       urlEvent.fire();
       */
    },
    update : function(cmp, event, helper){
        cmp.set("v.editMode", "false");
    } 
})