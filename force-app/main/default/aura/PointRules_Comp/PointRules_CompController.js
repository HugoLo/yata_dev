({
	init : function(cmp, event, helper) {
		helper.loadIncentiveRule(cmp, event);
	},
    showExampleModal : function(component, event, helper) {
        helper.showExampleModal(component, event);
    },
    hideExampleModal : function(component) {
        var modal = component.find("campaignModal");
        var modalBackdrop = component.find("myModal-Back");
        $A.util.removeClass(modal, 'slds-fade-in-open');
    	$A.util.removeClass(modalBackdrop, 'slds-backdrop_open');
    }, 
    save : function(component, event, helper) {
        var label = event.getSource().get("v.label")
        
        var incentiveRule = {}
        var canUpdate = true;
        var recordTypeName = component.get("v.recordTypeName");
        
        ////////////////////////  basicInfo  ////////////////////////
        var basicInfo = {}
        var recordData = component.get("v.recordData");
        var jsonString;
        var campaignStatus = recordData.Status;
        var applyStatus;
        if(label == 'Inactive'){
            applyStatus = 'Inactive';
            
            
            var basicInfo = component.get("v.defaultBasicInfo");
            var targetings = component.get("v.cloneDefaultTargetings");
            //alert(JSON.stringify(targetings));
            var conditions = component.get("v.cloneDefaultConditions");
            var actions = component.get("v.defaultActions");
            incentiveRule['Action'] = actions;
            incentiveRule["Condition"] = conditions;
            incentiveRule["BasicInfo"] = basicInfo;
            incentiveRule["Targeting"] = targetings;
            jsonString = JSON.stringify(incentiveRule);
            
        }
        else{
            if(label == 'Save'){
                applyStatus = campaignStatus;
            }
            else{
                applyStatus = "Active";
            }
            
            
            var actions = component.get("v.actions");
            var basicInfoTriggerSysCon = component.get("v.basicInfoTriggerSysCon");
            basicInfo["RuleFromObj"] = "Campaign";
            basicInfo["IncentiveRuleNo"] = recordData.IncentiveRuleNo__c ? recordData.IncentiveRuleNo__r.Name : null;
            basicInfo["RuleName"] = recordData.Name ? recordData.Name : null;
            basicInfo["RuleFromRecdID"] = recordData.Id ? recordData.Id : null;
            basicInfo["Status"] = recordData.Status ? recordData.Status: null;
            basicInfo["StartDate"] = recordData.StartDate ? recordData.StartDate : null;
            basicInfo["EndDate"] = recordData.EndDate ? recordData.EndDate : null;
            
            
            
            var triggerPoint = {}
            for(var key in basicInfoTriggerSysCon){
                triggerPoint[key] = basicInfoTriggerSysCon[key] ? basicInfoTriggerSysCon[key] : null;
            }
            basicInfo["TriggerPoint"] = triggerPoint;
            ////////////////////////  basicInfo  ////////////////////////
            
            
            ////////////////////////  targetings  ////////////////////////
            var targetings = component.get("v.targetings");
            var targetRowDataMap = component.get("v.targetRowDataMap");
            var SpecificMemberList = component.get("v.SpecificMemberList");
            //console.log("@@@SpecificMemberList: "+JSON.stringify(SpecificMemberList));
            targetings["SpecificMemberOnly"] = recordData.SpecificMember__c ? recordData.SpecificMember__c : false;        
            
            var SpecificMember = recordData.SpecificMember__c ? recordData.SpecificMember__c : false;
            targetings["SpecificMemberList"] = SpecificMember ? SpecificMemberList : [];
            //targetings["SpecificMemberList"] = null;
            var targetingLogic = targetings["Logic"];
            var targetingLogicIsBalanced = SpecificMember ? true : helper.isBalanced(targetingLogic) ;
            var inputTargetLogic = component.find("inputTargetLogic");
            if(inputTargetLogic){
                if(targetingLogicIsBalanced){
                    inputTargetLogic.setCustomValidity("");
                }
                else{
                    inputTargetLogic.setCustomValidity("Error: Check the spelling in your filter logic.");
                }
                inputTargetLogic.reportValidity();
            }
            
            
            canUpdate = canUpdate ? targetingLogicIsBalanced : false;
            var targetingDetails = targetings["Detail"];
            //alert(canUpdate);
            //alert(targetingDetails.length);
            for(var i=0; i<targetingDetails.length; i++){
                var detail = targetingDetails[i]
                detail["No"] = (i+1);
                var FieldType = detail["FieldType"];
                var FieldValueString = detail["FieldValueString"];
                var FieldValue = typeof FieldValueString == "string" ? FieldValueString.split(";") : [FieldValueString];
                //FieldValue.push(FieldValueString);
                detail["FieldValue"] = FieldValue;
                var Operator = detail["Operator"];
                if(FieldType == 'Date' && FieldValueString && Operator != '*'){
                    var isValidDate = helper.isValidDate(FieldValueString);
                    canUpdate = canUpdate ? isValidDate : false;
                    detail["isValidValue"] = isValidDate;
                }
                var rulesSettingName = detail["rulesSettingName"];
                var rowData = targetRowDataMap[rulesSettingName];
                var rulesSetting = rowData["rulesSetting"];
                var ObjectRelationship = rulesSetting["ObjectRelationship__c"];
                detail["ObjectRelation"] = ObjectRelationship;
                targetingDetails[i] = detail;
            }
            ////////////////////////  targetings  ////////////////////////
            
            
            ////////////////////////  conditions  ////////////////////////
            
            var conditions = component.get("v.conditions");
            
            var conditionDetails = [];
            if(conditions){
                var conditionDataMap = component.get("v.conditionDataMap");
                var conditionLogic = conditions["Logic"];
                var conditionLogicIsBalanced = helper.isBalanced(conditionLogic);
                var inputConditionLogic = component.find("inputConditionLogic");
                if(inputConditionLogic){
                    if(conditionLogicIsBalanced){
                        inputConditionLogic.setCustomValidity("");
                    }
                    else{
                        inputConditionLogic.setCustomValidity("Error: Check the spelling in your filter logic.");
                    }
                    inputConditionLogic.reportValidity();
                }
                
                canUpdate = canUpdate ? conditionLogicIsBalanced : false;
                conditionDetails = conditions["Detail"];
                var RelatedTo = conditions["RelatedTo"];
                for(var i=0; i<conditionDetails.length; i++){
                    var detail = conditionDetails[i];
                    detail["No"] = (i+1);
                    var FieldType = detail["FieldType"];
                    var FieldValueString = detail["FieldValueString"];
                    var FieldValue = typeof FieldValueString == "string" ? FieldValueString.split(";") : [FieldValueString];
                    detail["FieldValue"] = FieldValue;
                    var Operator = detail["Operator"];
                    
                    if(FieldType == 'Date' && FieldValueString && Operator != '*'){
                        var isValidDate = helper.isValidDate(FieldValueString);
                        canUpdate = canUpdate ? isValidDate : false;
                        detail["isValidValue"] = isValidDate;
                    }
                    else if(FieldType == 'Time' && FieldValueString){
                        console.log('@@@FieldValueString: '+FieldValueString);
                        var res = FieldValueString.split(":");
                        var d = new Date();
                        var hour = res[0]-8;
                        hour = hour <= 0 ? (24 + hour) : hour;
                        d.setHours(hour);
                        d.setMinutes(res[1]);
                        d.setSeconds(res[2]);
                        console.log('@@@toUTCString: '+d.getHours());
                        console.log('@@@toUTCString: '+d.getMinutes());
                        console.log('@@@toUTCString: '+d.getSeconds());
                        var hourString = d.getHours().toString();
                        hourString = hourString.length == 1 ? '0'+hourString : hourString;
                        var minutesString = d.getMinutes().toString();
                        minutesString = minutesString.length == 1 ? '0'+minutesString : minutesString;
                        var secondString = d.getSeconds().toString();
                        secondString = secondString.length == 1 ? '0'+secondString : secondString;
                        //detail["FieldValueString"] = hourString+':'+minutesString+':'+secondString;
                        
                        //console.log('@@@toUTCString: '+d.toUTCString());
                    }
                    
                    
                    var rulesSettingName = detail["rulesSettingName"];
                    var conditionData = conditionDataMap[RelatedTo];
                    var rowData = conditionData[rulesSettingName];
                    var rulesSetting = rowData["rulesSetting"];
                    var ObjectRelationship = rulesSetting["ObjectRelationship__c"];
                    var ObjectLevel = rulesSetting["ObjectLevel__c"];
                    detail["ObjectLevel"] = ObjectLevel;
                    detail["ObjectRelation"] = ObjectRelationship;
                }
                var RelatedTo = conditions["RelatedTo"];
                actions.ActionLevel = RelatedTo;
            }
            if(recordTypeName == 'CouponDistribution'){
                //actions["ActionLevel"] = "Backend";
                actions["ActionLevel"] = "Txn";
                actions["ActionCon"] = "Backend";
            }
            //alert(canUpdate);
            ////////////////////////  conditions  ////////////////////////
            
            
            ////////////////////////  actions  ////////////////////////
            var actionType = actions.ActionType;
            actions.PointTo = 'Campaign';
            if(actionType){
                if(actionType == 'PointMultiplier'){
                    
                    var inputPointMutliplierValue = actions["PointMultiplier"];
                    var inputSelectPointGroupValue = actions["PointGroup"];
                    var inputMaximumPointEarnedValue = actions["MaxPoint"];
                    //canUpdate = (inputPointMutliplierValue && inputSelectPointGroupValue && inputMaximumPointEarnedValue) ? canUpdate : false;
                    canUpdate = (inputPointMutliplierValue) ? canUpdate : false;		
                                
                    actions.RefObj = null;
                    actions.RefObjAccField = null;
                    actions.ActionTo = "Self";
                    actions.RefObj = null;
                    actions.RefObjAccField = null;
                    actions.PointBase = "Net Spending";
                    actions.MaxTimes = null;
                    actions.PointAmount = null;
                    actions.Reward = null;
                }
                else if(actionType == 'FixPoint'){
                    
                    var inputPointAmountValue = actions["PointAmount"];
                    var inputSelectPointGroupValue = actions["PointGroup"];
                    var inputMaximumPointEarnedValue = actions["MaxPoint"];
                    //canUpdate = (inputPointAmountValue && inputSelectPointGroupValue && inputMaximumPointEarnedValue) ? canUpdate : false;
                    canUpdate = (inputPointAmountValue) ? canUpdate : false;
                    
                    actions.RefObj = null;
                    actions.RefObjAccField = null;
                    actions.ActionTo = "Self";
                    actions.RefObj = null;
                    actions.RefObjAccField = null;
                    actions.PointBase = "Net Spending";
                    actions.MaxTimes = null;
                    actions.Reward = null;
                    actions.PointMultiplier = null;
                }
                else if(actionType == 'Coupon'){
                    var inputSelectRewardValue = actions["Reward"];
                    if(recordTypeName == 'CouponDistribution'){
                        actions.MaxTimes = null;
                        canUpdate = inputSelectRewardValue ? canUpdate : false;		
                    }
                    else{
                        var inputMaximumTimesValue = actions["MaxTimes"];
                        //canUpdate = (inputSelectRewardValue && inputMaximumTimesValue) ? canUpdate : false;
                        canUpdate = (inputSelectRewardValue) ? canUpdate : false;
                    }
                    
                    actions.RefObj = null;
                    actions.RefObjAccField = null;
                    actions.ActionTo = "Self";
                    actions.RefObj = null;
                    actions.RefObjAccField = null;
                    actions.PointBase = "Net Spending";
                    actions.PointMultiplier = null;
                    actions.PointGroup = null;
                    actions.MaxPoint = null;
                    actions.PointAmount = null;            
                }
            }
            else{
                canUpdate = false;
            }
       
            
            
            incentiveRule['Action'] = actions;
            ////////////////////////  actions  ////////////////////////
            
            
            incentiveRule["Condition"] = conditions;
            incentiveRule["BasicInfo"] = basicInfo;
            incentiveRule["Targeting"] = targetings;
            console.log(JSON.stringify(incentiveRule));
            
            jsonString = JSON.stringify(incentiveRule);
        }
        
        
        //alert(canUpdate);
        if(canUpdate){
        	
            var action = component.get("c.apex_saveJson");
            action.setParams({ 
                "recordId": component.get("v.recordId"),
                "jsonString": jsonString,
                "applyStatus": applyStatus
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    
                    var modal = component.find("campaignModal");
                    var modalBackdrop = component.find("myModal-Back");
                    $A.util.removeClass(modal, 'slds-fade-in-open');
                    $A.util.removeClass(modalBackdrop, 'slds-backdrop_open');
                    
                    helper.loadIncentiveRule(component);
                }
    
            });
            
            $A.enqueueAction(action); 
          	
        }else{
            
            
            if(SpecificMember){
                if(recordTypeName == 'CouponDistribution'){
                    
                }
                else{
                    conditions["Detail"] = conditionDetails;
                    component.set("v.conditions", conditions);
                }
            }
            else{
                if(recordTypeName == 'CouponDistribution'){
                    targetings["Detail"] = targetingDetails;
                    component.set("v.targetings", targetings);
                }
                else{
                    targetings["Detail"] = targetingDetails;
                    component.set("v.targetings", targetings);
                    conditions["Detail"] = conditionDetails;
                    component.set("v.conditions", conditions);
                }
            }
            alert("Please input all fields.");
        }
        

    },
    addTargetDetail: function(cmp, event, helper){
     
        //alert('testing123');
        
        var action = cmp.get("c.apex_addTargetDetail");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                var resultDict = JSON.parse(result);
                var targetings = cmp.get("v.targetings");
                var detail = targetings.Detail;
                detail.push(resultDict);
                targetings["Detail"] = detail;
                cmp.set("v.targetings", targetings);
            }

            // Display toast message to indicate load status
            /*
            var toastEvent = $A.get("e.force:showToast");
            if (state === 'SUCCESS'){
                toastEvent.setParams({
                    "title": "Success!",
                    "message": " Your record have been loaded successfully."
                });
            }
            else {
                toastEvent.setParams({
                        "title": "Error!",
                        "message": " Something has gone wrong."
                });
            }
            toastEvent.fire();
            */
        });
        
        $A.enqueueAction(action);
        
    },
    inputSelectActionTypeChange: function(component, event, helper){
        var inputSelectActionType = component.find("inputSelectActionType");
        var ActionType = inputSelectActionType.get("v.value");
        var actions = component.get("v.actions");
        actions["ActionType"] = ActionType;
		component.set("v.actions", actions);
    },
    inputSelectPointGroupChange : function (component, event, helper){
        var inputSelectPointGroup = component.find("inputSelectPointGroup");
        var PointGroup = inputSelectPointGroup.get("v.value");
        var actions = component.get("v.actions");
        //alert(PointGroup);
        if(PointGroup){
            actions["PointGroup"] = PointGroup;
        }
        else{
            actions["PointGroup"] = null;
        }
        component.set("v.actions", actions);
    },
    /*
    inputPointMultiplierChange: function(component, event, helper){
        var inputPointMultiplier = component.find("inputPointMultiplier");
        var pointMultiplier = inputPointMultiplier.get("v.value");
        var actions = component.get("v.actions");
        actions["PointMultiplier"] = pointMultiplier;
		component.set("v.actions", actions); 
    },
    */
    /*
    inputPointAmountChange: function(component, event, helper){
        var inputPointAmount = component.find("inputPointAmount");
        var pointAmount = inputPointAmount.get("v.value");
        var actions = component.get("v.actions");
        actions["PointAmount"] = pointAmount;
		component.set("v.actions", actions); 
    },
    */
    /*
    inputMaxPointChange: function(component, event, helper){
        var inputMaxPoint = component.find("inputMaxPoint");
        var maxPoint = inputMaxPoint.get("v.value");
        var actions = component.get("v.actions");
        actions["MaxPoint"] = maxPoint;
		component.set("v.actions", actions); 
    },
    */
    inputSelectRewardChange: function(component, event, helper){
        var inputSelectReward = component.find("inputSelectReward");
        var rewardId = inputSelectReward.get("v.value");  
        console.log(rewardId);
        var actions = component.get("v.actions");
        actions["Reward"] = rewardId;
        component.set("v.actions", actions);
    },
    addConditionDetail: function(cmp, event, helper){
             
        var action = cmp.get("c.apex_addConditionDetail");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                var resultDict = JSON.parse(result);
                var conditions = cmp.get("v.conditions");
                var detail = conditions.Detail;
                detail.push(resultDict);
                conditions["Detail"] = detail;
                cmp.set("v.conditions", conditions);
            }

            // Display toast message to indicate load status
            var toastEvent = $A.get("e.force:showToast");
            if (state === 'SUCCESS'){
                toastEvent.setParams({
                    "title": "Success!",
                    "message": " Your record have been loaded successfully."
                });
            }
            else {
                toastEvent.setParams({
                        "title": "Error!",
                        "message": " Something has gone wrong."
                });
            }
            toastEvent.fire();
        });
        
        $A.enqueueAction(action);
        
    },
    inputSelectConditionObjectChange: function(component, event, helper){
        var inputSelectConditionObject = component.find("inputSelectConditionObject");
        var relatedObject = inputSelectConditionObject.get("v.value");  
        var conditions = component.get("v.conditions");
        var Detail = conditions["Detail"]; 
        console.log(relatedObject);
        conditions["RelatedTo"] = relatedObject;
        component.set("v.conditions.Detail", []);
        component.set("v.conditions.Detail", Detail);
        component.set("v.conditions", conditions);
    },
    inputSelectConditionTypeChange: function(component, event, helper){
        var inputSelectConditionType = component.find("inputSelectConditionType");
        var ConditionType = inputSelectConditionType.get("v.value");
        var SysCon = inputSelectConditionType.get("v.value");
        var conditions = component.get("v.conditions");
        conditions["ConditionType"] = ConditionType;
        conditions["SysCon"] = SysCon;
        component.set("v.conditions", conditions);
    },
    /*
    inputConditionAmountChange: function(component, event, helper){
        var inputConditionAmount = component.find("inputConditionAmount");
        var conditionAmount = inputConditionAmount.get("v.value");
        console.log("conditionAmount: "+conditionAmount);
        var conditions = component.get("v.conditions");
        conditions["Amount"] = conditionAmount;
		component.set("v.conditions", conditions); 
    },
    */
    createIncentiveRule: function(component, event, helper){
        component.set("v.modalSpinner", true);
        var modal = component.find("campaignModal");
        var modalBackdrop = component.find("myModal-Back");
        $A.util.addClass(modal, 'slds-fade-in-open');
        $A.util.addClass(modalBackdrop, 'slds-backdrop_open');
        
        var incentiveRule = {}
        
        var exeAction1 = component.get("c.apex_getRecordById");
        exeAction1.setParams({ 
            "objectName" : "Campaign",
            "recordId": component.get("v.recordId")
        });
        
        var recordTypeName = component.get("v.recordTypeName");
        //alert(recordTypeName);
        var SysConName = '';
        if(recordTypeName == 'CouponDistribution'){
            SysConName = 'acctManual';  
        }
        else{
        //if(recordTypeName == 'PromotionCampaign'){
            SysConName = 'txnCreate';  
        }
       //alert(SysConName);
        var exeAction2 = component.get("c.apex_getBasicInfotxnCreate");    
        exeAction2.setParams({ 
            SysConName: SysConName
        });
        
        var exeAction3 = component.get("c.apex_getCampaignMemberList");
        exeAction3.setParams({ 
            "recordId": component.get("v.recordId")
        });
        
        Promise.all([
            helper.serverSideCall(component, exeAction1),
            helper.serverSideCall(component, exeAction2),
            helper.serverSideCall(component, exeAction3) 
        ]).then(
            function(response) {
                var resultString = response[0];
                var recordData = JSON.parse(resultString);
                
                var result1String = response[1];
                var result1Dict = JSON.parse(result1String);
                var JSON__c = result1Dict.JSON__c;
                var basicInfoTriggerSysCon = JSON.parse(JSON__c);
                
                var result2String = response[2];
                var result2Arr = JSON.parse(result2String);
                
                ////////////////////////  basicInfo  ////////////////////////
                var basicInfo = {}
                basicInfo["RuleFromObj"] = "Campaign";
                basicInfo["IncentiveRuleNo"] = null;
                basicInfo["RuleName"] = recordData.Name ? recordData.Name : null;
                basicInfo["RuleFromRecdID"] = recordData.Id ? recordData.Id : null;
                basicInfo["Status"] = recordData.Status ? recordData.Status: null;
                basicInfo["StartDate"] = recordData.StartDate ? recordData.StartDate : null;
                basicInfo["EndDate"] = recordData.EndDate ? recordData.EndDate : null;
                
                var triggerPoint = {}
                for(var key in basicInfoTriggerSysCon){
                    triggerPoint[key] = basicInfoTriggerSysCon[key] ? basicInfoTriggerSysCon[key] : null;
                }
                basicInfo["TriggerPoint"] = triggerPoint;
                incentiveRule['BasicInfo'] = basicInfo;
                ////////////////////////  basicInfo  ////////////////////////
                
                
                ////////////////////////  targetings  ////////////////////////
                var targetings = {}
                targetings["SpecificMemberOnly"] = recordData.SpecificMember__c ? recordData.SpecificMember__c : false;
                /*
                var SpecificMemberList = [];
                for(var i=0; i<result2Arr.length; i++){
                    var specificMember = result2Arr[i];
                	SpecificMemberList.push(specificMember["Contact.AccountId"]);
                }
                */
                //var SpecificMember = recordData.SpecificMember__c ? recordData.SpecificMember__c : false;
                //targetings["SpecificMemberList"] = SpecificMember ? result2Arr : [];
                targetings["SpecificMemberList"] = null;
                targetings["Detail"] = [];
                targetings["Logic"] = null;
                incentiveRule['Targeting'] = targetings;
                ////////////////////////  targetings  ////////////////////////
                
                
                ////////////////////////  conditions  ////////////////////////
                var conditions = {}
            	
                if(recordTypeName == 'CouponDistribution'){
                    conditions = null;
                }
                else{
                    conditions["ConditionType"] = null; 
                    conditions["SysCon"] = null; 
                    conditions["RelatedTo"] = null; 
                    conditions["Detail"] = [];
                    conditions["Logic"] = null;
                }
                
                
                
                
                incentiveRule['Condition'] = conditions;
                ////////////////////////  conditions  ////////////////////////
                
                
                ////////////////////////  actions  ////////////////////////
                var action = {};        
                action["actionType"] = null;
                action["RefObj"] = null;
                action["RefObjAccField"] = null;
                action["ActionTo"] = null;
                action["RefObj"] = null;
                action["RefObjAccField"] = null;
                action["PointBase"] = "Net Spending";
                action["MaxTimes"] = null;
                action["PointAmount"] = null;
                action["Reward"] = null;
                action["PointMultiplier"] = null;
                action["PointGroup"] = null;
                action["MaxPoint"] = null;
                action["PointAmount"] = null;
                incentiveRule['Action'] = action;
                ////////////////////////  actions  ////////////////////////
                
                console.log(JSON.stringify(incentiveRule));
           
                var action = component.get("c.apex_createIncentiveRule");
                action.setParams({ 
                    jsonString: JSON.stringify(incentiveRule),
                    compaing_id: component.get("v.recordId"),
                    objectName: "Campaign",
                    ruleName: recordData.Name,
                    status: recordData["Status"]
                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        
                        var self = this;
                        helper.loadIncentiveRule(component, event);
                        helper.showExampleModal(component, event);
                    }
        
                });
                
                $A.enqueueAction(action); 
            
                
            }
        ).catch(
            function(error) {
                
            }
        );
        

    },
    inputTargetLogicChange : function(component, event, helper){
        var inputTargetLogic = component.find("inputTargetLogic");
        var inputTargetLogicValue = inputTargetLogic.get("v.value");
        var targetings = component.get("v.targetings");
        targetings["Logic"] = inputTargetLogicValue;
		component.set("v.targetings", targetings);
    },
    inputConditionLogicChange : function(component, event, helper){
        var inputConditionLogic = component.find("inputConditionLogic");
        var inputConditionLogicValue = inputConditionLogic.get("v.value");
        var conditions = component.get("v.conditions");
        conditions["Logic"] = inputConditionLogicValue;
		component.set("v.conditions", conditions);
    }
})