({
    loadIncentiveRule : function(cmp, event) {
        cmp.set("v.Spinner", true);
        
        var exeAction1 = cmp.get("c.apex_getIncentiveRule");
        exeAction1.setParams({ 
            "recordId": cmp.get("v.recordId")
        });
		
        var exeAction2 = cmp.get("c.apex_getObjectField");
        exeAction2.setParams({ 
            "appliedObject": "Campaign"
        });
        
        var exeAction3 = cmp.get("c.apex_getConditionType");
        
        var exeAction4 = cmp.get("c.apex_getRecordById");
        exeAction4.setParams({ 
            "objectName": "Campaign",
            "recordId": cmp.get("v.recordId")
        });
        
        var self = this;
        
        Promise.all([
            self.serverSideCall(cmp, exeAction1),
            self.serverSideCall(cmp, exeAction2),
            self.serverSideCall(cmp, exeAction3),
            self.serverSideCall(cmp, exeAction4)
        ]).then(
            function(response) {
                cmp.set("v.Spinner", false);
                var returnValue = response[0];
               	var returnValueDict = JSON.parse(returnValue);
                console.log(JSON.stringify(returnValueDict));
                var returnCode = returnValueDict.returnCode;
                var hasRule = false;
                if(returnCode == '1'){
                    hasRule = true;
                    var resultDict = returnValueDict.result;
                    var basicInfo = resultDict.infoJson;
                    var targeting = resultDict.targetingJson;
                    var condition = resultDict.conditionJson;
                    var action = resultDict.actionJson;
                   
                    var targetingDetail = targeting["Detail"];
                    if(targetingDetail.length > 0){
                        targetingDetail = targetingDetail.sort(function(a, b) {
                            return a.No - b.No;
                        });
                        targeting["Detail"] = targetingDetail;
                    }
                    var return1Value = response[1];
                    //console.log("@@@return1Value: "+return1Value);
                    var result1Dict = JSON.parse(return1Value);
                   	
                    var defaultTargeting = JSON.parse(JSON.stringify(targeting));
                    var cloneDefaultTargeting = JSON.parse(JSON.stringify(targeting));
                    //console.log("@@@targetingtargeting: "+JSON.stringify(targeting));
                    var defaultCondition = condition ? JSON.parse(JSON.stringify(condition)) : {};
                    var cloneDefaultCondition = JSON.parse(JSON.stringify(condition));
                    //var defaultCondition = condition ? JSON.parse(JSON.stringify(condition)) : null;
                    
                    var return2Value = response[2];
                    var conditionTypeArr = JSON.parse(return2Value);
                    console.log("conditionTypeArr: "+JSON.stringify(conditionTypeArr));
                    
                    var ConditionType = '';
                    for(var i=0; i<conditionTypeArr.length; i++){
                        var conditionTypeMap = conditionTypeArr[i];
                        var SysConName = conditionTypeMap['SysConName__c'];
                        var defaultConditionConditionType = defaultCondition['ConditionType'];
                        if(SysConName == defaultConditionConditionType){
                            ConditionType = conditionTypeMap['SysConAttributeLabel__c'];
                            defaultCondition['ConditionType'] = ConditionType;
                            break;
                        }
                    }
                    
                  	
                    cmp.set("v.defaultRulesSetting", result1Dict);
                    cmp.set("v.defaultBasicInfo", basicInfo);
                    cmp.set("v.defaultTargetings", defaultTargeting);
                    cmp.set("v.defaultConditions", defaultCondition);
                    cmp.set("v.defaultActions", action); 
                    cmp.set("v.cloneDefaultTargetings", cloneDefaultTargeting);
                    cmp.set("v.cloneDefaultConditions", cloneDefaultCondition);
                    
                }
                else{
                    hasRule = false;
                }
                //alert(hasRule);
                var return3Value = response[3];
                //console.log(return3Value);
                var recordDict = JSON.parse(return3Value);
                if(recordDict["RecordType"]){
                    //alert(recordDict["RecordType"]["DeveloperName"]); 
                    cmp.set("v.recordTypeName", recordDict["RecordType"]["DeveloperName"]);   
                }
                else{
                    //alert('No RecordType');
                    cmp.set("v.recordTypeName", "PromotionCampaign");
                }
                cmp.set("v.hasRule", hasRule);
                
                /*
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": " Your record have been loaded successfully."
                });
                toastEvent.fire();
                */
            }
        ).catch(
            function(error) {
                cmp.set("v.Spinner", false);
            }
        );
    },
    serverSideCall : function(component,action) {
        return new Promise(function(resolve, reject) { 
            action.setCallback(this, 
                function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                       	resolve(response.getReturnValue());
                    } else {
                       reject(new Error(response.getError()));
                    }
                }); 
            $A.enqueueAction(action);
        });
    },
    showExampleModal : function(component, event) {
        
        component.set("v.modalSpinner", true);
        var modal = component.find("campaignModal");
        var modalBackdrop = component.find("myModal-Back");
        $A.util.addClass(modal, 'slds-fade-in-open');
        $A.util.addClass(modalBackdrop, 'slds-backdrop_open');
        
        var exeAction1 = component.get("c.apex_getIncentiveRule");
        exeAction1.setParams({ 
            "recordId": component.get("v.recordId")
        });
		
        var exeAction2 = component.get("c.apex_getObjectField2");
        exeAction2.setParams({ 
            appliedObject: "Campaign",
            module: "Targeting"
        });
        
        var exeAction3 = component.get("c.apex_getRecordById");
        exeAction3.setParams({ 
            "objectName" : "Campaign",
            "recordId": component.get("v.recordId")
        });
        
        var recordTypeName = component.get("v.recordTypeName");
        var SysConName = '';
        if(recordTypeName == 'CouponDistribution'){
            SysConName = 'acctManual';  
        }
        //else{
        if(recordTypeName == 'PromotionCampaign'){
            SysConName = 'txnCreate';  
        }
        var exeAction4 = component.get("c.apex_getBasicInfotxnCreate");
        exeAction4.setParams({ 
            SysConName: SysConName
        });
      	
        var exeAction5 = component.get("c.apex_getAllReward");
        
        var exeAction6 = component.get("c.apex_getConditionObjectField");
        
        var exeAction7 = component.get("c.apex_getConditionType");
        
        var exeAction8 = component.get("c.apex_getFieldTypeList");
       
        var exeAction9 = component.get("c.apex_getCampaignMemberList");
        exeAction9.setParams({ 
            "recordId": component.get("v.recordId")
        });
       	
        var exeAction10 = component.get("c.apex_getSpecificDateSetting");
        
       	var self = this;
        	
        Promise.all([
            self.serverSideCall(component, exeAction1),
            self.serverSideCall(component, exeAction2),
            self.serverSideCall(component, exeAction3),
            self.serverSideCall(component, exeAction4),
            self.serverSideCall(component, exeAction5),
            self.serverSideCall(component, exeAction6),
			self.serverSideCall(component, exeAction7),
            self.serverSideCall(component, exeAction8),
            self.serverSideCall(component, exeAction9),
            self.serverSideCall(component, exeAction10)
        ]).then(
            function(response) {
                
                
                ///////////////////////
                var result7String = response[7];
                var fieldTypeDict = JSON.parse(result7String);
                component.set("v.fieldTypeDict", fieldTypeDict);
                ///////////////////////
                
                
                ///////////////////////
                var result9String = response[9];
                var specificDateSettingArr = JSON.parse(result9String);
                component.set("v.specificDateSettingArr", specificDateSettingArr);
                ///////////////////////
                
                
                var operatorMap = {}
                for (var key in fieldTypeDict) {
                    var fieldType = fieldTypeDict[key];
                    var Name = fieldType['Name'];
                    var operatorMappingString = fieldType['OperatorMapping__c'];
                    var operatorMapping = JSON.parse(operatorMappingString);
                    var operatorArr = [];
                    for(var label in operatorMapping){
                        var operator = operatorMapping[label];
                        operatorArr.push({'text' : label, 'operator': operator});
                    }
                    Name = Name.toUpperCase();
                    operatorMap[Name] = operatorArr;
                }
                
                var returnValue = response[0];
                //console.log("@@@returnValue: "+returnValue);
               	var returnValueDict = JSON.parse(returnValue);
                var returnCode = returnValueDict.returnCode;
                var resultDict = returnValueDict.result;
                
                var basicInfo = {}
                var targetings = {}
                var condition = {}
                var action = {}
                if(returnCode == '1'){
                    basicInfo = resultDict.infoJson;
                    targetings = resultDict.targetingJson;
                    condition = resultDict.conditionJson;
                    action = resultDict.actionJson;
                    
                    
                    
                }
                else{
                    
                }
                
                if(targetings){
                    var targetingDetail = targetings["Detail"];
                    targetingDetail = targetingDetail.sort(function(a, b) {
                        return a.No - b.No;
                    });
                    for(var i=0; i<targetingDetail.length; i++){
                        var Detail = targetingDetail[i];
                        Detail["isValidValue"] = true;
                        targetingDetail[i] = Detail;
                    }
                    targetings["Detail"] = targetingDetail;
                }
                console.log("@@@targetings: "+JSON.stringify(targetings));
                
                
                if(condition){
                    var conditionDetail = condition["Detail"];
                    conditionDetail = conditionDetail.sort(function(a, b) {
                        return a.No - b.No;
                    });
                    for(var i=0; i<conditionDetail.length; i++){
                        var Detail = conditionDetail[i];
                        Detail["isValidValue"] = true;
                        conditionDetail[i] = Detail;
                    }
                    condition["Detail"] = conditionDetail;
                }
        		
        		console.log("@@@condition: "+JSON.stringify(condition));
                
                
                ///////////////////////    target rulesSetting    ///////////////////////
                var return1Value = response[1];
                var targetRowDataMap = JSON.parse(return1Value);
                ///////////////////////    target rulesSetting    ///////////////////////
                
                
                ///////////////////////
                var return2Value = response[2];
                console.log("@@@return2Value: "+return2Value);
                var result2Dict = JSON.parse(return2Value);
                var compaignStatus = result2Dict.Status;
                component.set("v.SpecificMember__c", result2Dict.SpecificMember__c);
                if(result2Dict.SpecificMember__c){
                    if(recordTypeName == 'CouponDistribution'){
                        component.set("v.tabId", 'three');
                    }
                    //else{
                    if(recordTypeName == 'PromotionCampaign'){
                        component.set("v.tabId", 'two');
                    }
                }
                else{
                    component.set("v.tabId", 'one');
                }
                
                component.set("v.recordData", result2Dict);
                ///////////////////////
                
                
                ///////////////////////
                var return3Value = response[3];
                console.log("@@@return3Value: "+return3Value);
                var result3Dict = JSON.parse(return3Value);
                var JSON__c = result3Dict.JSON__c;
                var JSONDict = JSON.parse(JSON__c);
                component.set("v.basicInfoTriggerSysCon", JSONDict);
                ///////////////////////
                
                
                ///////////////////////
                var return4Value = response[4];
                console.log("@@@return4Value: "+return4Value);
                var result4Dict = JSON.parse(return4Value);
                component.set("v.actionRewardOption", result4Dict);
                ///////////////////////
                
                
                ///////////////////////    condition rulesSetting    ///////////////////////
                if(condition){
                    var conditionObjectArr = [];
                    var conditionDataMap = {}
                    var result5String = response[5];
                    var conditionDataMap = JSON.parse(result5String);
                    var RelatedTo = condition.RelatedTo;
                    var RelatedToDict = conditionDataMap[RelatedTo];
             		
                    for(var key in conditionDataMap){
                        var objectDict = conditionDataMap[key];
                        var RelatedObjectLabel__c = '';
                        for(var objectName in objectDict){
                            var rulesSetting = objectDict[objectName].rulesSetting;
                            RelatedObjectLabel__c =  rulesSetting.RelatedObjectLabel__c;
                            break;
                        }
                        conditionObjectArr.push({RelatedObjectLabel__c: RelatedObjectLabel__c, RelatedObject__c: key});
                    }
                    component.set("v.conditionObjectArr", conditionObjectArr);
                    component.set("v.conditionDataMap", conditionDataMap);
                }
                ///////////////////////    condition rulesSetting    ///////////////////////
                
                
                ///////////////////////
                var result6String = response[6];
                console.log("@@@result6String: "+result6String);
                var conditionTypeArr = JSON.parse(result6String);
                component.set("v.conditionTypeArr", conditionTypeArr);
                ///////////////////////
                
              	                
                ///////////////////////
                var result8String = response[8];
                console.log("SpecificMemberList: "+result8String);
                var SpecificMemberList = JSON.parse(result8String);
                component.set("v.SpecificMemberList", SpecificMemberList);
                ///////////////////////
                
                
                ///////////////////////
                console.log("@@@targetRowDataMap: "+JSON.stringify(targetRowDataMap));
                component.set("v.targetRowDataMap", targetRowDataMap);
                console.log("@@@operatorMap: "+JSON.stringify(operatorMap));
                component.set("v.operatorMap", operatorMap);
                console.log("@@@targetings: "+JSON.stringify(targetings));
                component.set("v.targetings", targetings);
                console.log("@@@condition: "+JSON.stringify(condition));
                component.set("v.conditions", condition);
                console.log("@@@action: "+JSON.stringify(action));
                component.set("v.actions", action);
                ///////////////////////
                
                component.set("v.modalSpinner", false);
                
                /*
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": " Your record have been loaded successfully."
                });
                toastEvent.fire();
                */
            }
        ).catch(
            function(error) {
                
            }
        );
        
    },
    isValidDate: function(DateEntered) {
        if(DateEntered.match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/)){
        	return true;
        }
        else{
            return false;
        }
    },
    isBalanced: function(inputStr){
        var self = this;
        /*
        var parensStr = document.getElementById('input-one');
        var inputStr = parensStr.value
        if (inputStr === null) {
            printToScreen(true); 
        }
        */
        if (!inputStr) {
            return false;
        }
        
        var expression = inputStr.split('');
        var stack = [];
        
        for (var i = 0; i < expression.length; i++) {
            if (self.isParanthesis(expression[i])) {
            	if (self.isOpenParenthesis(expression[i])) {
        			stack.push(expression[i]);
        		} 
                else {
                    if (stack.length === 0) {
                        //return printToScreen(false);
                        return false;
                    }
                    var top = stack.pop(); // pop off the top element from stack
                    if (!self.matches(top, expression[i])) {
                        //return printToScreen(false);
                        return false;
                    }
        		}
        	}
    	}
        var returnValue = stack.length === 0 ? true : false;
        //printToScreen(returnValue)
        return returnValue;
    },
    isParanthesis: function(char){
        var str = '{}[]()';
        if (str.indexOf(char) > -1) {
        	return true;
        } 
        else {
        	return false;
        }
    },
    isOpenParenthesis: function(parenthesisChar){
        var tokens = [ ['{','}'] , ['[',']'] , ['(',')'] ];
        for (var j = 0; j < tokens.length; j++) {
            if (tokens[j][0] === parenthesisChar) {
                return true;
            }
        }
        return false;
    },
    matches: function(topOfStack, closedParenthesis){
        var tokens = [ ['{','}'] , ['[',']'] , ['(',')'] ];
        for (var k = 0; k < tokens.length; k++) {
            if (tokens[k][0] === topOfStack && tokens[k][1] === closedParenthesis) {
            	return true;
            }	
        }
        return false;
    } 
})