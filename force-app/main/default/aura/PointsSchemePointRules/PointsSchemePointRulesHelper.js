({
	init : function(cmp) {
        
        var exeAction1 = cmp.get("c.apex_getIncentiveRule");
        exeAction1.setParams({ 
            "recordId": cmp.get("v.recordId")
        });
		
        var exeAction2 = cmp.get("c.apex_getRecordById");
        exeAction2.setParams({ 
            "recordId": cmp.get("v.recordId")
        });
        
        var exeAction3 = cmp.get("c.apex_getIncentiveRuleData");
        exeAction3.setParams({ 
            "recordId": cmp.get("v.recordId")
        });
        
        var exeAction4 = cmp.get("c.apex_getPicklistFieldValues");
        exeAction4.setParams({ 
            "objectName": "IncentiveRule__c",
            "pickListFieldName" : "Status__c"
        });
        
        var exeAction5 = cmp.get("c.apex_getPicklistFieldValues");
        exeAction5.setParams({ 
            "objectName": "PointsScheme__c",
            "pickListFieldName" : "PointGroup__c"
        });
        
        
        var self = this;
        Promise.all([
            self.serverSideCall(cmp, exeAction1),
            self.serverSideCall(cmp, exeAction2),
            self.serverSideCall(cmp, exeAction3),
            self.serverSideCall(cmp, exeAction4),
            self.serverSideCall(cmp, exeAction5)
        ]).then(
            function(response) {
                
                var returnValue = response[0];
               	var returnValueDict = JSON.parse(returnValue);
                //console.log(JSON.stringify(returnValueDict));
                var returnCode = returnValueDict.returnCode;
                
                if(returnCode == '1'){
                    var resultDict = returnValueDict.result;
                    var basicInfo = resultDict.infoJson;
                    var targeting = resultDict.targetingJson;
                    var condition = resultDict.conditionJson;
                    var action = resultDict.actionJson;
                    cmp.set("v.basicInfo", basicInfo);
                    cmp.set("v.targeting", targeting);
                    cmp.set("v.condition", condition);
                    cmp.set("v.action", action);
                    cmp.set("v.incentiveRule", returnValueDict.result);
                    
                    var dayOfWeekValue = null;
                    var defaultDayOfWeekValue = null;
                    var excludedDateFromValue = null;
                    var defaultExcludedDateFromValue = null;
                    var excludedDateToValue = null;
                    var defaultExcludedDateToValue = null;
                    
                    if(condition != null){
                        var details = condition["Detail"];
                        if(details.length == 1){
                            var transactionday = details[0];
                            var transactiondayFieldValue = transactionday.FieldValue;
                            for(var j=0; j<transactiondayFieldValue.length; j++){
                                dayOfWeekValue = transactiondayFieldValue[j];
                                defaultDayOfWeekValue = transactiondayFieldValue[j];
                                break;
                            }
                        }
                        else if(details.length == 2){
                            var excludedDateFrom = details[0];
                            var excludedDateFromFieldValue = excludedDateFrom.FieldValue;
                            for(var j=0; j<excludedDateFromFieldValue.length; j++){
                                excludedDateFromValue = excludedDateFromFieldValue[j];
                                defaultExcludedDateFromValue = excludedDateFromFieldValue[j];
                                break;
                            }
                            var excludedDateto = details[1];
                            var excludedDatetoFieldValue = excludedDateto.FieldValue;
                            for(var j=0; j<excludedDatetoFieldValue.length; j++){
                                excludedDateToValue = excludedDatetoFieldValue[j];
                                defaultExcludedDateToValue = excludedDatetoFieldValue[j];
                                break;
                            }
                        }
                        else if(details.length == 3){
                            var transactionday = details[0];
                            var excludedDateFrom = details[1];
                            var excludedDateto = details[2];
                            
                            var transactiondayFieldValue = transactionday.FieldValue;
                            for(var j=0; j<transactiondayFieldValue.length; j++){
                                dayOfWeekValue = transactiondayFieldValue[j];
                                defaultDayOfWeekValue = transactiondayFieldValue[j];
                                break;
                            }
                            
                            var excludedDateFromFieldValue = excludedDateFrom.FieldValue;
                            for(var j=0; j<excludedDateFromFieldValue.length; j++){
                                excludedDateFromValue = excludedDateFromFieldValue[j];
                                defaultExcludedDateFromValue = excludedDateFromFieldValue[j];
                                break;
                            }
                            
                            var excludedDatetoFieldValue = excludedDateto.FieldValue;
                            for(var j=0; j<excludedDatetoFieldValue.length; j++){
                                excludedDateToValue = excludedDatetoFieldValue[j];
                                defaultExcludedDateToValue = excludedDatetoFieldValue[j];
                                break;
                            }
                            
                        }
                    }
					
                    cmp.set("v.defaultDayOfWeekValue", defaultDayOfWeekValue);
                    cmp.set("v.dayOfWeekValue", dayOfWeekValue);
                    cmp.set("v.defaultExcludedDateFromValue", defaultExcludedDateFromValue);
                    cmp.set("v.excludedDateFromValue", excludedDateFromValue);
                    cmp.set("v.defaultExcludedDateToValue", defaultExcludedDateToValue);
                    cmp.set("v.excludedDateToValue", excludedDateToValue);
                    
					                    
                    var defaultBasicInfo = JSON.parse(JSON.stringify(basicInfo));
                    var defaultTargeting = JSON.parse(JSON.stringify(targeting));
                    var defaultCondition = JSON.parse(JSON.stringify(condition));
                    var defaultAction = JSON.parse(JSON.stringify(action));
                    cmp.set("v.defaultBasicInfo", defaultBasicInfo);
                    cmp.set("v.defaultTargeting", defaultTargeting);
                    cmp.set("v.defaultCondition", defaultCondition);
                    cmp.set("v.defaultAction", defaultAction);
                    
                    var return1Value = response[1];
                    var return1ValueDict = JSON.parse(return1Value);
                    var ruleCode = return1ValueDict.RuleCode__c;
                    cmp.set("v.ruleCode", ruleCode);
                    
                    var return2Value = response[2];
                    var return2ValueDict = JSON.parse(return2Value);
                    var status = return2ValueDict.Status__c;
                    var defaultStatus = return2ValueDict.Status__c;
                    cmp.set("v.status", status);
                    cmp.set("v.defaultStatus", defaultStatus);
                    cmp.set("v.isEdit", false);
                    
                    var return3Value = response[3];
                    var return3ValueArr = JSON.parse(return3Value);
                    cmp.set("v.picklistFieldValues", return3ValueArr);
                    
                    
                    var return4Value = response[4];
                    //alert(return4Value);
                    var return4ValueArr = JSON.parse(return4Value);
                    cmp.set("v.pointGroupPLValues", return4ValueArr);
                    
                    if(defaultAction["Reward"]){
                        var action = cmp.get("c.apex_getRewardById");
                        //Set the Object parameters and Field Set name
                        action.setParams({
                            objectName: "Reward__c", 
                            recordId: defaultAction["Reward"]
                        });
                        action.setCallback(this, function(response){
                            var state = response.getState();
                            if(state === 'SUCCESS'){
                                cmp.set("v.defaultselectedLookUpRecord", response.getReturnValue());
                                cmp.set("v.selectedLookUpRecord", response.getReturnValue());
                            }else if (state === 'ERROR'){
                                var errors = response.getError();
                                if (errors) {
                                    if (errors[0] && errors[0].message) {
                                        console.log("Error message: " +
                                                    errors[0].message);
                                    }
                                } else {
                                    console.log("Unknown error");
                                }
                            }else{
                                console.log('Something went wrong, Please check with your admin');
                            }
                        });
                        $A.enqueueAction(action);
                    }
                    
                    
                }
                
                /*
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": " Your record have been loaded successfully."
                });
                toastEvent.fire();
                */
            }
        ).catch(
            function(error) {
                
            }
        );
	},
    serverSideCall : function(component,action) {
        return new Promise(function(resolve, reject) { 
            action.setCallback(this, 
                function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                       	resolve(response.getReturnValue());
                    } else {
                       reject(new Error(response.getError()));
                    }
                }); 
            $A.enqueueAction(action);
        });
    }
})