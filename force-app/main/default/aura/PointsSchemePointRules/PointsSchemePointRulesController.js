({
	init : function(cmp, event, helper) {
        helper.init(cmp);
    },
    edit : function(cmp, event, helper) {
        cmp.set("v.isEdit", true);
    },
    save : function(cmp, event, helper) {
        var basicInfo = cmp.get("v.basicInfo");
        var targeting = cmp.get("v.targeting");
        var condition = cmp.get("v.condition");
        var action = cmp.get("v.action");
		var status = cmp.get("v.status");
        var ruleCode = cmp.get("v.ruleCode");
		        
        var incentiveRule = {}
        incentiveRule["BasicInfo"] = basicInfo;
        incentiveRule["Targeting"] = targeting; 
        incentiveRule["Condition"] = null;
        
        var inputStartDate = cmp.find("inputStartDate");
        var inputStartDateValue = inputStartDate.get("v.value") ? inputStartDate.get("v.value") : null;
        var inputEndDate = cmp.find("inputEndDate");
        var inputEndDateValue = inputEndDate.get("v.value") ? inputEndDate.get("v.value") : null; 
        var excludedDateFrom = cmp.find("excludedDateFrom");
        var excludedDateFromValue = excludedDateFrom.get("v.value") ? excludedDateFrom.get("v.value") : null;
        var excludedDateTo = cmp.find("excludedDateTo");
        var excludedDateToValue = excludedDateTo.get("v.value") ? excludedDateTo.get("v.value") : null; 
        //console.log("excludedDateFromValue: "+excludedDateFromValue);
        //console.log("excludedDateToValue: "+excludedDateToValue);
        
        var ActionType = action["ActionType"];
        var ActionCon = action["ActionCon"];
       	/*
        
        */
        if(ActionType == 'PointMultiplier'){
            
            var inputSelectActionType = cmp.find("inputSelectActionType");
            var inputSelectActionTypeValue = inputSelectActionType.get("v.value") ? inputSelectActionType.get("v.value") : null;
            var inputActive = cmp.find("inputActive");
            var inputActiveValue = inputActive.get("v.value") ? inputActive.get("v.value") : null;
            var inputPointMultiplier = cmp.find("inputPointMultiplier");
            var inputPointMultiplierValue = inputPointMultiplier.get("v.value") ? Number(inputPointMultiplier.get("v.value")) : null;
            var inputMaxPointEarned = cmp.find("inputMaxPointEarned");
            var inputMaxPointEarnedValue = inputMaxPointEarned.get("v.value") ? Number(inputMaxPointEarned.get("v.value")) : null;
            var inputSelectPointGroup = cmp.find("inputSelectPointGroup");
            var inputSelectPointGroupValue = inputSelectPointGroup.get("v.value") ? inputSelectPointGroup.get("v.value") : null;
            //var inputMaxTimes = cmp.find("inputMaxTimes");
            //var inputMaxTimesValue = inputMaxTimes.get("v.value") ? inputMaxTimes.get("v.value") : null;
            //var inputDayOfWeek = cmp.find("inputDayOfWeek");
            //var inputDayOfWeekValue = inputDayOfWeek.get("v.value") ? inputDayOfWeek.get("v.value") : null; 
            //var rewardId = cmp.get("v.selectedLookUpRecord") ? cmp.get("v.selectedLookUpRecord").id : null;
            
            for(var key in action){
                action[key] = null;
            }
            
            action["PointTo"] = "Bonus";
            action["ActionType"] = ActionType;
            action["ActionTo"] = "Self";
            action["ActionLevel"] = "Txn";
            action["ActionCon"] = ActionCon;
            
            action["PointBase"] = "Net Spending";
            action["PointGroup"] = inputSelectPointGroupValue;
            action["ActionType"] = inputSelectActionTypeValue;
            action["PointMultiplier"] = inputPointMultiplierValue;
            action["MaxPoint"] = inputMaxPointEarnedValue;
            basicInfo["Status"] = inputActiveValue;
        }
        else if(ActionType == 'FixPoint'){
			var inputSelectActionType = cmp.find("inputSelectActionType");
            var inputSelectActionTypeValue = inputSelectActionType.get("v.value") ? inputSelectActionType.get("v.value") : null;
            var inputActive = cmp.find("inputActive");
            var inputActiveValue = inputActive.get("v.value") ? inputActive.get("v.value") : null;
            var inputPointAmount = cmp.find("inputPointAmount");
            var inputPointAmountValue = inputPointAmount.get("v.value") ? Number(inputPointAmount.get("v.value")) : null;
            var inputSelectPointGroup = cmp.find("inputSelectPointGroup");
            var inputSelectPointGroupValue = inputSelectPointGroup.get("v.value") ? inputSelectPointGroup.get("v.value") : null;
            var inputMaxPointEarned = cmp.find("inputMaxPointEarned");
            var inputMaxPointEarnedValue = inputMaxPointEarned.get("v.value") ? Number(inputMaxPointEarned.get("v.value")) : null;
            //alert(inputMaxPointEarnedValue);
            //var inputMaxTimes = cmp.find("inputMaxTimes");
            //var inputMaxTimesValue = inputMaxTimes.get("v.value") ? inputMaxTimes.get("v.value") : null;
            //var inputDayOfWeek = cmp.find("inputDayOfWeek");
            //var inputDayOfWeekValue = inputDayOfWeek.get("v.value") ? inputDayOfWeek.get("v.value") : null; 
            //var rewardId = cmp.get("v.selectedLookUpRecord") ? cmp.get("v.selectedLookUpRecord").id : null;
            
            for(var key in action){
                action[key] = null;
            }
            
            action["PointTo"] = "Bonus";
            action["ActionType"] = ActionType;
            action["ActionTo"] = "Self";
            action["ActionLevel"] = "Txn";
            action["ActionCon"] = ActionCon;
            
            action["PointBase"] = "Net Spending";
            action["PointGroup"] = inputSelectPointGroupValue;
            action["ActionType"] = inputSelectActionTypeValue;
            action["PointAmount"] = inputPointAmountValue;
            action["MaxPoint"] = inputMaxPointEarnedValue;
            basicInfo["Status"] = inputActiveValue;        
        }
        else if(ActionType == 'Coupon'){
         	var inputSelectActionType = cmp.find("inputSelectActionType");
            var inputSelectActionTypeValue = inputSelectActionType.get("v.value") ? inputSelectActionType.get("v.value") : null;
            var inputActive = cmp.find("inputActive");
            var inputActiveValue = inputActive.get("v.value") ? inputActive.get("v.value") : null;
            var inputSelectPointGroup = cmp.find("inputSelectPointGroup");
            var inputSelectPointGroupValue = inputSelectPointGroup.get("v.value") ? inputSelectPointGroup.get("v.value") : null;
            //var inputPointMultiplier = cmp.find("inputPointMultiplier");
            //var inputPointMultiplierValue = inputPointMultiplier.get("v.value") ? Number(inputPointMultiplier.get("v.value")) : null;
            //var inputMaxPointEarned = cmp.find("inputMaxPointEarned");
            //var inputMaxPointEarnedValue = inputMaxPointEarned.get("v.value") ? Number(inputMaxPointEarned.get("v.value")) : null;
            var inputMaxTimes = cmp.find("inputMaxTimes");
            var inputMaxTimesValue = inputMaxTimes.get("v.value") ? Number(inputMaxTimes.get("v.value")) : null;
            //var inputDayOfWeek = cmp.find("inputDayOfWeek");
            //var inputDayOfWeekValue = inputDayOfWeek.get("v.value") ? inputDayOfWeek.get("v.value") : null; 
            var rewardId = cmp.get("v.selectedLookUpRecord") ? cmp.get("v.selectedLookUpRecord").Id : null;
            //alert(JSON.stringify(cmp.get("v.selectedLookUpRecord")));
            
            
            for(var key in action){
                action[key] = null;
            }
            
            action["PointTo"] = "Bonus";
            action["ActionType"] = ActionType;
            action["ActionTo"] = "Self";
            action["ActionLevel"] = "Txn";
            action["ActionCon"] = ActionCon;
            
            action["PointGroup"] = inputSelectPointGroupValue;
            action["ActionType"] = inputSelectActionTypeValue;
            action["MaxTimes"] = inputMaxTimesValue;
            action["Reward"] = rewardId;
            basicInfo["Status"] = inputActiveValue;
        }
        basicInfo["StartDate"] = inputStartDateValue;
        basicInfo["EndDate"] = inputEndDateValue;
        
        
        if(ruleCode == 'DayofWeekBonus'){
            
            var theCondition = {}  
            var conditionDetails = [];
            theCondition["SysCon"] = "Consecutive";
            theCondition["RelatedTo"] = "TxnLine";
            theCondition["ConditionType"] = "Consecutive";
            theCondition["Amount"] = null;
            
            var inputDayOfWeek = cmp.find("inputDayOfWeek");
            var inputDayOfWeekValue = inputDayOfWeek.get("v.value") ? inputDayOfWeek.get("v.value") : null; 
            
            var detail = {
                rulesSettingName : null,
                Operator : "=",
                ObjectRelation : "Txn",
                ObjectName: "Transaction__c",
                ObjectLevel: "Header",
                No: 1,
                FieldValueString: inputDayOfWeekValue,
                FieldValue: [
                  inputDayOfWeekValue
                ],
                FieldType: "Date",
                FieldName: "transactionday__c"
            }
            
            conditionDetails.push(detail);
            theCondition["Logic"] = "1";
            
            if(excludedDateFromValue && excludedDateToValue){
                
                detail = {
                    rulesSettingName : null,
                    Operator : "<",
                    ObjectRelation : "Txn",
                    ObjectName: "Transaction__c",
                    ObjectLevel: "Header",
                    No: 2,
                    FieldValueString: excludedDateFromValue,
                    FieldValue: [
                      excludedDateFromValue
                    ],
                    FieldType: "Date",
                    FieldName: "transactionday__c"
                }
                
                conditionDetails.push(detail);
                
                detail = {
                    rulesSettingName : null,
                    Operator : ">",
                    ObjectRelation : "Txn",
                    ObjectName: "Transaction__c",
                    ObjectLevel: "Header",
                    No: 3,
                    FieldValueString: excludedDateToValue,
                    FieldValue: [
                      excludedDateToValue
                    ],
                    FieldType: "Date",
                    FieldName: "transactionday__c"
                }
                
                conditionDetails.push(detail);
                theCondition["Logic"] = "1 AND (2 OR 3)";
                
            }
            
            theCondition["Detail"] = conditionDetails;
            incentiveRule["Condition"] = theCondition;
        }
        else{
            if(excludedDateFromValue && excludedDateToValue){
                var theCondition = {}  
                var conditionDetails = [];
                theCondition["SysCon"] = "Consecutive";
                theCondition["RelatedTo"] = "TxnLine";
                theCondition["ConditionType"] = "Consecutive";
                theCondition["Amount"] = null;
                
                var detail = {
                    rulesSettingName : null,
                    Operator : "<",
                    ObjectRelation : "Txn",
                    ObjectName: "Transaction__c",
                    ObjectLevel: "Header",
                    No: 1,
                    FieldValueString: excludedDateFromValue,
                    FieldValue: [
                      excludedDateFromValue
                    ],
                    FieldType: "Date",
                    FieldName: "transactionday__c"
                }
                
                conditionDetails.push(detail);
                
                detail = {
                    rulesSettingName : null,
                    Operator : ">",
                    ObjectRelation : "Txn",
                    ObjectName: "Transaction__c",
                    ObjectLevel: "Header",
                    No: 2,
                    FieldValueString: excludedDateToValue,
                    FieldValue: [
                      excludedDateToValue
                    ],
                    FieldType: "Date",
                    FieldName: "transactionday__c"
                }
                
                conditionDetails.push(detail);
                
                theCondition["Detail"] = conditionDetails;
                theCondition["Logic"] = "1 OR 2";
                incentiveRule["Condition"] = theCondition;
            }
        }
       
        var isValid = true;
        var errorMessage = '';
        if(excludedDateFromValue || excludedDateToValue){
            if(excludedDateFromValue && excludedDateToValue){
                var excludedDateFromValueDate = new Date(excludedDateFromValue);
                var excludedDateToValueDate = new Date(excludedDateToValue);
                if(excludedDateFromValueDate > excludedDateToValueDate){
                    isValid = false;
                    errorMessage = 'Excluded Date To must equals to or larger than Excluded Date To';
                }
            }
            else{
                isValid = false;
                errorMessage = 'Please input excluded Date From and excluded Date To';
            }
        }
        
          
        if(inputStartDateValue && inputEndDateValue){
            var inputStartDateValueDate = new Date(inputStartDateValue);
            var inputEndDateValueDate = new Date(inputEndDateValue);
            if(inputStartDateValueDate > inputEndDateValueDate){
            	isValid = false;
                errorMessage = 'End Date must equals to or larger than Start Date';
            }
        }
        
        
        incentiveRule['Action'] = action;
        incentiveRule['BasicInfo'] = basicInfo;
        var jsonString = JSON.stringify(incentiveRule);
       
        if(isValid){
            var actionExecute = cmp.get("c.apex_saveJson");
            actionExecute.setParams({ 
                recordId: cmp.get("v.recordId"),
                jsonString: jsonString,
                status: status,
                startDate: inputStartDateValue,
                endDate: inputEndDateValue,
                ruleCode: ruleCode,
                PointGroup: action["PointGroup"]
            });
            actionExecute.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    
                    var ReturnValueJSON = response.getReturnValue();
                    var ReturnValue = JSON.parse(ReturnValueJSON);
                    var ReturnCode = ReturnValue.ReturnCode;
                    var ReturnMessage = ReturnValue.ReturnMessage;
                    if(ReturnCode == 1){
						helper.init(cmp);                        
                    }
                    else{
                        alert(ReturnMessage);
                    }
                }
            });
            
            $A.enqueueAction(actionExecute);
        }
        else{
            alert(errorMessage);
        }
         
        
    },
    cancel : function(cmp, event, helper) {
        var defaultAction = cmp.get("v.defaultAction");
        var defaultStatus = cmp.get("v.defaultStatus");
        var action = cmp.get("v.action");
        var status = cmp.get("v.status");
        action = defaultAction;
        status = defaultStatus;
        cmp.set("v.action", action);
        cmp.set("v.status", status);
        cmp.set("v.isEdit", false);
    },
    inputPointAmountChange : function(cmp, event, helper) {
        var inputPointAmount = cmp.find("inputPointAmount");
        var inputPointAmountValue = inputPointAmount.get("v.value");
        var action = cmp.get("v.action");
        action["PointAmount"] = inputPointAmountValue;
        cmp.set("v.action", action);
    },
    inputActiveChange : function(cmp, event, helper) {
        var inputActive = cmp.find("inputActive");
        var inputActiveValue = inputActive.get("v.value");
        //alert(inputActiveValue);
        var status = cmp.get("v.status");
        cmp.set("v.status", inputActiveValue);
    },
    inputPointMultiplierChange : function(cmp, event, helper) {
        var inputValue = event.getSource().get("v.value");
        inputValue = Math.round(inputValue * 100) / 100; 
        event.getSource().set("v.value", inputValue);
        /*
        var inputPointMultiplier = cmp.find("inputPointMultiplier");
        var inputPointMultiplierValue = inputPointMultiplier.get("v.value");
        var action = cmp.get("v.action");
        action["PointMultiplier"] = inputPointMultiplierValue;
        cmp.set("v.action", action);
        */
    },
    inputDayOfWeekChange : function(cmp, event, helper) {
        var inputDayOfWeek = cmp.find("inputDayOfWeek");
        var inputDayOfWeekValue = inputDayOfWeek.get("v.value");
        var condition = cmp.get("v.condition");
        var detail = condition["Detail"];
        if(detail && detail.length > 0){
            var FieldValue = [inputDayOfWeekValue];
            var theDetail = detail[0];
            theDetail["FieldValue"] = FieldValue;
        }
        cmp.set("v.dayOfWeekValue", inputDayOfWeekValue);
        condition["Detail"] = detail;
        cmp.set("v.condition", condition);
    },
    inputSelectActionTypeChange : function(cmp, event, helper){
        var inputSelectActionType = cmp.find("inputSelectActionType");
        var inputSelectActionTypeValue = inputSelectActionType.get("v.value");
		var action = cmp.get("v.action");
        action["ActionType"] = inputSelectActionTypeValue;
        cmp.set("v.action", action);
    },
    inputSelectPointGroupChange : function(cmp, event, helper){
        var inputSelectPointGroup = cmp.find("inputSelectPointGroup");
        var inputSelectPointGroupValue = inputSelectPointGroup.get("v.value");
		var action = cmp.get("v.action");
        action["PointGroup"] = inputSelectPointGroupValue;
        cmp.set("v.action", action);
    }
})