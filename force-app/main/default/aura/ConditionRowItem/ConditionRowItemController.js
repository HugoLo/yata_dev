({
	doInit : function(component, event, helper) {
		helper.helperMethod(component);
	},
    inputSelectObjectChange: function(component, event, helper) {
		var inputSelectObject = component.find("inputSelectObject");
        var Name = inputSelectObject.get("v.value");
        var relatedTo = component.get("v.relatedTo");
        var conditionDataMap = component.get("v.conditionDataMap");
        var conditionData = conditionDataMap[relatedTo];
        var condition = component.get("v.condition");
        var rowData = conditionData[Name];
        var rulesSetting = rowData["rulesSetting"];
        condition["ObjectName"] = rulesSetting["Object__c"];
        condition["ObjectLevel"] = rulesSetting["ObjectLevel__c"];
        condition["rulesSettingName"] = rulesSetting["Name"];
       	component.set("v.condition", condition);
        helper.helperMethod(component);
	},
    inputSelectFieldChange: function(cmp, event, helper){
        var inputSelectField = cmp.find("inputSelectField");
        var FieldName = inputSelectField.get("v.value");
        var condition = cmp.get("v.condition");
        condition.FieldName = FieldName;
        var FieldValueString = '';
        var FieldValue = [];
        condition.FieldValueString = FieldValueString;
        condition.FieldValue = FieldValue;
        cmp.set("v.selectedLookUpRecord", {});
        cmp.set("v.condition", condition);
        helper.helperMethod(cmp);
    },
    inputSelectOperatorChange: function(component, event, helper){
        var inputSelectOperator = component.find("inputSelectOperator");
        var Operator = inputSelectOperator.get("v.value");
        var condition = component.get("v.condition");
        var FieldValueString = '';
        var FieldValue = [];
        condition["Operator"] = Operator;
        condition.FieldValueString = FieldValueString;
        condition.FieldValue = FieldValue;
        component.set("v.selectedLookUpRecord", {});
        component.set("v.condition", condition);
        helper.helperMethod(component);
    },
    inputValuePickChange: function(cmp, event, helper){
        var inputValue = cmp.find("inputValuePick");
        var fieldValueString = inputValue.get("v.value");
        fieldValueString = fieldValueString.toString();
        var FieldValue = fieldValueString.split(";");
        var condition = cmp.get("v.condition");
        condition["FieldValueString"] = fieldValueString;
		cmp.set("v.condition", condition);
        //helper.helperMethod(cmp);
    },
    inputValueMultipickChange: function(cmp, event, helper){
        var inputValue = cmp.find("inputValueMultipick");
        var fieldValueString = inputValue.get("v.value");
        fieldValueString = fieldValueString.toString();
        var FieldValue = fieldValueString.split(";");
        var condition = cmp.get("v.condition");
        condition["FieldValueString"] = fieldValueString;
		cmp.set("v.condition", condition);
        var pickListArr = cmp.get("v.pickListArr");
        for(var i=0; i<pickListArr.length; i++){
            var pickList = pickListArr[i];
            var value = pickList["value"];
            var isSelect = FieldValue.includes(value) ? true : false;
            pickList["isSelect"] = isSelect;
        }
    },
    inputValueDateChange : function(cmp, event, helper){
     	var inputValue = cmp.find("inputValueDate");
        var fieldValueString = inputValue.get("v.value");
        fieldValueString = fieldValueString.toString();
        var condition = cmp.get("v.condition");
        condition["FieldValueString"] = fieldValueString;
		cmp.set("v.condition", condition);
    },
    deleteDetail : function(cmp, event, helper){
        var rowIndex = cmp.get("v.rowIndex");
        var detail = cmp.get("v.detail");
        detail.splice(rowIndex, 1);
        cmp.set("v.detail", detail);
    },
    inputValueNumberChange: function(cmp, event, helper){
        var inputNumberValue = cmp.find("inputNumberValue");
        var inputNumberValueString = inputNumberValue.get("v.value");
        var condition = cmp.get("v.condition");
        if(inputNumberValueString != null){
        	inputNumberValueString = inputNumberValueString.toString();
            condition["FieldValueString"] = inputNumberValueString;
            cmp.set("v.condition", condition);             
        }
        else{       
            condition["FieldValueString"] = '';
            cmp.set("v.condition", condition); 
        }
    },
    handleComponentEvent : function(cmp, event, helper) {
    // get the selected Account record from the COMPONETN event 	 
        var selectedAccountGetFromEvent = event.getParam("recordByEvent");
    	var fieldValueString = selectedAccountGetFromEvent["Id"];
        console.log(fieldValueString);
        fieldValueString = fieldValueString.toString();
        var FieldValue = [];
        FieldValue.push(fieldValueString);
        var condition = cmp.get("v.condition");
        condition["FieldValueString"] = fieldValueString;                      
		cmp.set("v.condition", condition);
    }
})