({
	helperMethod : function(component) {
        var condition = component.get("v.condition");
        var operatorMap = component.get("v.operatorMap");
        var relatedTo = component.get("v.relatedTo");
        var conditionDataMap = component.get("v.conditionDataMap");
        var fieldTypeDict = component.get("v.fieldTypeDict");
        
        var ObjectName = condition.ObjectName;
        var rulesSettingName = condition.rulesSettingName;
        var conditionData = conditionDataMap[relatedTo];
        
        var fieldSetArr = [];
        var operatorOption = operatorMap['STRING'];
        if(conditionData != null){
            
            var rowData = conditionData[rulesSettingName];
            console.log("@@@rowData: "+JSON.stringify(rowData));
            fieldSetArr = rowData ? rowData.fieldSet : [];
            if(rowData != null){
                
                
                var fieldSetMap = {}
                
                for(var i=0; i<fieldSetArr.length; i++){
                    var fieldSet = fieldSetArr[i];
                    fieldSetMap[fieldSet.fieldPath] = fieldSet;
                }
                
                var FieldName = condition.FieldName;
                var fieldSet = fieldSetMap[FieldName];
                var objectAPINameDict = rowData.objectAPIName;
                var objectAPIName = objectAPINameDict[FieldName];
                component.set("v.objectAPIName", objectAPIName);
                console.log("FieldName: "+FieldName);
                console.log("objectAPIName: "+objectAPIName);
                
                var operatorOption;
                if(fieldSet){
                    var typeApex = fieldSet.typeApex;
                    var type = fieldSet.type;
                    var jsonFieldTypeMap = fieldTypeDict[type];
                    if(jsonFieldTypeMap){
                        condition["FieldType"] = jsonFieldTypeMap["JSONFieldType__c"];
                    }
                    else{
                        condition["FieldType"] = "String";
                    }
                    if(ObjectName && type == 'picklist'){
                        var action = component.get("c.apex_getPicklistFieldValues");
                        action.setParams({ 
                            "objectName": ObjectName,
                            "pickListFieldName": fieldSet.fieldPath
                        });
                        action.setCallback(this, function(response){
                            var state = response.getState();
                            if(state === 'SUCCESS'){
                                var result = response.getReturnValue();
                                var resultArr = JSON.parse(result);
                                var FieldValueString = condition.FieldValueString;
                                var FieldValue = FieldValueString.split(";");
                            
                                for(var i=0; i<resultArr.length; i++){
                                    var pickList = resultArr[i];
                                    var value = pickList["value"];
                                    var isSelect = FieldValue.includes(value) ? true : false;
                                    pickList["isSelect"] = isSelect;
                                }
                                component.set("v.pickListArr", resultArr);
                                component.set("v.type", type);
                                
                            }else if (state === 'ERROR'){
                                component.set("v.type", type);
                                var errors = response.getError();
                                if (errors) {
                                    if (errors[0] && errors[0].message) {
                                        console.log("Error message: " +
                                                    errors[0].message);
                                    }
                                } else {
                                    console.log("Unknown error");
                                }
                            }else{
                                console.log('Something went wrong, Please check with your admin');
                            }
                        });
                        $A.enqueueAction(action);
                    }
                    else{
                        component.set("v.type", type);
                        console.log(type);
                    }
                    if(objectAPIName && (type == 'reference' || type == 'id') && condition.FieldValueString){
                        var action = component.get("c.apex_getRecordById");
                        //Set the Object parameters and Field Set name
                        console.log("objectName: "+objectAPIName);
                        console.log("recordId: "+condition.FieldValueString);
                        action.setParams({
                            objectName: objectAPIName, 
                            recordId: condition.FieldValueString
                        });
                        action.setCallback(this, function(response){
                            var state = response.getState();
                            
                            if(state === 'SUCCESS'){
                                console.log("selectedLookUpRecord: "+JSON.stringify(response.getReturnValue()));
                                component.set("v.selectedLookUpRecord", response.getReturnValue());
                                component.set("v.type", type);
                                console.log(type);
                            }else if (state === 'ERROR'){
                                component.set("v.type", type);
                                var errors = response.getError();
                                if (errors) {
                                    if (errors[0] && errors[0].message) {
                                        console.log("Error message: " +
                                                    errors[0].message);
                                    }
                                } else {
                                    console.log("Unknown error");
                                }
                            }else{
                                console.log('Something went wrong, Please check with your admin');
                            }
                        });
                        $A.enqueueAction(action);	
                    }
                    else{
                        component.set("v.type", type);
                        console.log(type);
                    }
                    
                    operatorOption = operatorMap[typeApex];
                    if(operatorOption){
                        
                    }
                    else{
                        operatorOption = operatorMap['STRING'];
                    }
                }
            }
        }
        
        var allObjectArr = [];
        for (var key in conditionData) {
            var rowData = conditionData[key];
            var rulesSetting = rowData.rulesSetting;
            var relatedObject = rulesSetting.RelatedObject__c;
            if(relatedObject == relatedTo){
                allObjectArr.push(rulesSetting);
            }
        }
        
        
        
        component.set("v.conditionMap", condition);
        component.set("v.fieldSetArr", fieldSetArr);
        component.set("v.operatorOption", operatorOption);
        component.set("v.allObjectArr", allObjectArr);
	}
})