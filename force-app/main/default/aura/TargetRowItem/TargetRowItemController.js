({
	doInit : function(component, event, helper) {
		helper.helperMethod(component);
	},
    myAction : function(component, event, helper) {
		
	},
    inputSelectObjectChange: function(cmp, event, helper) {
        var inputSelectObject = cmp.find("inputSelectObject");
        var Name = inputSelectObject.get("v.value");
        var targeting = cmp.get("v.targeting");
        var rowDataMap = cmp.get("v.rowDataMap");
        var rowData = rowDataMap[Name];
        var rulesSetting = rowData["rulesSetting"];
        targeting.ObjectName = rulesSetting["Object__c"];
        targeting.rulesSettingName = rulesSetting["Name"];
       	cmp.set("v.targeting", targeting);
        helper.helperMethod(cmp);
    },
    inputSelectFieldChange: function(cmp, event, helper){
        var inputSelectField = cmp.find("inputSelectField");
        var FieldName = inputSelectField.get("v.value");
        var targeting = cmp.get("v.targeting");
        //console.log(JSON.stringify(targeting));
        targeting.FieldName = FieldName;
        var FieldValueString = '';
        var FieldValue = [];
        targeting.FieldValueString = FieldValueString;
        targeting.FieldValue = FieldValue;
        //console.log(JSON.stringify(targeting));
        
        cmp.set("v.selectedLookUpRecord", {});
        cmp.set("v.targeting", targeting);
        helper.helperMethod(cmp);
    },
    inputSelectOperatorChange: function(cmp, event, helper){
        var inputSelectOperator = cmp.find("inputSelectOperator");
        var Operator = inputSelectOperator.get("v.value");
        var targeting = cmp.get("v.targeting");
        var FieldValueString = '';
        var FieldValue = [];
        targeting.FieldValueString = FieldValueString;
        targeting.FieldValue = FieldValue;
        targeting["Operator"] = Operator;
        cmp.set("v.selectedLookUpRecord", {});
        cmp.set("v.targeting", targeting);
		helper.helperMethod(cmp);        
    },
    inputValuePickChange: function(cmp, event, helper){
        var inputValue = cmp.find("inputValuePick");
        var fieldValueString = inputValue.get("v.value");
        fieldValueString = fieldValueString.toString();
        var targeting = cmp.get("v.targeting");
        targeting["FieldValueString"] = fieldValueString;
		cmp.set("v.targeting", targeting);
    },
    inputValueMultipickChange: function(cmp, event, helper){
        var inputValue = cmp.find("inputValueMultipick");
        var fieldValueString = inputValue.get("v.value");
        fieldValueString = fieldValueString.toString();
        var FieldValue = fieldValueString.split(";");
        var targeting = cmp.get("v.targeting");
        targeting["FieldValueString"] = fieldValueString;
		cmp.set("v.targeting", targeting);
        var pickListArr = cmp.get("v.pickListArr");
        for(var i=0; i<pickListArr.length; i++){
            var pickList = pickListArr[i];
            var value = pickList["value"];
            var isSelect = FieldValue.includes(value) ? true : false;
            pickList["isSelect"] = isSelect;
        }
        //helper.helperMethod(cmp);
    },
    inputValueDateChange : function(cmp, event, helper){
     	var inputValue = cmp.find("inputValueDate");
        var fieldValueString = inputValue.get("v.value");
        fieldValueString = fieldValueString.toString();
        var targeting = cmp.get("v.targeting");
        targeting["FieldValueString"] = fieldValueString;
		cmp.set("v.targeting", targeting);
    },
    deleteTargetDetail : function(cmp, event, helper){
        var rowIndex = cmp.get("v.rowIndex");
        var detail = cmp.get("v.detail");
        detail.splice(rowIndex, 1);
        cmp.set("v.detail", detail);
    },
    handleComponentEvent : function(cmp, event, helper) {
    // get the selected Account record from the COMPONETN event 	 
        var selectedAccountGetFromEvent = event.getParam("recordByEvent");
        //alert(JSON.stringify(selectedAccountGetFromEvent));
    	var fieldValueString = selectedAccountGetFromEvent["Id"];
        console.log(fieldValueString);
        fieldValueString = fieldValueString.toString();
        var FieldValue = [];
        FieldValue.push(fieldValueString);
		cmp.set("v.targeting.FieldValue", FieldValue);         
        cmp.set("v.targeting.FieldValueString", fieldValueString); 
    }
})