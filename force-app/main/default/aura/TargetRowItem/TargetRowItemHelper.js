({
	helperMethod : function(component) {
        var rowIndex = component.get("v.rowIndex");
        var targetingMap = component.get("v.targeting");
        var rowDataMap = component.get("v.rowDataMap");
        var operatorMap = component.get("v.operatorMap");
        var fieldTypeDict = component.get("v.fieldTypeDict");
        //console.log(JSON.stringify(targetingMap));
        //alert('rowIndex: '+rowIndex+'\n targetingMap: '+JSON.stringify(targetingMap)+'\n rowDataMap'+JSON.stringify(rowDataMap));
		
        var ObjectName = targetingMap.ObjectName;
		component.set("v.ObjectName", ObjectName);
        var rulesSettingName = targetingMap.rulesSettingName;
        var rowData = rowDataMap[rulesSettingName];
        var fieldSetArr = rowData ? rowData.fieldSet : [];
		
        if(rowData != null){
            var fieldSetMap = {}
            
            for(var i=0; i<fieldSetArr.length; i++){
                var fieldSet = fieldSetArr[i];
                fieldSetMap[fieldSet.fieldPath] = fieldSet;
            }
            
            var FieldName = targetingMap.FieldName;
            var fieldSet = fieldSetMap[FieldName];
            var objectAPINameDict = rowData.objectAPIName;
            var objectAPIName = objectAPINameDict[FieldName];
            component.set("v.objectAPIName", objectAPIName);
            console.log("FieldName: "+FieldName);
            console.log("objectAPIName: "+objectAPIName); 
            
            var operatorOption;
            if(fieldSet){
                var typeApex = fieldSet.typeApex;
                var type = fieldSet.type;
                var jsonFieldTypeMap = fieldTypeDict[type];
                if(jsonFieldTypeMap){
                    targetingMap["FieldType"] = jsonFieldTypeMap["JSONFieldType__c"];
                }
                else{
                    targetingMap["FieldType"] = "String";
                }
                
                var FieldValueString = targetingMap.FieldValueString;
                console.log("FieldValueString: "+FieldValueString);
                if(ObjectName && (type == 'picklist' || type == 'multipicklist')){
                    var action = component.get("c.apex_getPicklistFieldValues");
                    action.setParams({ 
                        "objectName": ObjectName,
                        "pickListFieldName": fieldSet.fieldPath
                    });
                    action.setCallback(this, function(response){
                        var state = response.getState();
                        if(state === 'SUCCESS'){
                            var result = response.getReturnValue();
                            var resultArr = JSON.parse(result);
                            
                            var FieldValue = FieldValueString.split(";");
                            
                            for(var i=0; i<resultArr.length; i++){
                                var pickList = resultArr[i];
                                var value = pickList["value"];
                                var isSelect = FieldValue.includes(value) ? true : false;
                                pickList["isSelect"] = isSelect;
                            }
                            component.set("v.pickListArr", resultArr);
                            component.set("v.type", type);
                           	
                        }else if (state === 'ERROR'){
                            component.set("v.type", type);
                            var errors = response.getError();
                            if (errors) {
                                if (errors[0] && errors[0].message) {
                                    console.log("Error message: " +
                                                errors[0].message);
                                }
                            } else {
                                console.log("Unknown error");
                            }
                        }else{
                            console.log('Something went wrong, Please check with your admin');
                        }
                    });
                    $A.enqueueAction(action);
                }
                else{
                    component.set("v.type", type);
                    console.log(type);
                }
                if(objectAPIName && (type == 'reference' || type == 'id') && targetingMap.FieldValueString){
                    var action = component.get("c.apex_getRecordById");
                    //Set the Object parameters and Field Set name
                    action.setParams({
                        objectName: objectAPIName, 
                        recordId: targetingMap.FieldValueString
                    });
                    action.setCallback(this, function(response){
                        var state = response.getState();
                        
                        if(state === 'SUCCESS'){
                            console.log("selectedLookUpRecord: "+JSON.stringify(response.getReturnValue()));
                            component.set("v.selectedLookUpRecord", response.getReturnValue());
                            component.set("v.type", type);
                            console.log(type);
                        }else if (state === 'ERROR'){
                            component.set("v.type", type);
                            var errors = response.getError();
                            if (errors) {
                                if (errors[0] && errors[0].message) {
                                    console.log("Error message: " +
                                                errors[0].message);
                                }
                            } else {
                                console.log("Unknown error");
                            }
                        }else{
                            console.log('Something went wrong, Please check with your admin');
                        }
                    });
                    $A.enqueueAction(action);	
                }
                else{
                    component.set("v.type", type);
                    console.log(type);
                }
                operatorOption = operatorMap[typeApex];
                if(operatorOption){
                    
                }
                else{
                    operatorOption = operatorMap['STRING'];
                }
            }
        }        

        
        var allObjectArr = [];
        for (var key in rowDataMap) {
            var rowData = rowDataMap[key];
            var rulesSetting = rowData.rulesSetting;
            allObjectArr.push(rulesSetting);
        }
        
        component.set("v.targetingMap", targetingMap);
        component.set("v.fieldSetArr", fieldSetArr);
        component.set("v.operatorOption", operatorOption);
        component.set("v.allObjectArr", allObjectArr);
        
	}
})