({
	serverSideCall : function(component,action) {
        return new Promise(function(resolve, reject) { 
            action.setCallback(this, 
                function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                       	resolve(response.getReturnValue());
                    } else {
                       reject(new Error(response.getError()));
                    }
                }); 
            $A.enqueueAction(action);
        });
    },
    sortData: function (cmp, fieldName, sortDirection) {
        var data = cmp.get("v.mydata");
        var reverse = sortDirection !== 'asc';

        data = Object.assign([],
            data.sort(this.sortBy(fieldName, reverse ? -1 : 1))
        );
        cmp.set("v.mydata", data);
    },
    sortBy: function (field, reverse, primer) {
        var key = primer
            ? function(x) { return primer(x[field]) }
            : function(x) { return x[field] };

        return function (a, b) {
            var A = key(a);
            var B = key(b);
            return reverse * ((A > B) - (B > A));
        };
    },
    view_all : function(cmp, event, offset) {
        var modal = cmp.find("campaignModal");
        var modalBackdrop = cmp.find("myModal-Back");
        $A.util.addClass(modal, 'slds-fade-in-open');
        $A.util.addClass(modalBackdrop, 'slds-backdrop_open');
        cmp.set("v.show_detail", false);
        cmp.set("v.Spinner_view_all", true);
        
        
        var exeAction1 = cmp.get("c.apex_getFieldset");
        exeAction1.setParams({
            objectName : 'PointsMovement__c',
            fieldSetName : 'PointsMovementFieldSet'
        });
        
        var exeAction2 = cmp.get("c.apex_getFieldset");
        exeAction2.setParams({
            objectName : 'PointsMovement__c',
            fieldSetName : 'PointsMovementFieldSet2'
        });
        
        var exeAction3 = cmp.get("c.apex_getFieldset");
        exeAction3.setParams({
            objectName : 'PointsMovementDetail__c',
            fieldSetName : 'PointsMovementDetailFieldSet'
        });
        
        var exeAction4 = cmp.get("c.apex_getRecordType");
        exeAction4.setParams({
            objectName : 'PointsMovement__c'
        });
        
        var exeAction5 = cmp.get("c.apex_getNoRecordDisplay");
        //Set the Object parameters and Field Set name
        exeAction5.setParams({
            name : 'NoRecordDisplay_Txn'
        });
        
        var self = this;
        
        Promise.all([
            self.serverSideCall(cmp, exeAction1),
            self.serverSideCall(cmp, exeAction2),
            self.serverSideCall(cmp, exeAction3),
            self.serverSideCall(cmp, exeAction4),
            self.serverSideCall(cmp, exeAction5)
        ]).then(
            function(response) {
                
                var result1 = response[0];             
                var result1Arr = JSON.parse(result1);
                
                var result2 = response[1];             
                var result2Arr = JSON.parse(result2);
                
                var result3 = response[2];             
                var result3Arr = JSON.parse(result3);
                
                var result4 = response[3];  
                var result4Arr = JSON.parse(result4);
                
                var result5 = response[4];  
                cmp.set("v.limit", result5);
                
                var mycolumns = [];
                mycolumns.push({label: 'View', type: 'button', initialWidth: 135, typeAttributes: { label: 'View Details', name: 'view_details', title: 'Click to View Details'}});
                for(var i=0; i<result1Arr.length; i++){
                    var theResult = result1Arr[i];
                    var fieldPath = theResult.fieldPath;
                    fieldPath = fieldPath.toLowerCase();
                    if(theResult.type == 'double'){
                        mycolumns.push({ label: theResult.label, fieldName: fieldPath, type: 'number', sortable: true});
                    }
                    else if(theResult.type == 'datetime'){
                        mycolumns.push({ label: theResult.label, fieldName: fieldPath, type: 'date', sortable: true, typeAttributes:{ year: 'numeric', month: '2-digit', day: 'numeric', hour: '2-digit', minute: '2-digit', second: '2-digit', hour12: false}});
                    }
                    else if(theResult.type == 'date'){
                        mycolumns.push({ label: theResult.label, fieldName: fieldPath, type: 'date', sortable: true, typeAttributes:{ year: 'numeric', month: '2-digit', day: 'numeric'}});
                    }
                    else if(theResult.type == 'reference'){
                        mycolumns.push({label: theResult.label, fieldName: fieldPath, type: 'url', typeAttributes: { label: { fieldName: fieldPath+'_name' } }});
                    }
                    else{
                        mycolumns.push({ label: theResult.label, fieldName: fieldPath, type: theResult.type, sortable: true});
                    }
                }
                
                cmp.set("v.detailFieldSet", result2Arr);
                cmp.set("v.relatedFieldSet", result3Arr);
                cmp.set("v.recordTypeList", result4Arr);
                cmp.set("v.mycolumns", mycolumns);
				self.loadData(cmp, event, 0);

            }
        ).catch(
            function(error) {
                
            }
        );
        
        
    },
    loadData : function(cmp, event, offset) {
        
        cmp.set("v.show_detail", false);
        cmp.set("v.Spinner_view_all", true);
        
        var limit = cmp.get("v.limit");
        var inputPGID = cmp.find("inputPGID");
        var inputPGIDValue = inputPGID.get("v.value") ? inputPGID.get("v.value") : null;
        var inputTransferDateFrom = cmp.find("inputTransferDateFrom");
        var inputTransferDateFromValue = inputTransferDateFrom.get("v.value") ? inputTransferDateFrom.get("v.value") : null;
		var inputTransferDateTo = cmp.find("inputTransferDateTo");        
        var inputTransferDateToValue = inputTransferDateTo.get("v.value") ? inputTransferDateTo.get("v.value") : null;
		var inputRecordTypeOption = cmp.find("inputRecordTypeOption");
        var inputRecordTypeOptionValue = inputRecordTypeOption.get("v.value") ? inputRecordTypeOption.get("v.value") : null;

        var parameter = {}
        var exeAction1 = cmp.get("c.apex_getPointMovementList");
        parameter["MemberSFId"] = cmp.get("v.recordId");
        parameter["PointMovementPGId"] = inputPGIDValue;
        parameter["RecordType"] = inputRecordTypeOptionValue;
        parameter["TransferDateFrom"] = inputTransferDateFromValue;
        parameter["TransferDateTo"] = inputTransferDateToValue;
        parameter["limit"] = limit.toString();
        parameter["offset"] = offset.toString();
        exeAction1.setParams({ 
            parameter: parameter
        });
        
        var self = this;
        
        Promise.all([
            self.serverSideCall(cmp, exeAction1)
        ]).then(
            function(response) {
                
                var result = response[0];            
                var resultDict = JSON.parse(result);
                var ReturnCode = resultDict.ReturnCode;
                var ReturnMessage = resultDict.ReturnMessage;
                
                var mydata = [];
                var defaultData = [];
                if(ReturnCode == "1"){
                    var TotalCount = resultDict.TotalCount;
                    var page = Math.ceil(TotalCount/limit);
                    var options = [];
                    for(var i=0; i<page; i++){
                    	var pageDict = {'label': (i+1).toString(), 'value': (i+1).toString()}
                        options.push(pageDict);   
                    }
                    cmp.set("v.options", options);
                    var PointMovements = resultDict.PointMovements;
                    for(var i=0; i<PointMovements.length; i++){
                        var thePointMovement = PointMovements[i];
                        var PointMovement = thePointMovement.PointMovement;
                        mydata.push(PointMovement);
                        defaultData.push(PointMovement);
                    }
                    cmp.set("v.mydata", mydata);
                    cmp.set("v.dataResult", PointMovements);
                    cmp.set("v.defaultData", defaultData);
                    
                }
                else{
                    
                    
                }
                
                cmp.set("v.Spinner_view_all", false);
                
            }
        ).catch(
            function(error) {
                cmp.set("v.Spinner_view_all", false);
            }
        );

    }
})