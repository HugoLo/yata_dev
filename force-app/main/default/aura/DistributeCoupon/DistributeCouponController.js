({
	save: function(cmp, event, helper){
        event.preventDefault();
        
        cmp.set("v.isLoading", true); 
        var action = cmp.get("c.apex_distributeCoupon");
        action.setParams({ 
            recordId: cmp.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            cmp.set("v.isLoading", false); 
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                var resultDict = JSON.parse(result);
                if(resultDict.success){
                    alert('Distribut sucuessfully');
                    $A.get("e.force:closeQuickAction").fire(); 
                }
                else{
                    alert(resultDict.message);
                }
                
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        //console.log("Error message: " + 
                                 //errors[0].message);
                        alert("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    //console.log("Unknown error");
                    alert("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action); 
    	
    },
    cancel: function(cmp, event, helper){
    	$A.get("e.force:closeQuickAction").fire(); 
    }
})