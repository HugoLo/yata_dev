({
    getDataHelper : function(component, event) {
		
    },
    serverSideCall : function(component,action) {
        return new Promise(function(resolve, reject) { 
            action.setCallback(this, 
                function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                       	resolve(response.getReturnValue());
                    } else {
                       reject(new Error(response.getError()));
                    }
                }); 
            $A.enqueueAction(action);
        });
    },
    sortData: function (cmp, fieldName, sortDirection) {
        var data = cmp.get("v.mydata");
        var reverse = sortDirection !== 'asc';

        data = Object.assign([],
            data.sort(this.sortBy(fieldName, reverse ? -1 : 1))
        );
        cmp.set("v.mydata", data);
    },
    sortBy: function (field, reverse, primer) {
        var key = primer
            ? function(x) { return primer(x[field]) }
            : function(x) { return x[field] };

        return function (a, b) {
            var A = key(a);
            var B = key(b);
            return reverse * ((A > B) - (B > A));
        };
    },
    view_all_member_transaction : function(cmp, event){
        var modal = cmp.find("campaignModal");
        var modalBackdrop = cmp.find("myModal-Back");
        $A.util.addClass(modal, 'slds-fade-in-open');
        $A.util.addClass(modalBackdrop, 'slds-backdrop_open');
        
        cmp.set("v.show_detail", false);
        cmp.set("v.Spinner_view_all", true);
        cmp.set("v.optionValue", "1");
        
        var self = this;
        
        var exeAction1 = cmp.get("c.apex_getFieldset");
        //Set the Object parameters and Field Set name
        exeAction1.setParams({
            objectName : 'Transaction__c',
            fieldSetName : 'TransactionFieldSet'
        });
        
        var exeAction2 = cmp.get("c.apex_getFieldset");
        //Set the Object parameters and Field Set name
        exeAction2.setParams({
            objectName : 'Transaction__c',
            fieldSetName : 'TransactionFieldSet2'
        });
        
        var exeAction3 = cmp.get("c.apex_getFieldset");
        //Set the Object parameters and Field Set name
        exeAction3.setParams({
            objectName : 'TransactionLineItem__c',
            fieldSetName : 'TransactionLineItemFieldset'
        });
        
        var exeAction4 = cmp.get("c.apex_getFieldset");
        //Set the Object parameters and Field Set name
        exeAction4.setParams({
            objectName : 'PointDetail__c',
            fieldSetName : 'PointDetailFieldSet'
        });
        
        var exeAction5 = cmp.get("c.apex_getNoRecordDisplay");
        //Set the Object parameters and Field Set name
        exeAction5.setParams({
            name : 'NoRecordDisplay_Txn'
        });
        
        Promise.all([
            self.serverSideCall(cmp, exeAction1),
            self.serverSideCall(cmp, exeAction2),
            self.serverSideCall(cmp, exeAction3),
            self.serverSideCall(cmp, exeAction4),
            self.serverSideCall(cmp, exeAction5)
        ]).then(
            function(response) {
                
                var result = response[0];             
                var resultArr = JSON.parse(result);
                
                var result2 = response[1];               
                var result2Arr = JSON.parse(result2);
                
                var result3 = response[2];             
                var result3Arr = JSON.parse(result3);
        
                var result4 = response[3];             
                var result4Arr = JSON.parse(result4);
                
                var result5 = response[4]; 
                cmp.set("v.limit", result5);
                
                var mycolumns = [];
                mycolumns.push({label: 'View', type: 'button', initialWidth: 135, typeAttributes: { label: 'View Details', name: 'view_details', title: 'Click to View Details'}});
                for(var i=0; i<resultArr.length; i++){
                    var theResult = resultArr[i];
                    var fieldPath = theResult.fieldPath;
                    fieldPath = fieldPath.toLowerCase();
                    if(theResult.type == 'double'){
                        mycolumns.push({ label: theResult.label, fieldName: fieldPath, type: 'number', sortable: true});
                    }
                    else if(theResult.type == 'datetime'){
                        mycolumns.push({ label: theResult.label, fieldName: fieldPath, type: 'date', sortable: true, typeAttributes:{ year: 'numeric', month: '2-digit', day: 'numeric', hour: '2-digit', minute: '2-digit', second: '2-digit', hour12: false}});
                    }
                    else if(theResult.type == 'date'){
                        mycolumns.push({ label: theResult.label, fieldName: fieldPath, type: 'date', sortable: true, typeAttributes:{ year: 'numeric', month: '2-digit', day: 'numeric'}});
                    }
                    else if(theResult.type == 'reference'){
                        mycolumns.push({label: theResult.label, fieldName: fieldPath, type: 'url', typeAttributes: { label: { fieldName: fieldPath+'_name' } }});
                    }
                    else{
                        mycolumns.push({ label: theResult.label, fieldName: fieldPath, type: theResult.type, sortable: true});
                    }
                }
                
                cmp.set("v.result2Arr", result2Arr);
                cmp.set("v.result3Arr", result3Arr);
                cmp.set("v.result4Arr", result4Arr);
                //alert(JSON.stringify(mycolumns));
                cmp.set("v.mycolumns", mycolumns);
				self.loadData(cmp, event, 0);	                    
            }
        ).catch(
            function(error) {
                
            }
        );
        
        
        
    },
    loadData : function(cmp, event, offset) {
        
        cmp.set("v.show_detail", false);
        cmp.set("v.Spinner_view_all", true);
        
        var inputTypeOption = cmp.find("inputTypeOption");
        var inputTypeOptionValue = inputTypeOption.get("v.value") ? inputTypeOption.get("v.value") : null ;
        var inputSubTypeOption = cmp.find("inputSubTypeOption");
        var inputSubTypeOptionValue = inputSubTypeOption.get("v.value") ? inputSubTypeOption.get("v.value") : null ;
        var inputSourceOption = cmp.find("inputSourceOption");
        var inputSourceOptionValue = inputSourceOption.get("v.value") ? inputSourceOption.get("v.value") : null ;
        var inputTransactionDateFrom = cmp.find("inputTransactionDateFrom");
        var inputTransactionDateFromValue = inputTransactionDateFrom.get("v.value") ? inputTransactionDateFrom.get("v.value") : null ;
        var inputTransactionDateTo = cmp.find("inputTransactionDateTo");
        var inputTransactionDateToValue = inputTransactionDateTo.get("v.value") ? inputTransactionDateTo.get("v.value") : null ;
        var inputRefundDateFrom = cmp.find("inputRefundDateFrom");
        var inputRefundDateFromValue = inputRefundDateFrom.get("v.value") ? inputRefundDateFrom.get("v.value") : null ;
        var inputRefundDateTo = cmp.find("inputRefundDateTo");
        var inputRefundDateToValue = inputRefundDateTo.get("v.value") ? inputRefundDateTo.get("v.value") : null ;
    	var inputTransactionNo = cmp.find("inputTransactionNo");
        var inputTransactionNoValue = inputTransactionNo.get("v.value") ? inputTransactionNo.get("v.value") : null ;
        var selectedLookUpRecord = cmp.get("v.selectedLookUpRecord");
        var inputPGID = cmp.find("inputPGID");
        var inputPGIDValue = inputPGID.get("v.value") ? inputPGID.get("v.value") : null;
        var inputShopValue = selectedLookUpRecord["Id"] ? selectedLookUpRecord["Id"] : null;
        var limit = cmp.get("v.limit");
        
        var mydata = [];
		cmp.set("v.mydata", mydata);
        
        var parameter = {}
        var exeAction = cmp.get("c.apex_getTransactionList2");
        parameter['MemberSFId'] =  cmp.get("v.recordId");
        parameter['TransactionPGId'] = inputPGIDValue;
        parameter['TransactionNo'] = inputTransactionNoValue;
        parameter['Type'] = inputTypeOptionValue;
        parameter['SubType'] = inputSubTypeOptionValue;
        parameter['Source'] = inputSourceOptionValue;
        parameter['Shop'] = inputShopValue;
        parameter['TxnDateFrom'] = inputTransactionDateFromValue;
        parameter['TxnDateTo'] = inputTransactionDateToValue;
        parameter['RefundDateFrom'] = inputRefundDateFromValue
        parameter['RefundDateTo'] = inputRefundDateToValue;
        parameter['limit'] = limit.toString();
        parameter['offset'] = offset.toString();
        exeAction.setParams({ 
            parameter : parameter
        });
		
        
        var self = this;
        
        Promise.all([
            self.serverSideCall(cmp, exeAction)
        ]).then(
            function(response) {
                
                var result1 = response[0];
                var result1Dict = JSON.parse(result1);
                var ReturnCode = result1Dict.ReturnCode;
                var ReturnMessage = result1Dict.ReturnMessage;
                
                //var mydata = [];
                if(ReturnCode == "1"){
                    var TotalCount = result1Dict.TotalCount;
                    var page = Math.ceil(TotalCount/limit);
                    var options = [];
                    for(var i=0; i<page; i++){
                    	var pageDict = {'label': (i+1).toString(), 'value': (i+1).toString()}
                        options.push(pageDict);   
                    }
                    cmp.set("v.options", options);
                    var Transactions = result1Dict.Transactions;
                    for(var i=0; i<Transactions.length; i++){
                        var Txn = Transactions[i];
                        mydata.push(Txn);
                    }
                    
                    //alert(JSON.stringify(mydata));
                    cmp.set("v.mydata", mydata);
					                    
                }
                else{
                    
                    
                }
                
                cmp.set("v.Spinner_view_all", false);
            }
        ).catch(
            function(error) {
                cmp.set("v.Spinner_view_all", false);
            }
        );
    },
    resetInput : function(cmp, keepPGID){
        var inputTypeOption = cmp.find("inputTypeOption");
        inputTypeOption.set("v.value", "");
        var inputSubTypeOption = cmp.find("inputSubTypeOption");
        inputSubTypeOption.set("v.value", "");
        var inputSourceOption = cmp.find("inputSourceOption");
        inputSourceOption.set("v.value", "");
        var inputTransactionDateFrom = cmp.find("inputTransactionDateFrom");
        inputTransactionDateFrom.set("v.value", "");
        var inputTransactionDateTo = cmp.find("inputTransactionDateTo");
        inputTransactionDateTo.set("v.value", "");
        var inputRefundDateFrom = cmp.find("inputRefundDateFrom");
        inputRefundDateFrom.set("v.value", "");
        var inputRefundDateTo = cmp.find("inputRefundDateTo");
        inputRefundDateTo.set("v.value", "");
    	var inputTransactionNo = cmp.find("inputTransactionNo");
        inputTransactionNo.set("v.value", "");
        if(!keepPGID){
        	var inputPGID = cmp.find("inputPGID");
            inputPGID.set("v.value", "");    
        }
        cmp.set("v.inputShop", "");
        cmp.set("v.selectedLookUpRecord", {});
    } 
})