({
    loadInitData : function(cmp, event, helper) {

        cmp.set("v.Spinner", true);
        cmp.set("v.haveRecord", false);
        var previewLimit = cmp.get("v.previewLimit");
        
        var parameter = {}
        var exeAction1 = cmp.get("c.apex_getTransactionList2");
        parameter['MemberSFId'] =  cmp.get("v.recordId");
        parameter['TransactionNo'] = null;
        parameter['Type'] = null;
        parameter['SubType'] = null;
        parameter['Source'] = null;
        parameter['Shop'] = null;
        parameter['TxnDateFrom'] = null;
        parameter['TxnDateTo'] = null;
        parameter['RefundDateFrom'] = null
        parameter['RefundDateTo'] = null;
        parameter['limit'] = previewLimit.toString();
        exeAction1.setParams({ 
            parameter : parameter
        });
		
        var exeAction2 = cmp.get("c.apex_getRecordType");
        exeAction2.setParams({ 
            "objectName": "Transaction__c"
        });      
        
        var exeAction3 = cmp.get("c.apex_getPicklistFieldValues");
        exeAction3.setParams({ 
            "objectName": "Transaction__c",
            "pickListFieldName": "SubType__c"
        });
        
        var exeAction4 = cmp.get("c.apex_getPicklistFieldValues");
        exeAction4.setParams({ 
            "objectName": "Transaction__c",
            "pickListFieldName": "Source__c"
        });
        
        var exeAction5 = cmp.get("c.apex_getFieldset");
        exeAction5.setParams({
            objectName : 'Transaction__c',
            fieldSetName : 'TransactionFieldSetPerview'
        }); 
		
        
        Promise.all([
            helper.serverSideCall(cmp, exeAction1),
            helper.serverSideCall(cmp, exeAction2),
            helper.serverSideCall(cmp, exeAction3),
            helper.serverSideCall(cmp, exeAction4),
            helper.serverSideCall(cmp, exeAction5)
        ]).then(
            function(response) {
                
                var result4 = response[4];
                var resul4Arr = JSON.parse(result4);
                
                var result = response[0];
                var resultDict = JSON.parse(result);
                var ReturnCode = resultDict.ReturnCode;
                var ReturnMessage = resultDict.ReturnMessage;
                
                if(ReturnCode == "1"){
                    var TotalCount = resultDict.TotalCount;
                    cmp.set("v.TotalCount", TotalCount);
                    var Transactions = resultDict.Transactions;
                    if(Transactions.length > 0){
                        cmp.set("v.haveRecord", true);
                    } 
                    
                    var mydata = [];
                    for(var i=0; i<Transactions.length; i++){
                        var Txn = Transactions[i];
                        mydata.push(Txn);
                    }
                    
                    var mycolumns = [];
                    for(var i=0; i<resul4Arr.length; i++){
                        var theResult = resul4Arr[i];
                        var fieldPath = theResult.fieldPath;
                        fieldPath = fieldPath.toLowerCase();
                        
                        if(fieldPath == 'pgid__c'){
                            mycolumns.push({ label:theResult.label, type: 'button', typeAttributes: { label: { fieldName: fieldPath }, variant:'base', title: 'Click to View Details'}});
                        }
                        else{
                         	if(theResult.type == 'double'){
                                mycolumns.push({label: theResult.label, fieldName: fieldPath, type: 'number'});
                            }
                            else if(theResult.type == 'datetime'){
                                mycolumns.push({ label: theResult.label, fieldName: fieldPath, type: 'date', sortable: true, typeAttributes:{ year: 'numeric', month: '2-digit', day: 'numeric', hour: '2-digit', minute: '2-digit', second: '2-digit', hour12: false}});
                            }
                            else if(theResult.type == 'date'){
                                mycolumns.push({ label: theResult.label, fieldName: fieldPath, type: 'date', sortable: true, typeAttributes:{ year: 'numeric', month: '2-digit', day: 'numeric'}});
                            }
                            else if(theResult.type == 'reference'){
                                mycolumns.push({label: theResult.label, fieldName: fieldPath, type: 'url', typeAttributes: { label: { fieldName: fieldPath+'_name' } }});
                            }
                            else{
                                mycolumns.push({label: theResult.label, fieldName: fieldPath, type: theResult.type});
                            }   
                        }
                    }
                    
                    cmp.set("v.previewcolumns", mycolumns);
                    cmp.set("v.previewdata", mydata);
                }
                else{
                	
                    
                }
                
                var result1 = response[1];
                var result1Dict = JSON.parse(result1);
                cmp.set("v.typeOption", result1Dict);
                
                var result2 = response[2];
                var result2Dict = JSON.parse(result2);
                cmp.set("v.subTypeOption", result2Dict);
                
                var result3 = response[3];
                var result3Dict = JSON.parse(result3);
                cmp.set("v.sourceOption", result3Dict);
                cmp.set("v.Spinner", false);
                /*
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": " Your record have been loaded successfully."
                });
                toastEvent.fire();
                */
            }
        ).catch(
            function(error) {
                
            }
        );
	},
    view_all_member_redemption : function(cmp, event, helper) {
        helper.resetInput(cmp, false);
        helper.view_all_member_transaction(cmp, event);
    },
    hideExampleModal : function(cmp) {
        var modal = cmp.find("campaignModal");
        var modalBackdrop = cmp.find("myModal-Back");
        $A.util.removeClass(modal, 'slds-fade-in-open');
    	$A.util.removeClass(modalBackdrop, 'slds-backdrop_open');
    },
    hideExampleModalDetail : function(cmp) {
        var modal = cmp.find("campaignModalDetail");
        var modalBackdrop = cmp.find("myModal-Back-Detail");
        $A.util.removeClass(modal, 'slds-fade-in-open');
    	$A.util.removeClass(modalBackdrop, 'slds-backdrop_open');
    },
    handleRowAction: function (cmp, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        switch (action.name) {
            case 'view_details':
                
                
                var modal = cmp.find("campaignModalDetail");
                var modalBackdrop = cmp.find("myModal-Back-Detail");
                $A.util.addClass(modal, 'slds-fade-in-open');
                $A.util.addClass(modalBackdrop, 'slds-backdrop_open');
                
                
                var result2Arr = cmp.get("v.result2Arr");
                var Txn = row;
                for(var i=0; i<result2Arr.length; i++){
                    var result = result2Arr[i];
                    var fieldPath = result.fieldPath;
                    fieldPath = fieldPath.toLowerCase();
                    var type = result.type;
                    var value = '';
                    if(type == 'reference'){
                        var fieldPathName = fieldPath+'_name';
                        value = '<a href="'+Txn[fieldPath]+'" >'+Txn[fieldPathName]+'</a>'
                    }
                    else{
                    	value = Txn[fieldPath];    
                    }
                    
                    result["value"] = value;
                }
                cmp.set("v.result2Arr", result2Arr);
                
                
                
                var pgid = Txn.pgid__c;
                var parameter = {}
                var exeAction = cmp.get("c.apex_getTransactionLineList");
                parameter['TransactionPGId'] =  pgid.toString();
                exeAction.setParams({ 
                    parameter : parameter
                });
                
                
                cmp.set("v.show_detail", false);
                cmp.set("v.Spinner_Detail", true);
                exeAction.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        cmp.set("v.Spinner_Detail", false);
                        cmp.set("v.show_detail", true);
                        var resultDict = JSON.parse(response.getReturnValue());
                        var transaction = resultDict['Transactions'];
                        
                        /////////////////////////   TxnLines   /////////////////////////

                        var result3Arr = cmp.get("v.result3Arr");
                        var my_item_detail_columns = [];
                        var item_detail_fieldMap = {};
                        
                        for(var i=0; i<result3Arr.length; i++){
                            var theResult = result3Arr[i];
                            var fieldPath = theResult.fieldPath;
                            fieldPath = fieldPath.toLowerCase();
                            if(theResult.type == 'double'){
                                my_item_detail_columns.push({ label: theResult.label, fieldName: fieldPath, type: 'number', sortable: true});
                            }
                            else if(theResult.type == 'datetime'){
                                my_item_detail_columns.push({ label: theResult.label, fieldName: fieldPath, type: 'date', sortable: true, typeAttributes:{ year: 'numeric', month: '2-digit', day: 'numeric', hour: '2-digit', minute: '2-digit', second: '2-digit', hour12: false}});
                            }
                            else if(theResult.type == 'date'){
                                my_item_detail_columns.push({ label: theResult.label, fieldName: fieldPath, type: 'date', sortable: true, typeAttributes:{ year: 'numeric', month: '2-digit', day: 'numeric'}});
                            }
                            else if(theResult.type == 'reference'){
                                my_item_detail_columns.push({label: theResult.label, fieldName: fieldPath, type: 'url', typeAttributes: { label: { fieldName: fieldPath+'_name' } }});
                            }
                            else{
                                my_item_detail_columns.push({ label: theResult.label, fieldName: fieldPath, type: theResult.type, sortable: true});
                            }
                            item_detail_fieldMap[fieldPath] = theResult;
                        }
                        
                        var my_item_detail_mydata = [];
                        var TxnLines = transaction[0].TxnLines;
                        for(var i=0; i<TxnLines.length; i++){
                            var TxnLine = TxnLines[i];
                            var my_item_detail_dataMap = {}
                            for(var fieldName in item_detail_fieldMap){
                                var theResult = item_detail_fieldMap[fieldName];
                                var type = theResult.type;
                                if(type == 'reference'){
                                    my_item_detail_dataMap[fieldName+'_name'] = TxnLine[fieldName+'_name'];
                                }
                                my_item_detail_dataMap[fieldName] = TxnLine[fieldName];
                            }
                            my_item_detail_mydata.push(my_item_detail_dataMap);
                        }
                        //alert(JSON.stringify(my_item_detail_columns));
                        //alert(JSON.stringify(my_item_detail_mydata));
                        cmp.set("v.my_item_detail_columns", my_item_detail_columns);
                        cmp.set("v.my_item_detail_mydata", my_item_detail_mydata);
                        
                        
                        var result4Arr = cmp.get("v.result4Arr");
                        var my_point_detail_columns = [];
                        var point_detail_fieldMap = {};
        
                        for(var i=0; i<result4Arr.length; i++){
                            var theResult = result4Arr[i];
                            var fieldPath = theResult.fieldPath;
                            fieldPath = fieldPath.toLowerCase();
                            
                            if(theResult.type == 'double'){
                                my_point_detail_columns.push({ label: theResult.label, fieldName: fieldPath, type: 'number', sortable: true});
                            }
                            else if(theResult.type == 'datetime'){
                                my_point_detail_columns.push({ label: theResult.label, fieldName: fieldPath, type: 'date', sortable: true, typeAttributes:{ year: 'numeric', month: '2-digit', day: 'numeric', hour: '2-digit', minute: '2-digit', second: '2-digit', hour12: false}});
                            }
                            else if(theResult.type == 'date'){
                                my_point_detail_columns.push({ label: theResult.label, fieldName: fieldPath, type: 'date', sortable: true, typeAttributes:{ year: 'numeric', month: '2-digit', day: 'numeric'}});
                            }
                            else if(theResult.type == 'reference'){
                                my_point_detail_columns.push({label: theResult.label, fieldName: fieldPath, type: 'url', typeAttributes: { label: { fieldName: fieldPath+'_name' } }});
                            }
                            else{
                                my_point_detail_columns.push({ label: theResult.label, fieldName: fieldPath, type: theResult.type, sortable: true});
                            }
                            point_detail_fieldMap[fieldPath] = theResult;
                        }
                        
                        var my_point_detail_mydata = [];
                        var PointDetails = transaction[0].PointDetail;
                        for(var i=0; i<PointDetails.length; i++){
                            var PointDetail = PointDetails[i];
                            var my_point_detail_dataMap = {}
                            for(var fieldName in point_detail_fieldMap){
                                var theResult = point_detail_fieldMap[fieldName];
                                var type = theResult.type;
                                if(type == 'reference'){
                                    my_point_detail_dataMap[fieldName+'_name'] = PointDetail[fieldName+'_name'];
                                }
                                my_point_detail_dataMap[fieldName] = PointDetail[fieldName];
                            }
                            my_point_detail_mydata.push(my_point_detail_dataMap);
                        }
                        
                        cmp.set("v.my_point_detail_columns", my_point_detail_columns);
                        cmp.set("v.my_point_detail_mydata", my_point_detail_mydata);
                        
                        cmp.set("v.show_detail", true);
                        /////////////////////////   TxnLines   /////////////////////////
                        
                    }
                    else if (state === "INCOMPLETE") {
                        // do something
                    }
                    else if (state === "ERROR") {
                        cmp.set("v.Spinner_Detail", false);
                        cmp.set("v.show_detail", true);
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                         errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
                });
        
                $A.enqueueAction(exeAction);
                
                
                
                
                /*
                var mydata = cmp.get("v.mydata");
                var defualtData = cmp.get("v.defualtData");
                var index = defualtData.indexOf(row);
                var Transactions = cmp.get("v.Transactions");
                var transaction = Transactions[index];
                */
                /////////////////////////   Txn   /////////////////////////
                /*
                var result2Arr = cmp.get("v.result2Arr");
                var Txn = transaction.Txn;
                for(var i=0; i<result2Arr.length; i++){
                    var result = result2Arr[i];
                    var fieldPath = result.fieldPath;
                    fieldPath = fieldPath.toLowerCase();
                    var type = result.type;
                    var value = '';
                    if(type == 'reference'){
                        var fieldPathName = fieldPath+'_name';
                        value = '<a href="'+Txn[fieldPath]+'" >'+Txn[fieldPathName]+'</a>'
                    }
                    else{
                    	value = Txn[fieldPath];    
                    }
                    
                    result["value"] = value;
                }
                cmp.set("v.result2Arr", result2Arr);
                */
                /////////////////////////   Txn   /////////////////////////
				
                
                /////////////////////////   TxnLines   /////////////////////////
                /*
                var result3Arr = cmp.get("v.result3Arr");
                var my_item_detail_columns = [];
                var item_detail_fieldMap = {};
                
                for(var i=0; i<result3Arr.length; i++){
                    var theResult = result3Arr[i];
                    var fieldPath = theResult.fieldPath;
                    fieldPath = fieldPath.toLowerCase();
                    if(theResult.type == 'double'){
                        my_item_detail_columns.push({ label: theResult.label, fieldName: fieldPath, type: 'number', sortable: true});
                    }
                    else if(theResult.type == 'datetime'){
                        mycolumns.push({ label: theResult.label, fieldName: fieldPath, type: 'date', sortable: true, typeAttributes:{ year: 'numeric', month: '2-digit', day: 'numeric', hour: '2-digit', minute: '2-digit', second: '2-digit', hour12: false}});
                    }
                    else if(theResult.type == 'date'){
                        mycolumns.push({ label: theResult.label, fieldName: fieldPath, type: 'date', sortable: true, typeAttributes:{ year: 'numeric', month: '2-digit', day: 'numeric'}});
                    }
                    else if(theResult.type == 'reference'){
                        mycolumns.push({label: theResult.label, fieldName: fieldPath, type: 'url', typeAttributes: { label: { fieldName: fieldPath+'_name' } }});
                    }
                    else{
                        my_item_detail_columns.push({ label: theResult.label, fieldName: fieldPath, type: theResult.type, sortable: true});
                    }
                    item_detail_fieldMap[fieldPath] = theResult;
                }
                
                var my_item_detail_mydata = [];
                var TxnLines = transaction.TxnLines;
                for(var i=0; i<TxnLines.length; i++){
                    var TxnLine = TxnLines[i];
                    var my_item_detail_dataMap = {}
                    for(var fieldName in item_detail_fieldMap){
                        my_item_detail_dataMap[fieldName] = TxnLine[fieldName];
                    }
                    my_item_detail_mydata.push(my_item_detail_dataMap);
                }
                cmp.set("v.my_item_detail_columns", my_item_detail_columns);
                cmp.set("v.my_item_detail_mydata", my_item_detail_mydata);
                */
                /////////////////////////   TxnLines   /////////////////////////
                
                              
                /////////////////////////   PointDetail   /////////////////////////
                /*
                var result4Arr = cmp.get("v.result4Arr");
                var my_point_detail_columns = [];
                var point_detail_fieldMap = {};

                for(var i=0; i<result4Arr.length; i++){
                    var theResult = result4Arr[i];
                    var fieldPath = theResult.fieldPath;
                    fieldPath = fieldPath.toLowerCase();
                    
                    if(theResult.type == 'double'){
                        my_point_detail_columns.push({ label: theResult.label, fieldName: fieldPath, type: 'number', sortable: true});
                    }
                    else if(theResult.type == 'datetime'){
                        my_point_detail_columns.push({ label: theResult.label, fieldName: fieldPath, type: 'date', sortable: true, typeAttributes:{ year: 'numeric', month: '2-digit', day: 'numeric', hour: '2-digit', minute: '2-digit', second: '2-digit', hour12: false}});
                    }
                    else if(theResult.type == 'date'){
                        my_point_detail_columns.push({ label: theResult.label, fieldName: fieldPath, type: 'date', sortable: true, typeAttributes:{ year: 'numeric', month: '2-digit', day: 'numeric'}});
                    }
                    else if(theResult.type == 'reference'){
                        my_point_detail_columns.push({label: theResult.label, fieldName: fieldPath, type: 'url', typeAttributes: { label: { fieldName: fieldPath+'_name' } }});
                    }
                    else{
                        my_point_detail_columns.push({ label: theResult.label, fieldName: fieldPath, type: theResult.type, sortable: true});
                    }
                    point_detail_fieldMap[fieldPath] = theResult;
                }
                
                var my_point_detail_mydata = [];
                var PointDetails = transaction.PointDetail;
                for(var i=0; i<PointDetails.length; i++){
                    var PointDetail = PointDetails[i];
                    var my_point_detail_dataMap = {}
                    for(var fieldName in point_detail_fieldMap){
                        my_point_detail_dataMap[fieldName] = PointDetail[fieldName];
                    }
                    my_point_detail_mydata.push(my_point_detail_dataMap);
                }
                cmp.set("v.my_point_detail_columns", my_point_detail_columns);
                cmp.set("v.my_point_detail_mydata", my_point_detail_mydata);
                cmp.set("v.show_detail", true);
                */
                /////////////////////////   PointDetail   /////////////////////////
                
                
                break;
            case 'edit_status':
                //helper.editRowStatus(cmp, row, action);
                break;
            default:
                //helper.showRowDetails(row);
                break;
        }
    },
    updateColumnSorting: function (cmp, event, helper) {
        cmp.set('v.isLoading', true);
        // We use the setTimeout method here to simulate the async
        // process of the sorting data, so that user will see the
        // spinner loading when the data is being sorted.
        setTimeout(function() {
            var fieldName = event.getParam('fieldName');
            var sortDirection = event.getParam('sortDirection');
            cmp.set("v.sortedBy", fieldName);
            cmp.set("v.sortedDirection", sortDirection);
            helper.sortData(cmp, fieldName, sortDirection);
            cmp.set('v.isLoading', false);
        }, 0);
    },
    handleFilterRedemption: function (cmp, event, helper) {
        helper.loadData(cmp, event, 0);
    },
    handleComponentEvent : function(cmp, event, helper) {
    // get the selected Account record from the COMPONETN event 	 
        var selectedAccountGetFromEvent = event.getParam("recordByEvent");
    	var fieldValueString = selectedAccountGetFromEvent["Id"];
        //alert(fieldValueString);
        cmp.set('v.inputShop', fieldValueString);
    },
    handlePerviewRowAction: function (cmp, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        var pgid = row.pgid__c;
        var inputPGID = cmp.find("inputPGID");
        inputPGID.set("v.value", pgid.toString());
        helper.resetInput(cmp, true);
        helper.view_all_member_transaction(cmp, event);
    },
    comboboxHandleChange: function(cmp, event, helper){
        var selectedOptionValue = event.getParam("value");
        cmp.set("v.optionValue", selectedOptionValue);
        var limit = cmp.get("v.limit");
        var offset = (selectedOptionValue - 1) * limit;
        helper.loadData(cmp, event, offset);
    }
})