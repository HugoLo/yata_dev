({
	init : function(cmp, event, helper) {
		var action = cmp.get("c.apex_getCatHierarchy");
        action.setParams({ 
            "recId": cmp.get("v.recId")
        });
	
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                var resultArr = JSON.parse(result);
                cmp.set("v.gridData", resultArr);
                
                var columns = [
                    {
                        type: 'text',
                        fieldName: 'Name',
                        label: 'Name',
                        initialWidth: 300
                    },
                    {
                        type: 'number',
                        fieldName: 'CategoryLevel__c',
                        label: 'Level',
                        initialWidth: 300
                    }
                ];
                
                cmp.set('v.gridColumns', columns);
                //var expandedRows = ["YATA", "Supermarket"];
                //cmp.set('v.gridExpandedRows', expandedRows);
                var tree = cmp.find('mytree');
                tree.expandAll();
            }
            
        });
        
        $A.enqueueAction(action);
	},
    expandAllRows: function(cmp, event) {
        var tree = cmp.find('mytree');
        tree.expandAll();
    }
})