({
    doInit : function(component, event, helper) {
        var action = component.get("c.checkAllowProceed"); 
        action.setParams({
            "id": component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            if(response.getState() === "SUCCESS") {
                component.set('v.inited', true);
                var checkresult = response.getReturnValue();
                component.set('v.allowed', checkresult==null);
                if(checkresult=='status')
                    component.set('v.checkresult', 'Prepare member list should be done in "Planning" stage.');
                else if(checkresult=='specificmember')
                    component.set('v.checkresult', 'This campaign is not "Segmentation By Specific Member".');
                else if(checkresult=='membercount')
                    component.set('v.checkresult', 'No campaign members.');
                else
                    component.set('v.checkresult', '');
            }
        });
        $A.enqueueAction(action);
    },
    
    cancel: function(component, event, helper) {
        helper.close();
    },
    
    confirmMemberList: function(component, event, helper) {
        var id = component.get("v.recordId");
        var action = component.get("c.proceed");
        action.setParams({
            id: id,
        });
        action.setCallback(this, function(response) {
            helper.close();
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result.Error) {
                    helper.showToast('ERROR', result.Error, 'error', false);
                } else {
					helper.refreshView();
                }
            } else {
                console.log(state);
                var err = response.getError();
                console.log(err);
                helper.showToast('ERROR', JSON.stringify(response.getError()), 'error', false);
            }
        });
        $A.enqueueAction(action);
    }

})