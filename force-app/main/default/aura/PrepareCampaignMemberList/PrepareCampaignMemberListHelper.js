({
    showToast : function(title, message, type, stay) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type,
            "mode": stay? "sticky" : "dismissible",
            "duration": 8000
        });
        toastEvent.fire();
    },
    
    refreshView : function() {
        $A.get("e.force:refreshView").fire();
    },
    
    close : function() {
        $A.get("e.force:closeQuickAction").fire();
    }
})