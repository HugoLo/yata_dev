({
	loadInitData : function(cmp, event, helper) {
        
        cmp.set("v.Spinner", true);
        cmp.set("v.haveRecord", false);
        
        var previewLimit = cmp.get("v.previewLimit");
        var parameter = {}
        var exeAction1 = cmp.get("c.apex_getAppMessageList");
        parameter["MemberSFId"] = cmp.get("v.recordId");
        parameter["Status"] = null;
        parameter["MessageDateFrom"] = null;
        parameter["MessageDateTo"] = null;
        parameter['limit'] = previewLimit.toString();
        exeAction1.setParams({ 
            parameter : parameter
        });
        
        var exeAction2 = cmp.get("c.apex_getFieldset");
        exeAction2.setParams({
            objectName : 'InAppMessage__c',
            fieldSetName : 'InAppMessageFieldSetPreview'
        });  
        
        Promise.all([
            helper.serverSideCall(cmp, exeAction1),
            helper.serverSideCall(cmp, exeAction2)
        ]).then(
            function(response) {
                
                var result1 = response[1];
                var result1Arr = JSON.parse(result1);
                
                var result = response[0];
                var resultDict = JSON.parse(result);
                var ReturnCode = resultDict.ReturnCode;
                var ReturnMessage = resultDict.ReturnMessage;
                
                
                if(ReturnCode == "1"){
                    var Messages = resultDict.Messages;
                   	if(Messages.length > 0){
                        cmp.set("v.haveRecord", true);
                    } 
                    var mycolumns = [];
                    for(var i=0; i<result1Arr.length; i++){
                        var theResult = result1Arr[i];
                        var fieldPath = theResult.fieldPath;
                        fieldPath = fieldPath.toLowerCase();
                        if(fieldPath == 'pgid__c'){
                            mycolumns.push({ label:theResult.label, type: 'button', typeAttributes: { label: { fieldName: fieldPath }, variant:'base', title: 'Click to View Details'}});
                        }
                        else{
                         	if(theResult.type == 'double'){
                                mycolumns.push({ label: theResult.label, fieldName: fieldPath, type: 'number'});
                            }
                            else if(theResult.type == 'datetime'){
                                mycolumns.push({ label: theResult.label, fieldName: fieldPath, type: 'date', sortable: true, typeAttributes:{ year: 'numeric', month: '2-digit', day: 'numeric', hour: '2-digit', minute: '2-digit', second: '2-digit', hour12: false}});
                            }
                            else if(theResult.type == 'date'){
                                mycolumns.push({ label: theResult.label, fieldName: fieldPath, type: 'date', sortable: true, typeAttributes:{ year: 'numeric', month: '2-digit', day: 'numeric'}});
                            }
                            else if(theResult.type == 'reference'){
                                mycolumns.push({label: theResult.label, fieldName: fieldPath, type: 'url', typeAttributes: { label: { fieldName: fieldPath+'_name' } }});
                            }
                            else{
                                mycolumns.push({ label: theResult.label, fieldName: fieldPath, type: theResult.type});
                            }   
                        }
                    }
                    
                    cmp.set("v.previewcolumns", mycolumns);
                    cmp.set("v.previewdata", Messages);
                }
                else{
                	
                    
                }
            	
                cmp.set("v.Spinner", false);
                /*
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": " Your record have been loaded successfully."
                });
                toastEvent.fire();
                */
            }
        ).catch(
            function(error) {
                
            }
        );        
    },
    hideExampleModal : function(cmp) {
        var modal = cmp.find("campaignModal");
        var modalBackdrop = cmp.find("myModal-Back");
        $A.util.removeClass(modal, 'slds-fade-in-open');
    	$A.util.removeClass(modalBackdrop, 'slds-backdrop_open');
    },
    view_all : function(cmp, event, helper) {
        var inputPGID = cmp.find("inputPGID");
        inputPGID.set("v.value", null);
        helper.view_all(cmp, event, 0);
    },
    handleRowAction: function (cmp, event, helper) {
        cmp.set("v.show_detail", true);
        var action = event.getParam('action');
        var row = event.getParam('row');
        switch (action.name) {
            case 'view_details':
                var mydata = cmp.get("v.mydata");
                var index = mydata.indexOf(row);
                var Message = mydata[index];
                
                var detailFieldSet = cmp.get("v.detailFieldSet");
                for(var i=0; i<detailFieldSet.length; i++){
                    var result = detailFieldSet[i];
                    var fieldPath = result.fieldPath;
                    fieldPath = fieldPath.toLowerCase();
                    var type = result.type;
                    var value = '';
                    if(type == 'reference'){
                        var fieldPathName = fieldPath+'_name';
                        value = '<a href="'+Message[fieldPath]+'" >'+Message[fieldPathName]+'</a>'
                    }
                    else{
                    	value = Message[fieldPath];    
                    }
                    result["value"] = value;
                }
                cmp.set("v.detailFieldSet", detailFieldSet);
                
                
                break;
            case 'edit_status':
                //helper.editRowStatus(cmp, row, action);
                break;
            default:
                //helper.showRowDetails(row);
                break;
        }
    },
    updateColumnSorting: function (cmp, event, helper) {
        cmp.set('v.isLoading', true);
        // We use the setTimeout method here to simulate the async
        // process of the sorting data, so that user will see the
        // spinner loading when the data is being sorted.
        setTimeout(function() {
            var fieldName = event.getParam('fieldName');
            var sortDirection = event.getParam('sortDirection');
            cmp.set("v.sortedBy", fieldName);
            cmp.set("v.sortedDirection", sortDirection);
            helper.sortData(cmp, fieldName, sortDirection);
            cmp.set('v.isLoading', false);
        }, 0);
    },
    handleFilter: function (cmp, event, helper) {
		helper.loadData(cmp, event, 0);
    },
    handlePerviewRowAction: function (cmp, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        var pgid = row.pgid__c;
        var inputPGID = cmp.find("inputPGID");
        inputPGID.set("v.value", pgid.toString());
        helper.view_all(cmp, event, 0);
    },
    comboboxHandleChange: function(cmp, event, helper){
        var selectedOptionValue = event.getParam("value");
        var limit = cmp.get("v.limit");
        var offset = (selectedOptionValue - 1) * limit;
        helper.view_all(cmp, event, offset);
    }
})