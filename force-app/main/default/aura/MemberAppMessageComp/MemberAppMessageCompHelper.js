({
	serverSideCall : function(component,action) {
        return new Promise(function(resolve, reject) { 
            action.setCallback(this, 
                function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                       	resolve(response.getReturnValue());
                    } else {
                       reject(new Error(response.getError()));
                    }
                }); 
            $A.enqueueAction(action);
        });
    },
    sortData: function (cmp, fieldName, sortDirection) {
        var data = cmp.get("v.mydata");
        var reverse = sortDirection !== 'asc';

        data = Object.assign([],
            data.sort(this.sortBy(fieldName, reverse ? -1 : 1))
        );
        cmp.set("v.mydata", data);
    },
    sortBy: function (field, reverse, primer) {
        var key = primer
            ? function(x) { return primer(x[field]) }
            : function(x) { return x[field] };

        return function (a, b) {
            var A = key(a);
            var B = key(b);
            return reverse * ((A > B) - (B > A));
        };
    },
    view_all : function(cmp, event, offset) {
        var modal = cmp.find("campaignModal");
        var modalBackdrop = cmp.find("myModal-Back");
        $A.util.addClass(modal, 'slds-fade-in-open');
        $A.util.addClass(modalBackdrop, 'slds-backdrop_open');
        cmp.set("v.show_detail", false);
        cmp.set("v.Spinner_view_all", true);
        
        var exeAction1 = cmp.get("c.apex_getFieldset");
        exeAction1.setParams({
            objectName : 'InAppMessage__c',
            fieldSetName : 'InAppMessageFieldSet'
        });
        
        var exeAction2 = cmp.get("c.apex_getFieldset");
        exeAction2.setParams({
            objectName : 'InAppMessage__c',
            fieldSetName : 'InAppMessageFieldSet2'
        });
        
        var exeAction3 = cmp.get("c.apex_getPicklistFieldValues");
        exeAction3.setParams({
            objectName : 'InAppMessage__c',
            pickListFieldName : 'Status__c'
        });
        
        var exeAction4 = cmp.get("c.apex_getNoRecordDisplay");
        //Set the Object parameters and Field Set name
        exeAction4.setParams({
            name : 'NoRecordDisplay_Txn'
        });

        var self = this;
        
        Promise.all([
            self.serverSideCall(cmp, exeAction1),
            self.serverSideCall(cmp, exeAction2),
            self.serverSideCall(cmp, exeAction3),
            self.serverSideCall(cmp, exeAction4)
        ]).then(
            function(response) {
                
                var result1 = response[0];             
                var result1Arr = JSON.parse(result1);
                
                var result2 = response[1]; 
                var result2Arr = JSON.parse(result2);
                
                var result3 = response[2]; 
                var result3Arr = JSON.parse(result3);
                
                var result4 = response[3]; 
               	cmp.set("v.limit", result4);
                
                var mycolumns = [];
                mycolumns.push({label: 'View', type: 'button', initialWidth: 135, typeAttributes: { label: 'View Details', name: 'view_details', title: 'Click to View Details'}});
                for(var i=0; i<result1Arr.length; i++){
                    var theResult = result1Arr[i];
                    var fieldPath = theResult.fieldPath;
                    fieldPath = fieldPath.toLowerCase();
                    if(theResult.type == 'double'){
                        mycolumns.push({ label: theResult.label, fieldName: fieldPath, type: 'number', sortable: true});
                    }
                    else if(theResult.type == 'datetime'){
                        mycolumns.push({ label: theResult.label, fieldName: fieldPath, type: 'date', sortable: true, typeAttributes:{ year: 'numeric', month: '2-digit', day: 'numeric', hour: '2-digit', minute: '2-digit', second: '2-digit', hour12: false}});
                    }
                    else if(theResult.type == 'date'){
                        mycolumns.push({ label: theResult.label, fieldName: fieldPath, type: 'date', sortable: true, typeAttributes:{ year: 'numeric', month: '2-digit', day: 'numeric'}});
                    }
                    else if(theResult.type == 'reference'){
                        mycolumns.push({label: theResult.label, fieldName: fieldPath, type: 'url', typeAttributes: { label: { fieldName: fieldPath+'_name' } }});
                    }
                    else{
                        mycolumns.push({ label: theResult.label, fieldName: fieldPath, type: theResult.type, sortable: true});
                    }
                }

                
                cmp.set("v.detailFieldSet", result2Arr);
                cmp.set("v.statusOptionList", result3Arr);
                cmp.set("v.mycolumns", mycolumns);
                
                self.loadData(cmp, event, 0);
                
            }
        ).catch(
            function(error) {
                
            }
        );
    },
    loadData : function(cmp, event, offset) {
        
        cmp.set("v.show_detail", false);
        cmp.set("v.Spinner_view_all", true);
        
        var limit = cmp.get("v.limit");
        var inputPGID = cmp.find("inputPGID");
        var inputPGIDValue = inputPGID.get("v.value") ? inputPGID.get("v.value") : null;
        var inputMessageDateFrom = cmp.find("inputMessageDateFrom");
        var inputMessageDateFromValue = inputMessageDateFrom.get("v.value") ? inputMessageDateFrom.get("v.value") : null ;
		var inputMessageDateTo = cmp.find("inputMessageDateTo");        
        var inputMessageDateToValue = inputMessageDateTo.get("v.value") ? inputMessageDateTo.get("v.value") : null ;
		var inputStatusOptionListOption = cmp.find("inputStatusOptionListOption");
        var inputStatusOptionListOptionValue = inputStatusOptionListOption.get("v.value") ? inputStatusOptionListOption.get("v.value") : null ;
        
        var parameter = {}
        var exeAction1 = cmp.get("c.apex_getAppMessageList");
        parameter["MemberSFId"] = cmp.get("v.recordId");
        parameter["MessagePGId"] = inputPGIDValue;
        parameter["Status"] = inputStatusOptionListOptionValue;
        parameter["MessageDateFrom"] = inputMessageDateFromValue;
        parameter["MessageDateTo"] = inputMessageDateToValue;
        parameter["offset"] = offset.toString();
        parameter["limit"] = limit.toString();
        exeAction1.setParams({ 
            parameter : parameter
        });

        var self = this;
        
        Promise.all([
            self.serverSideCall(cmp, exeAction1)
        ]).then(
            function(response) {
               
                var result = response[0];            
                var resultDict = JSON.parse(result);
                var ReturnCode = resultDict.ReturnCode;
                
                var mydata = [];
                if(ReturnCode == "1"){
                    var TotalCount = resultDict.TotalCount;
                    var page = Math.ceil(TotalCount/limit);
                    var options = [];
                    for(var i=0; i<page; i++){
                    	var pageDict = {'label': (i+1).toString(), 'value': (i+1).toString()}
                        options.push(pageDict);   
                    }
                    cmp.set("v.options", options);
                    var Messages = resultDict.Messages;
                    cmp.set("v.mydata", Messages);
                }
                else{
                    
                    
                }
                
                cmp.set("v.Spinner_view_all", false);
                
            }
        ).catch(
            function(error) {
                cmp.set("v.Spinner_view_all", false);
            }
        );
    }
})