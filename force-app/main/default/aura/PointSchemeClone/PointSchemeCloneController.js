({
	loadInitData : function(cmp, event, helper) {

        
    },
    save: function(cmp, event, helper){
        var action = cmp.get("c.apex_clonePointsScheme");
        action.setParams({ 
            recordId: cmp.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var cloneRecordId = response.getReturnValue();
                //alert(cloneRecordId);
                //$A.get("e.force:closeQuickAction").fire();    
                var event = $A.get("e.force:navigateToSObject");
                event.setParams({
                    "recordId": cloneRecordId
                });
                event.fire();
                             
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        //console.log("Error message: " + 
                                 //errors[0].message);
                        alert("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    //console.log("Unknown error");
                    alert("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action); 
    	
    },
 	cancel: function(cmp, event, helper){
    	$A.get("e.force:closeQuickAction").fire(); 
    }
})