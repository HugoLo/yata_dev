({
	loadInitData : function(cmp, event, helper) {
		
        var exeAction1 = cmp.get("c.apex_getMemberInfo");
        exeAction1.setParams({ 
            recordId: cmp.get("v.recordId")
        });
        
        var exeAction2 = cmp.get("c.apex_getPicklistFieldValues");
        exeAction2.setParams({ 
            objectName: 'Transaction__c',
            pickListFieldName: 'SubType__c'
        });
        
        var exeAction3 = cmp.get("c.apex_getTodayDatetime");
        
        var exeAction4 = cmp.get("c.apex_getRecordTypeId"); 
        exeAction4.setParams({ 
            SobjectTypeDevName: 'Transaction__cPointAdjustment'
        });
        
        Promise.all([
            helper.serverSideCall(cmp, exeAction1),
            helper.serverSideCall(cmp, exeAction2),
            helper.serverSideCall(cmp, exeAction3),
            helper.serverSideCall(cmp, exeAction4)
        ]).then(
            function(response) {
                
                var result = response[0];
                var resultDict = JSON.parse(result);
                cmp.set("v.memberDict", resultDict);
                
                var result1 = response[1];
                //alert(result1);
                //var result1Arr = JSON.parse(result1);
                
                var result1Arr = [{label: 'Issue', value: 'Issue'}, {label: 'Campaign', value: 'Campaign'}];
                cmp.set("v.subTypeList", result1Arr);

                var defaultDatetime = response[2];
				cmp.set("v.defaultDatetime", defaultDatetime);    
                
                var recordTypeId = response[3];
                cmp.set("v.recordTypeId", recordTypeId);  
            }
        ).catch(
            function(error) {
                cmp.set("v.Spinner", false);
            }
        );
        
	},
    cancel: function(){
        $A.get("e.force:closeQuickAction").fire(); 
    },
    save: function(cmp, event, helper){
        
        var inputSelectSubType = cmp.find("inputSelectSubType");
        var inputSelectSubTypeValue = inputSelectSubType.get("v.value");
        //console.log(inputSelectSubTypeValue);
        var inputTransfer = cmp.find("inputTransfer");
        var inputTransferValue = inputTransfer.get("v.value");
        //console.log(inputTransferValue);
        var inputPoint = cmp.find("inputPoint");
        var inputPointValue = inputPoint.get("v.value");
        inputPointValue = Math.round(inputPointValue * 100) / 100;
        //alert("inputPointValue: "+inputPointValue);
		//console.log(inputPointValue);        
        var inputExternalRemark = cmp.find("inputExternalRemark");
        var inputExternalRemarkValue = inputExternalRemark.get("v.value");
        var inputInternalRemark = cmp.find("inputInternalRemark");
        var inputInternalRemarkValue = inputInternalRemark.get("v.value");
        //console.log(inputRemarkValue);
        var memberDict = cmp.get("v.memberDict");
        var recordTypeId = cmp.get("v.recordTypeId");
        
        if(inputSelectSubTypeValue && inputTransferValue && inputPointValue){
            var userchoice = confirm('Are you sure to process the point adjustment for '+'"'+memberDict.Name+'"');
            if(userchoice){
                var action = cmp.get("c.apex_createPointAdjustment");
                var jsonFormat = {};
                var adjustList = [];
                var parameter = {};
                parameter['MemberSFId'] = cmp.get("v.recordId");
                parameter['RecordType'] = recordTypeId;
                parameter['SubType'] = inputSelectSubTypeValue;
                parameter['TransferDateTime'] = inputTransferValue;
                parameter['Points'] = inputPointValue.toString();
                parameter['ExtRemark'] = inputExternalRemarkValue; 
                parameter['IntRemark'] = inputInternalRemarkValue; 
                adjustList.push(parameter);
                jsonFormat['adjustList'] = adjustList;
                action.setParams({ 
                    parameter: jsonFormat
                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        
                        var resultDict = JSON.parse(response.getReturnValue());
                        if(resultDict.ReturnCode == '1'){
                            
                            alert('Point adjusted sucessfully. The point balance will be updated in the while. Please refresh later.');
                            $A.get("e.force:closeQuickAction").fire(); 
                            
                        }
                        else{
                            alert(resultDict.ReturnMessage);
                        }
                                        
                    }
                    else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                //console.log("Error message: " + 
                                         //errors[0].message);
                                alert("Error message: " + 
                                         errors[0].message);
                            }
                        } else {
                            //console.log("Unknown error");
                            alert("Unknown error");
                        }
                    }
                });
                
                $A.enqueueAction(action); 
                
            }
        }
        else{
            alert('Please enter valid value.');
            /*
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Reminder",
                "message": "Please enter valid value."
            });
            toastEvent.fire();
            */
        }
    },
    handlechange: function(cmp, event, helper){
    	var inputPoint = cmp.find("inputPoint");
        var inputPointValue = inputPoint.get("v.value");
        inputPointValue = Math.round(inputPointValue * 100) / 100;   
        inputPoint.set("v.value", inputPointValue);
    }
})