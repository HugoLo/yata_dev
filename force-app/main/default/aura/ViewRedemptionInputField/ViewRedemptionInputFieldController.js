({
	loadInitData : function(component, event, helper) {
        var fieldName = component.get("v.fieldName");
        var inputFieldMap = component.get("v.inputFieldMap");
        var fieldvalue = inputFieldMap[fieldName];
        //component.set("v.fieldvalue", fieldvalue);
        
        var inputfieldValue = component.find("inputfieldValue");
        if(fieldvalue){
        	inputfieldValue.set("v.value", fieldvalue);    
        }
        
	}
})