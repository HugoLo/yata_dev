@isTest
public class RedemptionController_Test {
	
    static testMethod void RedemptionController_Test(){
        
        TestClassHelper.createIntegrationControl();
        Account account = TestClassHelper.createMember();
        Map<String, String> parameter = new Map<String, String>();
        parameter.put('MemberSFId', account.Id);
        parameter.put('Status', 'Expired');
        parameter.put('RedeemDateFrom', '2018-09-04 00:00:00');
        parameter.put('RedeemDateTo', '2018-10-04 00:00:00');
        parameter.put('CouponValidDate', null);
        parameter.put('CouponCode', null);
        parameter.put('limit', '10');
        
        User userA = TestClassHelper.createUser();
        System.runAs(userA){
            
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new HTTPMockCallout());
            String responseString = RedemptionController.apex_getRedemptionList(parameter);
            String fieldset = RedemptionController.apex_getFieldset('Redemption__c', 'RedemptionFieldSetPreview');
            Test.stopTest();
            
        }
		        
    }
 
    public class HTTPMockCallout implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(200);
            string jsonResBody = '{"ReturnCode":"1","ReturnMessage":"Success","Redemptions":[{"effectiveto__c":"2018-09-12T04:00:00.000Z","transaction__c":null,"recordtypeid":null,"lastvieweddate":null,"member__c":"001p000000URpupAAD","name":"E10000080","membershipno__c":"M1000055","lastmodifieddate":null,"usedate__c":null,"ownerid":null,"redeempoint__c":-1000,"facevalue__c":50,"isdeleted":null,"systemmodstamp":null,"lastmodifiedbyid":null,"status__c":"Expired","systemrunningno__c":10000080,"effectivefrom__c":"2018-09-10T04:00:00.000Z","lastactivitydate":null,"transactionpgid__c":null,"reward__c":"a0bp00000034G7MAAU","createddate":null,"code__c":"E100000803M1000055","memberpgid__c":93,"redeemdate__c":"2018-09-04T10:38:26.878Z","createdbyid":null,"lastreferenceddate":null,"sfid":null,"pgid__c":74,"source__c":null,"rulename__c":null,"ruleid__c":null,"issuefromtxpgid__c":null,"member__c_name":"*Gordon Hoo","reward__c_name":"士多啤梨","transactionpgid__c_name":null}]}';
            res.setBody(jsonResBody);
            return res;
        }
    }
	
	
    
}