global class LogShipment_Batch implements Database.Batchable<sObject>, Database.Stateful{
    private String query;
    private Boolean isDuplicated;
	
	global LogShipment_Batch() {
        isDuplicated = false;
        List<CronTrigger> l_ct = new List<CronTrigger>();
        l_ct = [SELECT Id,CronJobDetailId,CronJobDetail.Name,NextFireTime,PreviousFireTime,State,StartTime,EndTime,CronExpression,TimeZoneSidKey,OwnerId,TimesTriggered 
                FROM CronTrigger
                WHERE CronJobDetail.Name like '%LogShipment_Sched%' AND NextFireTime= null];
        if(l_ct!=null && l_ct.size()>0){
            System.abortJob(l_ct[0].id);
        }
        
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator([SELECT 
                                         Id, ActionType__c, Detail__c, RetryTimes__c,
                                         Object__c, PGID__c, Source__c, Exception__c,
            							 Status__c, CreatedDate, DateLog__c
         								FROM LogShipment__c 
         								WHERE ((Status__c ='Open') OR (Status__c ='Failed' AND (RetryTimes__c <= 3 OR RetryTimes__c = NULL))) 
                                        AND Source__c = 'H'
         								ORDER BY DateLog__c, PGID__c]);
        
	}

   	global void execute(Database.BatchableContext BC, List<LogShipment__c> scope) {
        
        
        List<LogShipment__c> logshipmentInsertRecord = new List<LogShipment__c>();
        List<LogShipment__c> logshipmentUpdateRecord = new List<LogShipment__c>();
        List<sObject> l_InsertRecord = new List<sObject>();
        List<sObject> l_UpdateRecord = new List<sObject>();
        Set<Decimal> subscription_pgid_set = new Set<Decimal>();
        Set<Decimal> bbInformation_pgid_set = new Set<Decimal>();
        Set<Decimal> account_pgid_set = new Set<Decimal>();
        
        Map<Decimal, Map<String, Object>> updateFieldMap = new Map<Decimal, Map<String, Object>>();
        
        
       	//////////////////   logShipment id Set   //////////////////
        Set<Id> s_logShipment = new Set<Id>();
        for(LogShipment__c logShipment : scope){
        	s_logShipment.add(logShipment.Id);
        }
        system.debug('@@@s_logShipment: '+s_logShipment);
        //////////////////   logShipment id Set   //////////////////
        
        
        //////////////////   logShipment for update   //////////////////
		List<LogShipment__c> l_lock_logshipment = [SELECT 
                                                 Id, ActionType__c, Detail__c, RetryTimes__c, 
                                                 Object__c, PGID__c, Source__c, Exception__c,
                                                 Status__c, CreatedDate, DateLog__c
                                                FROM LogShipment__c 
                                                WHERE ((Status__c ='Open') OR (Status__c ='Failed' AND (RetryTimes__c <= 3 OR RetryTimes__c = NULL))) 
                                                AND Source__c = 'H'
                                                AND Id IN :s_logShipment
                                                FOR UPDATE];   
        //////////////////   logShipment for update   //////////////////
        
        
        //////////////////   update logShipment's status to In Progress   //////////////////
        map<id, LogShipment__c> m_logShipment = new map<id, LogShipment__c>();
        List<LogShipment__c> l_logShipment_InProgress = new List<LogShipment__c>();
        for(LogShipment__c logShipment : l_lock_logshipment){
            logShipment.Status__c = 'In Progress';
            l_logShipment_InProgress.add(logShipment);
            //m_logShipment.put(logShipment.Id, logShipment);
        }
        if(l_logShipment_InProgress.size() > 0){
        	//update l_logShipment_InProgress;    
            Database.SaveResult[] l_InProgress = Database.update(l_logShipment_InProgress, false);
            for(Integer i=0; i<l_logShipment_InProgress.size(); i++){
                Database.SaveResult sr = l_InProgress[i];
                LogShipment__c logshipment = l_logShipment_InProgress[i];
                if (!sr.isSuccess()) {
                    
                }
                else{
                    m_logShipment.put(logShipment.Id, logShipment);
                }
            }
        }
        system.debug('@@@m_logShipment: '+m_logShipment.keySet());
        //////////////////   update logShipment's status to In Progress   //////////////////
        
        
        List<LogShipment__c> l_logShipment_duplicated = new List<LogShipment__c>();
        Set<Decimal> s_haveSameIdUpdate = new Set<Decimal>();
        for(LogShipment__c logShipment : scope){
            
            LogShipment__c lock_logshipment = m_logShipment.containsKey(logShipment.Id) ? m_logShipment.get(logShipment.Id) : null;
            if(lock_logshipment != null){
                
                String detail = lock_logshipment.Detail__c;
                String source = lock_logshipment.Source__c;
                String status = lock_logshipment.Status__c;
                String objName = lock_logshipment.Object__c;
                String actionType = lock_logshipment.ActionType__c;
                
                //////////////////   filter insert type   //////////////////
                if(actionType == 'Insert'){
                    Map<String, Object> meta = (Map<String, Object>) JSON.deserializeUntyped(detail);
                    meta.remove('id');
                    if(objName == 'BBInformation__c'){
                        BBInformation__c obj = (BBInformation__c) JSON.deserialize( JSON.serialize( meta ), BBInformation__c.class );
                        system.debug('@@@BBInformation__c Insert obj: '+obj);
                        obj.LogChecking__c = obj.LogChecking__c ? false : true;
                        obj.OwnerId = UserInfo.getUserId();
                        l_InsertRecord.add(obj);
                    }
                    else if(objName == 'Subscription__c'){
                        Subscription__c obj = (Subscription__c) JSON.deserialize( JSON.serialize( meta ), Subscription__c.class );
                        system.debug('@@@Subscription__c Insert obj: '+obj);
                        obj.LogChecking__c = obj.LogChecking__c ? false : true;
                        obj.OwnerId = UserInfo.getUserId();
                        l_InsertRecord.add(obj);
                    }
                    /*
                    else if(objName == 'Account'){
                        Account obj = (Account) JSON.deserialize( JSON.serialize( meta ), Account.class );
                        system.debug('@@@Account Insert obj: '+obj);
                        obj.LogChecking__c = obj.LogChecking__c ? false : true;
                        obj.OwnerId = UserInfo.getUserId();
                        l_InsertRecord.add(obj);
                    }
					*/
                    logshipmentInsertRecord.add(lock_logshipment);
                }
                //////////////////   filter insert type   //////////////////
                
                //////////////////   filter update type   //////////////////
                else if(actionType == 'Update'){
                    Map<String, Object> meta = (Map<String, Object>) JSON.deserializeUntyped(detail);
                    Object objectPgid = meta.get('PGID__c');
                    Decimal pgid;
                    if(objectPgid instanceof Decimal){
                        pgid = (Decimal)meta.get('PGID__c');
                    }
                    else if(objectPgid instanceof String){
                        pgid = Decimal.valueOf((String)meta.get('PGID__c'));
                    }
                    
                    Boolean isContain = s_haveSameIdUpdate.contains(pgid);
                    if(isContain){
                        //////////////////   filter duplicated recordId   //////////////////
                        isDuplicated = true;
                        l_logShipment_duplicated.add(lock_logshipment);
                        //////////////////   filter duplicated recordId   //////////////////
                    }
                    else{
                        s_haveSameIdUpdate.add(pgid);
                        if(objName == 'BBInformation__c'){
                            //String pgidString = String.valueOf(pgid);
                            bbInformation_pgid_set.add(pgid);
                        }
                        else if(objName == 'Subscription__c'){
                            //String pgidString = String.valueOf(pgid);
                            subscription_pgid_set.add(pgid);
                        }
                        /*
                        else if(objName == 'Account'){
                            //String pgidString = String.valueOf(pgid);
                            account_pgid_set.add(pgid);
                        }
						*/
                        logshipmentUpdateRecord.add(lock_logshipment);
                    }
                }
                //////////////////   filter update type   //////////////////
            }
        }
        
        //////////////////   pgid mapping   //////////////////
        List<Subscription__c> subscriptionToUpdateList = new List<Subscription__c>();
        if(subscription_pgid_set.size() > 0){
            subscriptionToUpdateList = [SELECT PGID__c, Id FROM Subscription__c WHERE PGID__c IN :subscription_pgid_set];    
        }
        Map<Decimal, Id> subscriptionToUpdateMap = new Map<Decimal, Id>();
        for(Subscription__c subscription : subscriptionToUpdateList){
            subscriptionToUpdateMap.put(subscription.PGID__c, subscription.Id);
        }
        system.debug('@@@subscriptionToUpdateMap: '+subscriptionToUpdateMap);
        
        List<BBInformation__c> bbinfoToUpdateList = new List<BBInformation__c>();
        if(bbInformation_pgid_set.size() > 0){
            bbinfoToUpdateList = [SELECT PGID__c, Id FROM BBInformation__c WHERE PGID__c IN :bbInformation_pgid_set];
        }
        Map<Decimal, Id> bbinfoToUpdateMap = new Map<Decimal, Id>();
        for(BBInformation__c bbinfo : bbinfoToUpdateList){
            bbinfoToUpdateMap.put(bbinfo.PGID__c, bbinfo.Id);
        }
        system.debug('@@@bbinfoToUpdateMap: '+bbinfoToUpdateMap);
        
        /*
        List<Account> accountToUpdateList = new List<Account>();
        if(account_pgid_set.size() > 0){
            accountToUpdateList = [SELECT PGID__c, Id FROM Account WHERE PGID__c IN :account_pgid_set];
        }
        Map<Decimal, Id> accountToUpdateMap = new Map<Decimal, Id>();
        for(Account account : accountToUpdateList){
            accountToUpdateMap.put(account.PGID__c, account.Id);
        }
        system.debug('@@@accountToUpdateMap: '+accountToUpdateMap);
		*/
        //////////////////   pgid mapping   //////////////////  
        
        
        //////////////////   update record by logShipment   //////////////////  
        for(LogShipment__c logShipment : logshipmentUpdateRecord){
            String detail = logShipment.Detail__c;
            String source = logShipment.Source__c;
            String status = logShipment.Status__c;
            String objName = logShipment.Object__c;
            String actionType = logShipment.ActionType__c;
            
            Map<String, Object> meta = (Map<String, Object>) JSON.deserializeUntyped(detail);
            String idString = String.valueOf(meta.get('Id'));
            if(objName == 'Subscription__c'){
                if(String.isBlank(idString)){
                    Object objectPgid = meta.get('PGID__c');
                    Decimal pgid;
                    if(objectPgid instanceof Decimal){
                        pgid = (Decimal)meta.get('PGID__c');
                    }
                    else if(objectPgid instanceof String){
                        pgid = Decimal.valueOf((String)meta.get('PGID__c'));
                    }
                    Id objectId = subscriptionToUpdateMap.get(pgid);
                    meta.put('Id', objectId);
                }
                Subscription__c obj = (Subscription__c) JSON.deserialize( JSON.serialize( meta ), Subscription__c.class );
                obj.LogChecking__c = obj.LogChecking__c ? false : true;
                system.debug('@@@Subscription Update obj: '+obj);
                l_UpdateRecord.add(obj);
            }
            else if(objName == 'BBInformation__c'){
            	if(String.isBlank(idString)){
                    Object objectPgid = meta.get('PGID__c');
                    Decimal pgid;
                    if(objectPgid instanceof Decimal){
                        pgid = (Decimal)meta.get('PGID__c');
                    }
                    else if(objectPgid instanceof String){
                        pgid = Decimal.valueOf((String)meta.get('PGID__c'));
                    }
                    Id objectId = bbinfoToUpdateMap.get(pgid);
                    meta.put('Id', objectId);
                }
                BBInformation__c obj = (BBInformation__c) JSON.deserialize( JSON.serialize( meta ), BBInformation__c.class );
                obj.LogChecking__c = obj.LogChecking__c ? false : true;
                system.debug('@@@bb info Update obj: '+obj);
                l_UpdateRecord.add(obj);
            }
            /*
            else if(objName == 'Account'){
            	if(String.isBlank(idString)){
                    Object objectPgid = meta.get('PGID__c');
                    Decimal pgid;
                    if(objectPgid instanceof Decimal){
                        pgid = (Decimal)meta.get('PGID__c');
                    }
                    else if(objectPgid instanceof String){
                        pgid = Decimal.valueOf((String)meta.get('PGID__c'));
                    }
                    Id objectId = accountToUpdateMap.get(pgid);
                    meta.put('Id', objectId);
                }
                Account obj = (Account) JSON.deserialize( JSON.serialize( meta ), Account.class );
                obj.LogChecking__c = obj.LogChecking__c ? false : true;
                system.debug('@@@Account info Update obj: '+obj);
                l_UpdateRecord.add(obj);
            }
			*/
        }
        Database.SaveResult[] insetList = Database.Insert(l_InsertRecord, false);
        for(Integer i=0; i<logshipmentInsertRecord.size(); i++){
            Database.SaveResult sr = insetList[i];
            LogShipment__c logshipment = logshipmentInsertRecord[i];
            if (!sr.isSuccess()) {
                // Operation failed, so get all errors
                String Error_log = '';
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Account fields that affected this error: ' + err.getFields());
                    String errorStatusCode = 'errorStatusCode: '+err.getStatusCode()+'\n';
                    String errorMessage = 'errorMessage: '+err.getMessage()+'\n';
                    String errorFields = 'errorFields: '+err.getFields()+'\n';
                    Error_log += errorStatusCode;
                    Error_log += errorMessage;
                    Error_log += errorFields;
                }
                logshipment.Status__c = 'Failed';
               	Decimal RetryTimes = logshipment.RetryTimes__c != null ? logshipment.RetryTimes__c : 0;
                RetryTimes = RetryTimes + 1;
                logshipment.RetryTimes__c = RetryTimes;
                logShipment.Exception__c = Error_log;
            }
            else{
                logshipment.Status__c = 'Complete';
            }
        }
        //////////////////   update record by logShipment   ////////////////// 
        
        
        //////////////////   update logShipment's status to Complete or Failed   ////////////////// 
        if(logshipmentInsertRecord.size() > 0){
        	update logshipmentInsertRecord;    
        }
        //////////////////   update logShipment's status to Complete or Failed   ////////////////// 
        
        
        //////////////////   insert record by logShipment   //////////////////  
        Database.SaveResult[] updateList = Database.update(l_UpdateRecord, false);
        for(Integer i=0; i<logshipmentUpdateRecord.size(); i++){
            Database.SaveResult sr = updateList[i];
            LogShipment__c logshipment = logshipmentUpdateRecord[i];
            if (!sr.isSuccess()) {
                // Operation failed, so get all errors
                String Error_log = '';
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Account fields that affected this error: ' + err.getFields());
                    String errorStatusCode = 'errorStatusCode: '+err.getStatusCode()+'\n';
                    String errorMessage = 'errorMessage: '+err.getMessage()+'\n';
                    String errorFields = 'errorFields: '+err.getFields()+'\n';
                    Error_log += errorStatusCode;
                    Error_log += errorMessage;
                    Error_log += errorFields;
                }
                logshipment.Status__c = 'Failed';
                Decimal RetryTimes = logshipment.RetryTimes__c != null ? logshipment.RetryTimes__c : 0;
                RetryTimes = RetryTimes + 1;
                logshipment.RetryTimes__c = RetryTimes;
                logShipment.Exception__c = Error_log;
            }
            else{
                logshipment.Status__c = 'Complete';
            }
        }
        //////////////////   insert record by logShipment   ////////////////// 
       	 
        
        //////////////////   update logShipment's status to Complete or Failed   ////////////////// 
        if(logshipmentUpdateRecord.size() > 0){
        	update logshipmentUpdateRecord;    
        }
        //////////////////   update logShipment's status to Complete or Failed   ////////////////// 
        
        
        //////////////////   update logShipment's status to Open   //////////////////
        if(l_logShipment_duplicated.size() > 0){
            for(LogShipment__c logshipment : l_logShipment_duplicated){
                logshipment.Status__c = 'Open';
            }
            update l_logShipment_duplicated;
        }
        //////////////////   update logShipment's status to Open   //////////////////
	}
	
	global void finish(Database.BatchableContext BC) {
        //////////////////   re-run batch to handle Duplicated recordId   //////////////////
        if(isDuplicated){
            LogShipment_Sched sch = new LogShipment_Sched();
            sch.executeLogShipmentBatch();
        }
        //////////////////   re-run batch to handle Duplicated recordId   //////////////////
	}
}