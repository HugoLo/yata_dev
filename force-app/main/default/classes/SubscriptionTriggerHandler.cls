public class SubscriptionTriggerHandler extends TriggerHandler{
    //private static final String ObjName = 'Subscription__c';
    private static final String ObjName = Trigger.isDelete ? Trigger.Old.getSObjectType().getDescribe().getName() : Trigger.New.getSObjectType().getDescribe().getName();
        
    public SubscriptionTriggerHandler(){}

    protected override void beforeInsert() {
        system.debug('@@@beforeInsert');
        
        List<Subscription__c> l_Subscription = (List<Subscription__c>)Trigger.new;
        system.debug('@@@l_Subscription '+l_Subscription);
        Set<Double> s_MemberPGID = new Set<Double>();
        for(Subscription__c Subscription : l_Subscription){
            Double MemberPGID = Subscription.MemberPGID__c;
            Id MemberId = Subscription.Member__c;
            
            if(MemberPGID != null && MemberId == null){
                s_MemberPGID.add(MemberPGID);
            }
            
        }
        
        system.debug('@@@s_MemberPGID '+s_MemberPGID);
        
        Map<Double, Id> m_MemberId = new Map<Double, Id>();
        if(s_MemberPGID.size() > 0){
            
            List<Account> l_account = [SELECT Id, PGID__c 
                                       FROM Account 
                                       WHERE PGID__c IN :s_MemberPGID];
            system.debug('@@@l_account '+l_account);
            
            
            for(Account account : l_account){
                Double PGID = account.PGID__c;
                Id memberId = account.Id;
                if(PGID != null){
                    m_MemberId.put(PGID, memberId);    
                }
            }
            
            system.debug('@@@m_MemberId '+m_MemberId);
            
        }
        
        for(Subscription__c Subscription : l_Subscription){
            Double MemberPGID = Subscription.MemberPGID__c;
            if(Subscription.MobileNo__c != null){
                String MobileNo = Subscription.MobileNo__c;
                String removeSpaceMobileNo = MobileNo.remove(' ');
                system.debug('@@@removeSpaceMobileNo: '+removeSpaceMobileNo);
                Subscription.MobileNo__c = removeSpaceMobileNo;    
            }
            Boolean contains = m_MemberId.containsKey(MemberPGID);
            if(contains){
                Id MemberId = m_MemberId.get(MemberPGID);
                Subscription.Member__c = MemberId;
            }
        }
	        
    }
    
    
    protected override void beforeUpdate () {
        system.debug('@@@beforeUpdate');
        
        List<Subscription__c> l_Subscription = (List<Subscription__c>)Trigger.new;
        system.debug('@@@l_Subscription '+l_Subscription);
        Set<Double> s_MemberPGID = new Set<Double>();
        for(Subscription__c Subscription : l_Subscription){
            Double MemberPGID = Subscription.MemberPGID__c;
            Id MemberId = Subscription.Member__c;
            
            if(MemberPGID != null && MemberId == null){
                s_MemberPGID.add(MemberPGID);
            }
            
        }
        
        system.debug('@@@s_MemberPGID '+s_MemberPGID);
        
        Map<Double, Id> m_MemberId = new Map<Double, Id>();
        if(s_MemberPGID.size() > 0){
            
            List<Account> l_account = [SELECT Id, PGID__c 
                                       FROM Account 
                                       WHERE PGID__c IN :s_MemberPGID];
            system.debug('@@@l_account '+l_account);
            
            
            for(Account account : l_account){
                Double PGID = account.PGID__c;
                Id memberId = account.Id;
                if(PGID != null){
                    m_MemberId.put(PGID, memberId);    
                }
            }
            
            system.debug('@@@m_MemberId '+m_MemberId);
        }
        
        for(Subscription__c Subscription : l_Subscription){
            Double MemberPGID = Subscription.MemberPGID__c;
            if(Subscription.MobileNo__c != null){
                String MobileNo = Subscription.MobileNo__c;
                String removeSpaceMobileNo = MobileNo.remove(' ');
                system.debug('@@@removeSpaceMobileNo: '+removeSpaceMobileNo);
                Subscription.MobileNo__c = removeSpaceMobileNo;    
            }
            Boolean contains = m_MemberId.containsKey(MemberPGID);
            if(contains){
                Id MemberId = m_MemberId.get(MemberPGID);
                Subscription.Member__c = MemberId;
            }
        }
        
    }
    
    /*
    protected override void beforeDelete () {
        system.debug('@@@beforeDelete');
    }
    */    
    protected override void afterInsert () {
        system.debug('@@@Subscription afterInsert');
        if(Common_Security_Cls.toRunTriggerMethod('SubscriptionTriggerHandler','afterInsert')){
            
            //Create Log shipment records:
            if(Common_Utilities_Cls.needLogShipment(ObjName)){
                LogShipmentHandler.createLogShipment(ObjName, Trigger.new);   
            }
        }
    }
    
    protected override void afterDelete () {
        if(Common_Security_Cls.toRunTriggerMethod('SubscriptionTriggerHandler','afterDelete')){
            //Create Log shipment records:
            if(Common_Utilities_Cls.needLogShipment(ObjName)){
                LogShipmentHandler.createDeleteLogShipment(ObjName, Trigger.old);
            }   
        }
    }
    
        protected override void afterUpdate () {
        system.debug('@@@Subscription afterUpdate');
        if(Common_Security_Cls.toRunTriggerMethod('SubscriptionTriggerHandler','afterUpdate')){
            
            //Create Log shipment records:
            if(Common_Utilities_Cls.needLogShipment(ObjName)){
                LogShipmentHandler.updateLogShipment(ObjName, Trigger.new, Trigger.oldMap);
            }
        }
    }
    /*
    protected override void afterUndelete  () {
        system.debug('@@@afterUndelete');        
    }
    */
}