global class BatchPointAdjustment_Sched implements Schedulable {
    
	global void execute(SchedulableContext sc) {
        
        Integer batchSize = 200;
        IntegrationControl__c batchSize_CS = IntegrationControl__c.getInstance('PointAdjustmentBatchSize');
        if(batchSize_CS!=null && batchSize_CS.num__c!=null){
            batchSize= Integer.valueOf(batchSize_CS.num__c);
        }
		PointAdjustment_Batch adjPT_batch = new PointAdjustment_Batch();
		database.executebatch(adjPT_batch, batchSize);
		
	}
}