public with sharing class DistributeCoupon_Ctrl {
    
	@AuraEnabled
    public static String apex_distributeCoupon(String recordId) {
        Campaign campaign = [SELECT Id, Status, IncentiveRuleNo__c FROM Campaign WHERE Id = :recordId];
        String jsonString;
        if(campaign.Status != 'Active'){
            Map<String, Object> jsonDict = new Map<String, Object>();
            jsonDict.put('success', false);
            jsonDict.put('message', 'Please avtive incentive rule');
            jsonString = JSON.serialize(jsonDict);
        }
        else{
            Common_HerokuIntegration_Cls.distributeCouponRespond jsonDict = Common_HerokuIntegration_Cls.distributeCoupon(String.valueOf(campaign.IncentiveRuleNo__c));
			jsonString = JSON.serialize(jsonDict);
        }
     	return jsonString;   
    }
}