@isTest
public class ProductCatHierarchy_Ctrl_Test {
	
    
    static testMethod void ProductCatHierarchy_Ctrl_Test(){
        
        
        User userA = TestClassHelper.createUser();
        System.runAs(userA){
            
            //Account account = TestClassHelper.createMember();
            ProductCategory__c productCategoryParent = TestClassHelper.createProductCategory(null);
            ProductCategory__c productCategoryChild1 = TestClassHelper.createProductCategory(productCategoryParent);
            ProductCategory__c productCategoryChild2 = TestClassHelper.createProductCategory(productCategoryChild1);
 
            ProductCatHierarchy_Ctrl.apex_getCatHierarchy(productCategoryChild2.Id);
			update productCategoryChild2;
            
        }
        
    }

    
}