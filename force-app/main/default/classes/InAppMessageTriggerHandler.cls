public class InAppMessageTriggerHandler extends TriggerHandler{
	private static Boolean needToCallOut = false;
    
     
    protected override void beforeInsert () {
        if(Common_Security_Cls.toRunTriggerMethod('InAppMessageTriggerHandler','beforeInsert')){
			//Validate Data and Get Member SF Id by Member No.
			validateData();
        }
    }
    
    protected override void beforeUpdate () {
        if(Common_Security_Cls.toRunTriggerMethod('InAppMessageTriggerHandler','beforeUpdate')){
			//Validate Data and Get Member SF Id by Member No.
			validateData();
        }
    }
    
    protected override void afterInsert () {
        if(Common_Security_Cls.toRunTriggerMethod('InAppMessageTriggerHandler','afterInsert')){
            //Call out API to make adjustment:
            APICallOutUpdate();
        }
    }
    
    protected override void afterUpdate () {
        if(Common_Security_Cls.toRunTriggerMethod('InAppMessageTriggerHandler','afterUpdate')){
            //Call out API to make adjustment:
            APICallOutUpdate();
        }
    }
    
    private void validateData(){
        
    }
    
    private void APICallOutUpdate(){
        system.debug('@@@APICallOutUpdate'); 
        List<InAppMessage__c> l_InAppMessage = (List<InAppMessage__c>)Trigger.new;
        Boolean needCallBatch = false;
        for(InAppMessage__c inAppMessage : l_InAppMessage){
            if(inAppMessage.SyncStatus__c == 'Open'){
                needCallBatch = true;
                break;
            }
        }
        if(needCallBatch){
        	InAppMessage_Sched sch = new InAppMessage_Sched();
            sch.executeBatch();    
        }
    }
}