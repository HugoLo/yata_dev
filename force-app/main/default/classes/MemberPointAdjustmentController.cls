public with sharing class MemberPointAdjustmentController {
    
	@AuraEnabled
    public static String apex_getPicklistFieldValues(String objectName, String pickListFieldName){
        
        Schema.DescribeFieldResult fieldResult = Transaction__c.SubType__c.getDescribe();
        
        return CommonController.apex_getPicklistFieldValues(objectName, pickListFieldName);
    }
    
    @AuraEnabled
    public static String apex_getMemberInfo(String recordId){
        Account account = [SELECT Id, Name, MobileKey__c, PersonEmail, MembershipNo__pc FROM Account WHERE Id = :recordId];
        return JSON.serialize(account);
    }
    
    @AuraEnabled
    public static String apex_createPointAdjustment(Map<String, Object> parameter){
        system.debug('@@@parameter: '+JSON.serialize(parameter));
        Common_HerokuIntegration_Cls.adjustmentResponse adjResponse = Common_HerokuIntegration_Cls.batchAdjustPoint(JSON.serialize(parameter));
        system.debug('@@@parameter123: '+JSON.serialize(adjResponse));
        return JSON.serialize(adjResponse);
    }
    
    @AuraEnabled
    public static Datetime apex_getTodayDatetime(){
        Datetime todayDatetime = system.now();
        return todayDatetime;
    }
    
    @AuraEnabled
    public static Id apex_getRecordTypeId(String SobjectTypeDevName){
        return Common_RecordType_Cls.getRtId(SobjectTypeDevName);
    }
}