@isTest
public class SubscriptionTriggerHandler_Test {
    
    static testMethod void SubscriptionTriggerHandler_Test(){
        User userA = TestClassHelper.createUser();
        System.runAs(userA){
            
            TestClassHelper.createObjectControl('Account');
            TestClassHelper.createObjectControl('Subscription__c');
            
            Account account = TestClassHelper.createMember();
            
            Subscription__c subscription = TestClassHelper.createSubscription(account);
            subscription.Member__c = null;
            update subscription;
            
            LogShipment__c logShipment = [SELECT Id, RetryTimes__c, Source__c, Status__c, ActionType__c, Detail__c
                                             FROM LogShipment__c 
                                             WHERE Object__c = 'Subscription__c' 
                                             LIMIT 1];
           
            
            String jsonString = logShipment.Detail__c;
            Map<String, Object> meta = (Map<String, Object>) JSON.deserializeUntyped(jsonString);
            meta.remove('Id');
            jsonString = JSON.serialize( meta );
            TestClassHelper.createLogShipment('Insert', 'Subscription__c', jsonString);
            meta.put('Id', subscription.Id);
            meta.put('PGID__c', 123456);
            jsonString = JSON.serialize( meta );
            TestClassHelper.createLogShipment('Update', 'Subscription__c', jsonString);
            
            LogShipment__c logShipmentl = TestClassHelper.createDuplicatedLogShipment('Update', 'Subscription__c', jsonString);
            LogShipment__c logShipment2 = TestClassHelper.createDuplicatedLogShipment('Update', 'Subscription__c', jsonString);
            List<LogShipment__c> l_LogShipment = new List<LogShipment__c>();
            l_LogShipment.add(logShipmentl);
            l_LogShipment.add(logShipment2);
            insert l_LogShipment;
            
            
        	update subscription;
            delete subscription;
            
        }
    }

}