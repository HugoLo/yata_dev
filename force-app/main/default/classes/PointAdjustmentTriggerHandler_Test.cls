@isTest
public class PointAdjustmentTriggerHandler_Test {
	
    static testMethod void PointAdjustmentTriggerHandler_Test() {
     	TestClassHelper.createIntegrationControl();
        
        User userA = TestClassHelper.createUser();
        System.runAs(userA){
           	Test.StartTest();
			
            Account account = TestClassHelper.createMember();
			
            Test.setMock(HttpCalloutMock.class, new HTTPMockCallout());
            PointAdjustment__c pointAdjustment = new PointAdjustment__c();
            pointAdjustment.PointAdjustment__c = -50;
            pointAdjustment.TransactionDate__c = date.today();
            pointAdjustment.CountryCode__c = account.CountryCode__pc;
            pointAdjustment.MobileNo__c = account.PersonMobilePhone;
            pointAdjustment.Status__c = 'Pending';
            pointAdjustment.SubType__c = 'Issue';
            insert pointAdjustment;
            
        }
    }
    
    static testMethod void PointAdjustmentTriggerHandler2_Test() {
     	TestClassHelper.createIntegrationControl();
        
        User userA = TestClassHelper.createUser();
        System.runAs(userA){
           	Test.StartTest();
			
            Account account = TestClassHelper.createMember();
			
            PointAdjustment__c pointAdjustment = new PointAdjustment__c();
            pointAdjustment.PointAdjustment__c = -50;
            pointAdjustment.TransactionDate__c = date.today();
            pointAdjustment.CountryCode__c = account.CountryCode__pc;
            pointAdjustment.MobileNo__c = account.PersonMobilePhone;
            pointAdjustment.MembershipNo__c = '123456';
            pointAdjustment.Status__c = 'Pending';
            pointAdjustment.SubType__c = 'Issue';
            insert pointAdjustment;
            Test.setMock(HttpCalloutMock.class, new HTTPMockCallout2(pointAdjustment.Id));
            
            Test.stopTest();
        }
    }
    
    static testMethod void PointAdjustmentTriggerHandler3_Test() {
     	TestClassHelper.createIntegrationControl();
        
        User userA = TestClassHelper.createUser();
        System.runAs(userA){
           	Test.StartTest();
			
            Account account = TestClassHelper.createMember();
			
            PointAdjustment__c pointAdjustment = new PointAdjustment__c();
            pointAdjustment.PointAdjustment__c = -50;
            pointAdjustment.TransactionDate__c = date.today();
            pointAdjustment.MembershipNo__c = '123456'; 
            pointAdjustment.Status__c = 'Pending';
            pointAdjustment.SubType__c = 'Issue';
            insert pointAdjustment;
            Test.setMock(HttpCalloutMock.class, new HTTPMockCallout3(pointAdjustment.Id));
            
        }
    }
    
    public class HTTPMockCallout implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(200);
            string jsonResBody = '{"ReturnCode":"1","ReturnMessage":"All point adjustments complete","ReturnDetails":[{"RecordId":"035ab6be-60aa-496e-9e59-4de0e41f5951","ReturnCode":"1","ReturnMessage":"success","txnPGID":null}]}';
            res.setBody(jsonResBody);
            return res;
        }
    }
    
    public class HTTPMockCallout2 implements HttpCalloutMock {
        public String recordId {set; get;}
        public HTTPMockCallout2(String recordId){
            this.recordId = recordId;
        }
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(200);
            string jsonResBody = '{"ReturnCode":"2","ReturnMessage":"All point adjustments complete","ReturnDetails":[{"RecordId":"'+recordId+'","ReturnCode":"1","ReturnMessage":"success","txnPGID":null}]}';
            res.setBody(jsonResBody);
            return res;
        }
    }
    
    public class HTTPMockCallout3 implements HttpCalloutMock {
        public String recordId {set; get;}
        public HTTPMockCallout3(String recordId){
            this.recordId = recordId;
        }
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(200);
            string jsonResBody = '{"ReturnCode":"2","ReturnMessage":"All point adjustments complete","ReturnDetails":[{"RecordId":"'+recordId+'","ReturnCode":"2","ReturnMessage":"success","txnPGID":null}]}';
            res.setBody(jsonResBody);
            return res;
        }
    }
    
}