public class UpdateCampaignMemberList implements Database.Batchable<sObject>, Database.Stateful {
    
    static public void run(id campaignId) {
        Database.executeBatch(new UpdateCampaignMemberList(campaignId), 5000);
    }

    
    public id campaignId;
    private integer partNo=0;
    private Campaign_Member_List__c lastInsert = null;
    public UpdateCampaignMemberList(id campaignId) {
        this.campaignId = campaignId;
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC){
        delete [SELECT id FROM Campaign_Member_List__c WHERE Campaign__c=:campaignId];
        return Database.getQueryLocator([SELECT Contact.AccountId FROM CampaignMember WHERE CampaignId=:campaignId]);
    }
    
    public void execute(Database.BatchableContext BC, list<sObject> scope){
        string[] ids = new string[]{};
        for(sObject o : scope) {
            CampaignMember cm = (CampaignMember)o;
            if(cm.Contact!=null && cm.Contact.AccountId!=null)
                ids.add(cm.Contact.AccountId);
        }
        if(ids.size()>0) {
            lastInsert = new Campaign_Member_List__c(
                Campaign__c=campaignId,
                List_Part_No__c=partNo++,
                List_Part__c=JSON.serialize(ids),
                Is_Last_List_Part__c=false    
            );
            insert lastInsert;
        }
    }
    
    public void finish(Database.BatchableContext BC){
        if(lastInsert!=null)
            update new Campaign_Member_List__c(
                id=lastInsert.id,
                Is_Last_List_Part__c=true
            );
    }
    
}