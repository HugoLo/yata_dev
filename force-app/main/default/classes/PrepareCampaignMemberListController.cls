public class PrepareCampaignMemberListController {

    public class FunctionResult {
        @AuraEnabled public string Id {get;set;}
        @AuraEnabled public string Error {get;set;}
        @AuraEnabled public string Debug {get;set;}
    }
    
    @AuraEnabled public static string checkAllowProceed(Id id) {
        try {
            Campaign c = [SELECT Status, SpecificMember__c FROM Campaign WHERE Id=:id];
            integer mcnt = [SELECT COUNT() FROM CampaignMember WHERE CampaignId=:id];
            if(c.Status!='Planning')
                return 'status';
            else if (!c.SpecificMember__c)
                return 'specificmember';
            else if (mcnt<=0)
                return 'membercount';
            else 
                return null;
        } catch(Exception ex) {
            return ex.getMessage()==null ? 'Unhandled exception' : ex.getMessage();
        }
    }

    @AuraEnabled public static FunctionResult proceed(Id id) {
        try {
            UpdateCampaignMemberList.run(id);
            FunctionResult fr = new FunctionResult();
            fr.Id = id;
            return fr;
            
        } catch(Exception ex) {
            FunctionResult fr = new FunctionResult();
            fr.Error = ex.getMessage();
            fr.Debug = ex.getStackTraceString();
            return fr;
        }
    }
        
}