public with sharing class PointSchemeCloneController {
	@AuraEnabled
    public static String apex_clonePointsScheme(String recordId){	
        PointsScheme__c pointsScheme = [SELECT Id, Membership__c, PointSchemeName__c, RuleCode__c, MembershipTier__c FROM PointsScheme__c WHERE Id = :recordId];
        PointsScheme__c clonePointsScheme = pointsScheme.clone();
        insert clonePointsScheme;
        
        IncentiveRule__c incentiveRule = [SELECT Id, JSON__c, ObjectName__c, RecordID__c, RuleName__c FROM IncentiveRule__c WHERE RecordID__c = :recordId];
        IncentiveRule__c cloneIncentiveRule = incentiveRule.clone();
        insert cloneIncentiveRule;
        IncentiveRule__c updateIncentiveRule = [SELECT Id, Name, JSON__c, ObjectName__c, RecordID__c, RuleName__c FROM IncentiveRule__c WHERE Id = :cloneIncentiveRule.Id];
        
        String jsonString = updateIncentiveRule.JSON__c;
        Map<String, Object> jsonDict = (Map<String, Object>) JSON.deserializeUntyped(jsonString);
        Map<String, Object> BasicInfo = (Map<String, Object>) jsonDict.get('BasicInfo');
        Map<String, Object> Targeting = (Map<String, Object>) jsonDict.get('Targeting');
        Map<String, Object> Condition = (Map<String, Object>) jsonDict.get('Condition');
        Map<String, Object> Action = (Map<String, Object>) jsonDict.get('Action');
        BasicInfo.put('Status', null);
        BasicInfo.put('StartDate', null);
        BasicInfo.put('EndDate', null);
        BasicInfo.put('RuleFromRecdID', clonePointsScheme.Id);
        BasicInfo.put('IncentiveRuleNo', updateIncentiveRule.Name);
        
        Action.put('Reward', null);
        Action.put('PointMultiplier', null);
        Action.put('PointAmount', null);
        Action.put('MaxTimes', null);
        Action.put('MaxPoint', null);
        
        jsonDict.put('BasicInfo', BasicInfo);
        jsonDict.put('Action', Action);
        jsonDict.put('Condition', null);
        updateIncentiveRule.RecordID__c = clonePointsScheme.Id;
        updateIncentiveRule.JSON__c = JSON.serialize(jsonDict);
        update updateIncentiveRule;
        
        clonePointsScheme.IncentiveRuleNo__c = updateIncentiveRule.Id;
        update clonePointsScheme;
            
        return clonePointsScheme.Id;
    }
}