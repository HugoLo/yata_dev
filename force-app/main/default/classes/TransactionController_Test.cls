@isTest
public class TransactionController_Test {
	
    static testMethod void TransactionController_Test(){
        
        TestClassHelper.createIntegrationControl();
        TestClassHelper.createNoRecordDisplay('NoRecordDisplay_Txn');
        Account account = TestClassHelper.createMember();
        Map<String, String> parameter = new Map<String, String>();
        parameter.put('MemberSFId', account.Id);
        parameter.put('TransactionNo', '202126');
        parameter.put('Type', '');
        parameter.put('SubType', 'Void');
        parameter.put('Source', 'POS');
        parameter.put('Shop', null);
        parameter.put('TxnDateFrom', '2018-09-01 00:00:00');
        parameter.put('TxnDateTo', '2018-10-01 00:00:00');
        parameter.put('RefundDateFrom', '2018-09-01 00:00:00');
        parameter.put('RefundDateTo', '2018-10-01 00:00:00');
        parameter.put('limit', '10');
        
        Map<String, String> parameter2 = new Map<String, String>();
        parameter2.put('TransactionPGId', '123456');
        
        
        User userA = TestClassHelper.createUser();
        System.runAs(userA){
            
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new HTTPMockCallout());
            String responseString = TransactionController.apex_getTransactionList2(parameter);
            Test.setMock(HttpCalloutMock.class, new HTTPMockCallout2());
            String responseString2 = TransactionController.apex_getTransactionLineList(parameter2);
            TransactionController.apex_getNoRecordDisplay('NoRecordDisplay_Txn');
            String fieldset = TransactionController.apex_getFieldset('Transaction__c', 'TransactionFieldSetPerview');
            String recordType = TransactionController.apex_getRecordType('Transaction__c');
            String picklistFieldValues = TransactionController.apex_getPicklistFieldValues('Transaction__c', 'SubType__c');
            Test.stopTest();
            
        }
        
    }
    
    public class HTTPMockCallout implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(200);
            string jsonResBody = '{"ReturnCode":"1","ReturnMessage":"Success","TotalCount":67,"Transactions":[{"total_count":"67","countrycode__c":"","transactionno__c":"24593916","sysstatus__c":"Completed","recordtypeid":"0127F000000BZi6QAG","totalcampaignpoint__c":0,"telno__c":"","relatedrule__c":null,"source__c":"EFT","lastvieweddate":null,"subtype__c":"Sales","member__c":"001p000000VgkMTAAZ","name":null,"transactionamount__c":108,"shop__c":"a0cp0000001gdqMAAQ","lastmodifieddate":null,"posno__c":"9939713301","totalbonuspoint__c":0,"ownerid":null,"refundreferenceno__c":null,"isdeleted":null,"systemmodstamp":null,"remarks__c":null,"lastmodifiedbyid":null,"refundreftxpgid__c":null,"createddate":null,"totalpoint__c":0,"transactiondate__c":"2018-10-12T07:38:34.000Z","memberpgid__c":159,"manualpoint__c":null,"createdbyid":null,"merchant__c":"a0Tp0000001WGe6EAG","receiptdate__c":null,"lastreferenceddate":null,"totalbasicpoint__c":0,"refunddate__c":null,"pointsexpirydate__c":"2019-12-31T00:00:00.000Z","sfid":null,"pgid__c":235937,"refkey__c":"00199397133012018101224593916                                                                       ","transactionnetamount__c":108,"relatedruleid__c":null,"fy__c":2019,"transactionday__c":"2018-10-12T00:00:00.000Z","transactiontime__c":"07:38:34","recordcreatedtime__c":null,"tx2_transactionno__c":null,"recordtypeid_name":"Purchase","member__c_name":"Test 1 Test 1","merchant__c_name":"GMS","refundreftxpgid__c_name":null,"shop__c_name":"SH"}]}';
            res.setBody(jsonResBody);
            return res;
        }
    }
    
    public class HTTPMockCallout2 implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(200);
            string jsonResBody = '{"ReturnCode":"1","ReturnMessage":"Success","TotalCount":null,"Transactions":[{"Txn":{"pgid__c":"235937"},"TxnLines":[{"basicpoint__c":null,"campaignpoint__c":null,"transaction__c":null,"paymentamount__c":null,"redemptionpgid__c":null,"paymentlineno__c":null,"lastvieweddate":null,"member__c":null,"subcategoryid__c":"","amount__c":108,"name":null,"lastmodifieddate":null,"ownerid":null,"lineno__c":1,"isdeleted":null,"item__c":"","systemmodstamp":null,"lastmodifiedbyid":null,"bonuspoint__c":null,"lastactivitydate":null,"transactionpgid__c":235937,"lineitemqty__c":3,"linetype__c":"Line Item","createddate":null,"totalpoint__c":null,"memberpgid__c":null,"createdbyid":null,"upc__c":"289445507411809","paymentmethod__c":"    ","lastreferenceddate":null,"sfid":null,"pgid__c":18428,"couponno__c":null,"paymentmethodsfid__c":null,"item__c_name":null,"member__c_name":null,"paymentmethodsfid__c_name":null,"transactionpgid__c_name":null},{"basicpoint__c":null,"campaignpoint__c":null,"transaction__c":null,"paymentamount__c":108,"redemptionpgid__c":null,"paymentlineno__c":null,"lastvieweddate":null,"member__c":null,"subcategoryid__c":"","amount__c":108,"name":null,"lastmodifieddate":null,"ownerid":null,"lineno__c":2,"isdeleted":null,"item__c":null,"systemmodstamp":null,"lastmodifiedbyid":null,"bonuspoint__c":null,"lastactivitydate":null,"transactionpgid__c":235937,"lineitemqty__c":0,"linetype__c":"Payment","createddate":null,"totalpoint__c":null,"memberpgid__c":null,"createdbyid":null,"upc__c":null,"paymentmethod__c":"1005","lastreferenceddate":null,"sfid":null,"pgid__c":18429,"couponno__c":null,"paymentmethodsfid__c":"a0np0000003H9x3AAC","item__c_name":null,"member__c_name":null,"paymentmethodsfid__c_name":"銀聯","transactionpgid__c_name":null}],"PointDetail":[]}]}';
            res.setBody(jsonResBody);
            return res;
        }
    }
    
}