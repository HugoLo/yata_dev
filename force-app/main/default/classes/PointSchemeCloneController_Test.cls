@isTest
public class PointSchemeCloneController_Test {

    static testMethod void PointSchemeCloneController_Test(){
        
        User userA = TestClassHelper.createUser();
        System.runAs(userA){
            
            Membership__c membership = TestClassHelper.createMembership();
            MembershipTier__c membershipTier = TestClassHelper.createMembershipTier(membership);
            PointsScheme__c pointsScheme = TestClassHelper.createPointsScheme(membership, membershipTier);
            IncentiveRule__c incentiveRule = TestClassHelper.createIncentiveRule_PointScheme(pointsScheme);
            Test.startTest();
       		
            PointSchemeCloneController.apex_clonePointsScheme(pointsScheme.Id);
            Test.stopTest();
            
        }
        
    }
}