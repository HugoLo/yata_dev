public class FixContentTriggerHandler extends TriggerHandler{
    //private static final String ObjName = 'FixContent__c';
    private static final String ObjName = Trigger.isDelete ? Trigger.Old.getSObjectType().getDescribe().getName() : Trigger.New.getSObjectType().getDescribe().getName();
        
    public FixContentTriggerHandler(){}
    /*
    protected override void beforeInsert() {
        system.debug('@@@beforeInsert');
    }
    
    protected override void beforeUpdate () {
        system.debug('@@@beforeUpdate');
    }
    
    protected override void beforeDelete () {
        system.debug('@@@beforeDelete');
    }
    */    
    protected override void afterInsert () {
        system.debug('@@@FixContent afterInsert');
        if(Common_Security_Cls.toRunTriggerMethod('FixContentTriggerHandler','afterInsert')){
            
            //Create Log shipment records:
            if(Common_Utilities_Cls.needLogShipment(ObjName)){
                LogShipmentHandler.createLogShipment(ObjName, Trigger.new);   
            }
        }
    }
    
    protected override void afterUpdate () {
        system.debug('@@@FixContent afterUpdate');
        if(Common_Security_Cls.toRunTriggerMethod('FixContentTriggerHandler','afterUpdate')){
            
            //Create Log shipment records:
            if(Common_Utilities_Cls.needLogShipment(ObjName)){
                LogShipmentHandler.updateLogShipment(ObjName, Trigger.new, Trigger.oldMap);
            }
        }
    }
    
    protected override void afterDelete () {
        if(Common_Security_Cls.toRunTriggerMethod('FixContentTriggerHandler','afterDelete')){
            //Create Log shipment records:
            if(Common_Utilities_Cls.needLogShipment(ObjName)){
                LogShipmentHandler.createDeleteLogShipment(ObjName, Trigger.old);
            }   
        }
    }
    
    
}