/********************************************************************************************
*							Class Name: ProductCategoryTriggerHandler
* Desciption: To run trigger methods
* 
* Version History:
* 2018-07-01 Created by TC (Introv Limited)
*                                   
*-------------------------------------------------------------------------------------------*
*
* 1. afterInsert():
*	trigger to after insert
* 
* 2. afterUpdate():
*	trigger logic after update
*
********************************************************************************************/

public class ProductCategoryTriggerHandler extends TriggerHandler{
    private static final String ObjName = Trigger.isDelete ? Trigger.Old.getSObjectType().getDescribe().getName() : Trigger.New.getSObjectType().getDescribe().getName();
	
    protected override void beforeInsert() {
  		if(Common_Security_Cls.toRunTriggerMethod('ProductCategoryTriggerHandler','beforeInsert')){
            
            // Auto fill the level number based on parent product category:
            for(ProductCategory__c pc : (List<ProductCategory__c>)Trigger.New){
                if(pc.ParentCategoryID__c==null){
                    pc.Level__c =1;
                }else if(pc.ParentProductCategoryLevel__c!=null ){
                    pc.Level__c =pc.ParentProductCategoryLevel__c+1;
                }
            }
        }
    }
    
    protected override void beforeUpdate () {
        if(Common_Security_Cls.toRunTriggerMethod('ProductCategoryTriggerHandler','beforeUpdate')){
        	
            // Auto fill the level number based on parent product category:
            for(ProductCategory__c pc : (List<ProductCategory__c>)Trigger.New){
                if(pc.ParentCategoryID__c==null){
                    pc.Level__c =1;
                }else if(pc.ParentProductCategoryLevel__c!=null ){
                    pc.Level__c =pc.ParentProductCategoryLevel__c+1;
                }
            }
        }
  	}
    
    
}