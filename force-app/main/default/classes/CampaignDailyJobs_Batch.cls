global class CampaignDailyJobs_Batch implements Database.Batchable<sObject> {
    
	global CampaignDailyJobs_Batch() {
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        Date today = Common_Utilities_Cls.getToday();
        return Database.getQueryLocator([SELECT Id 
                                         FROM Campaign 
                                         WHERE EndDate < :today AND Status != 'Ended'
                                        ]);
    }

    global void execute(Database.BatchableContext BC, List<Campaign> scope) {
        for(Campaign campaign : scope){
            campaign.Status = 'Ended';
        }
        //system.debug('@@@scope: '+scope);
        update scope;
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }    
    
}