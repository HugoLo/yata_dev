/********************************************************************************************
*							Class Name: LogShipmentTriggerHandler
* Desciption: To run trigger methods
* 
* Version History:
* 2018-06-07 Created by Hugo (Introv Limited)
*                                   
*-------------------------------------------------------------------------------------------*
*
* 1. afterInsert():
*	trigger to insert object record.
* 
* 2. afterUpdate():
*	trigger to update object record.
*
********************************************************************************************/
public class LogShipmentTriggerHandler extends TriggerHandler{
        
    public LogShipmentTriggerHandler(){}
    /*
    protected override void beforeInsert() {
        system.debug('@@@beforeInsert');
  	}
    
    protected override void beforeUpdate () {
        system.debug('@@@beforeUpdate');
  	}
    
    protected override void beforeDelete () {
        system.debug('@@@beforeDelete');
  	}
    */
    protected override void afterInsert () {
        system.debug('@@@Log Shipment afterInsert');
        system.debug('@@@'+Common_Security_Cls.toRunTriggerMethod('LogShipmentTriggerHandler','afterInsert'));
        if(Common_Security_Cls.toRunTriggerMethod('LogShipmentTriggerHandler','afterInsert')){
        	handleLogShipment();
        }
  	}
    
    protected override void afterUpdate () {
        system.debug('@@@Log Shipment afterUpdate');
        if(Common_Security_Cls.toRunTriggerMethod('LogShipmentTriggerHandler','afterUpdate')){
            //handleLogShipment();
        }
  	}
    
    private void handleLogShipment(){
        //LogShipmentHandler.logShipmentUpdateRecord(Trigger.new);   
        if(!system.isBatch()){
            //LogShipmentHandler.updateLogShipmentStatus(Trigger.NewMap.keyset());
        }
        
        List<LogShipment__c> l_LogShipment = (List<LogShipment__c>)Trigger.new;
        Boolean isExecute = false;
        for(LogShipment__c logShipment : l_LogShipment){
            String source = logShipment.Source__c;
            if(source == 'H'){
                isExecute = true;
            }
        }
        if(isExecute){
        	LogShipment_Sched sch = new LogShipment_Sched();
            sch.executeLogShipmentBatch();    
        }
        
        /*
        if ([SELECT count() FROM AsyncApexJob WHERE JobType='BatchApex' AND (Status = 'Processing' OR Status = 'Preparing') AND ApexClass.Name = 'LogShipment_Batch'] >= 1){ 
            if ([SELECT count() FROM CronTrigger WHERE  CronJobDetail.Name like '%LogShipment_Sched%' AND NextFireTime != null] >= 1){
                //If there is a scheduled job, do nothing
                //The first one should be the daily job
                system.debug('@@@CronTrigger');
            } 
            else {
                //schedule the schedulable class again in 2 mins
                LogShipment_Sched scRetry = new LogShipment_Sched(); 
                Datetime dt = Datetime.now();
                dt = dt.addMinutes(2);
                //dt = dt.addSeconds(5);
                String timeForScheduler = dt.format('s m H d M \'?\' yyyy'); 
                Id schedId = System.Schedule('LogShipment_Sched'+timeForScheduler,timeForScheduler,scRetry);
                
            }
        } 
        else {
            List<LogShipment__c> l_LogShipment = (List<LogShipment__c>)Trigger.new;
            List<LogShipment__c> l_s_LogShipment = new List<LogShipment__c>();
            for(LogShipment__c logShipment : l_LogShipment){
                String source = logShipment.Source__c;
                if(source == 'H'){
                    l_s_LogShipment.add(logShipment);
                }
            }
            if(l_s_LogShipment.size() > 0){
                LogShipment_Batch batch = new LogShipment_Batch();
                Database.executeBatch(batch, 200);
            }
        }
		*/
    }
    
    /*
    protected override void afterDelete () {
        system.debug('@@@afterDelete');
  	}
    
    protected override void afterUndelete  () {
		system.debug('@@@afterUndelete');        
  	}
	*/
}