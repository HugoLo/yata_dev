@isTest
public class CampaignDailyJobs_BatchSchedule_Test {
    
    static testMethod void CampaignDailyJobs_BatchSchedule_Test(){
        
        Campaign campaign = TestClassHelper.createExpiredCampaign();
        
        User userA = TestClassHelper.createUser();
        System.runAs(userA){
            
            Test.startTest();
            
            CampaignDailyJobs_Schedule sched = new CampaignDailyJobs_Schedule ();   
			String chron = '0 0 23 * * ?';        
			system.schedule('Test Sched', chron, sched);
            
            
            Test.stopTest();
         	   
        }
        
    }

}