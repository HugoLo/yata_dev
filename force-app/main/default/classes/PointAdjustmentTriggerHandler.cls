public class PointAdjustmentTriggerHandler extends TriggerHandler{
    private static Boolean needToCallOut = false;
    
    
    protected override void beforeInsert () {
        if(Common_Security_Cls.toRunTriggerMethod('PointAdjustmentTriggerHandler','beforeInsert')){
			//Validate Data and Get Member SF Id by Member No.
			validateData();
        }
    }
    
    protected override void beforeUpdate () {
        if(Common_Security_Cls.toRunTriggerMethod('PointAdjustmentTriggerHandler','beforeUpdate')){
			//Validate Data and Get Member SF Id by Member No.
			validateData();
        }
    }
    
    private void validateData(){
        Set<String> s_membNo = new Set<String>();
        Set<String> s_membMobileNo = new Set<String>();
        List<PointAdjustment__c> l_noMembRcd = new List<PointAdjustment__c>();
        for(PointAdjustment__c pa: (List<PointAdjustment__c>)Trigger.New){
            system.debug('@@@CountryCode__c: '+pa.CountryCode__c+pa.MobileNo__c);
            if(pa.PointAdjustment__c ==null){
                pa.PointAdjustment__c.addError('Adjustment Point cannot be null');
            }
            else if(pa.TransactionDate__c ==null){
                pa.TransactionDate__c.addError('Transaction Date cannot be null');
            }
            else if(String.isEmpty(pa.MembershipNo__c) && String.isEmpty(pa.CountryCode__c)){
                pa.addError('Membership No. or Mobile No. cannot be null');
            }
            else if(String.isEmpty(pa.SubType__c)){
                pa.SubType__c.addError('Sub Type cannot be null');
            }
            else if((!String.isEmpty(pa.CountryCode__c) && String.isEmpty(pa.MobileNo__c)) || (String.isEmpty(pa.CountryCode__c) &&  !String.isEmpty(pa.MobileNo__c))){
                if(String.isEmpty(pa.CountryCode__c)){
					pa.CountryCode__c.addError('Country code and Mobile no cannot be null at the same time.');                    
                }
                else if(String.isEmpty(pa.MobileNo__c)){
					pa.MobileNo__c.addError('Country code and Mobile no cannot be null at the same time.');                                  
                }
            }
            else if(pa.Member__c == null ){
                s_membNo.add(pa.MembershipNo__c);
                if(!String.isEmpty(pa.CountryCode__c) &&  !String.isEmpty(pa.MobileNo__c)){
                    system.debug('@@@s_membMobileNo: '+pa.CountryCode__c+pa.MobileNo__c);
                    s_membMobileNo.add(pa.CountryCode__c+pa.MobileNo__c);
                }
                l_noMembRcd.add(pa);
            }
            
            if(String.isEmpty(pa.Status__c)){
                pa.Status__c ='Pending';
            }
            
            if(pa.Status__c =='Pending'){
                needToCallOut = true;
            }
        }
        
        //Map<String,Id> m_MembNo_Id = new Map<String,Id>();
        Map<String,account> m_MembNo_Acct = new Map<String,account>();
        Map<String,account> m_Mobile_Acct = new Map<String,account>();
        if(s_membNo.size()>0){
            List<Account> l_memb = new List<Account>();
            l_memb = [SELECT Id, Name, MembershipNo__pc, MemberStatus__pc, MobileKey__c FROM Account WHERE isPersonAccount = true AND MembershipNo__pc in:s_membNo];
            for(Account memb: l_memb){
                //m_MembNo_Id.put(memb.MembershipNo__pc, memb.Id);
                m_MembNo_Acct.put(memb.MembershipNo__pc, memb);
            }
        }
        if(s_membMobileNo.size()>0){
            List<Account> l_memb = new List<Account>();
            l_memb = [SELECT Id, Name, MembershipNo__pc, MemberStatus__pc, MobileKey__c FROM Account WHERE isPersonAccount = true AND MobileKey__c in:s_membMobileNo];
            for(Account memb: l_memb){
                m_Mobile_Acct.put(memb.MobileKey__c,memb);
                /**
                if(!m_MembNo_Id.containsKey(memb.MembershipNo__pc)){
                	m_MembNo_Id.put(memb.MembershipNo__pc, memb.Id);    
                }**/
            }
        }
        /**
        system.debug('m_MembNo_Id='+m_MembNo_Id);
        if(m_MembNo_Id.size()>0){
            for(PointAdjustment__c pa: l_noMembRcd){
                system.debug('pa.MembershipNo__c='+pa.MembershipNo__c);
                system.debug('m_MembNo_Id.get(pa.MembershipNo__c)='+m_MembNo_Id.get(pa.MembershipNo__c));
                if(!m_MembNo_Id.containsKey(pa.MembershipNo__c)){
                    pa.MembershipNo__c.addError('Membership No. does not exist');
                }else{
                    pa.Member__c = m_MembNo_Id.get(pa.MembershipNo__c);
                }
            }
        }else{
            for(PointAdjustment__c pa: l_noMembRcd){
                pa.MembershipNo__c.addError('Membership No. does not exist');
            }
        }**/
        if(m_MembNo_Acct.size()>0 || m_Mobile_Acct.size()>0 ){
            for(PointAdjustment__c pa: l_noMembRcd){
                if(String.isEmpty(pa.CountryCode__c) && !String.isEmpty(pa.MembershipNo__c)){
                    if(!m_MembNo_Acct.containsKey(pa.MembershipNo__c)){
                       pa.MembershipNo__c.addError('Membership No. does not exist');
                    }
                    else{
                        String status = m_MembNo_Acct.get(pa.MembershipNo__c).MemberStatus__pc;
                        if(status == 'Active'){
                        	pa.Member__c = m_MembNo_Acct.get(pa.MembershipNo__c).Id;    
                        }
                        else{
                            pa.Member__c.addError('Not an active member');
                        }
                   		
                    }
                }
                else if(!String.isEmpty(pa.CountryCode__c) && String.isEmpty(pa.MembershipNo__c)){
                    String pa_MobileKey=pa.CountryCode__c+pa.MobileNo__c;
                    if(!m_Mobile_Acct.containsKey(pa_MobileKey)){
                        pa.MobileNo__c.addError('Mobile No. does not exist');
                    }
                    else{
                        String status = m_Mobile_Acct.get(pa_MobileKey).MemberStatus__pc;
                        if(status == 'Active'){
                        	pa.Member__c = m_Mobile_Acct.get(pa_MobileKey).Id;
                        }
                        else{
                            pa.Member__c.addError('Not an active member');
                        }
                    }
                }
                else if(!String.isEmpty(pa.CountryCode__c) && !String.isEmpty(pa.MembershipNo__c)){
                    String pa_MobileKey=pa.CountryCode__c+pa.MobileNo__c;
                    if(m_MembNo_Acct.containsKey(pa.MembershipNo__c) && m_Mobile_Acct.containsKey(pa_MobileKey)){
                        
                        system.debug('@@@m_MembNo_Acct '+m_MembNo_Acct.get(pa.MembershipNo__c).MobileKey__c);
                        system.debug('@@@pa_MobileKey '+pa_MobileKey);
                        if(m_MembNo_Acct.get(pa.MembershipNo__c).MobileKey__c!= pa_MobileKey){
                            //pa.MembershipNo__c.addError('Membership No. does not exist');
                            pa.MembershipNo__c.addError('Membership No. does not match mobile no.');
                        }
                        else{
                            String status = m_MembNo_Acct.get(pa.MembershipNo__c).MemberStatus__pc;
                            if(status == 'Active'){
                            	pa.Member__c = m_MembNo_Acct.get(pa.MembershipNo__c).Id;    
                            }
                            else{
                                pa.Member__c.addError('Not an active member');
                            }
                            
                        }   
                    }else if(m_MembNo_Acct.containsKey(pa.MembershipNo__c) && !m_Mobile_Acct.containsKey(pa_MobileKey)){
                        //if(!String.isEmpty(pa_MobileKey)){
                           pa.MobileNo__c.addError('Mobile No. does not exist');
                        //}else{
                        //    pa.Member__c = m_MembNo_Acct.get(pa.MembershipNo__c).Id;
                        //}   
                    }else if(!m_MembNo_Acct.containsKey(pa.MembershipNo__c) && m_Mobile_Acct.containsKey(pa_MobileKey)){
                        //if(!String.isEmpty(pa.MembershipNo__c)){
                            pa.MembershipNo__c.addError('Membership No. does not exist');
                        //}else{
                        //    pa.Member__c = m_Mobile_Acct.get(pa_MobileKey).Id;
                        //} 
                    }else{
                        pa.addError('Member does not exist with Membership No. or Mobile No.');
                    }
                }
            }
        }else{
            for(PointAdjustment__c pa: l_noMembRcd){
                pa.MembershipNo__c.addError('Membership No. does not exist');
            }
        }
    }
    
    
    protected override void afterInsert () {
        if(Common_Security_Cls.toRunTriggerMethod('PointAdjustmentTriggerHandler','afterInsert')){
            //Call out API to make adjustment:
            APICallOutUpdate();
        }
    }
    
    protected override void afterUpdate () {
        if(Common_Security_Cls.toRunTriggerMethod('PointAdjustmentTriggerHandler','afterUpdate')){
            //Call out API to make adjustment:
            APICallOutUpdate();
        }
    }
    
    
    private void APICallOutUpdate(){
        if(needToCallOut){
            Datetime now = Datetime.now();
            //Check if there is any batch point adjustment job is in the queue:
            
            //if ([SELECT count() FROM AsyncApexJob WHERE JobType='BatchApex' AND Status = 'Queued' AND ApexClass.Name ='PointAdjustment_Batch'] >=2)
            if ([SELECT count() FROM CronTrigger WHERE  CronJobDetail.Name like '%BatchPointAdjustment_Sched%' AND NextFireTime != null] >=2){
                //If there is a scheduled job, do nothing
                //The first one should be the daily job
                system.debug('@@@CronTrigger');
            } 
            else {
                //schedule the schedulable class again in 2 mins
                BatchPointAdjustment_Sched scRetry = new BatchPointAdjustment_Sched(); 
                Datetime dt = Datetime.now();
                dt = dt.addMinutes(2);
                //dt = dt.addSeconds(5);
                String timeForScheduler = dt.format('s m H d M \'?\' yyyy'); 
                Id schedId = System.Schedule('BatchPointAdjustment_Sched'+timeForScheduler,timeForScheduler,scRetry);
            }
        }
    }
}