@isTest
public class CMDHandler_Test {
    static testmethod void CMDHandler_Test() {
//        TestClassHelper.createObjectControl('Account');
        Account member = TestClassHelper.createMember();
        member.PGID__c = 99999;
        update member;
        insert new CMDQueue__c(DateTime__c=Datetime.now(), Status__c='Pending',Detail__c='{}');
        insert new CMDQueue__c(DateTime__c=Datetime.now(), Status__c='Pending',
        Detail__c='{"cmd":"UPSERT","obj":"loyaltysummary__c","data":{"pgid__c":99999,"member__c":"' + member.Id + '","memberpgid__c":99999,"pointsearnedytd__pc":0,"manualpointpye__pc":0,"bonuspointytd__pc":0,"basicpointpye__pc":0,"pointexpirednextfy__pc":0,"campaignpointpye__pc":0,"pointsredeemedpye__pc":0,"manualpointytd__pc":0,"pointsearnedpye__pc":2000,"pointexpiredthisfy__pc":2000,"basicpointytd__pc":0,"campaignpointytd__pc":0,"bonuspointpye__pc":2000,"pointsredeemedytd__pc":0,"pointbalance__pc":0,"bbbannerexpirydate__pc":null,"pointstransferredytd__pc":0,"pointstransferredpye__pc":0,"fy__c":2019,"opening_pfy":0,"potentialbbclubexpirydate__pc":null,"bbproductspending__pc":null}}');
        CMDHandler.run();
    }
}