public class CampaignTriggerHandler extends TriggerHandler{
    
	protected override void afterUpdate () {
        system.debug('@@@afterUpdate');
        if(Common_Security_Cls.toRunTriggerMethod('CampaignTriggerHandler','afterUpdate')){
            List<Campaign> l_campaign = (List<Campaign>)Trigger.new;
            Map<Id, Campaign> m_campaign = (Map<Id, Campaign>)Trigger.newMap;
            
            Set<Id> s_incentiveRule = new Set<Id>();
            Map<Id, String> m_incentiveRule_status = new Map<Id, String>();
            for(Campaign campaign : l_campaign){
                Id incentiveRuleNo = campaign.IncentiveRuleNo__c;
                if(incentiveRuleNo != null){
                   s_incentiveRule.add(incentiveRuleNo);
                    String status = campaign.Status;
                    if(status == 'Active' || status == 'Ended'){
                        m_incentiveRule_status.put(incentiveRuleNo, 'Active');
                    }
                    else if(status == 'Inactive'){
                        m_incentiveRule_status.put(incentiveRuleNo, 'Aborted');
                    }
                    else{
                        m_incentiveRule_status.put(incentiveRuleNo, null);
                    }
                }
            }
            
            List<IncentiveRule__c> l_incentiveRule = new List<IncentiveRule__c>();
            l_incentiveRule = [SELECT Id, Status__c, JSON__c, RecordID__c, RuleName__c
                               FROM IncentiveRule__c 
                               WHERE Id IN :s_incentiveRule];
            
            List<IncentiveRule__c> l_u_incentiveRule = new List<IncentiveRule__c>();
            for(IncentiveRule__c incentiveRule : l_incentiveRule){
                /*
                Id incentiveRuleNo = incentiveRule.Id;
                incentiveRule.Status__c = m_incentiveRule_status.get(incentiveRuleNo);
                l_u_incentiveRule.add(incentiveRule);
				*/
                Id incentiveRuleNo = incentiveRule.Id;
                incentiveRule.Status__c = m_incentiveRule_status.get(incentiveRuleNo);
                Id RecordID = incentiveRule.RecordID__c;
                String JSONString = incentiveRule.JSON__c;
                
                Map<String, Object> jsonObject = (Map<String, Object>)JSON.deserializeUntyped(JSONString);
                Map<String, Object> BasicInfo = (Map<String, Object>)jsonObject.get('BasicInfo');
                Map<String, Object> Targeting = (Map<String, Object>)jsonObject.get('Targeting');
                
                Campaign campaign = m_campaign.get(RecordID);
                boolean specificMember = campaign.SpecificMember__c;
                Targeting.put('SpecificMemberOnly', specificMember);
                
                String campaignName = campaign.Name;
               	String campaignStatus = campaign.Status;
                Date campaignStartDate = campaign.StartDate;
                Date campaignEndDate = campaign.EndDate;
                BasicInfo.put('Status', campaignStatus);
                BasicInfo.put('RuleName', campaignName);
                BasicInfo.put('StartDate', campaignStartDate);
                BasicInfo.put('EndDate', campaignEndDate);
                
                incentiveRule.RuleName__c = campaign.Name;
                incentiveRule.JSON__c = JSON.serialize(jsonObject);
				
            }
            
            update l_incentiveRule;
        }
  	}
}