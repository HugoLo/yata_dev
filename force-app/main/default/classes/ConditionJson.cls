public class ConditionJson {
	@AuraEnabled
	public String ConditionType{ get; set; }
    @AuraEnabled
	public String SysCon{ get; set; }
    @AuraEnabled
	public String RelatedTo{ get; set; }
    @AuraEnabled
	public Integer Amount{ get; set; }
    @AuraEnabled
	public List<Detail> Detail{ get; set; }
    @AuraEnabled
	public String Logic{ get; set; }

	public class Detail {
        @AuraEnabled
		public Integer No{ get; set; }
        @AuraEnabled
		public String ObjectRelation{ get; set; }
        @AuraEnabled
		public String ObjectName{ get; set; }
        @AuraEnabled
		public String FieldName{ get; set; }
        @AuraEnabled
		public String Operator{ get; set; }
        @AuraEnabled
		public String rulesSettingName{ get; set; }
        @AuraEnabled
		public List<String> FieldValue{ get; set; }
        @AuraEnabled
		public String FieldType{ get; set; }
        @AuraEnabled
		public String ObjectLevel{ get; set; }
        @AuraEnabled
        public String FieldValueString{ get; set; }
        
        public Detail(Integer No, String ObjectRelation, String ObjectName, String FieldName, String Operator, List<String> FieldValue, String FieldType, String ObjectLevel, String rulesSettingName){
            this.No = No;
            this.ObjectRelation = ObjectRelation;
            this.ObjectName = ObjectName;
            this.FieldName = FieldName;
            this.Operator = Operator;
            this.FieldValue = FieldValue;
            this.FieldType = FieldType;
            this.ObjectLevel = ObjectLevel;
            this.rulesSettingName = rulesSettingName;
            String valueString = '';
            for(Integer i=0; i<FieldValue.size(); i++){
				String theValue = FieldValue.get(i);
                valueString += theValue;
                if(i == FieldValue.size()-1){
                    
                }
                else{
                    valueString += ', ';
                }
            }
            this.FieldValueString = valueString;
        }
        
	}
    
    
    public static Detail addDetail(){
        Detail detail = new Detail(null, null, null, null, null, new List<String>(), null, null, null);
        return detail;
    }

	
	public static ConditionJson parse(String json) {
		return (ConditionJson) System.JSON.deserialize(json, ConditionJson.class);
	}
}