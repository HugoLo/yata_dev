@isTest
public class MembershipTierCloneController_Test {

    static testMethod void MembershipTierCloneController_Test(){
        User userA = TestClassHelper.createUser();
        System.runAs(userA){
            
            Test.startTest();
            
            Membership__c membership = TestClassHelper.createMembership();
            MembershipTier__c membershipTier = TestClassHelper.createMembershipTier(membership);
            IncentiveRule__c incentiveRule = TestClassHelper.createIncentiveRule(membershipTier);
            MembershipTierCloneController.apex_cloneMemberShipTier(membershipTier.Id);
            
	       	Test.stopTest();
        }
    }
    
}