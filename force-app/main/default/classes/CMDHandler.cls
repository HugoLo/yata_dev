public class CMDHandler implements Database.Batchable<sObject>{
    
    public static void run() {
        if ([SELECT count() 
             FROM AsyncApexJob 
             WHERE JobType='BatchApex' 
             AND ApexClass.Name ='CMDHandler'
             AND Status != 'Aborted'
             AND Status != 'Completed'
             AND Status != 'Failed'] < 1) {
                 Database.executeBatch(new CMDHandler());
             } else
                 system.debug('CMDHandler already running');
    }
    
    public class CMDHandlerException extends Exception {}
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([SELECT Id FROM CMDQueue__c WHERE Status__c='Pending' ORDER BY DateTime__c,PGID__c ASC]);
    }
    
    public void execute(Database.BatchableContext BC, List<sObject> batch) {
        
        set<id> cmdIds = new set<id>();
        for(sObject o : batch) {
            system.debug(o);
            cmdIds.add(o.id);
        }
        
        map<id, CmdQueue__c> idCmds = new map<id, CmdQueue__c>();
        map<integer, CmdQueue__c> cmds = new map<integer, CmdQueue__c>();
        for(CmdQueue__c cmd : [SELECT Id, PGID__c, Detail__c, Status__c
                               FROM CMDQueue__c 
                               WHERE Status__c='Pending' AND Id IN :cmdIds 
                               FOR UPDATE]) 
        {
            idCmds.put(cmd.id, cmd);
            cmds.put((integer)cmd.PGID__c, cmd);        
        }
        
        set<CmdQueue__c> cmdsFailed = new set<CmdQueue__c>();
        map<integer, map<String, Object>> fieldsUpdates = new map<integer, map<String, Object>>();
        for(sObject o : batch) {
            CmdQueue__c cmd = idCmds.containsKey(o.id) ? idCmds.get(o.id) : null;
            if(cmd!=null && cmd.Status__c=='Pending' ) {
                try {
                    map<String, Object> d = (map<String, Object>)JSON.deserializeUntyped(cmd.Detail__c);
                    
                    if(d.containsKey('cmd') && d.containsKey('obj')) {
                        
                        if((string)d.get('obj')=='loyaltysummary__c') {
                            if((string)d.get('cmd')=='UPSERT' && d.containsKey('data')) {
                                map<String, Object> data = (map<String, Object>)d.get('data');
                                if(data==null)
                                    throw new CMDHandlerException('Unknown cmd (data is empty)');
                                data.put('obj', 'loyaltysummary__c');
                                fieldsUpdates.put((integer)cmd.PGID__c, data);
                            } else if((string)d.get('cmd')=='DELETE') {
                                throw new CMDHandlerException('DELETE is not implemented');
                            } else {
                                throw new CMDHandlerException('Unknown cmd (invalid cmd or data is empty)');
                            }
                        } else if((string)d.get('obj')=='account') {
                            if((string)d.get('cmd')=='UPDATE_PGID') {
                                string sfid = (string)d.get('sfid');
                                integer pgid = (integer)d.get('pgid__c');
                                if(sfid==null)
                                    throw new CMDHandlerException('Unknown cmd (sfid is empty)');
                                if(pgid==null)
                                    throw new CMDHandlerException('Unknown cmd (pgid__c is empty)');
                                map<String, Object> data = new map<String, Object>();
                                data.put('obj', 'account');
                                data.put('sfid', sfid);
                                data.put('pgid__c', pgid);
                                fieldsUpdates.put((integer)cmd.PGID__c, data);
                            } else {
                                throw new CMDHandlerException('Unknown cmd (invalid cmd)');
                            }
                        } else {
                            throw new CMDHandlerException('Unknown cmd (invalid obj)');
                        }
                    } else {
                        throw new CMDHandlerException('Unknown cmd 5');
                    }
                    
                } catch(Exception ex) {
                    cmd.Status__c = 'Failed';
                    cmd.Exception__c = ex.getMessage() + '\r\n' + ex.getStackTraceString();
                    cmdsFailed.add(cmd);
                }
            }
        }
        
        set<integer> accPGIDs = new set<integer>();
        for(integer pgid : fieldsUpdates.keySet()) {
            CmdQueue__c cmd = cmds.get(pgid);
            map<String, Object> data = fieldsUpdates.get(pgid);
            if(data.containsKey('memberpgid__c')) {
                integer accPGID = (integer)data.get('memberpgid__c');
                if(accPGID!=null)
                    accPGIDs.add(accPGID);
            }
        }
        map<decimal, Id> accIdMap = new map<decimal, Id>();
        for(Account acc : [SELECT Id, PGID__c FROM Account WHERE PGID__c IN :accPGIDs]) {
            accIdMap.put(acc.PGID__c, acc.Id);
        }
        
        //Use list to order the sequence (20181207 By TC):
        List<integer> sortedPGID = new List<integer>();
        if(fieldsUpdates.size()>0){
            sortedPGID.addAll(fieldsUpdates.keySet());
            sortedPGID.sort();
        }
        
        map<id, account> accUpdates = new map<id, account>();
        //for(integer pgid : fieldsUpdates.keySet()) 
        map<Id, CmdQueue__c[]> mapUpdateCmds = new map<Id, CmdQueue__c[]>();
        for(integer pgid : sortedPGID) {
            system.debug(pgid);
            CmdQueue__c cmd = cmds.get(pgid);
            try {
                map<String, Object> data = fieldsUpdates.get(pgid);
                if(data.get('obj')=='loyaltysummary__c') {
                    if(data.containsKey('memberpgid__c')) {
                        system.debug(data);
                        decimal accPGID = (integer)data.get('memberpgid__c');
                        if(accPGID==null) continue;
                        system.debug(data);
                        id accId = accIdMap.containsKey(accPGID) ? accIdMap.get(accPGID) : null;
                        if(accId==null) { 
                            system.debug('ACC not found');
                            cmd.Status__c = 'Failed';
                            cmd.Exception__c = 'ACC not found';
                            cmdsFailed.add(cmd);
                            continue;
                        }
                        account acc = null;
                        if(accUpdates.containsKey(accId))
                            acc = accUpdates.get(accId);
                        else {
                            acc = new account();
                            acc.id = accId;
                            accUpdates.put(accId, acc);
                        }
                        if(!mapUpdateCmds.containsKey(accId)) 
                            mapUpdateCmds.put(accId, new list<CmdQueue__c>());
                        mapUpdateCmds.get(accId).add(cmd);
                        
                        acc.Summary_FY__pc = (integer)data.get('fy__c');
                        
                        acc.pointbalance__pc = (decimal)data.get('pointbalance__pc');
                        
                        acc.pointsearnedytd__pc = (decimal)data.get('pointsearnedytd__pc');
                        acc.basicpointytd__pc = (decimal)data.get('basicpointytd__pc');
                        acc.bonuspointytd__pc = (decimal)data.get('bonuspointytd__pc');
                        acc.campaignpointytd__pc = (decimal)data.get('campaignpointytd__pc');
                        acc.manualpointytd__pc = (decimal)data.get('manualpointytd__pc');
                        //acc.pointsredeemedytd__pc = (decimal)data.get('pointsredeemedytd__pc');
                        acc.pointsredeemedytd__pc = (decimal)data.get('pointsredeemedpye__pc');
                        acc.pointstransferredytd__pc = (decimal)data.get('pointstransferredytd__pc');
                        
                        acc.pointsearnedpye__pc = (decimal)data.get('pointsearnedpye__pc');
                        acc.basicpointpye__pc = (decimal)data.get('basicpointpye__pc');
                        acc.bonuspointpye__pc = (decimal)data.get('bonuspointpye__pc');            
                        acc.campaignpointpye__pc = (decimal)data.get('campaignpointpye__pc');
                        acc.manualpointpye__pc = (decimal)data.get('manualpointpye__pc');
                        acc.pointsredeemedpye__pc = 0;
                        acc.pointstransferredpye__pc = (decimal)data.get('pointstransferredpye__pc');
                        
                        decimal pointexpiredthisfy = data.get('pointexpiredthisfy__pc')==null ? 0 : (decimal)data.get('pointexpiredthisfy__pc');
                        acc.pointexpiredthisfy__pc = pointexpiredthisfy < 0 ? 0 : pointexpiredthisfy;
                        acc.pointexpirednextfy__pc = (decimal)data.get('pointexpirednextfy__pc');
                        
                        acc.bbbannerexpirydate__pc = data.get('bbbannerexpirydate__pc')==null ? null : date.valueOf((string)data.get('bbbannerexpirydate__pc'));
                        acc.potentialbbclubexpirydate__pc = (string)data.get('potentialbbclubexpirydate__pc')==null ? null : date.valueOf((string)data.get('potentialbbclubexpirydate__pc'));
                        acc.bbproductspending__pc = (decimal)data.get('bbproductspending__pc');
                        
                        //acc.lastpurchasedate__pc = data.get('lastpurchasedate__pc')==null ? null : date.valueOf((string)data.get('lastpurchasedate__pc'));
                        acc.lastpurchasedate__pc = data.get('lastpurchasedate__pc')==null ? null : (datetime)JSON.deserialize('"'+(string)data.get('lastpurchasedate__pc')+'"', datetime.class);
                        acc.lastspendingupdatetime__pc = data.get('lastspendingupdatetime')==null ? null : (datetime)JSON.deserialize('"'+(string)data.get('lastspendingupdatetime')+'"', datetime.class);
                        acc.spendingamountthismonth__pc = (decimal)data.get('spendingamountthismonth__pc');
                        acc.spendingamountlastmonth__pc = (decimal)data.get('spendingamountlastmonth__pc');
                        acc.spendingamountlast2month__pc = (decimal)data.get('spendingamountlast2month__pc');
                        acc.spendingamountlast3month__pc = (decimal)data.get('spendingamountlast3month__pc');
                        acc.nooftransasctionthismonth__pc = (decimal)data.get('nooftransasctionthismonth__pc');
                        acc.nooftransasctionlastmonth__pc = (decimal)data.get('nooftransasctionlastmonth__pc');
                        acc.nooftransasctionlast2month__pc = (decimal)data.get('nooftransasctionlast2month__pc');
                        acc.nooftransasctionlast3month__pc = (decimal)data.get('nooftransasctionlast3month__pc');
                    }
                } else if(data.get('obj')=='account') {
                    id accId = (id)data.get('sfid');
                    account acc = null;
                    if(accUpdates.containsKey(accId))
                        acc = accUpdates.get(accId);
                    else {
                        acc = new account();
                        acc.id = accId;
                        accUpdates.put(accId, acc);
                    }
                    acc.PGID__c = (integer)data.get('pgid__c');
                    if(!mapUpdateCmds.containsKey(accId)) 
                        mapUpdateCmds.put(accId, new list<CmdQueue__c>());
                    mapUpdateCmds.get(accId).add(cmd);
                }
            } catch(Exception ex) {
                cmd.Status__c = 'Failed';
                cmd.Exception__c = ex.getMessage() + '\r\n' + ex.getStackTraceString();
                cmdsFailed.add(cmd);
            }    
        }

        list<CmdQueue__c> cmdsCompleted = new list<CmdQueue__c>();        
        if(accUpdates.size()>0) {
            list<account> accUpdateList = accUpdates.values();
            Database.SaveResult[] results = database.update(accUpdateList, false);
            for(integer ri=0; ri<results.size(); ri++) {
                Database.SaveResult sr = results.get(ri);
                Account acc = accUpdateList.get(ri);
                CmdQueue__c[] updateCmds = mapUpdateCmds.get(acc.Id);
                integer i=0;
                for(CmdQueue__c cmd : updateCmds) {
                    i++;
                    if(sr.isSuccess()) {
                        cmd.Status__c = i==updateCmds.size() ? 'Completed' : 'Merged';
                        cmd.Exception__c = null;
                        cmdsCompleted.add(cmd);
                    } else {
                        cmd.Status__c = 'Failed';
                        cmd.Exception__c = '';
                        for(Database.Error err : sr.getErrors()) 
                            cmd.Exception__c += err.getStatusCode() + ': ' + err.getMessage() + '\r\n' + err.getFields() + '\r\n';
                        cmdsFailed.add(cmd);
                    }
                }
            }
        }
        //if(cmdsCompleted.size()>0)
        //    database.delete(cmdsCompleted, false);
        if(cmdsCompleted.size()>0) {
            database.update(cmdsCompleted, true);
        }
        if(cmdsFailed.size()>0) {
            database.update(new list<CmdQueue__c>(cmdsFailed), true);
        }
    }
    
    public void finish(Database.BatchableContext BC) {
    }
}