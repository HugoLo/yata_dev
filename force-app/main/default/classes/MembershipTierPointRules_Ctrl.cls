public with sharing class MembershipTierPointRules_Ctrl {
	@AuraEnabled
    public static String apex_getIncentiveRule(String recordId) {
    	IncentiveRule__c incentiveRule = 
                [SELECT Id, JSON__c, Status__c 
                 FROM IncentiveRule__c 
                 WHERE RecordID__c = :recordId 
                 LIMIT 1];
        
        
        String json_content = incentiveRule.JSON__c;
        Map<String, Object> meta = (Map<String, Object>) JSON.deserializeUntyped(json_content);
        Object BasicInfo = meta.get('BasicInfo');
        String basicInfoJsonString = JSON.serialize(BasicInfo);
        system.debug('@@@basicInfoJsonString: '+basicInfoJsonString);
        InfoJson basicInfoJsonObj = InfoJson.parse(basicInfoJsonString);
        system.debug('@@@infoJsonObj: '+basicInfoJsonObj);
        
		Object Targeting = meta.get('Targeting');
        String targetingJsonString = JSON.serialize(Targeting);
        system.debug('@@@targetingJsonString: '+targetingJsonString);
        TargetingJson targetingJsonObj = TargetingJson.parse(targetingJsonString);
        system.debug('@@@targetingJsonObj: '+targetingJsonObj);
        
        Object Condition = meta.get('Condition');
        String conditionJsonString = JSON.serialize(Condition);
        system.debug('@@@conditionJsonString: '+conditionJsonString);
        ConditionJson conditionJsonObj = ConditionJson.parse(conditionJsonString);
        system.debug('@@@conditionJsonObj: '+conditionJsonObj);
        
        Object Action = meta.get('Action');
        String actionJsonString = JSON.serialize(Action);
        system.debug('@@@actionJsonString: '+actionJsonString);
        ActionJson actionJsonObj = ActionJson.parse(actionJsonString);
        system.debug('@@@actionJsonObj: '+actionJsonObj);
        
        IncentiveRuleJson incentiveRuleJson = new IncentiveRuleJson(basicInfoJsonObj, targetingJsonObj, conditionJsonObj, actionJsonObj);
        Map<String, Object> jsonMap = new Map<String, Object>();
        if(incentiveRule != null){
            jsonMap.put('returnCode', '1');
            jsonMap.put('result', incentiveRuleJson);
        }
        else{
            jsonMap.put('returnCode', '2');
        }
        
        String jsonString = JSON.serialize(jsonMap);
        system.debug('@@@jsonString: '+jsonString);
        
		return jsonString;
    }
    
    @AuraEnabled
    public static String apex_getIncentiveRuleData(String recordId) {
    	IncentiveRule__c incentiveRule = 
                [SELECT Id, JSON__c, Status__c 
                 FROM IncentiveRule__c 
                 WHERE RecordID__c = :recordId 
                 LIMIT 1];
        String jsonString = JSON.serialize(incentiveRule);
        return jsonString;
        
    }
    
    @AuraEnabled
    public static String apex_getRecordById(String recordId){
      	MembershipTier__c membershipTier = 
                [SELECT Name 
                 FROM MembershipTier__c
                 WHERE Id = :recordId];
        String jsonString = JSON.serialize(membershipTier);
        system.debug('@@@jsonString: '+jsonString);
        return jsonString;
    }
    
    @AuraEnabled
    public static sObject apex_getRewardById(String objectName, String recordId){
      	sObject record = CommonController.apex_getRecordById(objectName, recordId);
        return record;
    }
    
    @AuraEnabled
    public static String apex_saveJson(String recordId, String jsonString, String status, String startDate, String endDate, String tierName){
        Date startDateDate = startDate != null ? Date.valueOf(startDate) : null;
        Date endDateDate = endDate != null ? Date.valueOf(endDate) : null;
        
        Map<String, Object> returnMap = new Map<String, Object>();
        returnMap.put('ReturnCode', 1);
        returnMap.put('ReturnMessage', 'Success');
        
        Boolean isValid = true;
        
        if(status == 'Active'){
            if(startDateDate != null || (startDateDate != null && endDateDate != null)){
                List<MembershipTier__c> l_MembershipTier = [SELECT Id, EffectiveFrom__c, EffectiveTo__c, IncentiveRuleNo__r.Status__c
                                                       FROM MembershipTier__c 
                                                       WHERE Name = :tierName AND IncentiveRuleNo__r.Status__c = 'Active' 
                                                       AND Id != :recordId];
                
                for(MembershipTier__c membershipTier : l_MembershipTier){
                    Date membershipTier_effectiveFrom = membershipTier.EffectiveFrom__c;
                    Date membershipTier_effectiveTo = membershipTier.EffectiveTo__c;
                 
                    if(membershipTier_effectiveFrom != null && membershipTier_effectiveTo != null){
                        /*
                        if(membershipTier_effectiveFrom >= startDateDate && startDateDate <= membershipTier_effectiveTo || 
                           membershipTier_effectiveFrom >= endDateDate && endDateDate <= membershipTier_effectiveTo){
                            isValid = false;
                        }
                        */
                        system.debug('@@@startDateDate: '+startDateDate);
                        system.debug('@@@endDateDate: '+endDateDate);
                        system.debug('@@@membershipTier_effectiveFrom: '+membershipTier_effectiveFrom);
                        system.debug('@@@membershipTier_effectiveTo: '+membershipTier_effectiveTo);
                        if(startDateDate != null && endDateDate != null){
                            if(startDateDate >= membershipTier_effectiveFrom && startDateDate <= membershipTier_effectiveTo){
                                isValid = false;
                                break;
                            }
                            else if(endDateDate >= membershipTier_effectiveFrom && endDateDate <= membershipTier_effectiveTo){
                                isValid = false;
                                break;
                            }
                            else if(startDateDate <= membershipTier_effectiveFrom && endDateDate >= membershipTier_effectiveTo){
                                isValid = false;
                                break;
                            }
                            
                        }
                        else if(startDateDate != null){
                            if(startDateDate >= membershipTier_effectiveFrom && startDateDate <= membershipTier_effectiveTo){
                                isValid = false;
                                break;
                            }
                        }
                    }
                    else{
                        if(membershipTier_effectiveFrom != null){
                            /*
                            if(membershipTier_effectiveFrom <= startDateDate || membershipTier_effectiveFrom <= endDateDate){
                                isValid = false;
                            }
                            */
                            if(startDateDate != null && endDateDate != null){
                                if(endDateDate >= membershipTier_effectiveFrom){
                                    isValid = false;
                                    break;
                                }
                                // pointsScheme_startDate >= startDateDate
                                else if(startDateDate >= membershipTier_effectiveFrom){
                                    isValid = false;
                                    break;
                                }
                            }
                            else if(startDateDate != null){
                                //if(membershipTier_effectiveFrom <= startDateDate){
                                    isValid = false;
                                	break;
                                //}
                            }
                        }
                    }
                }
            }
            else{
                isValid = false;
            }
        }
                
        if(isValid){
            IncentiveRule__c incentiveRule = 
                    [SELECT Id, JSON__c, Status__c
                     FROM IncentiveRule__c 
                     WHERE RecordID__c = :recordId 
                     LIMIT 1];
            incentiveRule.JSON__c = jsonString;
            incentiveRule.Status__c = status;
            update incentiveRule;
            
            MembershipTier__c membershipTier = [SELECT Id, Name, EffectiveFrom__c, EffectiveTo__c
                                            FROM MembershipTier__c 
                                            WHERE Id = :recordId];
            membershipTier.EffectiveFrom__c = String.isBlank(startDate) ? null : Date.valueOf(startDate);
            membershipTier.EffectiveTo__c = String.isBlank(endDate) ? null : Date.valueOf(endDate);
            update membershipTier;
            
        }
        else{
            MembershipTier__c membershipTier = [SELECT Id, Name, EffectiveFrom__c, EffectiveTo__c
                                            FROM MembershipTier__c 
                                            WHERE Id = :recordId];
            
            returnMap.put('ReturnCode', 2);
            returnMap.put('ReturnMessage', 'Only allow ONE active Point Scheme with same Point Scheme Name.\nPlease inactive the all point scheme with '+membershipTier.Name+' before save.');
        }
        
        return JSON.serialize(returnMap);
    }
    
    @AuraEnabled
    public static String apex_getPicklistFieldValues(String objectName, String pickListFieldName){
        List<Object> picklistValues = new List<Object>();
        SObjectType objectType = Schema.getGlobalDescribe().get(objectName);
        List<Schema.PicklistEntry> pick_list_values = objectType.getDescribe()
                                                       .fields.getMap()
                                                       .get(pickListFieldName)
                                                       .getDescribe().getPickListValues();
        for (Schema.PicklistEntry aPickListValue : pick_list_values) {                   
            picklistValues.add(aPickListValue); 
        }
        String jsonString = JSON.serialize(picklistValues);
        system.debug('@@@picklistValues: '+jsonString);
        return jsonString;
    }
}