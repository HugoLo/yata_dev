public class ActionJson {
	@AuraEnabled
	public String ActionTo { get; set; }
    @AuraEnabled
	public String RefObj { get; set; }
    @AuraEnabled
	public String RefObjAccField { get; set; }
    @AuraEnabled
	public String ActionType { get; set; }
    @AuraEnabled
	public String PointBase { get; set; }
    @AuraEnabled
	public Double PointMultiplier { get; set; }
    @AuraEnabled
	public Double PointAmount { get; set; }
    @AuraEnabled
	public String PointGroup { get; set; }
    @AuraEnabled
	public Integer MaxPoint { get; set; }
    @AuraEnabled
	public String MaxTimes { get; set; }
    @AuraEnabled
	public String Reward { get; set; }
    @AuraEnabled
	public String ActionLevel { get; set; }
    @AuraEnabled
	public String ActionCon { get; set; }
    @AuraEnabled
	public String PointTo { get; set; }
	
	public static ActionJson parse(String json) {
		return (ActionJson) System.JSON.deserialize(json, ActionJson.class);
	}
}