/********************************************************************************************
*                           Class Name: Common_Utilities_Cls
* Desciption: Class that provides common function for all classes to use
* 
* Version History:
* 2018-06-06 Created by TC (Introv Limited)
*                                   
*-------------------------------------------------------------------------------------------*
*1.getToday()
*   To get custom setting today; if not set, then use system today
*
*2.isStoredInSFDC (String ObjectName)
*   To check if records are stored in SFDC with specific object name:
*
*3.needLogShipment(String ObjectName)
*   To check if object need to logshipment or not with specific object name:
*
*4.getLogShipMentCS (String ObjectName) (private function)
*   To get the specific Log Shipment Custom Setting by Name:
*
********************************************************************************************/
 
public class Common_Utilities_Cls {
    private static Map<String, ObjectControl__c> m_logShipment;
    
    
    //1.Get Today
    public static Date getToday(){
        Date today = Date.today();
        LoyaltySetting__c todayCS = LoyaltySetting__c.getInstance('TODAY');
        if(todayCS!=null && todayCS.Date__c!=null){
            today = todayCS.Date__c;
        }
        return today;
        
    }
    
  
    
    //2.Check if store records in SFDC
    public static Boolean isStoredInSFDC (String ObjectName){
        Boolean storeInSFDC = true;
        ObjectControl__c ls = getLogShipMentCS(ObjectName);
        system.debug('Logshipment_CS='+ls);
        if(ls!=null && ls.StoredInSF__c !=null){
            storeInSFDC =ls.StoredInSF__c;
        }
        return storeInSFDC;
    }
    
    //3. Check if object need to logshipment or not:
    public static Boolean needLogShipment(String ObjectName){
        Boolean logshipment =false;
        ObjectControl__c ls = getLogShipMentCS(ObjectName);
        system.debug('Log Shipment Custom Setting by Name='+ls);
        if(ls!=null && ls.SyncByLogShippment__c !=null){
            logshipment =ls.SyncByLogShippment__c;
        }
        system.debug('Need LogShipment='+logshipment);
        return logshipment;
    }
    
    //4. To get the specific Log Shipment Custom Setting by Name:
    private static ObjectControl__c getLogShipMentCS (String ObjectName){
        if(m_logShipment ==null){
            m_logShipment = new Map<String,ObjectControl__c>();
            m_logShipment = ObjectControl__c.getall();
        }
        system.debug('m_logShipment='+m_logShipment);
        system.debug('ObjectName='+ObjectName);
        if(m_logShipment.get(ObjectName)!=null){
            return m_logShipment.get(ObjectName);
        }else{
            return null;
        }
    }
    
    //5. To get the mapping between person account Id and contact id, esp. for marketing cloud usage
    public static Map<Id,Id> getAccountContactMap (Set<Id> s_accountIds){
        Map<Id,Id> m_acctId_contId = new Map<Id,Id>();
        if(s_accountIds!=null && s_accountIds.size()>0){
            Map<Id, Contact> m_Id_cont =  new Map<Id, Contact>([SELECT Id, AccountId FROM Contact WHERE IsPersonAccount= true AND AccountId in: s_accountIds]);
            for(Contact cont: m_Id_cont.values()){
                if(s_accountIds.contains (cont.AccountId)){
                    m_acctId_contId.put(cont.AccountId, cont.Id);
                }
            }
        }
        return m_acctId_contId;
    }
    
}