@isTest
public class MemberPointAdjustmentController_Test {
	
    static testMethod void MemberPointAdjustmentController_Test(){
        
        TestClassHelper.createIntegrationControl();
        TestClassHelper.createNoRecordDisplay('NoRecordDisplay_Txn');
        Account account = TestClassHelper.createMember();
        Datetime TransferDateTime = System.now();
        
        Map<String, List<Map<String, String>>> m_parameter = new Map<String, List<Map<String, String>>>();
        List<Map<String, String>> l_parameter = new List<Map<String, String>>();
        Map<String, String> parameter = new Map<String, String>();
        parameter.put('MemberSFId', account.Id);
        parameter.put('RecordType', '0127F000000BZi5QAG');
        parameter.put('SubType', 'Issue');
        parameter.put('TransferDateTime', String.valueOf(TransferDateTime));
        parameter.put('Points', '100');
        parameter.put('IntRemark', 'Internal Remark');
        parameter.put('ExtRemark', 'External Remark');
        l_parameter.add(parameter);
		m_parameter.put('adjustList', l_parameter);       
        
        User userA = TestClassHelper.createUser();
        System.runAs(userA){
           	
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new HTTPMockCallout());
            String responseString = MemberPointAdjustmentController.apex_createPointAdjustment(m_parameter);
          	MemberPointAdjustmentController.apex_getMemberInfo(account.Id);
            MemberPointAdjustmentController.apex_getPicklistFieldValues('Transaction__c', 'SubType__c');
            MemberPointAdjustmentController.apex_getTodayDatetime();
            MemberPointAdjustmentController.apex_getRecordTypeId('Transaction__cPointAdjustment');
            Test.stopTest();
        	    
        }
    }
    
    
    public class HTTPMockCallout implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(200);
            string jsonResBody = '{"ReturnCode":"1","ReturnMessage":"All point adjustments complete","ReturnDetails":[{"RecordId":"f03b9765-eb21-4bee-aaae-d2da9f88a862","ReturnCode":"1","ReturnMessage":"success","txnPGID":253170}]}';
            res.setBody(jsonResBody);
            return res;
        }
    }
    
}