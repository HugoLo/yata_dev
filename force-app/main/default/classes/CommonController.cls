public with sharing class CommonController {
	
    @AuraEnabled
    public static String apex_getFieldset(String objectName, String fieldSetName){
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        
        system.debug('@@@objectName: '+objectName);
        system.debug('@@@fieldSetName: '+fieldSetName);
        List<Schema.FieldSetMember> fieldSetMemberList = new List<Schema.FieldSetMember>();
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(objectName);   
        if(SObjectTypeObj != null){
            system.debug('@@@SObjectTypeObj: '+SObjectTypeObj);
            Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
            Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
            if(fieldSetObj != null){
                system.debug('@@@fieldSetObj: '+fieldSetObj);
                
				fieldSetMemberList = fieldSetObj.getFields();
                for(Schema.FieldSetMember fieldSetMemberObj : fieldSetMemberList)
                {
                    system.debug('API Name ====>' + fieldSetMemberObj.getFieldPath()); //api name
                    system.debug('Label ====>' + fieldSetMemberObj.getLabel());
                    system.debug('Required ====>' + fieldSetMemberObj.getRequired());
                    system.debug('DbRequired ====>' + fieldSetMemberObj.getDbRequired());
                    system.debug('Type ====>' + fieldSetMemberObj.getType());   //type - STRING,PICKLIST
                }
            }
        }
        
        String jsonString = JSON.serialize(fieldSetMemberList);
        return jsonString;
    }
    
    @AuraEnabled
    public static String apex_getRecordType(String objectName){
        List<RecordType> recordTypeList = [SELECT Id, DeveloperName, Description, Name, 
                                           IsActive, SystemModstamp, SobjectType, NamespacePrefix, 
                                           LastModifiedDate, LastModifiedById, CreatedDate, 
                                           CreatedById, BusinessProcessId 
                                           FROM RecordType
                                           WHERE SobjectType = :objectName];
        
        system.debug('@@@recordTypeList: '+recordTypeList);
        String jsonString = JSON.serialize(recordTypeList);
        return jsonString;
        
    }
    
    @AuraEnabled
    public static String apex_getPicklistFieldValues(String objectName, String pickListFieldName){
        List<Object> picklistValues = new List<Object>();
        SObjectType objectType = Schema.getGlobalDescribe().get(objectName);
        List<Schema.PicklistEntry> pick_list_values = objectType.getDescribe()
                                                       .fields.getMap()
                                                       .get(pickListFieldName)
                                                       .getDescribe().getPickListValues();
        for (Schema.PicklistEntry aPickListValue : pick_list_values) {  
            system.debug('@@@aPickListValue: '+aPickListValue);
            picklistValues.add(aPickListValue); 
        }
        String jsonString = JSON.serialize(picklistValues);
        system.debug('@@@picklistValues: '+jsonString);
        return jsonString;
    }
    
    @AuraEnabled
    public static sObject apex_getRecordById(String objectName, String recordId){
        system.debug('ObjectName-->' + objectName);
        String sQuery =  'select id, Name from ' +objectName + ' where Id = \'' + recordId+'\'';
        sObject record = Database.query(sQuery);
        system.debug('@@@record: '+record);
        return record;
    }
    
    public static Map<String, Schema.SObjectField> apex_getAllFieldset(String objectName){
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(objectName).getDescribe().fields.getMap();
        return fieldMap;
    }
    
    public static void convertFieldValue(Map<String, Schema.SObjectField> fieldMap, Map<String, Object> contentMap){
        
        for(String key : contentMap.keySet()){
                
            Schema.SObjectField field = fieldMap.get(key);
            if(field != null){
                Schema.DisplayType fieldType = field.getDescribe().getType();
                
                if(fieldType == Schema.DisplayType.DATETIME) {
                    String dataString = (String) contentMap.get(key);
                    if(dataString != null){
                        Datetime datetimeValue = Datetime.valueOfGmt(dataString.replace('T', ' '));
                        contentMap.put(key, String.valueOf(datetimeValue));
                    }
                }
                else if(fieldType == Schema.DisplayType.DATE) {
                    String dataString = (String) contentMap.get(key);
                    if(dataString != null){
                        Date dateValue = Date.valueOf(dataString.replace('T', ' '));
                        contentMap.put(key, String.valueOf(dateValue));
                    }
                }
                else if(fieldType == Schema.DisplayType.REFERENCE) {
                    String dataString = (String) contentMap.get(key);
                    if(dataString != null){
                        contentMap.put(key, '/lightning/r/'+dataString+'/view');
                    }
                }
            } 
        }
    }
    
    public static Integer apex_getNoRecordDisplay(String name){
        LoyaltySetting__c NoRecordDisplay_CS = LoyaltySetting__c.getInstance(name);
        Integer NoRecordDisplay = 10;
        if(NoRecordDisplay_CS!=null && NoRecordDisplay_CS.Num__c!=null){
            NoRecordDisplay = (NoRecordDisplay_CS.Num__c).intValue();
        }
        system.debug('@@@NoRecordDisplay: '+NoRecordDisplay);
        return NoRecordDisplay;
    }
    
}