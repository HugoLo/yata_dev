public class TestClassHelper {
    
    //Create User
	public static User createUser(){
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User user = new User();
        user.FirstName = 'Testing';
        user.LastName = 'Introv';
        user.Alias = 'TIntrov';
        user.Email = 'gordon.ho2@introv.com';
        user.Username = 'technical2@introv.com.yata';
        user.CommunityNickname = 'test.introv';
        user.EmailEncodingKey='UTF-8';
        user.TimeZoneSidKey = 'Asia/Hong_Kong';
        user.LocaleSidKey = 'en_HK';
        user.ProfileId = p.Id;
        user.LanguageLocaleKey = 'en_US';
        
        insert user;
        return user;
    }
    
    public static Account createMember(){
        
        Date dateJoin = Common_Utilities_Cls.getToday();
        Date expiredDate = dateJoin.addYears(1);
        Id recordTypeId = Common_RecordType_Cls.getRtId('AccountMemberAccount');
            
        Account account = new Account();
        account.LastName = 'Yata';
        account.DateJoin__pc = dateJoin;
        account.DateofBirthMM__pc = 1;
        account.DateofBirthYYYY__pc = 1992;
        account.CountryCode__pc = '+852';
        account.PersonMobilePhone = '12345678';
        account.PersonEmail = 'gordon.ho@introv.com';
        account.PreferredLanguage__pc = 'Chinese';
        account.PrimaryCountry__pc = 'Hong Kong';
        account.MemberStatus__pc = 'Active';
        account.MembershipNo__pc = '123456';
        account.PGID__c = 123456;
        account.RecordTypeId = recordTypeId;
        
        insert account;
        return account;
    }
    
    public static Account createMember(Membership__c membership, MembershipTier__c membershipTier){
        
        Date dateJoin = Common_Utilities_Cls.getToday();
        Date expiredDate = dateJoin.addYears(1);
        Id recordTypeId = Common_RecordType_Cls.getRtId('AccountMemberAccount');
            
        Account account = new Account();
        account.LastName = 'Yata';
        account.DateJoin__pc = dateJoin;
        account.DateofBirthMM__pc = 1;
        account.DateofBirthYYYY__pc = 1992;
        Account.CountryCode__pc = '+852';
        account.PersonMobilePhone = '12345678';
        account.PersonEmail = 'gordon.ho@introv.com';
        account.PreferredLanguage__pc = 'Chinese';
        account.PrimaryCountry__pc = 'Hong Kong';
        account.RecordTypeId = recordTypeId;
        account.Membership__pc = membership.Id;
        account.MembershipTier__pc = membershipTier.Id;
        
        insert account;
        return account;
    }
    
    public static LogShipment__c createLogShipment(String actionType, String objectName, String Detail){
        LogShipment__c logshipment = new LogShipment__c();
        logshipment.Object__c = objectName;
        logshipment.Source__c = 'H';
        logshipment.Status__c = 'Open';
        logShipment.ActionType__c = actionType;
        logshipment.DateLog__c = Datetime.now();
        logshipment.Detail__c = Detail;
    	insert logshipment;
        return logshipment;
    }
    
    public static LogShipment__c createDuplicatedLogShipment(String actionType, String objectName, String Detail){
        LogShipment__c logshipment = new LogShipment__c();
        logshipment.Object__c = objectName;
        logshipment.Source__c = 'H';
        logshipment.Status__c = 'Open';
        logShipment.ActionType__c = actionType;
        logshipment.DateLog__c = Datetime.now();
        logshipment.Detail__c = Detail;
        return logshipment;
    }
    
    
    public static Campaign createCampaign(){
        
        Id recordTypeId = Common_RecordType_Cls.getRtId('CampaignPromotionCampaign');
        Date startDate = date.today();
		Date endDate =  startDate.addDays(30); 
        Campaign campaign = new Campaign();
        campaign.Name = 'testing';
        campaign.Status = 'Planning';
        campaign.StartDate = startDate;
        campaign.EndDate = endDate;
        campaign.RecordTypeId = recordTypeId; 
        
        insert campaign;
        return campaign;
        
    }
    
    public static Campaign createExpiredCampaign(){
        
        Date startDate = date.today();
        startDate = startDate.addDays(-30);
		Date endDate =  startDate.addDays(28);
        Campaign campaign = new Campaign();
        campaign.Name = 'testing';
        campaign.Status = 'Planning';
        campaign.StartDate = startDate;
        campaign.EndDate = endDate;
        
        insert campaign;
        return campaign;
        
    }
    
    public static Reward__c createReward(){
        
        Datetime EffectiveFrom = Datetime.now();
        Datetime EffectiveTo = EffectiveFrom.addDays(30);
        
        Reward__c reward = new Reward__c();
        reward.Name = 'testing';
        reward.Prefix__c = 'E';
        reward.RewardGroup__c = 'Limited';
        reward.RequiredPoint__c = 0;
        reward.Validity__c = 35;
        reward.RewardType__c = 'Free Item';
        reward.RewardSubType__c = 'Supermarket';
        reward.EffectiveFrom__c = EffectiveFrom;
        reward.EffectiveTo__c = EffectiveTo;
        reward.LiabilityAmount__c = 50;
        reward.RewardFaceValue__c = 50;
        reward.CouponTitleC__c = '消費獎賞';
        reward.CouponTitleE__c = 'Spending Reward';
        reward.CouponSubtitleC__c = '免費產品';
        reward.CouponSubtitleE__c = 'Free Item';
        reward.CouponRemarkC__c = '只限超級市場';
        reward.CouponRemarkE__c = 'Supermarket Only';
        reward.CouponDescriptionC__c = '憑此電子優惠券於任何一田百貨、超市、eShop或eGift網站購物即扣減$50。';
        reward.CouponDescriptionE__c = 'Present this eCoupon to enjoy $50 off at any YATA department stores, supermarkets, eShop or eGift shopping websites. Coupon is can be used from 10/9/2018 - 12/9/2018';
        reward.TCC__c = '- 於一田百貨、超市付款前，須向收銀職員出示此電子優惠券，方可享有優惠';
        reward.TCE__c = '- Present the eCoupon to cashier staff before payment in any YATA department stores or supermarkets to enjoy the offer';
        insert reward;
        return reward;
        
    }
    
    public static Subscription__c createSubscription(){
        Date EffectiveDate = date.today();
        Subscription__c subscription = new Subscription__c();
        subscription.Status__c = 'Active';
        subscription.SubscriptionType__c = 'BBClub';
        subscription.EffectiveDate__c = EffectiveDate;
        subscription.SubscriptionRefID__c = '67777772';
        subscription.PGID__c = 123456;
        insert subscription;
        return subscription;
    }
    
    public static Membership__c createMembership(){
        Membership__c membership = new Membership__c();
        membership.Name = 'Yata Fans';
        membership.MembershipCode__c = 'YataFans';
        membership.EffectiveFrom__c = Date.valueOf('2018-08-01');
        insert membership;
        return membership;
    }
    
    public static MembershipTier__c createMembershipTier(Membership__c membership){
        MembershipTier__c membershipTier = new MembershipTier__c();
        membershipTier.Membership__c = membership.Id;
        membershipTier.Name = 'Normal';
        membershipTier.EffectiveFrom__c = Date.valueOf('2018-08-01');
        membershipTier.DisplayName__c = 'Normal';
        membershipTier.Sequence__c = 10;
        insert membershipTier;
        return membershipTier;
    }
    
    public static IncentiveRule__c createIncentiveRule(MembershipTier__c membershipTier){
        IncentiveRule__c incentiveRule = new IncentiveRule__c();
        incentiveRule.JSON__c = '{"BasicInfo":{"TriggerPoint":{"TriggerSysCon":"txnCreate","TriggerObj":"Transaction__c","TriggerFieldValue":[],"TriggerField":null,"TriggerAction":"Create"},"Status":"Active","StartDate":"2018-08-01","RuleName":"Basic Point $1 equal 1 point","RuleFromRecdID":"a0Pp0000004pDZoEAM","RuleFromObj":"MembershipTier__c","IncentiveRuleNo":"IR10000008","EndDate":null},"Targeting":{"SpecificMemberOnly":false,"SpecificMemberList":[],"Logic":"1","Detail":[{"rulesSettingName":null,"Operator":"=","ObjectRelation":"Account","ObjectName":"Account","No":1,"FieldValueString":null,"FieldValue":["Active"],"FieldType":"String","FieldName":"MemberStatus__pc"}]},"Condition":null,"Action":{"Reward":null,"RefObjAccField":null,"RefObj":null,"PointTo":"Basic","PointMultiplier":1,"PointGroup":null,"PointBase":"Net Spending","PointAmount":null,"MaxTimes":null,"MaxPoint":null,"ActionType":"PointMultiplier","ActionTo":"Self","ActionLevel":"TxnLine","ActionCon":null}}';
        incentiveRule.ObjectName__c = 'MembershipTier__c';
        incentiveRule.RecordID__c = membershipTier.Id;
        incentiveRule.RuleName__c = 'Basic Point $1 equal 1 point';
        incentiveRule.Status__c = 'Active';
        insert incentiveRule;
        return incentiveRule;
    }
    
    public static IncentiveRule__c createIncentiveRule_PointScheme(PointsScheme__c pointsScheme){
        IncentiveRule__c incentiveRule = new IncentiveRule__c();
        incentiveRule.JSON__c = '{"BasicInfo":{"TriggerPoint":{"TriggerSysCon":"txnCreate","TriggerObj":"Transaction__c","TriggerFieldValue":[],"TriggerField":null,"TriggerAction":"Create"},"Status":"Active","StartDate":"2018-08-01","RuleName":"Basic Point $1 equal 1 point","RuleFromRecdID":"a0Pp0000004pDZoEAM","RuleFromObj":"MembershipTier__c","IncentiveRuleNo":"IR10000008","EndDate":null},"Targeting":{"SpecificMemberOnly":false,"SpecificMemberList":[],"Logic":"1","Detail":[{"rulesSettingName":null,"Operator":"=","ObjectRelation":"Account","ObjectName":"Account","No":1,"FieldValueString":null,"FieldValue":["Active"],"FieldType":"String","FieldName":"MemberStatus__pc"}]},"Condition":null,"Action":{"Reward":null,"RefObjAccField":null,"RefObj":null,"PointTo":"Basic","PointMultiplier":1,"PointGroup":null,"PointBase":"Net Spending","PointAmount":null,"MaxTimes":null,"MaxPoint":null,"ActionType":"PointMultiplier","ActionTo":"Self","ActionLevel":"TxnLine","ActionCon":null}}';
        incentiveRule.ObjectName__c = 'MembershipTier__c';
        incentiveRule.RecordID__c = pointsScheme.Id;
        incentiveRule.RuleName__c = 'Basic Point $1 equal 1 point';
        incentiveRule.Status__c = 'Active';
        insert incentiveRule;
        return incentiveRule;
    }
    
    public static PointsScheme__c createPointsScheme(Membership__c membership, MembershipTier__c membershipTier){
        PointsScheme__c pointsScheme = new PointsScheme__c();
        pointsScheme.Membership__c = membership.Id;
        pointsScheme.MembershipTier__c = membershipTier.Id;
        pointsScheme.PointSchemeName__c = 'Welcome Offer Coupon';
        pointsScheme.RuleCode__c = 'WelcomeOfferCoupon';
        pointsScheme.StartDate__c = Date.valueOf('2019-01-10');
        insert pointsScheme;
        return pointsScheme;
    }
    
    public static Subscription__c createSubscription(Account member){
        Date EffectiveDate = date.today();
        Subscription__c subscription = new Subscription__c();
        subscription.Status__c = 'Active';
        subscription.SubscriptionType__c = 'BBClub';
        subscription.EffectiveDate__c = EffectiveDate;
        subscription.SubscriptionRefID__c = '67777772';
        //subscription.Member__c = member.Id;
        subscription.MemberPGID__c = member.PGID__c;
        insert subscription;
        return subscription;
    }
    
    public static BBInformation__c createBBInformation(Account account, Subscription__c subscription){
        
        BBInformation__c BBInformation = new BBInformation__c();
        BBInformation.BBName__c = 'testing';
        //BBInformation.Member__c = account.Id;
        BBInformation.MemberPGID__c = account.PGID__c;
        BBInformation.Sex__c = 'Girl';
        BBInformation.Subscription__c = subscription.Id;
        insert BBInformation;
        return BBInformation;
    }
   
    public static FixContent__c createFixContent(){
        FixContent__c FixContent = new FixContent__c();
        FixContent.Name = 'Testing';
        insert FixContent;
        return FixContent;
    }
    
    public static Content__c createContent(FixContent__c FixContent){
    	Content__c Content = new Content__c();
        Content.FixContent__c = FixContent.Id;
        insert Content;
        return Content;
    }
    
    public static PromotionBanner__c createPromotionBanner(){
        Date FromDate = Date.today();
        FromDate = FromDate.addDays(-30);
        Date ToDate = FromDate.addDays(28);
        PromotionBanner__c PromotionBanner = new PromotionBanner__c();
        PromotionBanner.Name = 'Testing';
        PromotionBanner.Status__c = 'Planning';
		PromotionBanner.PopUp__c = true;
        PromotionBanner.Status__c = 'Active';
        PromotionBanner.FromDate__c = FromDate;
        PromotionBanner.ToDate__c = ToDate;
        PromotionBanner.TCC__c = 'tcc';
        PromotionBanner.TCE__c = 'tce';
        PromotionBanner.PromotionBannerTitleC__c = 'PromotionBannerTitleC';
        PromotionBanner.PromotionBannerTitleE__c = 'PromotionBannerTitleE';
        PromotionBanner.RemarkTextC__c = 'RemarkTextC';
        PromotionBanner.RemarkTextE__c = 'RemarkTextE';
        PromotionBanner.MainTextC__c = 'MainTextC';
        PromotionBanner.MainTextE__c = 'MainTextE';
        PromotionBanner.BannerImageC__c = 'https://eshop.yata.hk/app_img/Home_small_travel.jpg';
        PromotionBanner.BannerImageE__c = 'https://eshop.yata.hk/app_img/Home_small_travel.jpg';
        PromotionBanner.LargeImageC__c = 'https://eshop.yata.hk/app_img/Home_Big_travel.jpg';
        PromotionBanner.LargeImageE__c = 'https://eshop.yata.hk/app_img/Home_Big_travel.jpg';
        PromotionBanner.PopUpImageC__c = 'https://eshop.yata.hk/app_img/Home_small_travel.jpg';
        PromotionBanner.PopUpImageE__c = 'https://eshop.yata.hk/app_img/Home_small_travel.jpg';
        insert PromotionBanner;
        return PromotionBanner;
    }
    
    public static ProductCategory__c createProductCategory(ProductCategory__c parentPC){
        ProductCategory__c ProductCategory = new ProductCategory__c();
        ProductCategory.Name = 'testing';
        if(parentPC != null){
        	ProductCategory.ParentCategoryID__c = parentPC.Id;    
        }        
        insert ProductCategory;
        return ProductCategory;
    }	
    	
    public static ObjectControl__c createObjectControl(String Name){
        ObjectControl__c ls = new ObjectControl__c();
        ls.Name=Name;
        ls.DirectSyncByHerokuConnect__c = true;
        ls.StoredInSF__c = true;
		ls.SyncByLogShippment__c = true;
    	insert ls;
        return ls;
    }
    
    public static void createRuleSysConDefinition(){
        List<RuleSysConDefinition__c> l_ruleSysConDefinition = new List<RuleSysConDefinition__c>();
        
        RuleSysConDefinition__c ruleSysConDefinition = new RuleSysConDefinition__c();
        ruleSysConDefinition.Name = '1';
        ruleSysConDefinition.Section__c = 'Action';
        ruleSysConDefinition.SysConName__c = 'WelcomeOffer';
        ruleSysConDefinition.SysConAttribute__c  = 'ActionCon';
        ruleSysConDefinition.JSON__c = '"ActionTo": "Self","ActionCon": "WelcomeOffer","RefObj": null,"RefObjAccField": null';
        l_ruleSysConDefinition.add(ruleSysConDefinition);
        
        ruleSysConDefinition = new RuleSysConDefinition__c();
        ruleSysConDefinition.Name = '2';
        ruleSysConDefinition.Section__c = 'Action';
        ruleSysConDefinition.SysConName__c = 'Referral';
        ruleSysConDefinition.SysConAttribute__c  = 'TriggerSysCon';
        ruleSysConDefinition.JSON__c = '"ActionTo": "Ref","ActionCon": "Referral","RefObj": "Account","RefObjAccField": "ReferredBy__c"';
        l_ruleSysConDefinition.add(ruleSysConDefinition);
        
        
        ruleSysConDefinition = new RuleSysConDefinition__c();
        ruleSysConDefinition.Name = '3';
        ruleSysConDefinition.Section__c = 'BasicInfo';
        ruleSysConDefinition.SysConName__c = 'txnCreate';
        ruleSysConDefinition.SysConAttribute__c  = 'TriggerSysCon';
        ruleSysConDefinition.JSON__c = '{"TriggerSysCon":"txnCreate","TriggerObj":"Transaction__c","TriggerAction":"Create","TriggerField": null,"TriggerFieldValue": []}';
        l_ruleSysConDefinition.add(ruleSysConDefinition);
        
        
        ruleSysConDefinition = new RuleSysConDefinition__c();
        ruleSysConDefinition.Name = '4';
        ruleSysConDefinition.Section__c = 'BasicInfo';
        ruleSysConDefinition.SysConName__c = 'acctCreate';
        ruleSysConDefinition.SysConAttribute__c  = 'TriggerSysCon';
        ruleSysConDefinition.JSON__c = '{"TriggerSysCon":"acctCreate","TriggerObj":"Account","TriggerAction": "Create","TriggerField": null,"TriggerFieldValue": "[]"}';
        l_ruleSysConDefinition.add(ruleSysConDefinition);
        
        ruleSysConDefinition = new RuleSysConDefinition__c();
        ruleSysConDefinition.Name = '5';
        ruleSysConDefinition.Section__c = 'BasicInfo';
        ruleSysConDefinition.SysConName__c = 'acctUpdate';
        ruleSysConDefinition.SysConAttribute__c  = 'TriggerSysCon';
        ruleSysConDefinition.JSON__c = '{"TriggerSysCon":"acctUpdate","TriggerObj":"Account","TriggerAction":"Update","TriggerField": "MemberStatus__pc","TriggerFieldValue": ["Active"]}';
        l_ruleSysConDefinition.add(ruleSysConDefinition);
        
        ruleSysConDefinition = new RuleSysConDefinition__c();
        ruleSysConDefinition.Name = '6';
        ruleSysConDefinition.Section__c = 'Condition';
        ruleSysConDefinition.SysConName__c = 'OneOff';
        ruleSysConDefinition.SysConAttribute__c  = 'SysCon';
        ruleSysConDefinition.JSON__c  = '{"SysCon": "OneOff","ConditionType": "OneOff"}';
        l_ruleSysConDefinition.add(ruleSysConDefinition);
        
        ruleSysConDefinition = new RuleSysConDefinition__c();
        ruleSysConDefinition.Name = '7';
        ruleSysConDefinition.Section__c = 'Condition';
        ruleSysConDefinition.SysConName__c = 'Consecutive';
        ruleSysConDefinition.SysConAttribute__c  = 'TriggerSysCon';
        ruleSysConDefinition.JSON__c  = '{"SysCon": "Consecutive","ConditionType": "Consecutive"}';
        l_ruleSysConDefinition.add(ruleSysConDefinition);
        
        insert l_ruleSysConDefinition;
        
    }
    
    public static void createRulesSetting(){
        List<RulesSetting__c> l_rulesSetting = new List<RulesSetting__c>();
        RulesSetting__c rulesSetting = new RulesSetting__c();
        rulesSetting.Name = 'BB Club';
        rulesSetting.Object__c = 'Subscription__c';
        rulesSetting.FieldSet__c = 'BBClubFieldSet';
        rulesSetting.AppliedObject__c = 'Campaign';
        rulesSetting.Module__c = 'Targeting';
        rulesSetting.RelatedObject__c = 'Account';
        rulesSetting.RelatedObjectLabel__c = 'Member';
        rulesSetting.ObjectRelationship__c = 'BBClub';
        rulesSetting.ObjectLevel__c = null;
        l_rulesSetting.add(rulesSetting);
        
        rulesSetting = new RulesSetting__c();
        rulesSetting.Name = 'BB Information';
        rulesSetting.Object__c = 'BBInformation__c';
        rulesSetting.FieldSet__c = 'RulesBBInfoFileds';
        rulesSetting.AppliedObject__c = 'Campaign';
        rulesSetting.Module__c = 'Targeting';
        rulesSetting.RelatedObject__c = 'Account';
        rulesSetting.RelatedObjectLabel__c = 'Member';
        rulesSetting.ObjectRelationship__c = 'BBInfo';
        rulesSetting.ObjectLevel__c = null;
        l_rulesSetting.add(rulesSetting);
        
        rulesSetting = new RulesSetting__c();
        rulesSetting.Name = 'eShop';
        rulesSetting.Object__c = 'Subscription__c';
        rulesSetting.FieldSet__c = 'eShopFieldSet';
        rulesSetting.AppliedObject__c = 'Campaign';
        rulesSetting.Module__c = 'Targeting';
        rulesSetting.RelatedObject__c = 'Account';
        rulesSetting.RelatedObjectLabel__c = 'Member';
        rulesSetting.ObjectRelationship__c = 'eShop';
        rulesSetting.ObjectLevel__c = null;
        l_rulesSetting.add(rulesSetting);
        
        rulesSetting = new RulesSetting__c();
        rulesSetting.Name = 'Member';
        rulesSetting.Object__c = 'Account';
        rulesSetting.FieldSet__c = 'MemberFieldSet';
        rulesSetting.AppliedObject__c = 'Campaign';
        rulesSetting.Module__c = 'Targeting';
        rulesSetting.RelatedObject__c = 'Account';
        rulesSetting.RelatedObjectLabel__c = 'Member';
        rulesSetting.ObjectRelationship__c = 'Account';
        rulesSetting.ObjectLevel__c = null;
        l_rulesSetting.add(rulesSetting);
        
        rulesSetting = new RulesSetting__c();
        rulesSetting.Name = 'Product';
        rulesSetting.Object__c = 'Product__c';
        rulesSetting.FieldSet__c = 'ProductFieldSet';
        rulesSetting.AppliedObject__c = 'Campaign';
        rulesSetting.Module__c = 'Condition';
        rulesSetting.RelatedObject__c = 'TxnLine';
        rulesSetting.RelatedObjectLabel__c = 'Transaction Line';
        rulesSetting.ObjectRelationship__c = 'Product';
        rulesSetting.ObjectLevel__c = 'Line';
        l_rulesSetting.add(rulesSetting);
        
        rulesSetting = new RulesSetting__c();
        rulesSetting.Name = 'Transaction(Transaction)';
        rulesSetting.Object__c = 'Transaction__c';
        rulesSetting.FieldSet__c = 'RuleTransactionFieldSet';
        rulesSetting.AppliedObject__c = 'Campaign';
        rulesSetting.Module__c = 'Condition';
        rulesSetting.RelatedObject__c = 'Txn';
        rulesSetting.RelatedObjectLabel__c = 'Transaction';
        rulesSetting.ObjectRelationship__c = 'Txn';
        rulesSetting.ObjectLevel__c = 'Header';
        l_rulesSetting.add(rulesSetting);
        
        rulesSetting = new RulesSetting__c();
        rulesSetting.Name = 'Shop(Transaction)';
        rulesSetting.Object__c = 'Shop__c';
        rulesSetting.FieldSet__c = 'RuleShopFieldSet';
        rulesSetting.AppliedObject__c = 'Campaign';
        rulesSetting.Module__c = 'Condition';
        rulesSetting.RelatedObject__c = 'Txn';
        rulesSetting.RelatedObjectLabel__c = 'Transaction';
        rulesSetting.ObjectRelationship__c = 'Shop';
        rulesSetting.ObjectLevel__c = 'Header';
        l_rulesSetting.add(rulesSetting);
        
        rulesSetting = new RulesSetting__c();
        rulesSetting.Name = 'Shop(Transaction Line)';
        rulesSetting.Object__c = 'Shop__c';
        rulesSetting.FieldSet__c = 'RuleShopFieldSet';
        rulesSetting.AppliedObject__c = 'Campaign';
        rulesSetting.Module__c = 'Condition';
        rulesSetting.RelatedObject__c = 'TxnLine';
        rulesSetting.RelatedObjectLabel__c = 'Transaction Line';
        rulesSetting.ObjectRelationship__c = 'Shop';
        rulesSetting.ObjectLevel__c = 'Header';
        l_rulesSetting.add(rulesSetting);
        
        rulesSetting = new RulesSetting__c();
        rulesSetting.Name = 'Transaction(Transaction Line)';
        rulesSetting.Object__c = 'Transaction__c';
        rulesSetting.FieldSet__c = 'RuleTransactionFieldSet';
        rulesSetting.AppliedObject__c = 'Campaign';
        rulesSetting.Module__c = 'Condition';
        rulesSetting.RelatedObject__c = 'TxnLine';
        rulesSetting.RelatedObjectLabel__c = 'Transaction Line';
        rulesSetting.ObjectRelationship__c = 'Txn';
        rulesSetting.ObjectLevel__c = 'Header';
        l_rulesSetting.add(rulesSetting);
        
        rulesSetting = new RulesSetting__c();
        rulesSetting.Name = 'Payment Method';
        rulesSetting.Object__c = 'TransactionLineItem__c';
        rulesSetting.FieldSet__c = 'RulesPaymentFieldSet';
        rulesSetting.AppliedObject__c = 'Campaign';
        rulesSetting.Module__c = 'Condition';
        rulesSetting.RelatedObject__c = 'Txn';
        rulesSetting.RelatedObjectLabel__c = 'Transaction';
        rulesSetting.ObjectRelationship__c = 'TxnLine';
        rulesSetting.ObjectLevel__c = 'Line';
        l_rulesSetting.add(rulesSetting);
        
        rulesSetting = new RulesSetting__c();
        rulesSetting.Name = 'Line Item';
        rulesSetting.Object__c = 'TransactionLineItem__c';
        rulesSetting.FieldSet__c = 'RulesLineItemFieldSet';
        rulesSetting.AppliedObject__c = 'Campaign';
        rulesSetting.Module__c = 'Condition';
        rulesSetting.RelatedObject__c = 'TxnLine';
        rulesSetting.RelatedObjectLabel__c = 'Transaction Line';
        rulesSetting.ObjectRelationship__c = 'TxnLine';
        rulesSetting.ObjectLevel__c = 'Line';
        l_rulesSetting.add(rulesSetting);
        
		insert l_rulesSetting;      
    }
    
    public static void createFieldType(){
        
        List<FieldType__c> l_fieldType = new List<FieldType__c>();
        FieldType__c fieldType = new FieldType__c();
        fieldType.Name = 'boolean';
        fieldType.JSONFieldType__c  = 'Boolean';
        fieldType.OperatorMapping__c = '{"Equal To":"="}';
        l_fieldType.add(fieldType);
        
        fieldType = new FieldType__c();
        fieldType.Name = 'currency';
        fieldType.JSONFieldType__c  = 'Number';
        fieldType.OperatorMapping__c = '{"Equal To":"=", "Less Than":"<", "Greater Than":">", "Less Than Or Equal To":"<=", "Greater Than Or Equal To":">=", "Not Equal To":"!=" }';
        l_fieldType.add(fieldType);
        
        fieldType = new FieldType__c();
        fieldType.Name = 'date';
        fieldType.JSONFieldType__c  = 'Date';
        fieldType.OperatorMapping__c = '{"Equal To":"=", "Less Than":"<", "Greater Than":">", "Less Than Or Equal To":"<=", "Greater Than Or Equal To":">=", "Not Equal To":"!=", "Specific Date":"*" }';
        l_fieldType.add(fieldType);
        
        fieldType = new FieldType__c();
        fieldType.Name = 'datetime';
        fieldType.JSONFieldType__c  = 'Time';
        fieldType.OperatorMapping__c = '{"Equal To":"=", "Less Than":"<", "Greater Than":">", "Less Than Or Equal To":"<=", "Greater Than Or Equal To":">=", "Not Equal To":"!=", "Specific Date":"*" }';
        l_fieldType.add(fieldType);
        
        fieldType = new FieldType__c();
        fieldType.Name = 'double';
        fieldType.JSONFieldType__c  = 'Number';
        fieldType.OperatorMapping__c = '{"Equal To":"=", "Less Than":"<", "Greater Than":">", "Less Than Or Equal To":"<=", "Greater Than Or Equal To":">=", "Not Equal To":"!="}';
        l_fieldType.add(fieldType);
        
        fieldType = new FieldType__c();
        fieldType.Name = 'id';
        fieldType.JSONFieldType__c  = 'String';
        fieldType.OperatorMapping__c = '{ "Equal To":"=", "Not Equal To":"!=" }';
        l_fieldType.add(fieldType);
        
        fieldType = new FieldType__c();
        fieldType.Name = 'multipicklist';
        fieldType.JSONFieldType__c  = 'StringList';
        fieldType.OperatorMapping__c = '{ "Equal To":"=", "Not Equal To":"!=" }';
        l_fieldType.add(fieldType);
        
        fieldType = new FieldType__c();
        fieldType.Name = 'picklist';
        fieldType.JSONFieldType__c  = 'String';
        fieldType.OperatorMapping__c = '{ "Equal To":"=", "Not Equal To":"!=" }';
        l_fieldType.add(fieldType);
        
        fieldType = new FieldType__c();
        fieldType.Name = 'reference';
        fieldType.JSONFieldType__c  = 'String';
        fieldType.OperatorMapping__c = '{ "Equal To":"=", "Not Equal To":"!=" }';
        l_fieldType.add(fieldType);
        
        fieldType = new FieldType__c();
        fieldType.Name = 'string';
        fieldType.JSONFieldType__c  = 'String';
        fieldType.OperatorMapping__c = '{ "Equal To":"=", "Not Equal To":"!=" }';
        l_fieldType.add(fieldType);
        
        fieldType = new FieldType__c();
        fieldType.Name = 'time';
        fieldType.JSONFieldType__c  = 'Time';
        fieldType.OperatorMapping__c = '{ "Equal To":"=", "Less Than":"<", "Greater Than":">", "Less Than Or Equal To":"<=", "Greater Than Or Equal To":">=", "Not Equal To":"!=" }';
        l_fieldType.add(fieldType);
        
        insert l_fieldType;
    }
        
    public static void createSpecificDateSetting(){
        
        List<SpecificDateSetting__c> l_SpecificDateSetting = new List<SpecificDateSetting__c>();
        
    	SpecificDateSetting__c specificDateSetting = new SpecificDateSetting__c();
        specificDateSetting.Name = 'Mon';
        specificDateSetting.Label__c = 'Monday';
        specificDateSetting.Seq__c  = 1;
        l_SpecificDateSetting.add(specificDateSetting);
        
        specificDateSetting = new SpecificDateSetting__c();
        specificDateSetting.Name = 'Tue';
        specificDateSetting.Label__c = 'Tuesday';
        specificDateSetting.Seq__c  = 2;
        l_SpecificDateSetting.add(specificDateSetting);
        
        specificDateSetting = new SpecificDateSetting__c();
        specificDateSetting.Name = 'Wed';
        specificDateSetting.Label__c = 'Wednesday';
        specificDateSetting.Seq__c  = 3;
        l_SpecificDateSetting.add(specificDateSetting);
        
        specificDateSetting = new SpecificDateSetting__c();
        specificDateSetting.Name = 'Thu';
        specificDateSetting.Label__c = 'Thursday ';
        specificDateSetting.Seq__c  = 4;
        l_SpecificDateSetting.add(specificDateSetting);
        
        specificDateSetting = new SpecificDateSetting__c();
        specificDateSetting.Name = 'Fri';
        specificDateSetting.Label__c = 'Friday';
        specificDateSetting.Seq__c  = 5;
        l_SpecificDateSetting.add(specificDateSetting);
        
        specificDateSetting = new SpecificDateSetting__c();
        specificDateSetting.Name = 'Sat';
        specificDateSetting.Label__c = 'Saturday';
        specificDateSetting.Seq__c  = 6;
        l_SpecificDateSetting.add(specificDateSetting);
        
        specificDateSetting = new SpecificDateSetting__c();
        specificDateSetting.Name = 'Sun';
        specificDateSetting.Label__c = 'Sunday';
        specificDateSetting.Seq__c  = 7;
        l_SpecificDateSetting.add(specificDateSetting);
        
        insert l_SpecificDateSetting;
        
    }
    
    public static LoyaltySetting__c createNoRecordDisplay(String name){
        LoyaltySetting__c loyaltySetting = new LoyaltySetting__c();
        loyaltySetting.Name = name;
        loyaltySetting.Num__c = 50;
        insert loyaltySetting;
        return loyaltySetting;        
    } 
    
    public static void createIntegrationControl(){
        createHerokuAPIAuthenURLMapping();
        createHerokuAPIGetAppMessageURLMapping();
        createHerokuAPIGetPointMoveURLMapping();
        createHerokuAPIGetRedemptionURLMapping();
        createHerokuAPIGetTxnURLMapping();
        createHerokuAPIGetTxnLineURLMapping();
        createHerokuAPIToken();
        createHerokuIntegrationAppKey();
        createHerokuIntegrationAppSecret();
        createHerokuServerURL();
        createHerokuAPICreatePointAdjustURLMapping();
        createHerokuAPICreateInAppMessageURLMapping();
    }
    
    public static IntegrationControl__c createHerokuAPICreateInAppMessageURLMapping(){
        IntegrationControl__c integrationControl = new IntegrationControl__c();
        integrationControl.Name = '	HerokuAPICreateInAppMessageURLMapping';
        integrationControl.Text__c = '/AppMessage/create';
        insert integrationControl;
        return integrationControl;
    }
    
    public static IntegrationControl__c createHerokuAPICreatePointAdjustURLMapping(){
        IntegrationControl__c integrationControl = new IntegrationControl__c();
        integrationControl.Name = '	HerokuAPICreatePointAdjustURLMapping';
        integrationControl.Text__c = '/transaction/adjust';
        insert integrationControl;
        return integrationControl;
    }
    
    public static IntegrationControl__c createHerokuAPIAuthenURLMapping(){
        IntegrationControl__c integrationControl = new IntegrationControl__c();
        integrationControl.Name = 'HerokuAPIAuthenURLMapping';
        integrationControl.Text__c = '/login';
        insert integrationControl;
        return integrationControl;
    }
    
    public static IntegrationControl__c createHerokuAPIGetAppMessageURLMapping(){
        IntegrationControl__c integrationControl = new IntegrationControl__c();
        integrationControl.Name = 'HerokuAPIGetAppMessageURLMapping';
        integrationControl.Text__c = '/AppMessage/get';
        insert integrationControl;
        return integrationControl;
    }
    
    public static IntegrationControl__c createHerokuAPIGetPointMoveURLMapping(){
        IntegrationControl__c integrationControl = new IntegrationControl__c();
        integrationControl.Name = 'HerokuAPIGetPointMoveURLMapping';
        integrationControl.Text__c = '/PointMove/get';
        insert integrationControl;
        return integrationControl;
    }
    
    public static IntegrationControl__c createHerokuAPIGetRedemptionURLMapping(){
        IntegrationControl__c integrationControl = new IntegrationControl__c();
        integrationControl.Name = 'HerokuAPIGetRedemptionURLMapping';
        integrationControl.Text__c = '/redemption/get';
        insert integrationControl;
        return integrationControl;
    }
    
    public static IntegrationControl__c createHerokuAPIGetTxnURLMapping(){
        IntegrationControl__c integrationControl = new IntegrationControl__c();
        integrationControl.Name = 'HerokuAPIGetTxnURLMapping';
        integrationControl.Text__c = '/transaction/get/master';
        insert integrationControl;
        return integrationControl;
    }
    
    public static IntegrationControl__c createHerokuAPIGetTxnLineURLMapping(){
        IntegrationControl__c integrationControl = new IntegrationControl__c();
        integrationControl.Name = 'HerokuAPIGetTxnLineURLMapping';
        integrationControl.Text__c = '/transaction/get/line';
        insert integrationControl;
        return integrationControl;
    }
    
    public static IntegrationControl__c createHerokuAPIToken(){
        IntegrationControl__c integrationControl = new IntegrationControl__c();
        integrationControl.Name = 'HerokuAPIToken';
        integrationControl.Text__c = 'vFL9m8AE4G0lHiuR4hhHCB4B+jaJVv99zWqtvcNcWQlNvmhWplvytlzTaCoQ83AjQh197qt4F1hb50KRnqW7dtOSX8kCXYxHiz3WosCSAr11GqiBm7iILf9kNwzkP2ZYLyuVPTvcDUVRt3E1wefRWx';
        integrationControl.Text2__c = 'DgPbo655elaEeTw5Bdz/Gpe11ORPdj4QhD4/d/PdtDrE5ZcrkBDJTvOJN9o5HRhnCLeV+APNexv/nuuenEwJNAS/smBX6vdcMS1dKBhu6LHhGuP4co/UxvR0U3k8fDekUO04IZUaWrqeVRERUDrl4=';
        insert integrationControl;
        return integrationControl;
    }
    
    public static IntegrationControl__c createHerokuIntegrationAppKey(){
        IntegrationControl__c integrationControl = new IntegrationControl__c();
        integrationControl.Name = 'HerokuIntegrationAppKey';
        integrationControl.Text__c = 'YATA-SF-CB';
        insert integrationControl;
        return integrationControl;
    }
    
    public static IntegrationControl__c createHerokuIntegrationAppSecret(){
        IntegrationControl__c integrationControl = new IntegrationControl__c();
        integrationControl.Name = 'HerokuIntegrationAppSecret';
        integrationControl.Text__c = 'ulfH3o-WYDK1KlZkigMZBc3HnNrp6yvTjN-J51CagVI';
        insert integrationControl;
        return integrationControl;
    }
    
    public static IntegrationControl__c createHerokuServerURL(){
        IntegrationControl__c integrationControl = new IntegrationControl__c();
        integrationControl.Name = '	HerokuServerURL';
        integrationControl.Text__c = 'https://yata-loyaltyapi-dev.herokuapp.com';
        insert integrationControl;
        return integrationControl;
    }
    
}