public class IncentiveRuleJson {
    
    @AuraEnabled
    public InfoJson infoJson { get; set; }
    
    @AuraEnabled
    public TargetingJson targetingJson { get; set; }
    
    @AuraEnabled
    public ConditionJson conditionJson { get; set; }
    
    @AuraEnabled
    public ActionJson actionJson { get; set; }
    
    public IncentiveRuleJson(InfoJson infoJson, TargetingJson targetingJson, ConditionJson conditionJson, ActionJson actionJson){
        this.infoJson = infoJson;
        this.targetingJson = targetingJson;
        this.conditionJson = conditionJson;
        this.actionJson = actionJson;
    }
    

}