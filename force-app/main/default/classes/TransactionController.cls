public with sharing class TransactionController {
        
    @AuraEnabled
    public static String apex_getTransactionList2(Map<String, String> parameter){
        String transactionList = Common_HerokuIntegration_Cls.getTransactionList2(parameter);
        
        Map<String, Schema.SObjectField> transactionFieldMap = CommonController.apex_getAllFieldset('Transaction__c');
		       
        Map<String, Object> TransactionsMap = (Map<String, Object>) JSON.deserializeUntyped(transactionList);
        List<Object> transactions = (List<Object>) TransactionsMap.get('Transactions');
        for(Object theTransaction : transactions){
            Map<String, Object> transactionMap = (Map<String, Object>)theTransaction;
            CommonController.convertFieldValue(transactionFieldMap, transactionMap);
        }
        return JSON.serialize(TransactionsMap);
    }
    
    @AuraEnabled
    public static String apex_getTransactionLineList(Map<String, String> parameter){
    	String transactionLineListString = Common_HerokuIntegration_Cls.getTransactionLineList(parameter);
        
        Map<String, Schema.SObjectField> transactionLineItemFieldMap = CommonController.apex_getAllFieldset('TransactionLineItem__c');
        Map<String, Schema.SObjectField> pointDetailFieldMap = CommonController.apex_getAllFieldset('PointDetail__c');
        
        Map<String, Object> TransactionsMap = (Map<String, Object>) JSON.deserializeUntyped(transactionLineListString);
        List<Object> transactions = (List<Object>) TransactionsMap.get('Transactions');
        for(Object theTransaction : transactions){
            Map<String, Object> transactionMap = (Map<String, Object>)theTransaction;
            
            List<Object> txnLines = (List<Object>) transactionMap.get('TxnLines');
            for(Object txnLine : txnLines){
                Map<String, Object> txnLineMap = (Map<String, Object>)txnLine;
                CommonController.convertFieldValue(transactionLineItemFieldMap, txnLineMap);
            }
            
            List<Object> pointDetailList = (List<Object>) transactionMap.get('PointDetail');
            for(Object thePointDetail : pointDetailList){
                Map<String, Object> thePointDetailMap = (Map<String, Object>)thePointDetail;
                CommonController.convertFieldValue(pointDetailFieldMap, thePointDetailMap);
            }
            
        }
        return JSON.serialize(TransactionsMap);
    }
    
    @AuraEnabled
    public static Integer apex_getNoRecordDisplay(String name){
        return CommonController.apex_getNoRecordDisplay(name);
    }
    
    @AuraEnabled
    public static String apex_getFieldset(String objectName, String fieldSetName){
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        
        system.debug('@@@objectName: '+objectName);
        system.debug('@@@fieldSetName: '+fieldSetName);
        List<Schema.FieldSetMember> fieldSetMemberList = new List<Schema.FieldSetMember>();
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(objectName);   
        if(SObjectTypeObj != null){
            system.debug('@@@SObjectTypeObj: '+SObjectTypeObj);
            Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
            Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
            if(fieldSetObj != null){
                system.debug('@@@fieldSetObj: '+fieldSetObj);
                
				fieldSetMemberList = fieldSetObj.getFields();
                for(Schema.FieldSetMember fieldSetMemberObj : fieldSetMemberList)
                {
                    system.debug('API Name ====>' + fieldSetMemberObj.getFieldPath()); //api name
                    system.debug('Label ====>' + fieldSetMemberObj.getLabel());
                    system.debug('Required ====>' + fieldSetMemberObj.getRequired());
                    system.debug('DbRequired ====>' + fieldSetMemberObj.getDbRequired());
                    system.debug('Type ====>' + fieldSetMemberObj.getType());   //type - STRING,PICKLIST
                }
            }
        }
        
        String jsonString = JSON.serialize(fieldSetMemberList);
        list<Object> fields = (list<Object>)JSON.deserializeUntyped(jsonString);
        for(object field : fields){
            map<String,Object> f = (map<String,Object>) field;
            if((String)(f.get('label')) == 'Record Type ID'){
                f.put('label','Record Type');
            } 
        }
        jsonString = JSON.serialize(fields);
        return jsonString;
    }
    /*
    @AuraEnabled
    public static DataTableResponse apex_getTransactionRecords(String strObjectName, String strFieldSetName){                
       	
        //Get the fields from FieldSet
        Schema.SObjectType SObjectTypeObj = Schema.getGlobalDescribe().get(strObjectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();            
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(strFieldSetName);
        
        //To hold the table hearders 
        List<DataTableColumns> lstDataColumns = new List<DataTableColumns>();
        
        //Field to be queried - fetched from fieldset
        List<String> lstFieldsToQuery = new List<String>();
        
        //The final wrapper response to return to component
        DataTableResponse response = new DataTableResponse();
        
        Attributes attributesFirst = new Attributes();
        attributesFirst.label = 'View Details';
        attributesFirst.name = 'view_details';
        attributesFirst.title = 'Click to View Details';
        DataTableColumns datacolumnsFirst = new DataTableColumns();
        datacolumnsFirst.label = 'View';
        datacolumnsFirst.fieldName = null;
        datacolumnsFirst.type = 'button';
        datacolumnsFirst.initialWidth = 135;
        datacolumnsFirst.sortable = true;
        datacolumnsFirst.typeAttributes = attributesFirst;   
        lstDataColumns.add(datacolumnsFirst);
        for( Schema.FieldSetMember eachFieldSetMember : fieldSetObj.getFields() ){
            String dataType = String.valueOf(eachFieldSetMember.getType()).toLowerCase();
            //This way we can set the type of a column
            //We do not get the exact type from schema object which matches to lightning:datatable component structure
           	Attributes attributes = new Attributes();
            if(dataType == 'datetime'){
                dataType = 'date';
                attributes.weekday = 'short';
                attributes.year = 'numeric';
                attributes.month = 'short';
                attributes.day = 'numeric';
                attributes.hour = '2-digit';
                attributes.minute = '2-digit';
            }
            else if(dataType == 'double'){
                dataType = 'number';
            }
            
            //Create a wrapper instance and store label, fieldname and type.
            //system.debug('@@@getType '+String.valueOf(eachFieldSetMember.getType()).toLowerCase() );
            //Attributes attributes = new Attributes(null, null, null);
            DataTableColumns datacolumns = new DataTableColumns();
            datacolumns.label = String.valueOf(eachFieldSetMember.getLabel());
            datacolumns.fieldName = String.valueOf(eachFieldSetMember.getFieldPath());
            datacolumns.type = dataType;
            datacolumns.initialWidth = null;
            datacolumns.sortable = true;
            datacolumns.typeAttributes = attributes;                                 
			lstDataColumns.add(datacolumns);
            lstFieldsToQuery.add(String.valueOf(eachFieldSetMember.getFieldPath()));
        }
        
        //Form an SOQL to fetch the data - Set the wrapper instance and return as response
        if(! lstDataColumns.isEmpty()){            
            response.lstDataTableColumns = lstDataColumns;
            //String query = 'SELECT Id, ' + String.join(lstFieldsToQuery, ',') + ' FROM Account';
            //System.debug(query);
            String transactionListString = Common_HerokuIntegration_Cls.getTransactionList('001p000000UOsUrAAL', null, null, null, null, null, null, null, null, null);
            Map<String, Object> transactionMap = (Map<String, Object>) JSON.deserializeUntyped(transactionListString);           
			list<object> transactions = (list<object>)transactionMap.get('Transactions');
            List<Transaction__c> transactionList = new List<Transaction__c>();
            for(object tran : transactions){
            	map<String,Object> txn = (map<String,Object>)((map<String,Object>)tran).get('Txn');
              	
                String pointsexpirydateString = (String) txn.get('pointsexpirydate__c');
                if(pointsexpirydateString != null){
                    DateTime pointsexpiryDatetime = DateTime.Valueof(pointsexpirydateString.replace('T', ' ')); 
                    Date pointsexpiryDate = date.newinstance(pointsexpiryDatetime.year(), pointsexpiryDatetime.month(), pointsexpiryDatetime.day());
                    txn.put('pointsexpirydate__c', pointsexpiryDate);
                }
                
                String transactiondayString = (String) txn.get('transactionday__c');
                if(transactiondayString != null){
                    DateTime transactiondayDatetime = DateTime.Valueof(transactiondayString.replace('T', ' ')); 
                    Date transactiondayDate = date.newinstance(transactiondayDatetime.year(), transactiondayDatetime.month(), transactiondayDatetime.day());
                    txn.put('transactionday__c', transactiondayDate);
                }
				
                String transactiontimeString = (String) txn.get('transactiontime__c');
                Transaction__c theTransactionTime = new Transaction__c();
                if(transactiontimeString != null){
                    List<String> transactiontimeList = transactiontimeString.split(':');
                    Time transactiontime = Time.newInstance(integer.valueOf(transactiontimeList[0]), integer.valueOf(transactiontimeList[1]), integer.valueOf(transactiontimeList[2]), 0);
                    theTransactionTime.transactiontime__c = transactiontime;
                }
                
                
				txn.remove('transactiontime__c');                
                
                
            	Transaction__c theTransaction = (Transaction__c) JSON.deserialize(JSON.serialize(txn), Transaction__c.Class);
				theTransaction.transactiontime__c = theTransactionTime.transactiontime__c;
                
                transactionList.add(theTransaction);
            }
            
            system.debug('@@@transactionList: '+JSON.serialize(transactionList));
            response.lstDataTableData = transactionList;
        }
        system.debug('@@@response: '+response);
        return response;
    }
	*/
    
    /*
    @AuraEnabled
    public static DataTableResponse getsObjectRecords(String strObjectName, String strFieldSetName){                
       	
        //Get the fields from FieldSet
        Schema.SObjectType SObjectTypeObj = Schema.getGlobalDescribe().get(strObjectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();            
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(strFieldSetName);
        
        //To hold the table hearders 
        List<DataTableColumns> lstDataColumns = new List<DataTableColumns>();
        
        //Field to be queried - fetched from fieldset
        List<String> lstFieldsToQuery = new List<String>();
        
        //The final wrapper response to return to component
        DataTableResponse response = new DataTableResponse();
        
        
        for( Schema.FieldSetMember eachFieldSetMember : fieldSetObj.getFields() ){
            String dataType = String.valueOf(eachFieldSetMember.getType()).toLowerCase();
            //This way we can set the type of a column
            //We do not get the exact type from schema object which matches to lightning:datatable component structure
            if(dataType == 'datetime'){
                dataType = 'date';
            }
            //Create a wrapper instance and store label, fieldname and type.
            DataTableColumns datacolumns = new DataTableColumns(String.valueOf(eachFieldSetMember.getLabel()) , 
                                                                String.valueOf(eachFieldSetMember.getFieldPath()), 
                                                                String.valueOf(eachFieldSetMember.getType()).toLowerCase());
			lstDataColumns.add(datacolumns);
            lstFieldsToQuery.add(String.valueOf(eachFieldSetMember.getFieldPath()));
        }
        
        //Form an SOQL to fetch the data - Set the wrapper instance and return as response
        if(! lstDataColumns.isEmpty()){            
            response.lstDataTableColumns = lstDataColumns;
            String query = 'SELECT Id, ' + String.join(lstFieldsToQuery, ',') + ' FROM '+strObjectName;
            System.debug(query);
            response.lstDataTableData = Database.query(query);
        }
        
        return response;
    }
	*/
	/* 
    public Class Attributes{
		@AuraEnabled
        public String label {get;set;}
        @AuraEnabled
        public String name {get;set;}
        @AuraEnabled
        public String title {get;set;}
        @AuraEnabled
        public String weekday {get;set;}
        @AuraEnabled
        public String year {get;set;}
        @AuraEnabled
        public String month {get;set;}
        @AuraEnabled
        public String day {get;set;}
        @AuraEnabled
        public String hour {get;set;}
        @AuraEnabled
        public String minute {get;set;}
        
    }
    
    //Wrapper class to hold Columns with headers
    public class DataTableColumns {
        @AuraEnabled
        public String label {get;set;}
        @AuraEnabled       
        public String fieldName {get;set;}
        @AuraEnabled
        public String type {get;set;}
        @AuraEnabled
        public Integer initialWidth {get;set;}
        @AuraEnabled
        public Boolean sortable {get;set;}
        @AuraEnabled
        public Attributes typeAttributes {get;set;}
		
        public DataTableColumns(){
            
        }
		        
        //Create and set three variables label, fieldname and type as required by the lightning:datatable
        public DataTableColumns(String label, String fieldName, String type){
            this.label = label;
            this.fieldName = fieldName;
            this.type = type;
        }
    }
    
    //Wrapper calss to hold response - This response is used in the lightning:datatable component
    public class DataTableResponse {
        @AuraEnabled
        public List<DataTableColumns> lstDataTableColumns {get;set;}
        @AuraEnabled
        public List<sObject> lstDataTableData {get;set;}                
        
        public DataTableResponse(){
            lstDataTableColumns = new List<DataTableColumns>();
            lstDataTableData = new List<sObject>();
        }
    }
    */
    
    @AuraEnabled
    public static String apex_getRecordType(String objectName){
        return CommonController.apex_getRecordType(objectName);
    }
    
    @AuraEnabled
    public static String apex_getPicklistFieldValues(String objectName, String pickListFieldName){
        return CommonController.apex_getPicklistFieldValues(objectName, pickListFieldName);
    }
    
}