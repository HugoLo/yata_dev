public class ContentTriggerHandler extends TriggerHandler{
    //private static final String ObjName = 'Content__c';
    private static final String ObjName = Trigger.isDelete ? Trigger.Old.getSObjectType().getDescribe().getName() : Trigger.New.getSObjectType().getDescribe().getName();
        
    public ContentTriggerHandler(){}

    protected override void beforeInsert() {
        
        system.debug('@@@beforeInsert');
        if(Common_Security_Cls.toRunTriggerMethod('ContentTriggerHandler','beforeInsert')){
        	
            //To check duplicate content record:
            Map<String,Content__c> contentMap= new Map<String,Content__c>();
            Set<ID> parentIDSet = new Set<ID>();
            Set<String> codeSet = new Set<String>();
            Set<String> languageSet = new Set<String>();
            Set<double> sequenceSet = new Set<double>();
            
            for(Content__c obj : (list<Content__c>)trigger.new)
            {
                String Key = obj.FixContent__c + obj.ContentCode__c + obj.Language__c + obj.Sequence__c;
                
                if(contentMap.keySet().contains(key))
                {
                    obj.addError('Duplicate content record with the same content code, sequence and language.');
                }
                else
                {
                    contentMap.put(Key, obj);
                    parentIDSet.add(obj.FixContent__c);
                    codeSet.add(obj.ContentCode__c);
                    languageSet.add(obj.Language__c);
                    sequenceSet.add(obj.Sequence__c);
                }
            }
            if(contentMap !=null && contentMap.size() > 0)
            {
                for(content__c cont : [select id,FixContent__c,ContentCode__c,Language__c,Sequence__c from content__c WHERE FixContent__c IN :parentIDSet AND ContentCode__c IN :codeSet AND Language__c IN:languageSet AND Sequence__c IN:sequenceSet])
                {
                    String Key = cont.FixContent__c + cont.ContentCode__c + cont.Language__c + cont.Sequence__c;
                    if(contentMap.keySet().contains(key))
                    {
                        contentMap.get(key).addError('Duplicate content record with the same content code, sequence and language.');
                    }
                }
            }
        }
        
    }

    
    
    protected override void beforeUpdate () {
        system.debug('@@@beforeUpdate');
        
        if(Common_Security_Cls.toRunTriggerMethod('ContentTriggerHandler','beforeUpdate')){
            
            //To check duplicate content record:
            Map<String,Content__c> contentMap= new Map<String,Content__c>();
            Set<ID> parentIDSet = new Set<ID>();
            Set<String> codeSet = new Set<String>();
            Set<String> languageSet = new Set<String>();
            Set<double> sequenceSet = new Set<double>();
            Set<Id> existingContIds = new Set<Id>();
            
            for(Content__c obj : (list<Content__c>)trigger.new)
            {
                String Key = obj.FixContent__c + obj.ContentCode__c + obj.Language__c + obj.Sequence__c;
                
                Content__c oldobj = (Content__c) Trigger.OldMap.get(obj.Id);
                if(obj.FixContent__c != oldobj.FixContent__c || obj.ContentCode__c != oldobj.ContentCode__c ||
                   obj.Language__c != oldobj.Language__c || obj.Sequence__c != oldobj.Sequence__c)
                    
                {
                    if(contentMap.keySet().contains(key))
                    {
                        obj.addError('Duplicate content record with the same content code, sequence and language.');
                    }
                    else
                    {
                        contentMap.put(Key, obj);
                        parentIDSet.add(obj.FixContent__c);
                        codeSet.add(obj.ContentCode__c);
                        languageSet.add(obj.Language__c);
                        sequenceSet.add(obj.Sequence__c);
                        existingContIds.add(obj.Id);
                    }
                }          
            }
            if(contentMap !=null && contentMap.size() > 0)
            {
                for(content__c cont : [select id,FixContent__c,ContentCode__c,Language__c,Sequence__c from content__c WHERE ID NOT IN: existingContIds AND FixContent__c IN :parentIDSet AND ContentCode__c IN :codeSet AND Language__c IN:languageSet AND Sequence__c IN:sequenceSet])
                {
                    String Key = cont.FixContent__c + cont.ContentCode__c + cont.Language__c + cont.Sequence__c;
                    
                    if(contentMap.keySet().contains(key))
                    {
                        contentMap.get(key).addError('Duplicate content code with same sequence in same language.');
                    }
                }
            }
        }
		
    }
    
    /*    
    protected override void beforeDelete () {
        system.debug('@@@beforeDelete');
    }
    */    
    protected override void afterInsert () {
        system.debug('@@@Content afterInsert');
        if(Common_Security_Cls.toRunTriggerMethod('ContentTriggerHandler','afterInsert')){
            
            //Create Log shipment records:
            if(Common_Utilities_Cls.needLogShipment(ObjName)){
                LogShipmentHandler.createLogShipment(ObjName, Trigger.new);   
            }
        }
    }
    
    protected override void afterUpdate () {
        system.debug('@@@Content afterUpdate');
        if(Common_Security_Cls.toRunTriggerMethod('ContentTriggerHandler','afterUpdate')){
            
            //Create Log shipment records:
            if(Common_Utilities_Cls.needLogShipment(ObjName)){
                LogShipmentHandler.updateLogShipment(ObjName, Trigger.new, Trigger.oldMap);
            }
        }
    }
    
    protected override void afterDelete () {
        if(Common_Security_Cls.toRunTriggerMethod('ContentTriggerHandler','afterDelete')){
            //Create Log shipment records:
            if(Common_Utilities_Cls.needLogShipment(ObjName)){
                LogShipmentHandler.createDeleteLogShipment(ObjName, Trigger.old);
            }   
        }
    }
    
    
    /*
    protected override void afterUndelete  () {
        system.debug('@@@afterUndelete');        
    }
    */
}