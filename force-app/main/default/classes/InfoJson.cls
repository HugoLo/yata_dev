public class InfoJson {
	
	public class TriggerPoint {
        @AuraEnabled
		public String TriggerSysCon { get; set; }
        @AuraEnabled
		public String TriggerObj { get; set; }
        @AuraEnabled
		public String TriggerAction { get; set; }
        @AuraEnabled
		public String TriggerField { get; set; }
        @AuraEnabled
		public List<String> TriggerFieldValue { get; set; }
	}
	
    @AuraEnabled
	public String IncentiveRuleNo { get; set; }
    @AuraEnabled
	public String RuleName { get; set; }
    @AuraEnabled
	public String RuleFromObj { get; set; }
    @AuraEnabled
	public String RuleFromRecdID { get; set; }
    @AuraEnabled
	public String Status { get; set; }
    @AuraEnabled
	public String StartDate { get; set; }
    @AuraEnabled
	public String EndDate { get; set; }
    @AuraEnabled
	public TriggerPoint TriggerPoint { get; set; }

	
	public static InfoJson parse(String json) {
		return (InfoJson) System.JSON.deserialize(json, InfoJson.class);
	}
}