/********************************************************************************************
*                           Class Name: Common_Security_Cls
* Desciption: To check if the users has access right / need to run trigger
* 
* Version History:
* 2018-06-06 Created by TC (Introv Limited)
*                                   
*-------------------------------------------------------------------------------------------*
*
* 1. toRunTriggerMethod(String className, String methodName):
*   Check whether to run the trigger class method by exact class name and method name
* 
* 2. toRunTriggerMethod(String fullName):
*   Check whether to run the trigger class method by concatenated name, i.e. "ClassName.MethodName"
*
* 3. isUserTriggerOn()
*   Check whether user need to run all triggers; User based on current running user
*
* 4. isUserTriggerOn(String usrId)
*   Check whether user need to run all triggers; User based on user id
*
* 5. getAllTriggerControl()
*   To get all the trigger control list from custom setting by Class and Method
*
* 6. getNameTriggerCtrlMap()
*   To get the mapping of trigger control by full name (class+method name)
*
********************************************************************************************/


public class Common_Security_Cls {
    public static  List<TriggerControl__c> allTriggerControl;
    public static Map<String,Boolean> m_name_isTriggerOn;
    private static Boolean isUserTriggerOn;
    
    /*********1. Check whether to run the trigger class method***************/
    public static Boolean toRunTriggerMethod(String className, String methodName){
        Boolean toRunTrigger = true;
        if(!isUserTriggerOn()){
            toRunTrigger =false;
        }else{
            getNameTriggerCtrlMap();
            if(m_name_isTriggerOn.containsKey(className+'.'+methodName)){
                toRunTrigger = m_name_isTriggerOn.get(className+'.'+methodName);
            }
        }
        return toRunTrigger;
    }
    
    /*********2. Check whether to run the trigger class method***************/
    public static Boolean toRunTriggerMethod(String fullName){
        Boolean toRunTrigger = true;
        if(!isUserTriggerOn()){
            toRunTrigger =false;
        }else{
            getNameTriggerCtrlMap();
            if(m_name_isTriggerOn.containsKey(fullName)){
                toRunTrigger = m_name_isTriggerOn.get(fullName);
            }
        }
        return toRunTrigger;
    }
    
    /*********3. Check whether user need to run all triggers***************/
    public static Boolean isUserTriggerOn(){
        if(isUserTriggerOn==null){
            Boolean tempUserTriggerOn = true;
            User currentUser = [SELECT Id, TriggerOff__c FROM User WHERE Id=:UserInfo.getUserId()];
            if(currentUser!=null && currentUser.TriggerOff__c){
                tempUserTriggerOn =false;   
            }
            isUserTriggerOn= tempUserTriggerOn;
            system.debug('isUserTriggerOn='+isUserTriggerOn);
        }
        return isUserTriggerOn;
    }
    
    /*********4. Check whether user need to run all triggers***************/
    public static Boolean isUserTriggerOn(String usrId){
        Boolean tempUserTriggerOn = true;
        List<User> users = new List<User>();
        users = [SELECT Id, TriggerOff__c FROM User WHERE Id=:usrId];
        if(users.size()>0 && users[0].TriggerOff__c){
            tempUserTriggerOn =false;
        }
        system.debug('isUserTriggerOn='+tempUserTriggerOn);
        return tempUserTriggerOn;
    }
    
    /*********5. To get all the trigger control from custom setting***************/
    public static List<TriggerControl__c> getAllTriggerControl(){
        if(allTriggerControl == null ){
            allTriggerControl = new List<TriggerControl__c>();
            allTriggerControl =TriggerControl__c.getall().values();
        }
        return allTriggerControl;
    }
    
    /*********6. To get the mapping of trigger control***************/
    public static Map<String, Boolean> getNameTriggerCtrlMap(){
        if(m_name_isTriggerOn==null ){
            m_name_isTriggerOn = new Map<String, Boolean>();
            getAllTriggerControl();
            for(TriggerControl__c tc :allTriggerControl){
                m_name_isTriggerOn.put(tc.Class__c+'.'+tc.Method__c, tc.Active__c);
            }
        }
        system.debug('Class and Method with active status='+m_name_isTriggerOn);
        return m_name_isTriggerOn;
    }
}