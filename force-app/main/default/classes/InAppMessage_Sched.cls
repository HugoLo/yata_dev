global class InAppMessage_Sched implements Schedulable {
    
    public boolean testmode = false;
	global void execute(SchedulableContext sc) {
        executeBatch();
	}
    
    global void executeBatch(){
        if (testmode || [SELECT count() FROM AsyncApexJob WHERE JobType='BatchApex' AND (Status = 'Processing' OR Status = 'Preparing') AND ApexClass.Name = 'InAppMessage_Batch'] >= 1){ 
            if ([SELECT count() FROM CronTrigger WHERE  CronJobDetail.Name like '%InAppMessage_Sched%' AND NextFireTime != null] >= 1){
                system.debug('@@@CronTrigger');
            } 
            else {
                //schedule the schedulable class again in 2 mins
                InAppMessage_Sched scRetry = new InAppMessage_Sched(); 
                Datetime dt = Datetime.now();
                dt = dt.addMinutes(2);
                //dt = dt.addSeconds(5);
                String timeForScheduler = dt.format('s m H d M \'?\' yyyy'); 
                Id schedId = System.Schedule('InAppMessage_Sched'+timeForScheduler,timeForScheduler,scRetry);
            }
        } 
        else {
            Integer batchSize = 200;
            InAppMessage_Batch batch = new InAppMessage_Batch();
            database.executebatch(batch, batchSize);
        }
    }
}