@isTest
public class PromotionBannerTriggerHandler_Test {

    
	static testMethod void PromotionBannerTriggerHandler_Test(){
        
        
        User userA = TestClassHelper.createUser();
        System.runAs(userA){
            

            TestClassHelper.createObjectControl('PromotionBanner__c');
            
            PromotionBanner__c PromotionBanner = TestClassHelper.createPromotionBanner();
            PromotionBanner.PinToTop__c = true;
            //PromotionBanner.Status__c = 'Active';
            update PromotionBanner;
           
            
            Date FromDate = System.today();
            FromDate = FromDate.addDays(-30);
            PromotionBanner.FromDate__c = FromDate;
            PromotionBanner.ToDate__c = FromDate.addDays(10);
            update PromotionBanner;
            delete PromotionBanner;
            
            
            PromotionBanner__c PromotionBanner2 = TestClassHelper.createPromotionBanner();
            
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new HTTPMockCallout());
            PromotionBannerDailyJobs_Schedule sched = new PromotionBannerDailyJobs_Schedule ();   
			String chron = '0 0 23 * * ?';        
			system.schedule('Test Sched', chron, sched);
            
            Test.stopTest();
            
        }
    }
    
    public class HTTPMockCallout implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(200);
            string jsonResBody = '{"ReturnCode":"1","ReturnMessage":"All point adjustments complete","ReturnDetails":[{"RecordId":"9cf6d9b9-009b-4b51-8f6f-0995572e4c41","ReturnCode":"1","ReturnMessage":"success","txnPGID":null}]}';
            res.setBody(jsonResBody);
            return res;
        }
    }
    	
}