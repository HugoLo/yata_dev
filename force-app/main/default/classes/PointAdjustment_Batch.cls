global class PointAdjustment_Batch implements Database.Batchable<sObject>, Database.AllowsCallouts {
    private String query;
	
	global PointAdjustment_Batch() {
        
		List<CronTrigger> l_ct = new List<CronTrigger>();
        l_ct = [SELECT Id,CronJobDetailId,CronJobDetail.Name,NextFireTime,PreviousFireTime,State,StartTime,EndTime,CronExpression,TimeZoneSidKey,OwnerId,TimesTriggered 
                FROM CronTrigger
                WHERE CronJobDetail.Name like '%BatchPointAdjustment_Sched%' AND NextFireTime= null];
        if(l_ct!=null && l_ct.size()>0){
            System.abortJob(l_ct[0].id);
        }
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator([SELECT Id, CountryCode__c, ExternalRemark__c,
                                         InternalRemark__c, Member__c, MembershipNo__c,
                                         MobileNo__c, PointAdjustment__c, RetryTimes__c,
                                         Status__c, SyncTime__c, SystemLog__c,
                                         TransactionDate__c, SubType__c
                                         FROM PointAdjustment__c  
                                         WHERE Status__c != 'Completed' 
                                         AND (RetryTimes__c < 3 OR RetryTimes__c = null)
                                         ORDER BY TransactionDate__c, CreatedDate, Member__c ASC
                                        ]);
	}

   	global void execute(Database.BatchableContext BC, List<PointAdjustment__c> scope) {
        
        String successCode = '1';
        Map<Id, PointAdjustment__c> m_Id_PointAdj = new Map<Id, PointAdjustment__c> ();
        Id RecordType = Common_RecordType_Cls.getRtId('Transaction__cPointAdjustment');
        
        List<Common_HerokuIntegration_Cls.adjustmentWrapperDetail> l_pointAdj = new List<Common_HerokuIntegration_Cls.adjustmentWrapperDetail>();
        Common_HerokuIntegration_Cls.adjustmentWrapper pointAdj_Wrapper = new Common_HerokuIntegration_Cls.adjustmentWrapper();
        for(PointAdjustment__c pa : scope){
            Common_HerokuIntegration_Cls.adjustmentWrapperDetail pointAdj_WrapperDetail = new Common_HerokuIntegration_Cls.adjustmentWrapperDetail();
            pointAdj_WrapperDetail.RecordId = pa.Id;
            pointAdj_WrapperDetail.MemberSFId = pa.Member__c;
            pointAdj_WrapperDetail.RecordType = RecordType;
            pointAdj_WrapperDetail.SubType = pa.SubType__c;
            pointAdj_WrapperDetail.TransferDateTime = pa.TransactionDate__c;
            pointAdj_WrapperDetail.Points = pa.PointAdjustment__c;
            pointAdj_WrapperDetail.ExtRemark = pa.ExternalRemark__c;
            pointAdj_WrapperDetail.IntRemark = pa.InternalRemark__c;
            l_pointAdj.add(pointAdj_WrapperDetail);
            m_Id_PointAdj.put(pa.Id, pa);
            pa.Status__c ='In Progress';
        }
        pointAdj_Wrapper.adjustList = l_pointAdj;
        //update scope;		//Change status to "In Progress"
		      
        
        Common_HerokuIntegration_Cls.adjustmentResponse adjResponse = Common_HerokuIntegration_Cls.batchAdjustPoint(JSON.serialize(pointAdj_Wrapper));
        if(adjResponse!=null ){
            
            //All Success:
            if(adjResponse.ReturnCode == successCode){
                for(PointAdjustment__c pa : scope){
                    pa.SyncTime__c = system.now();
                    pa.RetryTimes__c = pa.RetryTimes__c != null ? (pa.RetryTimes__c+1) : 1;
                	pa.Status__c = 'Completed';
                }
                Database.update(scope, false) ;
            }
            //Partial Success / All Failed:
            else{
                if(adjResponse.ReturnDetails != null && adjResponse.ReturnDetails.size() > 0){
                    for(Common_HerokuIntegration_Cls.returnDetail dt: adjResponse.ReturnDetails){
                        m_Id_PointAdj.get(dt.RecordId).SyncTime__c = system.now();
                        m_Id_PointAdj.get(dt.RecordId).RetryTimes__c = m_Id_PointAdj.get(dt.RecordId).RetryTimes__c != null ? (m_Id_PointAdj.get(dt.RecordId).RetryTimes__c+1) : 1;
                        if(dt.ReturnCode == successCode){
                            m_Id_PointAdj.get(dt.RecordId).Status__c = 'Completed';
                        }else{
                            m_Id_PointAdj.get(dt.RecordId).Status__c = 'Failed';
                            m_Id_PointAdj.get(dt.RecordId).SystemLog__c = dt.ReturnMessage;
                        }
                    }
                }else{
                    for(PointAdjustment__c pa : scope){
                        pa.SyncTime__c = system.now();
                        pa.RetryTimes__c = pa.RetryTimes__c != null ? (pa.RetryTimes__c+1) : 1;
                        pa.SystemLog__c ='Connect to Heroku failed.';
                    }
                }
                Database.update(scope, false) ;
            }
        }
		
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}