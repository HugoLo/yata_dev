@isTest
public class PointMovementController_Test {
	
    static testMethod void PointMovementController_Test(){
        TestClassHelper.createIntegrationControl();
        Account account = TestClassHelper.createMember();
        Map<String, String> parameter = new Map<String, String>();
        parameter.put('MemberSFId', account.Id);
        parameter.put('RecordType', null);
        parameter.put('TransferDateFrom', '2018-10-03 00:00:00');
        parameter.put('TransferDateTo', '2018-11-03 00:00:00');
        parameter.put('limit', '10');
        
        User userA = TestClassHelper.createUser();
        System.runAs(userA){
            
            
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new HTTPMockCallout());
            String responseString = PointMovementController.apex_getPointMovementList(parameter);
            String fieldset = PointMovementController.apex_getFieldset('PointsMovement__c', 'PointsMovementFieldSetPreview');
            String recordType = PointMovementController.apex_getRecordType('PointsMovement__c');
            Test.setMock(HttpCalloutMock.class, new HTTPMockTokenCallout());
            Common_HerokuIntegration_Cls.getHerokuToken();
            Test.stopTest();
            
            
            
        }
        
    }
    
    public class HTTPMockTokenCallout implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(200);
            string jsonResBody = '{"token":"vFL9m8AE4G0lHiuR4hhHCB4B+jaJVv99zWqtvcNcWQlNvmhWplvytlzTaCoQ83AjQh197qt4F1hb50KRnqW7dtOSX8kCXYxHiz3WosCSAr11GqiBm7iILf9kNwzkP2ZYLyuVPTvcDUVRt3E1wefRWxDgPbo655elaEeTw5Bdz/Gpe11ORPdj4QhD4/d/PdtDrE5ZcrkBDJTvOJN9o5HRhkG+q8XDmq47vy2W9yML3cltL0JGMtxoSYfqpQrbh7hZ7Q0z68wultwXEZ+Ocpny9IjC3NaOIn2KnbrksKdiPjQ="}';
            res.setBody(jsonResBody);
            return res;
        }
    }
    
    public class HTTPMockCallout implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(200);
            string jsonResBody = '{"ReturnCode":"1","ReturnMessage":"Success","PointMovements":[{"PointMovement":{"points__c":-1000,"referencenopgid__c":117,"recordtypeid":"0127F000000BZi0QAG","datetransfer__c":"2018-10-03T00:00:00.000Z","name":null,"memberpgid__c":93,"lastmodifieddate":"2018-10-03T09:13:22.896Z","ownerid":null,"relatedmember__c":"001p000000WRU7nAAH","isdeleted":null,"systemmodstamp":null,"lastmodifiedbyid":"001p000000URpupAAD","pgid__c":116,"lastactivitydate":null,"createddate":"2018-10-03T09:13:22.896Z","referenceno__c":null,"createdbyid":"001p000000URpupAAD","relatedmemberpgid__c":305,"member__c":"001p000000URpupAAD","sfid":null,"remarks__c":"+852 44775588","member__c_name":"*Gordon Hoo","referencenopgid__c_name":null,"relatedmember__c_name":"ghghj hhjijj","recordtypeid_name":"Transfer Out"},"MovementDetails":[{"name":null,"lastmodifieddate":"2018-10-03T09:13:22.896Z","ownerid":null,"isdeleted":null,"systemmodstamp":null,"lastmodifiedbyid":"001p000000URpupAAD","pointsmovement__c":null,"point__c":-1000,"createddate":"2018-10-03T09:13:22.896Z","createdbyid":"001p000000URpupAAD","pointsmovementpgid__c":116,"sfid":null,"pgid__c":125,"expirydate__c":"2018-12-31T00:00:00.000Z","fy__c":2018}]}]}';
            res.setBody(jsonResBody);
            return res;
        }
    }
    
}