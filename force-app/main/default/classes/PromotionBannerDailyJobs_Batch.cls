global class PromotionBannerDailyJobs_Batch implements Database.Batchable<sObject> {
    
	global PromotionBannerDailyJobs_Batch() {
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        Date today = Common_Utilities_Cls.getToday();
        return Database.getQueryLocator([SELECT Id 
                                         FROM PromotionBanner__c 
                                         WHERE ToDate__c < :today AND Status__c != 'Ended'
                                        ]);
    }

    global void execute(Database.BatchableContext BC, List<PromotionBanner__c> scope) {
        for(PromotionBanner__c promotionBanner : scope){
            promotionBanner.Status__c = 'Ended';
        }
        update scope;
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }    
    
}