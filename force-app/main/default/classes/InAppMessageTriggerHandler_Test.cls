@isTest
public class InAppMessageTriggerHandler_Test {
    
	static testMethod void InAppMessageTriggerHandler_Test1(){
        TestClassHelper.createIntegrationControl();
        User userA = TestClassHelper.createUser();
        System.runAs(userA){
            
            /*
           	Test.StartTest();
            Account account = TestClassHelper.createMember();
            database.executebatch(new InAppMessage_Batch(),200);
     
            InAppMessage_Sched scRetry = new InAppMessage_Sched(); 
            Datetime dt = Datetime.now();
            dt = dt.addMinutes(2);
            String timeForScheduler = dt.format('s m H d M \'?\' yyyy'); 
            Id schedId = System.Schedule('InAppMessage_Sched'+timeForScheduler,timeForScheduler,scRetry);
            System.Schedule('InAppMessage_Sched1'+timeForScheduler,timeForScheduler,scRetry);
            Test.stopTest();
			*/
        }
    }
    
    static testMethod void InAppMessageTriggerHandler_Test2(){
        TestClassHelper.createIntegrationControl();
        User userA = TestClassHelper.createUser();
        System.runAs(userA){
           	
            Test.StartTest();
            
            
            
            Account account = TestClassHelper.createMember();
            

            
            Test.setMock(HttpCalloutMock.class, new HTTPMockCallout());
            List<InAppMessage__c> l_InAppMessage = new List<InAppMessage__c>();
            InAppMessage__c inappmessage = new InAppMessage__c();
            inappmessage.TitleC__c = 'testC';
            inappmessage.TitleE__c = 'testE';
            inappmessage.ContentC__c = 'testC';
            inappmessage.ContentE__c = 'testE';
            inappmessage.Member__c = account.Id;
            l_InAppMessage.add(inappmessage);
            
            inappmessage = new InAppMessage__c();
            inappmessage.TitleC__c = 'testC';
            inappmessage.TitleE__c = 'testE';
            inappmessage.ContentC__c = 'testC';
            inappmessage.ContentE__c = 'testE';
            l_InAppMessage.add(inappmessage);
            
            inappmessage = new InAppMessage__c();
            inappmessage.TitleE__c = 'testE';
            inappmessage.ContentC__c = 'testC';
            inappmessage.ContentE__c = 'testE';
            inappmessage.Member__c = account.Id;
            l_InAppMessage.add(inappmessage);
            
            inappmessage = new InAppMessage__c();
            inappmessage.TitleC__c = 'testC';
            inappmessage.ContentC__c = 'testC';
            inappmessage.ContentE__c = 'testE';
            inappmessage.Member__c = account.Id;
            l_InAppMessage.add(inappmessage);
            
            inappmessage = new InAppMessage__c();
            inappmessage.TitleC__c = 'testC';
            inappmessage.TitleE__c = 'testE';
            inappmessage.ContentE__c = 'testE';
            inappmessage.Member__c = account.Id;
            l_InAppMessage.add(inappmessage);
            
            inappmessage = new InAppMessage__c();
            inappmessage.TitleC__c = 'testC';
            inappmessage.TitleE__c = 'testE';
            inappmessage.ContentC__c = 'testC';
            inappmessage.Member__c = account.Id;
            l_InAppMessage.add(inappmessage);
            
            insert l_InAppMessage;
            
            
            InAppMessage_Sched scRetry = new InAppMessage_Sched(); 
            scRetry.testmode = true;
            scRetry.executeBatch();

            
            Test.stopTest();
        }
    }
    
    public class HTTPMockCallout implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(200);
            string jsonResBody = '{"ReturnCode":"1","ReturnMessage":"All in-App messages complete","ReturnDetails":[{"ReturnCode":"1","ReturnMessage":"success","RecordId":"aaaaaa","appMsgPGID":4634},{"ReturnCode":"1","ReturnMessage":"success","RecordId":"bbbbbb","appMsgPGID":4635}]}';
            res.setBody(jsonResBody);
            return res;
        }
    }
    
}