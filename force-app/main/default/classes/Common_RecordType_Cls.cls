/********************************************************************************************
*							Class Name: Common_RecordType_Cls
* Desciption: Class that handles logs RecordType object in memory to not consume Querys
* 
* Version History:
* 2018-06-06 Created by TC (Introv Limited)
*                                   
*-------------------------------------------------------------------------------------------*
*
*
********************************************************************************************/

public class Common_RecordType_Cls {
    
	private static map<String, List<RecordType>> mapObjTypeListRt = new map<String, List<RecordType>>();
    private static map<String, RecordType> mapObjTypeDevNameId = new map<String, RecordType>();
    private static map<Id, RecordType> mapIdRt = new map<Id, RecordType>();
    
    static{
        //This method works as "Constructor" and its only function is to load the maps
        mapIdRt = new map<Id, RecordType>([SELECT Id, DeveloperName, Description, Name, IsActive, SystemModstamp, SobjectType, NamespacePrefix, LastModifiedDate, LastModifiedById, CreatedDate, CreatedById, BusinessProcessId FROM RecordType]);
        for(RecordType rt: mapIdRt.values()){
            mapObjTypeDevNameId.put(rt.SobjectType+rt.DeveloperName, rt);
            
            if(mapObjTypeListRt.containsKey(rt.SobjectType)){
                mapObjTypeListRt.get(rt.SobjectType).add(rt);
            }else{
                mapObjTypeListRt.put(rt.SobjectType, new List<RecordType>{rt});
            }
        }
    }
    
    public static RecordType getRt(Id rtId){
        //Get the Id of a RecordType and returns the integer register RecordType
        return mapIdRt.get(rtId);
    }
    public static string getRTName(id rtId){
        return mapIdRt.get(rtId).Name;
    }
    public static String getRtDevName(Id rtId){
        //Get the Id of a RecordType and returns the integer register RecordType
        String DevName;
        if (mapIdRt.containskey(rtId)){
            DevName = mapIdRt.get(rtId).DeveloperName;
        }
        return DevName;
    }
    public static RecordType getRtRec(String SobjectTypeDevName){
        //Get concatenation of SObjectType + developername and returns the integer register RecordType
        return mapObjTypeDevNameId.get(SobjectTypeDevName);
    }
    public static Id getRtId(String SobjectTypeDevName){
        //Get concatenation of SObjectType + developername and returns the Id of the RecordType
        Id idReturn;
        if(mapObjTypeDevNameId.containsKey(SobjectTypeDevName)){idReturn = mapObjTypeDevNameId.get(SobjectTypeDevName).Id;}
        return idReturn;
    }
    public static List<RecordType> getRtList(String SobjectType){
        //Get a type of SObject and returns the list of RecordTypes of that type, if the type of object is not found returns an empty list
        List<RecordType> listRt = new List<RecordType>();
        if(mapObjTypeListRt.containsKey(SobjectType)){listRt = mapObjTypeListRt.get(SobjectType);}
        return listRt;
    }
    public static set<Id> getRtIdSet(String SobjectType, Set<String> setDevName){
        //Get a set with the DeveloperNames and a String containing the SObjectType. Returns a Set with the Ids of RecordTypes if you all, but returns an empty set
        set<Id> setRtId = new set<Id>();
        for(RecordType rt: mapObjTypeListRt.get(SobjectType)){
            if(setDevName.contains(rt.DeveloperName)){
                setRtId.add(rt.id);
            }
        }
        if(setDevName.size() != setRtId.size()){setRtId = new set<Id>();}
        return setRtId;
    }
}