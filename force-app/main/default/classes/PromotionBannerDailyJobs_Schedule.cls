global class PromotionBannerDailyJobs_Schedule implements Schedulable{
	global void execute(SchedulableContext sc) {
		PromotionBannerDailyJobs_Batch batch = new PromotionBannerDailyJobs_Batch();
		database.executebatch(batch);
	}
}