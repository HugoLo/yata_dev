/********************************************************************************************
*                           Class Name: LogShipmentHandler
* Desciption: To run LogShipment
* 
* Version History:
* 2018-06-06 Created by Hugo (Introv Limited)
* 2018-08-15 Updated by Gordon (Introv Limited)
*                                   
*-------------------------------------------------------------------------------------------*
*
* 1. createLogShipment(String ObjName, List<sObject> newRecordList):
*   Create logShipment from other object.
* 
* 2. updateLogShipment(String ObjName, List<sObject> newRecordList, Map<Id, sObject> oldRecordMap):
*   Create logShipment from other object.
*
* 3. createDeleteLogShipment(String ObjName, List<sObject> oldRecordList):
*   Create logShipment from other object.
*
* 4. logShipmentUpdateRecord(List<sObject> newRecordList):
*   Update record from logshipment.
*
* 5. updateLogShipmentStatus(Set<Id> s_LSIds):
*   Update status to Complete.
*
*
********************************************************************************************/

public class LogShipmentHandler {
 
    public static List<Schema.FieldSetMember> apex_getFieldset(String objectName, String fieldSetName){
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        
        system.debug('@@@objectName: '+objectName);
        system.debug('@@@fieldSetName: '+fieldSetName);
        List<Schema.FieldSetMember> fieldSetMemberList = new List<Schema.FieldSetMember>();
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(objectName);   
        if(SObjectTypeObj != null){
            system.debug('@@@SObjectTypeObj: '+SObjectTypeObj);
            Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
            Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
            
            if(fieldSetObj != null){
                system.debug('@@@fieldSetObj: '+fieldSetObj);
                
				fieldSetMemberList = fieldSetObj.getFields();
                
                for(Schema.FieldSetMember fieldSetMemberObj : fieldSetMemberList)
                {
                    system.debug('API Name ====>' + fieldSetMemberObj.getFieldPath()); //api name
                    system.debug('Label ====>' + fieldSetMemberObj.getLabel());
                    system.debug('Required ====>' + fieldSetMemberObj.getRequired());
                    system.debug('DbRequired ====>' + fieldSetMemberObj.getDbRequired());
                    system.debug('Type ====>' + fieldSetMemberObj.getType());   //type - STRING,PICKLIST
                }
				
            }
			
        }
       	return fieldSetMemberList;
    }
    
    public static void createLogShipment(String ObjName, List<sObject> newRecordList){
        
        List<LogShipment__c> logShipmentList = new List<LogShipment__c>();
        
    
        for(sObject newRecord : newRecordList){
            
            Boolean logChecking = (Boolean) newRecord.get('LogChecking__c');
            Decimal pgid;
            try{
                pgid = (Decimal) newRecord.get('PGID__c');
            }catch(exception e){
                pgid =null;
            }
//            Decimal pgid pgid = (Decimal) newRecord.get('PGID__c');
            if(!logChecking){
                //JSON.deserialize
                String newRecordJsonString = JSON.serialize(newRecord);
                system.debug('@@@createLogShipment || !logChecking || '+newRecordJsonString);
                LogShipment__c logShipment = new LogShipment__c();
                logShipment.Detail__c = newRecordJsonString;
                logShipment.Source__c = 'S';
                logShipment.Status__c = 'Open';
                logShipment.DateLog__c = Datetime.now();
                logShipment.ActionType__c = 'Insert';
                logShipment.Object__c = ObjName;
                logShipmentList.add(logShipment);
            }
            else if(pgid != null){
                	
                //JSON.deserialize
                String newRecordJsonString = JSON.serialize(newRecord);
                system.debug('@@@createLogShipment || pgid != null || '+newRecordJsonString);
                LogShipment__c logShipment = new LogShipment__c();
                logShipment.Detail__c = newRecordJsonString;
                logShipment.Source__c = 'S';
                logShipment.Status__c = 'Open';
                logShipment.DateLog__c = Datetime.now();
                logShipment.ActionType__c = 'Update';
                logShipment.Object__c = ObjName;
                logShipmentList.add(logShipment);
                
            }
        }
        
        System.Savepoint pSavepoint = Database.setSavepoint();
     
            if(logShipmentList.size() > 0){
                insert logShipmentList;
                System.debug('Log shipment record created');
            }

    }
    
    public static void updateLogShipment(String ObjName, List<sObject> newRecordList, Map<Id, sObject> oldRecordMap){
        
        List<LogShipment__c> logShipmentList = new List<LogShipment__c>();
     	//Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        //Map <String, Schema.SObjectField> fieldMap = schemaMap.get(ObjName).getDescribe().fields.getMap();
        Map<String, FieldsetControl__c> FieldsetControlMap = FieldsetControl__c.getAll();
        FieldsetControl__c fieldsetControl_trigger = FieldsetControlMap.get(ObjName+'Trigger');
        FieldsetControl__c fieldsetControl_detail = FieldsetControlMap.get(ObjName+'Detail');
        if(fieldsetControl_trigger != null && fieldsetControl_detail != null){
            List<Schema.FieldSetMember> fieldset_trigger = apex_getFieldset(ObjName, fieldsetControl_trigger.Fieldset_Name__c);
            List<Schema.FieldSetMember> fieldset_detail = apex_getFieldset(ObjName, fieldsetControl_detail.Fieldset_Name__c);
            
            for(sObject newRecord : newRecordList){
                
                Id newRecordId = (Id) newRecord.get('Id');
                system.debug('@@@newRecord: '+newRecord);
                sObject oldRecord = oldRecordMap.get(newRecordId);
                Boolean newLogChecking = (Boolean) newRecord.get('LogChecking__c');
                Boolean oldLogChecking = (Boolean) oldRecord.get('LogChecking__c');
                if(newLogChecking == oldLogChecking){
                    /*
                    Map<String, Object> jsonMap = new Map<String, Object>();
                    for(Schema.SObjectField sfield : fieldMap.Values()){
                        schema.describefieldresult dfield = sfield.getDescribe();
                        String fieldname = dfield.getname();
                        if(newRecord.get(fieldname) != oldRecord.get(fieldname)){
                            try { 
                                jsonMap.put(fieldname, newRecord.get(fieldname)); 
                            } 
                            catch(Exception e) { 
                                jsonMap.put(fieldname, null); 
                            }
                        }
                    }
                    system.debug('@@@Init Detail: '+JSON.serialize(jsonMap));
                    if(jsonMap.size() > 0){
                        jsonMap.put('Id', newRecord.get('Id'));
                        jsonMap.put('PGID__c', newRecord.get('PGID__c'));
                        String newRecordJsonString = JSON.serialize(jsonMap);
                        system.debug('@@@Final Detail: '+newRecordJsonString);
                        LogShipment__c logShipment = new LogShipment__c();
                        logShipment.Detail__c = newRecordJsonString;
                        logShipment.Source__c = 'S';
                        logShipment.Status__c = 'Open';
                        logShipment.DateLog__c = Datetime.now();
                        logShipment.ActionType__c = 'Update';
                        logShipment.Object__c = ObjName;
                        logShipmentList.add(logShipment);
                    }
                    */
                    Boolean genLogshipment = false;
                    for(Schema.FieldSetMember fieldSetMemberObj : fieldset_trigger){
                        String fieldname = fieldSetMemberObj.getFieldPath();
                        if(newRecord.get(fieldname) != oldRecord.get(fieldname)){
                            genLogshipment = true;
                            break;
                        }
                    }
                    
                    if(genLogshipment){
                        Map<String, Object> jsonMap = new Map<String, Object>();
                        for(Schema.FieldSetMember sfield : fieldset_detail){
                            String fieldname = sfield.getFieldPath();
                            try { 
                                jsonMap.put(fieldname, newRecord.get(fieldname)); 
                            } 
                            catch(Exception e) { 
                                jsonMap.put(fieldname, null); 
                            }
                        }
                        system.debug('@@@Init Detail: '+JSON.serialize(jsonMap));
                        if(jsonMap.size() > 0){
                            jsonMap.put('Id', newRecord.get('Id'));
                            jsonMap.put('PGID__c', newRecord.get('PGID__c'));
                            String newRecordJsonString = JSON.serialize(jsonMap);
                            system.debug('@@@Final Detail: '+newRecordJsonString);
                            LogShipment__c logShipment = new LogShipment__c();
                            logShipment.Detail__c = newRecordJsonString;
                            logShipment.Source__c = 'S';
                            logShipment.Status__c = 'Open';
                            logShipment.DateLog__c = Datetime.now();
                            logShipment.ActionType__c = 'Update';
                            logShipment.Object__c = ObjName;
                            logShipmentList.add(logShipment);
                        }
                    }
                }
            }
            
            
            //System.Savepoint pSavepoint = Database.setSavepoint();
    
            //try{
            
            if(logShipmentList.size() > 0){
                insert logShipmentList;
                System.debug('Log shipment record created');
            }
                
            
            //}
            //catch(Exception e){
                //String strError = e.getMessage() +'|'+ String.valueOf(e.getLineNumber());
                //System.debug('Insert Error: ' + strError);
                //Database.rollback(pSavepoint);    
            //} 
        }
        
    }
    
    public static void createDeleteLogShipment(String ObjName, List<sObject> oldRecordList){
        
        List<LogShipment__c> logShipmentList = new List<LogShipment__c>();
     
        for(sObject oldRecord : oldRecordList){
            
            //JSON.deserialize
            String oldRecordJsonString = JSON.serialize(oldRecord);
            system.debug('@@@createDeleteLogShipment: '+oldRecordJsonString);
            LogShipment__c logShipment = new LogShipment__c();
            logShipment.Detail__c = oldRecordJsonString;
            logShipment.Source__c = 'S';
            logShipment.Status__c = 'Open';
            logShipment.ActionType__c = 'Delete';
            logShipment.Object__c = ObjName;
            logShipmentList.add(logShipment);
            
        }
        
        if(logShipmentList.size() > 0){
            insert logShipmentList;
            System.debug('Log shipment record created');
        }
         
    }
    
    /*
    public static void logShipmentUpdateRecord(List<sObject> newRecordList){
        system.debug('@@@logShipmentUpdateRecord');
        List<BBInformation__c> bbInfoToUpdate = new List<BBInformation__c>();
        List<BBInformation__c> bbInfoToInsert = new List<BBInformation__c>();
        List<BBInformation__c> bbInfoToDelete = new List<BBInformation__c>();
        List<Subscription__c> subscriptionToUpdate = new List<Subscription__c>();
        List<Subscription__c> subscriptionToInsert = new List<Subscription__c>();
        List<Subscription__c> subscriptionToDelete = new List<Subscription__c>();
        
        
        List<LogShipment__c> logShipmentList = new List<LogShipment__c>();
        Map<String, Map<String, Map<String, Object>>> updateObjectMap = new Map<String, Map<String, Map<String, Object>>>();
        for(sObject newRecord : newRecordList){
            
            LogShipment__c logShipment = (LogShipment__c) newRecord;
            String detail = logShipment.Detail__c;
            String source = logShipment.Source__c;
            String status = logShipment.Status__c;
            String objName = logShipment.Object__c;
            String actionType = logShipment.ActionType__c;
            
            
            if(source == 'H' && status == 'Open' && actionType == 'Update'){
                system.debug('@@@Heroku Update Record');
                system.debug('@@@detail Update: '+detail);
                Map<String, Object> meta = (Map<String, Object>) JSON.deserializeUntyped(detail);
                Object objectPgid = meta.get('PGID__c');
                Decimal pgid;
                if(objectPgid instanceof Decimal){
                    pgid = (Decimal)meta.get('PGID__c');
                }
                else if(objectPgid instanceof String){
                    pgid = Decimal.valueOf((String)meta.get('PGID__c'));
                }
                String pgidString = String.valueOf(pgid);
           		if(updateObjectMap.containsKey(objName)){
                    Map<String, Map<String, Object>> updateFieldMap = updateObjectMap.get(objName);
                    if(updateFieldMap.containsKey(pgidString)){
                        Map<String, Object> oldmeta = updateFieldMap.get(pgidString);
                        for(String key : meta.keySet()){
                            if(meta.get(key) != null){
                                oldmeta.put(key, meta.get(key));    
                            }
                        }
                        updateFieldMap.put(pgidString, oldmeta);   
                    }
                    else{
                        updateFieldMap.put(pgidString, meta);   
                    }
                    updateObjectMap.put(objName, updateFieldMap);   
                }
                else{
                    Map<String, Map<String, Object>> updateFieldMap = new Map<String, Map<String, Object>>();
                    updateFieldMap.put(pgidString, meta);
                    updateObjectMap.put(objName, updateFieldMap);
                }
            }
            
            if(source == 'H' && status == 'Open' && actionType == 'Insert'){
                system.debug('@@@Heroku Insert Record');
                system.debug('@@@detail Insert: '+detail);
                Map<String, Object> meta = (Map<String, Object>) JSON.deserializeUntyped(detail);
                if(objName == 'BBInformation__c'){
                    if(actionType == 'Insert'){
                        meta.remove('id');
                        BBInformation__c obj = (BBInformation__c) JSON.deserialize( JSON.serialize( meta ), BBInformation__c.class );
                        system.debug('@@@BBInformation__c Insert obj: '+obj);
                        obj.LogChecking__c = obj.LogChecking__c ? false : true;
                        obj.OwnerId = UserInfo.getUserId();
                        bbInfoToInsert.add(obj);
                    }
                }
                if(objName == 'Subscription__c'){
                    if(actionType == 'Insert'){
                        meta.remove('id');
                        Subscription__c obj = (Subscription__c) JSON.deserialize( JSON.serialize( meta ), Subscription__c.class );
                        system.debug('@@@Subscription__c Insert obj: '+obj);
                        obj.LogChecking__c = obj.LogChecking__c ? false : true;
                        obj.OwnerId = UserInfo.getUserId();
                        subscriptionToInsert.add(obj);
                    }
                }
            }
        }
        Set<Decimal> subscription_pgid_set = new Set<Decimal>();
        Set<Decimal> bbInformation_pgid_set = new Set<Decimal>();
        
        for(String objectkey : updateObjectMap.keySet()){
            Map<String, Map<String, Object>> updateRecordMap = updateObjectMap.get(objectkey);
            for(String pgidString : updateRecordMap.keySet()){
                Map<String, Object> meta = updateRecordMap.get(pgidString);
				String idString = String.valueOf(meta.get('Id'));
                system.debug('@@@idString: '+idString);
                if(String.isBlank(idString)){
                    Object objectPgid = meta.get('PGID__c');
                    Decimal pgid;
                    if(objectPgid instanceof Decimal){
                        pgid = (Decimal)meta.get('PGID__c');
                    }
                    else if(objectPgid instanceof String){
                        pgid = Decimal.valueOf((String)meta.get('PGID__c'));
                    }
                    pgid = pgid.longValue();  
                    if(objectkey == 'Subscription__c'){
                        subscription_pgid_set.add(pgid);
                    }
                    if(objectkey == 'BBInformation__c'){
                        bbInformation_pgid_set.add(pgid);
                    }
                }
            }
        }
        List<Subscription__c> subscriptionToUpdateList = new List<Subscription__c>();
        if(subscription_pgid_set.size() > 0){
        	subscriptionToUpdateList = [SELECT PGID__c, Id FROM Subscription__c WHERE PGID__c IN :subscription_pgid_set];    
        }
        Map<Decimal, Id> subscriptionToUpdateMap = new Map<Decimal, Id>();
        for(Subscription__c subscription : subscriptionToUpdateList){
            subscriptionToUpdateMap.put(subscription.PGID__c, subscription.Id);
        }
        system.debug('@@@subscriptionToUpdateMap: '+subscriptionToUpdateMap);
        
        List<BBInformation__c> bbinfoToUpdateList = new List<BBInformation__c>();
        if(bbInformation_pgid_set.size() > 0){
            bbinfoToUpdateList = [SELECT PGID__c, Id FROM BBInformation__c WHERE PGID__c IN :bbInformation_pgid_set];
        }
        Map<Decimal, Id> bbinfoToUpdateMap = new Map<Decimal, Id>();
        for(BBInformation__c bbinfo : bbinfoToUpdateList){
            bbinfoToUpdateMap.put(bbinfo.PGID__c, bbinfo.Id);
        }
        
        for(String objectkey : updateObjectMap.keySet()){
            Map<String, Map<String, Object>> updateRecordMap = updateObjectMap.get(objectkey);
            for(String recordIdKey : updateRecordMap.keySet()){
                Map<String, Object> meta = updateRecordMap.get(recordIdKey);
                String idString = String.valueOf(meta.get('Id'));
           		if(objectkey == 'Subscription__c'){
                    if(String.isBlank(idString)){
                        Object objectPgid = meta.get('PGID__c');
                        Decimal pgid;
                        if(objectPgid instanceof Decimal){
                            pgid = (Decimal)meta.get('PGID__c');
                        }
                        else if(objectPgid instanceof String){
                            pgid = Decimal.valueOf((String)meta.get('PGID__c'));
                        }
                        pgid = pgid.longValue();
                        system.debug('@@@pgid: '+pgid);
                        Id objectId = subscriptionToUpdateMap.get(pgid);
                        system.debug('@@@Subscription Update tempObj: '+subscriptionToUpdateMap);
                        meta.put('Id', objectId);
                    }
                    
                    Subscription__c obj = (Subscription__c) JSON.deserialize( JSON.serialize( meta ), Subscription__c.class );
                    obj.LogChecking__c = obj.LogChecking__c ? false : true;
                    system.debug('@@@Subscription Update obj: '+obj);
                    subscriptionToUpdate.add(obj);
                }
                if(objectkey == 'BBInformation__c'){
                    if(String.isBlank(idString)){
                        Object objectPgid = meta.get('PGID__c');
                        Decimal pgid;
                        if(objectPgid instanceof Decimal){
                            pgid = (Decimal)meta.get('PGID__c');
                        }
                        else if(objectPgid instanceof String){
                            pgid = Decimal.valueOf((String)meta.get('PGID__c'));
                        }
                        pgid = pgid.longValue();
                        system.debug('@@@pgid: '+pgid);
                        Id objectId = bbinfoToUpdateMap.get(pgid);
                        system.debug('@@@bb info Update tempObj: '+bbinfoToUpdateMap);
                        meta.put('Id', objectId);
                    }
                    
                    BBInformation__c obj = (BBInformation__c) JSON.deserialize( JSON.serialize( meta ), BBInformation__c.class );
                    obj.LogChecking__c = obj.LogChecking__c ? false : true;
                    system.debug('@@@bb info Update obj: '+obj);
                    bbInfoToUpdate.add(obj);
                }
            }
        }
         
            if(bbInfoToUpdate.size() > 0 ){
                update bbInfoToUpdate;
            }
            if(bbInfoToInsert.size() > 0 ){
                insert bbInfoToInsert;
            }
            if(subscriptionToUpdate.size() > 0){
                update subscriptionToUpdate;
            }
            if(subscriptionToInsert.size() > 0){
                insert subscriptionToInsert;
            }
           
    }
	*/    

    /*
    @future
    public static void updateLogShipmentStatus(Set<Id> s_LSIds){
        
        //query log shipment
        List<LogShipment__c> logShipmentList = new List<LogShipment__c>();
        system.debug('@@@updateLogmentStatus: '+s_LSIds);
        logShipmentList = [SELECT id, Status__c, Source__c FROM LogShipment__c WHERE id IN :s_LSIds];
        
        List<LogShipment__c> completeLogShipmentList = new List<LogShipment__c>();
        for(LogShipment__c logShipment : logShipmentList){
            String status = logShipment.Status__c;
            String source = logShipment.Source__c;
            
            //check open status
            if(source == 'H' && status == 'Open'){
                logShipment.Status__c = 'Complete';     
                completeLogShipmentList.add(logShipment);
            }
        }
        
        
        //update status to complete
        if(completeLogShipmentList.size() > 0){
            update completeLogShipmentList;    
        }
   
    }
	*/
    
}