global class CampaignDailyJobs_Schedule implements Schedulable{
	global void execute(SchedulableContext sc) {
		CampaignDailyJobs_Batch batch = new CampaignDailyJobs_Batch();
		database.executebatch(batch);
	}
}