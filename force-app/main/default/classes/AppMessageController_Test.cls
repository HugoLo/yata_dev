@isTest
public class AppMessageController_Test {
    
	static testMethod void AppMessageController_Test(){
        
        TestClassHelper.createIntegrationControl();
        Account account = TestClassHelper.createMember();
        Map<String, String> parameter = new Map<String, String>();
        parameter.put('MemberSFId', account.Id);
        parameter.put('Status', 'Read');
        parameter.put('MessageDateFrom', '2018-09-04 00:00:00');
        parameter.put('MessageDateTo', '2018-10-04 00:00:00');
        parameter.put('limit', '10');
        
        
        User userA = TestClassHelper.createUser();
        System.runAs(userA){
            
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new HTTPMockCallout());
            String responseString = AppMessageController.apex_getAppMessageList(parameter);
            String fieldset = AppMessageController.apex_getFieldset('InAppMessage__c', 'InAppMessageFieldSetPreview');
            String picklistFieldValues = AppMessageController.apex_getPicklistFieldValues('InAppMessage__c', 'Status__c');
            Test.stopTest();
            
        }
    }
    
    public class HTTPMockCallout implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(200);
            string jsonResBody = '{"ReturnCode":"1","ReturnMessage":"Success","Messages":[{"lastvieweddate":null,"member__c":"001p000000URpupAAD","contentc__c":"Simon Chan分享了1000給你。","name":null,"titlec__c":"Simon Chan分享了1000給你。","lastmodifieddate":null,"ownerid":null,"isdeleted":null,"systemmodstamp":null,"redirecturl__c":"yata://myaccount_pointhistory","lastmodifiedbyid":null,"pgid__c":2,"status__c":"Read","messagecreatedtime__c":"2018-09-04T03:54:16.000Z","createddate":null,"memberpgid__c":93,"messagereadtime__c":"2018-09-05T03:54:16.000Z","createdbyid":null,"lastreferenceddate":null,"sfid":null,"templatecode__c":"PointTransfer","contente__c":"Simon Chan has shared 1000 points to you.","titlee__c":"Simon Chan has shared 1000 points to you.","member__c_name":"*Gordon Hoo"}]}';
            res.setBody(jsonResBody);
            return res;
        }
    }
    
}