global class LogShipment_Sched implements Schedulable{

    global void execute(SchedulableContext sc) {
        executeLogShipmentBatch();
	}
    
    global void executeLogShipmentBatch(){
        if ([SELECT count() FROM AsyncApexJob WHERE JobType='BatchApex' AND (Status = 'Processing' OR Status = 'Preparing' OR Status = 'Queued' OR Status = 'Holding') AND ApexClass.Name = 'LogShipment_Batch'] >= 1){ 
            if ([SELECT count() FROM CronTrigger WHERE  CronJobDetail.Name like '%LogShipment_Sched%' AND NextFireTime != null] >= 1){
                //If there is a scheduled job, do nothing
                //The first one should be the daily job
                system.debug('@@@CronTrigger');
            } 
            else {
                //schedule the schedulable class again in 2 mins
                LogShipment_Sched scRetry = new LogShipment_Sched(); 
                Datetime dt = Datetime.now();
                dt = dt.addMinutes(2);
                //dt = dt.addSeconds(5);
                String timeForScheduler = dt.format('s m H d M \'?\' yyyy'); 
                Id schedId = System.Schedule('LogShipment_Sched'+timeForScheduler,timeForScheduler,scRetry);
            }
        } 
        else {
            LogShipment_Batch batch = new LogShipment_Batch();
            Database.executeBatch(batch, 200);
        }
    }
    
}