public with sharing class PointSchemePointRules_Ctrl {
	
    @AuraEnabled
    public static String apex_getIncentiveRule(String recordId) {
    	IncentiveRule__c incentiveRule = 
                [SELECT Id, JSON__c, Status__c 
                 FROM IncentiveRule__c 
                 WHERE RecordID__c = :recordId 
                 LIMIT 1];
        
        
        String json_content = incentiveRule.JSON__c;
        Map<String, Object> meta = (Map<String, Object>) JSON.deserializeUntyped(json_content);
        Object BasicInfo = meta.get('BasicInfo');
        String basicInfoJsonString = JSON.serialize(BasicInfo);
        system.debug('@@@basicInfoJsonString: '+basicInfoJsonString);
        InfoJson basicInfoJsonObj = InfoJson.parse(basicInfoJsonString);
        system.debug('@@@infoJsonObj: '+basicInfoJsonObj);
        
		Object Targeting = meta.get('Targeting');
        String targetingJsonString = JSON.serialize(Targeting);
        system.debug('@@@targetingJsonString: '+targetingJsonString);
        TargetingJson targetingJsonObj = TargetingJson.parse(targetingJsonString);
        system.debug('@@@targetingJsonObj: '+targetingJsonObj);
        
        Object Condition = meta.get('Condition');
        String conditionJsonString = JSON.serialize(Condition);
        system.debug('@@@conditionJsonString: '+conditionJsonString);
        ConditionJson conditionJsonObj = ConditionJson.parse(conditionJsonString);
        system.debug('@@@conditionJsonObj: '+conditionJsonObj);
        
        Object Action = meta.get('Action');
        String actionJsonString = JSON.serialize(Action);
        system.debug('@@@actionJsonString: '+actionJsonString);
        ActionJson actionJsonObj = ActionJson.parse(actionJsonString);
        system.debug('@@@actionJsonObj: '+actionJsonObj);
        
        IncentiveRuleJson incentiveRuleJson = new IncentiveRuleJson(basicInfoJsonObj, targetingJsonObj, conditionJsonObj, actionJsonObj);
        Map<String, Object> jsonMap = new Map<String, Object>();
        if(incentiveRule != null){
            jsonMap.put('returnCode', '1');
            jsonMap.put('result', incentiveRuleJson);
        }
        else{
            jsonMap.put('returnCode', '2');
        }
        
        String jsonString = JSON.serialize(jsonMap);
        system.debug('@@@jsonString: '+jsonString);
        
		return jsonString;
    }
    
    @AuraEnabled
    public static String apex_getIncentiveRuleData(String recordId) {
    	IncentiveRule__c incentiveRule = 
                [SELECT Id, JSON__c, Status__c 
                 FROM IncentiveRule__c 
                 WHERE RecordID__c = :recordId 
                 LIMIT 1];
        String jsonString = JSON.serialize(incentiveRule);
        return jsonString;
        
    }
    
    @AuraEnabled
    public static String apex_getRecordById(String recordId){
      	PointsScheme__c pointScheme = 
                [SELECT RuleCode__c 
                 FROM PointsScheme__c
                 WHERE Id = :recordId];
        String jsonString = JSON.serialize(pointScheme);
        system.debug('@@@jsonString: '+jsonString);
        return jsonString;
    }
    
    @AuraEnabled
    public static sObject apex_getRewardById(String objectName, String recordId){
      	sObject record = CommonController.apex_getRecordById(objectName, recordId);
        return record;
    }
    
    @AuraEnabled
    public static String apex_saveJson(String recordId, String jsonString, String status, String startDate, String endDate, String ruleCode, String PointGroup){
        Date startDateDate = startDate != null ? Date.valueOf(startDate) : null;
        Date endDateDate = endDate != null ? Date.valueOf(endDate) : null;
        
        Map<String, Object> returnMap = new Map<String, Object>();
        returnMap.put('ReturnCode', 1);
        returnMap.put('ReturnMessage', 'Success');
        
        Boolean isValid = true;
        
        if(status == 'Active'){
            if(startDateDate != null || (startDateDate != null && endDateDate != null)){
                List<PointsScheme__c> l_pointScheme = [SELECT Id, StartDate__c, EndDate__c, IncentiveRuleNo__r.Status__c
                                                       FROM PointsScheme__c 
                                                       WHERE RuleCode__c = :ruleCode AND IncentiveRuleNo__r.Status__c = 'Active'  
                                                       AND Id != :recordId];
                
                for(PointsScheme__c pointsScheme : l_pointScheme){
                    Date pointsScheme_startDate = pointsScheme.StartDate__c;
                    Date pointsScheme_endDate = pointsScheme.EndDate__c;
                 
                    if(pointsScheme_startDate != null && pointsScheme_endDate != null){
                        /*
                        if(pointsScheme_startDate >= startDateDate && startDateDate <= pointsScheme_endDate || 
                           pointsScheme_startDate >= endDateDate && endDateDate <= pointsScheme_endDate){
                            isValid = false;
                        }
                        */
                        
                        system.debug('@@@startDateDate: '+startDateDate);
                        system.debug('@@@endDateDate: '+endDateDate);
                        system.debug('@@@pointsScheme_startDate: '+pointsScheme_startDate);
                        system.debug('@@@pointsScheme_endDate: '+pointsScheme_endDate);
                        
                        if(startDateDate != null && endDateDate != null){
                            // pointsScheme_startDate >= startDateDate <= pointsScheme_endDate
                            if(startDateDate >= pointsScheme_startDate && startDateDate <= pointsScheme_endDate){
                                isValid = false;
                                break;
                            }
                            // pointsScheme_startDate >= endDateDate <= pointsScheme_endDate
                            else if(endDateDate >= pointsScheme_startDate && endDateDate <= pointsScheme_endDate){
                                isValid = false;
                                break;
                            }
                            // startDateDate <= pointsScheme_startDate && endDateDate >= pointsScheme_endDate
                            else if(startDateDate <= pointsScheme_startDate && endDateDate >= pointsScheme_endDate){
                                isValid = false;
                                break;
                            }
                        }
                        else if(startDateDate != null){
                            if(startDateDate >= pointsScheme_startDate && startDateDate <= pointsScheme_endDate){
                                isValid = false;
                                break;
                            }
                        }
                    }
                    else{
                        if(pointsScheme_startDate != null){
                            /*
                            if(pointsScheme_startDate <= startDateDate || pointsScheme_startDate <= endDateDate){
                                isValid = false;
                            }
                            */
                            if(startDateDate != null && endDateDate != null){
                                if(endDateDate >= pointsScheme_startDate){
                                    isValid = false;
                                    break;
                                }
                                // pointsScheme_startDate >= startDateDate
                                else if(startDateDate >= pointsScheme_startDate){
                                    isValid = false;
                                    break;
                                }
                            }
                            else if(startDateDate != null){
                                //if(pointsScheme_startDate <= startDateDate){
                                    isValid = false;
                                    break;
                                //}
                            }
                        }
                    }
                }
            }
            else{
                isValid = false;
            }
        }
        
        
                
        if(isValid){
            IncentiveRule__c incentiveRule = 
                    [SELECT Id, JSON__c, Status__c
                     FROM IncentiveRule__c 
                     WHERE RecordID__c = :recordId 
                     LIMIT 1];
            incentiveRule.JSON__c = jsonString;
            incentiveRule.Status__c = status;
            update incentiveRule;
            
            PointsScheme__c pointsScheme = [SELECT Id, StartDate__c, EndDate__c, PointSchemeName__c, PointGroup__c 
                                            FROM PointsScheme__c 
                                            WHERE Id = :recordId];
            pointsScheme.StartDate__c = String.isBlank(startDate) ? null : Date.valueOf(startDate);
            pointsScheme.EndDate__c = String.isBlank(endDate) ? null : Date.valueOf(endDate);
            pointsScheme.PointGroup__c = PointGroup;
            update pointsScheme;
            
        }
        else{
            PointsScheme__c pointsScheme = [SELECT Id, StartDate__c, EndDate__c, PointSchemeName__c 
                                            FROM PointsScheme__c 
                                            WHERE Id = :recordId];
            
            returnMap.put('ReturnCode', 2);
            returnMap.put('ReturnMessage', 'Only allow ONE active Point Scheme with same Point Scheme Name.\nPlease inactive the all point scheme with '+pointsScheme.PointSchemeName__c+' before save.');
        }
        
        return JSON.serialize(returnMap);
    }
    
    @AuraEnabled
    public static String apex_getPicklistFieldValues(String objectName, String pickListFieldName){
        List<Object> picklistValues = new List<Object>();
        SObjectType objectType = Schema.getGlobalDescribe().get(objectName);
        List<Schema.PicklistEntry> pick_list_values = objectType.getDescribe()
                                                       .fields.getMap()
                                                       .get(pickListFieldName)
                                                       .getDescribe().getPickListValues();
        for (Schema.PicklistEntry aPickListValue : pick_list_values) {                   
            picklistValues.add(aPickListValue); 
        }
        String jsonString = JSON.serialize(picklistValues);
        system.debug('@@@picklistValues: '+jsonString);
        return jsonString;
    }
   
   	/*
    @AuraEnabled 
    public static String getPicklistValueBasedonRecordType(String objectAPIName, String fieldAPIName, String recordTypeDeveloperName){
       return PicklistBasedOnRecordTypeController.getPicklistValueBasedonRecordType(objectAPIName, fieldAPIName, recordTypeDeveloperName);
    }
    */
}