public class AccountTriggerHandler extends TriggerHandler{
    //private static final String ObjName = 'Account';
    private static final String ObjName = Trigger.isDelete ? Trigger.Old.getSObjectType().getDescribe().getName() : Trigger.New.getSObjectType().getDescribe().getName();
    
    protected override void beforeInsert() {
        system.debug('@@@Account beforeInsert');
        if(Common_Security_Cls.toRunTriggerMethod('AccountTriggerHandler','beforeInsert')){
            
            //Update default value(member age and address) to member
            Common_Member_Cls.setDefaultValAll((List<Account>) Trigger.New);
            
            //Update value to active member
            List<Account> l_ActiveMembers = new List<Account>();
            for(Account acct : (List <Account>) Trigger.New){
                if(acct.MemberStatus__pc == 'Active'){
                    l_ActiveMembers.add(acct);
                }
            }
            if(l_ActiveMembers.size()>0){
                Common_Member_Cls.setDefaultValActive(l_ActiveMembers);
            }
        }
    }
    
    protected override void beforeUpdate () {
        system.debug('@@@beforeUpdate');
        if(Common_Security_Cls.toRunTriggerMethod('AccountTriggerHandler','beforeUpdate')){
            
            //Update default value(member age and address) to member
            Common_Member_Cls.setDefaultValAll((List<Account>) Trigger.New);
            
            //Update value to active member and inactive member
            List<Account> l_ActiveMembers = new List<Account>();
            List<Account> l_InactiveMembers = new List<Account>();
            
            for(Account acct : (List <Account>) Trigger.New){
                //To inactive member if mobile changes:
                Account oldAcct = (Account) Trigger.OldMap.get(acct.Id);
                if(oldAcct.MemberStatus__pc != 'Active' && acct.MemberStatus__pc == 'Active'){
                    l_ActiveMembers.add(acct);
                }
                if((oldAcct.PersonMobilePhone != acct.PersonMobilePhone) || 
                   (oldAcct.MemberStatus__pc != 'Inactive' && acct.MemberStatus__pc == 'Inactive')){
                    l_InactiveMembers.add(acct);
                }
            }
            if(l_ActiveMembers.size()>0){
                Common_Member_Cls.setDefaultValActive(l_ActiveMembers);
            }
            if(l_InactiveMembers.size()>0){
                Common_Member_Cls.inactiveLoginFlag(l_InactiveMembers);
            }
            }
    }
    /*
    protected override void beforeDelete () {
        system.debug('@@@beforeDelete');
    }
        
    protected override void afterInsert () {
        system.debug('@@@Account afterInsert');
        }
    
    protected override void afterDelete () {
        system.debug('@@@afterDelete');
    }
    
    protected override void afterUpdate () {
        system.debug('@@@Account afterUpdate');

    }

    protected override void afterUndelete  () {
        system.debug('@@@afterUndelete');        
    }
    */
}