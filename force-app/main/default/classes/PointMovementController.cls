public with sharing class PointMovementController {

    @AuraEnabled
    public static String apex_getPointMovementList(Map<String, String> parameter){
        String transactionList = Common_HerokuIntegration_Cls.getPointmovementList(parameter);
        Map<String, Schema.SObjectField> pointsMovementFieldMap = CommonController.apex_getAllFieldset('PointsMovement__c');
        Map<String, Schema.SObjectField> pointsMovementDetailFieldMap = CommonController.apex_getAllFieldset('PointsMovementDetail__c');
        Map<String,Object> PointMovementsMap = (Map<String,Object>) JSON.deserializeUntyped(transactionList);
        list<object> PointMovements = (list<object>)PointMovementsMap.get('PointMovements');
        for(Object PointMovement : PointMovements){
            Map<String, Object> PointMovementMap = (Map<String, Object>) PointMovement;
            Map<String, Object> PointMovementObject = (Map<String, Object>)PointMovementMap.get('PointMovement');
            CommonController.convertFieldValue(pointsMovementFieldMap, PointMovementObject);
           	
            List<Object> MovementDetails = (List<Object>)PointMovementMap.get('MovementDetails');
            for(Object MovementDetail : MovementDetails){
                Map<String, Object> MovementDetailMap = (Map<String, Object>) MovementDetail;
                CommonController.convertFieldValue(pointsMovementDetailFieldMap, MovementDetailMap);
            }
        }
        
        return JSON.serialize(PointMovementsMap);
    } 
    
    @AuraEnabled
    public static String apex_getFieldset(String objectName, String fieldSetName){
        return CommonController.apex_getFieldset(objectName, fieldSetName);
    }
    
    @AuraEnabled
    public static String apex_getRecordType(String objectName){
        return CommonController.apex_getRecordType(objectName);
    }
    
    @AuraEnabled
    public static Integer apex_getNoRecordDisplay(String name){
        return CommonController.apex_getNoRecordDisplay(name);
    }
    
}