@isTest
public class Common_Utilities_Cls_Test {
    static testMethod void Common_Utilities_Cls_Test() {
        LoyaltySetting__c   controlToday = new LoyaltySetting__c( Name='TODAY', Date__c = Date.today());
        insert controlToday;
        Common_Utilities_Cls.getToday();
        
        
        //To test isStoredInSFDC() and needLogShipment
        Common_Utilities_Cls.isStoredInSFDC('Account');
        ObjectControl__c ls1 = new ObjectControl__c (Name='Account', SyncByLogShippment__c = true, StoredInSF__c= true);
        ObjectControl__c ls2 = new ObjectControl__c (Name='Transaction__c', SyncByLogShippment__c = true, StoredInSF__c= true);
        insert new List<ObjectControl__c>{ls1, ls2};
        Common_Utilities_Cls.isStoredInSFDC('Account');
        Common_Utilities_Cls.needLogShipment('Account');
        
        
        account acct =TestClassHelper.createMember();
        Common_Utilities_Cls.getAccountContactMap(new Set<Id>{acct.id});
    }
}