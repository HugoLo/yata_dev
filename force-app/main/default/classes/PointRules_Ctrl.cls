public with sharing class PointRules_Ctrl {
    
    @AuraEnabled
    public static String apex_getIncentiveRule(String recordId) {
        /*
    	IncentiveRule__c incentiveRule = 
                [SELECT Id, JSON__c 
                 FROM IncentiveRule__c 
                 WHERE RecordID__c = :recordId 
                 LIMIT 1];
		*/
        
        List<IncentiveRule__c> l_incentiveRule = 
                [SELECT Id, JSON__c 
                 FROM IncentiveRule__c 
                 WHERE RecordID__c = :recordId LIMIT 1];
        
        Map<String, Object> jsonMap = new Map<String, Object>();
        if(l_incentiveRule.size() > 0){
            IncentiveRule__c incentiveRule = l_incentiveRule[0];
            String json_content = incentiveRule.JSON__c;
            Map<String, Object> meta = (Map<String, Object>) JSON.deserializeUntyped(json_content);
            Object BasicInfo = meta.get('BasicInfo');
            String basicInfoJsonString = JSON.serialize(BasicInfo);
            system.debug('@@@basicInfoJsonString: '+basicInfoJsonString);
            InfoJson basicInfoJsonObj = InfoJson.parse(basicInfoJsonString);
            system.debug('@@@infoJsonObj: '+basicInfoJsonObj);
            
            Object Targeting = meta.get('Targeting');
            String targetingJsonString = JSON.serialize(Targeting);
            system.debug('@@@targetingJsonString: '+targetingJsonString);
            TargetingJson targetingJsonObj = TargetingJson.parse(targetingJsonString);
            system.debug('@@@targetingJsonObj: '+targetingJsonObj);
            
            Object Condition = meta.get('Condition');
            String conditionJsonString = JSON.serialize(Condition);
            system.debug('@@@conditionJsonString: '+conditionJsonString);
            ConditionJson conditionJsonObj = ConditionJson.parse(conditionJsonString);
            system.debug('@@@conditionJsonObj: '+conditionJsonObj);
            
            Object Action = meta.get('Action');
            String actionJsonString = JSON.serialize(Action);
            system.debug('@@@actionJsonString: '+actionJsonString);
            ActionJson actionJsonObj = ActionJson.parse(actionJsonString);
            system.debug('@@@actionJsonObj: '+actionJsonObj);
            
            IncentiveRuleJson incentiveRuleJson = new IncentiveRuleJson(basicInfoJsonObj, targetingJsonObj, conditionJsonObj, actionJsonObj);
            
            if(incentiveRule != null){
                jsonMap.put('returnCode', '1');
                jsonMap.put('result', incentiveRuleJson);
            }
            else{
                jsonMap.put('returnCode', '2');
            }
        }
        else{
            jsonMap.put('returnCode', '2');
        }
        
        
        String jsonString = JSON.serialize(jsonMap);
        system.debug('@@@jsonString: '+jsonString);
        
		return jsonString;
    }
   
    
    @AuraEnabled
    public static String apex_getFieldTypeList(){
         
        List<FieldType__c> fieldTypeList = 
            [SELECT Name, JSONFieldType__c, OperatorMapping__c    
             FROM FieldType__c];
        
        Map<String, FieldType__c> fieldTypeMap = new Map<String, FieldType__c>();
        for(FieldType__c fieldType : fieldTypeList){
            fieldTypeMap.put(fieldType.Name, fieldType);
        }
        
        String jsonString = JSON.serialize(fieldTypeMap);
        
        return jsonString;
    }
    
    
    @AuraEnabled
    public static String apex_getSpecificDateSetting(){
         
        List<SpecificDateSetting__c> specificDateSettingList = 
            [SELECT Name, Label__c, Seq__c    
             FROM SpecificDateSetting__c
             ORDER BY Seq__c];
  
        
        String jsonString = JSON.serialize(specificDateSettingList);
        
        return jsonString;
    }
    
    	
    @AuraEnabled
    public static String apex_getObjectField(String appliedObject){
        List<RulesSetting__c> rulesSettingList = 
            [SELECT Name, AppliedObject__c, Description__c, FieldSet__c, Module__c, 
             Object__c, ObjectRelationship__c, RelatedObjectLabel__c, RelatedObject__c, ObjectLevel__c   
             FROM RulesSetting__c
             WHERE AppliedObject__c = :appliedObject];
        system.debug('@@@rulesSettingList: '+rulesSettingList);
        
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        
        Map<String, Map<String, Object>> rulesSettingObjectMap = new Map<String, Map<String, Object>>();
        
        //Map<String, RulesSetting__c> rulesSettingMap = new Map<String, RulesSetting__c>();
        //Map<String, List<Schema.FieldSetMember>> objectFieldSetMap = new Map<String, List<Schema.FieldSetMember>>();
        
        for(RulesSetting__c rulesSetting : rulesSettingList){
            String objectName = rulesSetting.Object__c;
            String fieldSetName = rulesSetting.FieldSet__c;
            String Name = rulesSetting.Name;
            system.debug('@@@objectName: '+objectName);
            system.debug('@@@fieldSetName: '+fieldSetName);
        	Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(objectName);   
            if(SObjectTypeObj != null){
                system.debug('@@@SObjectTypeObj: '+SObjectTypeObj);
                Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
                Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
                
                if(fieldSetObj != null){
                    system.debug('@@@fieldSetObj: '+fieldSetObj);
                    
                    List<Schema.FieldSetMember> fieldSetMemberList = fieldSetObj.getFields(); 
                    Map<String, Object> objectMap = new Map<String, Object>();
                    objectMap.put('fieldSet', fieldSetMemberList);
                    objectMap.put('rulesSetting', rulesSetting);
                    
                    Map<String, Object> objectAPINameMap = new Map<String, Object>();
                    for(Schema.FieldSetMember fieldSetMemberObj : fieldSetMemberList)
                    {
                        system.debug('API Name ====>' + fieldSetMemberObj.getFieldPath()); //api name
                        system.debug('Label ====>' + fieldSetMemberObj.getLabel());
                        system.debug('Required ====>' + fieldSetMemberObj.getRequired());
                        system.debug('DbRequired ====>' + fieldSetMemberObj.getDbRequired());
                        system.debug('Type ====>' + fieldSetMemberObj.getType());   //type - STRING,PICKLIST
                        
                        
                        String fieldAPIName = fieldSetMemberObj.getFieldPath();
                        Schema.DescribeFieldResult f = Schema.getGlobalDescribe()
                        .get(objectName)
                        .getDescribe()
                        .fields
                        .getMap()
                        .get(fieldAPIName)
                        .getDescribe();
        

                        for(Schema.SObjectType reference : f.getReferenceTo()) {
                            System.debug('Lookup reference object name: ' + reference.getDescribe().getName());
                            System.debug('Lookup reference object label: ' + reference.getDescribe().getLabel());
                            String objectAPIName = reference.getDescribe().getName();
                            objectAPINameMap.put(fieldAPIName, objectAPIName);
                        }
                        objectMap.put('objectAPIName', objectAPINameMap);
                    }
                    rulesSettingObjectMap.put(Name, objectMap);
                }
            }
        }
        
        String jsonString = JSON.serialize(rulesSettingObjectMap);
        system.debug('@@@jsonString: '+jsonString);
        
        return jsonString;
    }
    
    
    @AuraEnabled
    public static String apex_getObjectField2(String appliedObject, String module){
        List<RulesSetting__c> rulesSettingList = 
            [SELECT Name, AppliedObject__c, Description__c, FieldSet__c, Module__c, 
             Object__c, ObjectRelationship__c, RelatedObjectLabel__c, RelatedObject__c, ObjectLevel__c   
             FROM RulesSetting__c
             WHERE AppliedObject__c = :appliedObject AND Module__c = :module];
        system.debug('@@@rulesSettingList2: '+JSON.serialize(rulesSettingList));
        
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        
        Map<String, Map<String, Object>> rulesSettingObjectMap = new Map<String, Map<String, Object>>();
        List<Object> rulesSettingObjectList = new List<Object>();
       
        for(RulesSetting__c rulesSetting : rulesSettingList){
            String objectName = rulesSetting.Object__c;
            String fieldSetName = rulesSetting.FieldSet__c;
            String Name = rulesSetting.Name;
            String ruleId = String.valueOf(rulesSetting.Id); 
            system.debug('@@@objectName2: '+objectName);
            system.debug('@@@fieldSetName2: '+fieldSetName);
        	Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(objectName);   
            if(SObjectTypeObj != null){
                system.debug('@@@SObjectTypeObj2: '+SObjectTypeObj);
                Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
                Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
                
                if(fieldSetObj != null){
                    system.debug('@@@fieldSetObj: '+fieldSetObj);
                    
                    List<Schema.FieldSetMember> fieldSetMemberList = fieldSetObj.getFields(); 
                    Map<String, Object> objectMap = new Map<String, Object>();
                    objectMap.put('fieldSet', fieldSetMemberList);
                    objectMap.put('rulesSetting', rulesSetting);
                    
                    Map<String, Object> objectAPINameMap = new Map<String, Object>();
                    for(Schema.FieldSetMember fieldSetMemberObj : fieldSetMemberList){
                        
                        system.debug('API Name ====>' + fieldSetMemberObj.getFieldPath()); //api name
                        system.debug('Label ====>' + fieldSetMemberObj.getLabel());
                        system.debug('Required ====>' + fieldSetMemberObj.getRequired());
                        system.debug('DbRequired ====>' + fieldSetMemberObj.getDbRequired());
                        system.debug('Type ====>' + fieldSetMemberObj.getType());   //type - STRING,PICKLIST
                        
                        
                        String fieldAPIName = fieldSetMemberObj.getFieldPath();
                        Schema.DescribeFieldResult f = Schema.getGlobalDescribe()
                        .get(objectName)
                        .getDescribe()
                        .fields
                        .getMap()
                        .get(fieldAPIName)
                        .getDescribe();
        

                        for(Schema.SObjectType reference : f.getReferenceTo()) {
                            System.debug('Lookup reference object name: ' + reference.getDescribe().getName());
                            System.debug('Lookup reference object label: ' + reference.getDescribe().getLabel());
                            String objectAPIName = reference.getDescribe().getName();
                            objectAPINameMap.put(fieldAPIName, objectAPIName);
                        }
                        objectMap.put('objectAPIName', objectAPINameMap);
                    }
                    rulesSettingObjectMap.put(Name, objectMap);
                    rulesSettingObjectList.add(objectMap);
                }
            }
        }
        
        String jsonString = JSON.serialize(rulesSettingObjectMap);
        system.debug('@@@jsonString2: '+jsonString);
        
        return jsonString;
    }
    
    
    @AuraEnabled
    public static String apex_getRecordById(String objectName, String recordId){
        /*
		Campaign campaign = 
                [SELECT Id, Name, StartDate, EndDate, Status, IncentiveRuleNo__r.Name
                 FROM Campaign
                 WHERE Id = :recordId];
        */
        /*
        String sQuery =  'SELECT Id, Name, StartDate, EndDate, Status, IncentiveRuleNo__r.Name ';
        sQuery += 'FROM ' + objectName + ' ';
        sQuery += 'WHERE Id = :'+recordId;
        sObject records = Database.query(sQuery);
        */
        sObject records;
        if(objectName == 'Campaign'){
            records = [SELECT 
                       Id, Name, SpecificMember__c, 
                       StartDate, EndDate, Status, 
                       IncentiveRuleNo__c, IncentiveRuleNo__r.Name,
                       RecordType.DeveloperName
                 	   FROM Campaign
                       WHERE Id = :recordId];
        }
        else if(objectName == 'PointsScheme__c'){
            records = [SELECT Id, Name, StartDate__c, EndDate__c, IncentiveRuleNo__c, IncentiveRuleNo__r.Name
                 	   FROM PointsScheme__c
                       WHERE Id = :recordId];
        }
        
        String jsonString = JSON.serialize(records);
        system.debug('@@@apex_getRecordById: '+records);
        return jsonString;
    }
    
    
    @AuraEnabled
    public static String apex_getBasicInfotxnCreate(String SysConName){ 
            RuleSysConDefinition__c ruleSysConDefinitionList = 
            [SELECT JSON__c  
             FROM RuleSysConDefinition__c
             WHERE Section__c = 'BasicInfo' AND SysConName__c = :SysConName
             LIMIT 1];
        String jsonString = JSON.serialize(ruleSysConDefinitionList);
        system.debug('@@@apex_getBasicInfotxnCreate: '+jsonString);
        return jsonString;
    }
    
    
    @AuraEnabled
    public static String apex_addTargetDetail(){
        TargetingJson.Detail detail = TargetingJson.addDetail();
        String jsonString = JSON.serialize(detail);
        return jsonString;
    }
    
    
    @AuraEnabled
    public static String apex_getAllReward(){
        Datetime todayTime = System.now();
		List<Reward__c> rewardList = 
                [SELECT Id, Name
                 FROM Reward__c
                 WHERE (EffectiveFrom__c <= :todayTime AND EffectiveTo__c >= :todayTime) 
                 OR (EffectiveFrom__c = NULL AND EffectiveTo__c = NULL)
                 OR (EffectiveFrom__c <= :todayTime AND EffectiveTo__c = NULL)];
        String jsonString = JSON.serialize(rewardList);
        return jsonString;
    }
    
    
    @AuraEnabled
    public static String apex_getConditionObjectField(){
        List<RulesSetting__c> rulesSettingList = 
            [SELECT Name, AppliedObject__c, Description__c, FieldSet__c, Module__c, 
             Object__c, ObjectRelationship__c, RelatedObjectLabel__c, RelatedObject__c, ObjectLevel__c
             FROM RulesSetting__c
             WHERE Module__c = 'Condition'
             ORDER BY RelatedObject__c];
        system.debug('@@@rulesSettingList: '+rulesSettingList);
        
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        
        Map<String, Map<String, Object>> rulesSettingObjectMap = new Map<String, Map<String, Object>>();
        Map<String, Map<String, Object>> relatedObjectMap = new Map<String, Map<String, Object>>();
        
        for(RulesSetting__c rulesSetting : rulesSettingList){
            String objectName = rulesSetting.Object__c;
            String fieldSetName = rulesSetting.FieldSet__c;
            String Name = rulesSetting.Name;
            String relatedObject = rulesSetting.RelatedObject__c ;
            system.debug('@@@objectName: '+objectName);
            system.debug('@@@fieldSetName: '+fieldSetName);
        	Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(objectName);  
            boolean contain = relatedObjectMap.containsKey(relatedObject);
            rulesSettingObjectMap = contain ? (Map<String, Map<String, Object>>)relatedObjectMap.get(relatedObject) : new Map<String, Map<String, Object>>(); 
            if(SObjectTypeObj != null){
                system.debug('@@@SObjectTypeObj: '+SObjectTypeObj);
                Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
                Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
                
                if(fieldSetObj != null){
                    system.debug('@@@fieldSetObj: '+fieldSetObj);
                    
                    List<Schema.FieldSetMember> fieldSetMemberList = fieldSetObj.getFields(); 
                    Map<String, Object> objectMap = new Map<String, Object>();
                    objectMap.put('fieldSet', fieldSetMemberList);
                    objectMap.put('rulesSetting', rulesSetting);
                    
                    Map<String, Object> objectAPINameMap = new Map<String, Object>();
                    for(Schema.FieldSetMember fieldSetMemberObj : fieldSetMemberList)
                    {
                        system.debug('API Name ====>' + fieldSetMemberObj.getFieldPath()); //api name
                        system.debug('Label ====>' + fieldSetMemberObj.getLabel());
                        system.debug('Required ====>' + fieldSetMemberObj.getRequired());
                        system.debug('DbRequired ====>' + fieldSetMemberObj.getDbRequired());
                        system.debug('Type ====>' + fieldSetMemberObj.getType());   //type - STRING,PICKLIST
                        
                        
                        String fieldAPIName = fieldSetMemberObj.getFieldPath();
                        Schema.DescribeFieldResult f = Schema.getGlobalDescribe()
                        .get(objectName)
                        .getDescribe()
                        .fields
                        .getMap()
                        .get(fieldAPIName)
                        .getDescribe();
        

                        for(Schema.SObjectType reference : f.getReferenceTo()) {
                            System.debug('Lookup reference object name: ' + reference.getDescribe().getName());
                            System.debug('Lookup reference object label: ' + reference.getDescribe().getLabel());
                            String objectAPIName = reference.getDescribe().getName();
                            objectAPINameMap.put(fieldAPIName, objectAPIName);
                        }
                        objectMap.put('objectAPIName', objectAPINameMap);
                    }
                    rulesSettingObjectMap.put(Name, objectMap);
                }
            }
            //Boolean contains = relatedObjectMap.containsKey(relatedObject);
            relatedObjectMap.put(relatedObject, rulesSettingObjectMap);
        }
        
        String jsonString = JSON.serialize(relatedObjectMap);
        system.debug('@@@jsonString: '+jsonString);
        
        return jsonString;
    }
    
    
    @AuraEnabled
    public static String apex_getConditionType(){ 
            List<RuleSysConDefinition__c> ruleSysConDefinitionList = 
            [SELECT SysConAttributeLabel__c, SysConName__c 
             FROM RuleSysConDefinition__c
             WHERE Section__c = 'Condition'];
        String jsonString = JSON.serialize(ruleSysConDefinitionList);
        return jsonString;
    }
    
    
    @AuraEnabled
    public static String apex_addConditionDetail(){
        ConditionJson.Detail detail = ConditionJson.addDetail();
        String jsonString = JSON.serialize(detail);
        return jsonString;
    }
    
    
    @AuraEnabled
    public static String apex_getCampaignMemberList(String recordId){
        List<CampaignMember> campaignMemberList = 
            [SELECT Id, Contact.AccountId
            FROM CampaignMember
            WHERE CampaignId = :recordId];
        
        List<String> campaignMemberStringList = new List<String>();
        for(CampaignMember cm : campaignMemberList){
            campaignMemberStringList.add(cm.Contact.AccountId);
        }
        
        String jsonString = JSON.serialize(campaignMemberStringList);
        system.debug('@@@jsonString: '+jsonString);
        return jsonString;
    }
    
    
    @AuraEnabled
    public static void apex_saveJson(String recordId, String jsonString, String applyStatus){
		IncentiveRule__c incentiveRule = 
                [SELECT Id, JSON__c 
                 FROM IncentiveRule__c 
                 WHERE RecordID__c = :recordId 
                 LIMIT 1];
        incentiveRule.JSON__c = jsonString;
        
        
        Campaign campaign = [SELECT Id, Status 
                             FROM Campaign 
                             WHERE Id = :recordId];
        campaign.Status = applyStatus;
        
        update incentiveRule;
        update campaign;
    }
    
    
    @AuraEnabled
    public static void apex_createIncentiveRule(String jsonString, String compaing_id, String objectName, String ruleName, String status){   
		IncentiveRule__c incentiveRule = new IncentiveRule__c();
        incentiveRule.JSON__c = jsonString;
        incentiveRule.RecordID__c = compaing_id;
        incentiveRule.ObjectName__c = objectName;
        incentiveRule.RuleName__c = ruleName;
        if(status == 'Active' || status == 'Ended'){
            incentiveRule.Status__c = 'Active';
        }
        else if(status == 'Inactive'){
            incentiveRule.Status__c = 'Aborted';
        }
        else{
            incentiveRule.Status__c = null;
        }
        insert incentiveRule;
        
        Campaign campaign = [SELECT Id, IncentiveRuleNo__c FROM Campaign WHERE Id = :compaing_id];  
        campaign.IncentiveRuleNo__c = incentiveRule.Id;
        update campaign;
        
    }
    
}