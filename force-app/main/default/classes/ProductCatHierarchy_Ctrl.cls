public without sharing class ProductCatHierarchy_Ctrl {
    
	@AuraEnabled
    public static String apex_getCatHierarchy(String recId) {
		
        transient List<ProductCategory__c> productCatList = [SELECT Id, Name, ParentCategoryID__c, CategoryID__c, CategoryLevel__c 
                                                   FROM ProductCategory__c
                                                   WHERE CategoryLevel__c != null 
                                                   ORDER BY CategoryLevel__c ASC];
        
        system.debug('@@@productCatList: '+JSON.serialize(productCatList));
        
        //Map<String, Map<String, List<ProductCategory__c>>> productCatMap = new Map<String, Map<String, List<ProductCategory__c>>>();
        Map<String, List<Map<String, Object>>> productCatMap = new Map<String, List<Map<String, Object>>>();
        Set<String> indexSet = new Set<String>();
        for(ProductCategory__c productCat : productCatList){
            String key = String.valueOf(productCat.CategoryLevel__c);
            indexSet.add(key);
            List<Map<String, Object>> productCatChildList = productCatMap.containsKey(key) ? productCatMap.get(key) : new List<Map<String, Object>>();
            Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(productCat));
           	m.remove('attributes');
            Map<String, Object> m_n = new Map<String, Object>();
            for(String fieldname : m.keySet()){
                system.debug('@@@fieldname'+m.get(fieldname));
                m_n.put(fieldname, m.get(fieldname));
            }
            productCatChildList.add(m_n);
            productCatMap.put(key, productCatChildList);       
        }
        
      
        List<Object> productCatByLevelMapList = new List<Object>();
        for(String key : indexSet){
            Integer index = Integer.valueOf(key) - 1;
            system.debug('@@@index: '+index);
            system.debug('@@@key: '+key);
            productCatByLevelMapList.add(productCatMap.get(key));
        }
        
		List<Map<String, Object>> objectList = new List<Map<String, Object>>();      
        for(Integer i=productCatByLevelMapList.size()-1; i>0; i--){
            
			//system.debug('@@@i: '+i);
            Integer currentLevelIndex = i;
            Integer perviousLevelIndex = currentLevelIndex - 1;
            List<Map<String, Object>> productCatByLevelList = (List<Map<String, Object>>) productCatByLevelMapList[currentLevelIndex];
            
            
            Map<String, List<Object>> parentCategoryChildMap = new Map<String, List<Object>>();
            if(objectList.size() > 0){
                for(Map<String, Object> productCat : objectList){
                    String parentCategoryID = String.valueOf(productCat.get('ParentCategoryID__c'));
                    List<Map<String, Object>> parentCategoryChildList = parentCategoryChildMap.containsKey(parentCategoryID) ? (List<Map<String, Object>>) parentCategoryChildMap.get(parentCategoryID) : new List<Map<String, Object>>();
                    parentCategoryChildList.add(productCat);
                    parentCategoryChildMap.put(parentCategoryID, parentCategoryChildList);
                    
                    system.debug('@@@parentCategoryChildMap: '+parentCategoryChildMap);
                }
            }
            else{
                for(Map<String, Object> productCat : productCatByLevelList){
                    String parentCategoryID = String.valueOf(productCat.get('ParentCategoryID__c'));
                    List<Map<String, Object>> parentCategoryChildList = parentCategoryChildMap.containsKey(parentCategoryID) ? (List<Map<String, Object>>) parentCategoryChildMap.get(parentCategoryID) : new List<Map<String, Object>>();
                    parentCategoryChildList.add(productCat);
                    parentCategoryChildMap.put(parentCategoryID, parentCategoryChildList);
                }
            }
            
            
            
            List<Map<String, Object>> perviousProductCatByLevelList = (List<Map<String, Object>>) productCatByLevelMapList[perviousLevelIndex];
            
            objectList = new List<Map<String, Object>>();
            Map<String, Object> objectMap = new Map<String, Object>();
            for(Map<String, Object> productCat : perviousProductCatByLevelList){
				String parentCategoryID = String.valueOf(productCat.get('ParentCategoryID__c'));
                String categoryID = String.valueOf(productCat.get('Id'));
                List<Map<String, Object>> parentCategoryChildList = parentCategoryChildMap.containsKey(categoryID) ? (List<Map<String, Object>>) parentCategoryChildMap.get(categoryID) : new List<Map<String, Object>>();
                objectMap = productCat;
                objectMap.put('_children', parentCategoryChildList);
              	objectList.add(objectMap);
            }
           	
        }
     
		return JSON.serialize(objectList);        
    }
    
}