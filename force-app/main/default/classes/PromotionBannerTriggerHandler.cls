public class PromotionBannerTriggerHandler extends TriggerHandler{
    
    private static final String ObjName = Trigger.isDelete ? Trigger.Old.getSObjectType().getDescribe().getName() : Trigger.New.getSObjectType().getDescribe().getName();
	private static final Integer MaxNoPopUp =1;
    private static final Integer MaxNoPinToTop =3;
    
    protected override void beforeInsert() {
        system.debug('@@@beforeInsert');
        
        if(Common_Security_Cls.toRunTriggerMethod('PromotionBannerTriggerHandler','beforeInsert')){
        	checkNumbRecords();
        }
        
        /**
        List<PromotionBanner__c> activePopUpBanner = new List<PromotionBanner__c>(); 
        List<PromotionBanner__c> activePinToTopBanner = new List<PromotionBanner__c>();
        for(PromotionBanner__c pb :[SELECT Id, PopUp__c, Status__c FROM PromotionBanner__c WHERE (PopUp__c = True OR PinToTop__c= true) AND Status__c = 'Active']){
            if(pb.PopUp__c=true){
                activePopUpBanner.add(pb);
            }
            if(pb.PinToTop__c=true){
                activePinToTopBanner.add(pb);
            }
        }
            
        for(PromotionBanner__c pb : (List <PromotionBanner__c>) Trigger.New){
            
            if(pb.Status__c == 'Active' && pb.PopUp__c == true){
            	activePopUpBanner.add(pb);  
            }
            
            if(pb.Status__c == 'Active' && pb.PinToTop__c == true){
            	activePinToTopBanner.add(pb);  
            }    
            
            system.debug('No of pop up banner='+activePopUpBanner.size()+', No of pin to top banner='+activePinToTopBanner.size());
            if(activePopUpBanner.size()>MaxNoPopUp || activePinToTopBanner.size()>MaxNoPinToTop){
                pb.addError('No of Pop Up or Pin To Top Banner exceed the limit(1 for Pop up and 3 for Pin To Top)');
            }
        }
        **/
        
        
        
    }
 	
    
    
    protected override void beforeUpdate () {
        system.debug('@@@beforeUpdate');
        if(Common_Security_Cls.toRunTriggerMethod('PromotionBannerTriggerHandler','beforeUpdate')){
        	checkNumbRecords();
        }
        /**
        List<PromotionBanner__c> activePopUpBanner = new List<PromotionBanner__c>([SELECT Id, PopUp__c, Status__c FROM PromotionBanner__c WHERE PopUp__c = True AND Status__c = 'Active']); 
        List<PromotionBanner__c> activePinToTopBanner = new List<PromotionBanner__c>([SELECT Id, PinToTop__c,Status__c FROM PromotionBanner__c WHERE PinToTop__c = True AND Status__c = 'Active']);
        List<PromotionBanner__c> inactivePopUpBanner = new List<PromotionBanner__c>();
        List<PromotionBanner__c> inactivePinToTopBanner = new List<PromotionBanner__c>();
        
        for(PromotionBanner__c pb : (List <PromotionBanner__c>) Trigger.New){
            
            PromotionBanner__c oldpb = (PromotionBanner__c) Trigger.OldMap.get(pb.Id);
            
                if(pb.Status__c == 'Active' && pb.PopUp__c == true && pb.PopUp__c != oldpb.PopUp__c){                    
                	activePopUpBanner.add(pb);  
                }
                
                if(pb.Status__c == 'Active' && pb.PinToTop__c == true && pb.PinToTop__c != oldpb.PinToTop__c){
                	activePinToTopBanner.add(pb);  
                }
            
            	if(pb.Status__c == 'Inactive' && pb.PopUp__c == true && pb.PopUp__c != oldpb.PopUp__c){                    
                	inactivePopUpBanner.add(pb);  
                }
                
            	if(pb.Status__c == 'Inative' && pb.PopUp__c == true && pb.PopUp__c != oldpb.PopUp__c){                    
                	inactivePopUpBanner.add(pb);  
                }
            
                system.debug('No of pop up banner='+activePopUpBanner.size()+', No of pin to top banner='+activePinToTopBanner.size());
                if(activePopUpBanner.size()-inactivePopUpBanner.size()>1 || activePinToTopBanner.size()-inactivePopUpBanner.size()>3){
                    pb.addError('No of Pop Up or Pin To Top Banner exceed the limit(1 for Pop up and 3 for Pin To Top)');
                }
        }**/        
    }
    
    
    private void checkNumbRecords(){
        Boolean hasPopUp =false;
        Boolean hasPinToTop =false;
        Set<Id> s_PopUpIds = new Set<Id>();
        Set<Id> s_PinToTopIds = new Set<Id>();
        
        for(PromotionBanner__c pb : (List <PromotionBanner__c>) Trigger.New){
        	if(pb.Status__c == 'Active' && pb.PopUp__c == true){
				hasPopUp =true;
                s_PopUpIds.add(pb.Id);
            }
            if(pb.Status__c == 'Active' && pb.PinToTop__c == true){
                hasPinToTop =true;
                s_PinToTopIds.add(pb.Id);
               
            }
        }
        if(hasPopUp){
            Integer noNewPopUp =0;
            AggregateResult aggr;
            if(Trigger.isInsert){
            	aggr = [SELECT Count(Id) totalPopUp FROM PromotionBanner__c WHERE  PopUp__c = True AND Status__c = 'Active'];   
            }else if(Trigger.isUpdate){
				aggr = [SELECT Count(Id) totalPopUp FROM PromotionBanner__c WHERE Id NOT IN:s_PopUpIds AND PopUp__c = True AND Status__c = 'Active'];   
            }
            
            Integer noExistingPopUp = (Integer)aggr.get('totalPopUp') <=0 ? 0 :(Integer)aggr.get('totalPopUp');
            
            for(PromotionBanner__c pb : (List <PromotionBanner__c>) Trigger.New){
            	if(pb.Status__c == 'Active' && pb.PopUp__c == true){
                	noNewPopUp++;
                    if(noNewPopUp + noExistingPopUp> MaxNoPopUp){
                        pb.addError('Only '+MaxNoPopUp+' active pop-up banner is allowed.');
                    }
                }
            }
            
        }
        if(hasPinToTop){
            Integer noNewPinToTop =0;
            AggregateResult aggr;
            if(Trigger.isInsert){
            	aggr = [SELECT Count(Id) totalPinToTop FROM PromotionBanner__c WHERE PinToTop__c = True AND Status__c = 'Active'];
            }else if(Trigger.isUpdate){
				aggr = [SELECT Count(Id) totalPinToTop FROM PromotionBanner__c WHERE ID NOT IN:s_PinToTopIds AND PinToTop__c = True AND Status__c = 'Active'];
            }
            
            Integer noExistingPinToTop = (Integer)aggr.get('totalPinToTop') <=0 ? 0 :(Integer)aggr.get('totalPinToTop');
            for(PromotionBanner__c pb : (List <PromotionBanner__c>) Trigger.New){
            	if(pb.Status__c == 'Active' && pb.PinToTop__c == true){
                	noNewPinToTop++;
                    if(noNewPinToTop + noExistingPinToTop> MaxNoPinToTop){
                        pb.addError('Only '+MaxNoPinToTop+' active Pin-To-Top banners are allowed.');
                    }
                }
            }
        }
    }

    /*
    protected override void beforeDelete () {
        system.debug('@@@beforeDelete');
    }
    */    
    
    
    protected override void afterInsert () {
        system.debug('@@@PromotionBanner afterInsert');
        if(Common_Security_Cls.toRunTriggerMethod('PromotionBannerTriggerHandler','afterInsert')){
            
            //Create Log shipment records:
            if(Common_Utilities_Cls.needLogShipment(ObjName)){
                LogShipmentHandler.createLogShipment(ObjName, Trigger.new);   
            }
        }
    }
    
    protected override void afterUpdate () {
        system.debug('@@@PromotionBanner afterUpdate');
        if(Common_Security_Cls.toRunTriggerMethod('PromotionBannerTriggerHandler','afterUpdate')){
            
            //Create Log shipment records:
            if(Common_Utilities_Cls.needLogShipment(ObjName)){
                LogShipmentHandler.updateLogShipment(ObjName, Trigger.new, Trigger.oldMap);
            }
        }
    }
    
    protected override void afterDelete () {
        if(Common_Security_Cls.toRunTriggerMethod('PromotionBannerTriggerHandler','afterDelete')){
            //Create Log shipment records:
            if(Common_Utilities_Cls.needLogShipment(ObjName)){
                LogShipmentHandler.createDeleteLogShipment(ObjName, Trigger.old);
            }   
        }
    }
    
    
    /*
    protected override void afterUndelete  () {
        system.debug('@@@afterUndelete');        
    }
    */
}