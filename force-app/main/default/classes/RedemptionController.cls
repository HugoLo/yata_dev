public with sharing class RedemptionController {
	
    @AuraEnabled
    public static String apex_getRedemptionList(Map<String, String> parameter){
        String redemptionList = Common_HerokuIntegration_Cls.getRedemptionList(parameter);
        Map<String, Schema.SObjectField> fieldMap = CommonController.apex_getAllFieldset('Redemption__c');
        Map<String,Object> redemptionMap = (Map<String,Object>) JSON.deserializeUntyped(redemptionList);
        list<object> redemptions = (list<object>)redemptionMap.get('Redemptions');
		for(object redemption : redemptions){
            Map<String, Object> r = (Map<String, Object>)redemption;
            CommonController.convertFieldValue(fieldMap, r);
        }        
        
        return JSON.serialize(redemptionMap);
    } 

    /*
    @AuraEnabled
    public static String apex_getRedemption(String RedemptionPGId){
        return Common_HerokuIntegration_Cls.getRedemption(RedemptionPGId);
    }
	*/
    
    @AuraEnabled
    public static String apex_getFieldset(String objectName, String fieldSetName){
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        
        system.debug('@@@objectName: '+objectName);
        system.debug('@@@fieldSetName: '+fieldSetName);
        List<Schema.FieldSetMember> fieldSetMemberList = new List<Schema.FieldSetMember>();
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(objectName);   
        if(SObjectTypeObj != null){
            system.debug('@@@SObjectTypeObj: '+SObjectTypeObj);
            Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
            Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
            if(fieldSetObj != null){
                system.debug('@@@fieldSetObj: '+fieldSetObj);
                
				fieldSetMemberList = fieldSetObj.getFields();
                for(Schema.FieldSetMember fieldSetMemberObj : fieldSetMemberList)
                {
                    system.debug('API Name ====>' + fieldSetMemberObj.getFieldPath()); //api name
                    system.debug('Label ====>' + fieldSetMemberObj.getLabel());
                    system.debug('Required ====>' + fieldSetMemberObj.getRequired());
                    system.debug('DbRequired ====>' + fieldSetMemberObj.getDbRequired());
                    system.debug('Type ====>' + fieldSetMemberObj.getType());   //type - STRING,PICKLIST
                }
            }
        }
        
        
        String jsonString = JSON.serialize(fieldSetMemberList);
        return jsonString;
    }
    
    @AuraEnabled
    public static Integer apex_getNoRecordDisplay(String name){
        return CommonController.apex_getNoRecordDisplay(name);
    }
    
    /*
    @AuraEnabled
    public static DataTableResponse getsObjectRecords(String strObjectName, String strFieldSetName){                
       	
        //Get the fields from FieldSet
        Schema.SObjectType SObjectTypeObj = Schema.getGlobalDescribe().get(strObjectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();            
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(strFieldSetName);
        
        //To hold the table hearders 
        List<DataTableColumns> lstDataColumns = new List<DataTableColumns>();
        
        //Field to be queried - fetched from fieldset
        List<String> lstFieldsToQuery = new List<String>();
        
        //The final wrapper response to return to component
        DataTableResponse response = new DataTableResponse();
        
        for( Schema.FieldSetMember eachFieldSetMember : fieldSetObj.getFields() ){
            String dataType = String.valueOf(eachFieldSetMember.getType()).toLowerCase();
            //This way we can set the type of a column
            //We do not get the exact type from schema object which matches to lightning:datatable component structure
            if(dataType == 'datetime'){
                dataType = 'date';
            }
            //Create a wrapper instance and store label, fieldname and type.
            DataTableColumns datacolumns = new DataTableColumns( String.valueOf(eachFieldSetMember.getLabel()) , 
                                                                String.valueOf(eachFieldSetMember.getFieldPath()), 
                                                                String.valueOf(eachFieldSetMember.getType()).toLowerCase() );
			lstDataColumns.add(datacolumns);
            lstFieldsToQuery.add(String.valueOf(eachFieldSetMember.getFieldPath()));
        }
        
        //Form an SOQL to fetch the data - Set the wrapper instance and return as response
        if(! lstDataColumns.isEmpty()){            
            response.lstDataTableColumns = lstDataColumns;
            String query = 'SELECT Id, ' + String.join(lstFieldsToQuery, ',') + ' FROM '+strObjectName;
            System.debug(query);
            response.lstDataTableData = Database.query(query);
        }
        
        return response;
    }
    
    //Wrapper class to hold Columns with headers
    public class DataTableColumns {
        @AuraEnabled
        public String label {get;set;}
        @AuraEnabled       
        public String fieldName {get;set;}
        @AuraEnabled
        public String type {get;set;}
        
        //Create and set three variables label, fieldname and type as required by the lightning:datatable
        public DataTableColumns(String label, String fieldName, String type){
            this.label = label;
            this.fieldName = fieldName;
            this.type = type;            
        }
    }
    
    //Wrapper calss to hold response - This response is used in the lightning:datatable component
    public class DataTableResponse {
        @AuraEnabled
        public List<DataTableColumns> lstDataTableColumns {get;set;}
        @AuraEnabled
        public List<sObject> lstDataTableData {get;set;}                
        
        public DataTableResponse(){
            lstDataTableColumns = new List<DataTableColumns>();
            lstDataTableData = new List<sObject>();
        }
    }
	*/
}