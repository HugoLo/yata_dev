@isTest
public class MembershipTierPointRules_Ctrl_Test {
	
    static testMethod void MembershipTierPointRules_Ctrl_Test(){
        User userA = TestClassHelper.createUser();
        System.runAs(userA){
            
            Membership__c membership = TestClassHelper.createMembership();
            MembershipTier__c membershipTier = TestClassHelper.createMembershipTier(membership);
            MembershipTier__c membershipTier2 = TestClassHelper.createMembershipTier(membership);
            IncentiveRule__c incentiveRule = TestClassHelper.createIncentiveRule(membershipTier);
            IncentiveRule__c incentiveRule2 = TestClassHelper.createIncentiveRule(membershipTier2);
            membershipTier.IncentiveRuleNo__c = incentiveRule.Id;
            update membershipTier;
            membershipTier2.IncentiveRuleNo__c = incentiveRule2.Id;
            update membershipTier2;
            MembershipTierPointRules_Ctrl.apex_getIncentiveRule(membershipTier.Id);
            MembershipTierPointRules_Ctrl.apex_getIncentiveRuleData(membershipTier.Id);
            MembershipTierPointRules_Ctrl.apex_getRecordById(membershipTier.Id);
            MembershipTierPointRules_Ctrl.apex_getPicklistFieldValues('IncentiveRule__c', 'Status__c');
            MembershipTierPointRules_Ctrl.apex_saveJson(membershipTier.Id, '', 'Active', '2019-01-10', '2019-01-13', membershipTier.Name);
            
        }
    }
    
    static testMethod void MembershipTierPointRules_Ctrl2_Test(){
        User userA = TestClassHelper.createUser();
        System.runAs(userA){
            
            Membership__c membership = TestClassHelper.createMembership();
            MembershipTier__c membershipTier = TestClassHelper.createMembershipTier(membership);
            IncentiveRule__c incentiveRule = TestClassHelper.createIncentiveRule(membershipTier);
            membershipTier.IncentiveRuleNo__c = incentiveRule.Id;
            update membershipTier;
            
            MembershipTierPointRules_Ctrl.apex_getIncentiveRule(membershipTier.Id);
            MembershipTierPointRules_Ctrl.apex_getIncentiveRuleData(membershipTier.Id);
            MembershipTierPointRules_Ctrl.apex_getRecordById(membershipTier.Id);
            MembershipTierPointRules_Ctrl.apex_getPicklistFieldValues('IncentiveRule__c', 'Status__c');
            MembershipTierPointRules_Ctrl.apex_saveJson(membershipTier.Id, '', 'Active', '2018-08-01', '2018-08-05', membershipTier.Name);
            
            
            MembershipTier__c membershipTier2 = TestClassHelper.createMembershipTier(membership);
            IncentiveRule__c incentiveRule2 = TestClassHelper.createIncentiveRule(membershipTier2);
            membershipTier2.IncentiveRuleNo__c = incentiveRule2.Id;
            update membershipTier2;
            MembershipTierPointRules_Ctrl.apex_saveJson(membershipTier.Id, '', 'Active', '2018-08-01', '2018-08-05', membershipTier.Name);
            
            membershipTier2.EffectiveFrom__c = Date.valueOf('2018-08-06');
            membershipTier2.EffectiveTo__c = Date.valueOf('2018-08-08');
            update membershipTier2;
            MembershipTierPointRules_Ctrl.apex_saveJson(membershipTier.Id, '', 'Active', '2018-08-01', '2018-08-05', membershipTier.Name);
            
            membershipTier2.EffectiveFrom__c = Date.valueOf('2018-08-01');
            membershipTier2.EffectiveTo__c = null;
            update membershipTier2;
            MembershipTierPointRules_Ctrl.apex_saveJson(membershipTier.Id, '', 'Active', '2018-08-01', '2018-08-05', membershipTier.Name);
        }
    }
    
}