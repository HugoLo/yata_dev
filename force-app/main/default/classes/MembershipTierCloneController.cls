public class MembershipTierCloneController {
		@AuraEnabled
    public static String apex_cloneMemberShipTier(String recordId){	
        
        // Initialize setup variables
        String objectName = 'MembershipTier__c';  // modify as needed
        String query = 'SELECT';
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
        
        // Grab the fields from the describe method and append them to the queryString one by one.
        Integer count = 0;
        for(String s : objectFields.keySet()) {
        	count++;
            if(count == (objectFields.size())){
                query += ' ' + s;
            }
            else{
            	query += ' ' + s + ', ';    
            }
        }
        
        // Manually add related object's fields that are needed.
        //query += 'Account.Name,'; // modify as needed
        
        // Strip off the last comma if it exists.
        if (query.subString(query.Length()-1,query.Length()) == ','){
            query = query.subString(0,query.Length()-1);
        }
        
        // Add FROM statement
        query += ' FROM ' + objectName;
        
        // Add on a WHERE/ORDER/LIMIT statement as needed
        //query += ' WHERE firstName = \'test\''; // modify as needed
        
  		system.debug('@@@query: '+query);
        MembershipTier__c membershipTier = database.query(query);
        
        MembershipTier__c cloneMembershipTier = membershipTier.clone();
        insert cloneMembershipTier;
        system.debug('@@@cloneMembershipTier: '+cloneMembershipTier);
        
                
        
        IncentiveRule__c incentiveRule = [SELECT Id, JSON__c, ObjectName__c, RecordID__c, RuleName__c FROM IncentiveRule__c WHERE RecordID__c = :recordId];
        system.debug('@@@incentiveRule: '+incentiveRule);
        IncentiveRule__c cloneIncentiveRule = incentiveRule.clone();
        insert cloneIncentiveRule;
        IncentiveRule__c updateIncentiveRule = [SELECT Id, Name, JSON__c, ObjectName__c, RecordID__c, RuleName__c FROM IncentiveRule__c WHERE Id = :cloneIncentiveRule.Id];
        
        String jsonString = updateIncentiveRule.JSON__c;
        Map<String, Object> jsonDict = (Map<String, Object>) JSON.deserializeUntyped(jsonString);
        Map<String, Object> BasicInfo = (Map<String, Object>) jsonDict.get('BasicInfo');
        Map<String, Object> Targeting = (Map<String, Object>) jsonDict.get('Targeting');
        Map<String, Object> Condition = (Map<String, Object>) jsonDict.get('Condition');
        Map<String, Object> Action = (Map<String, Object>) jsonDict.get('Action');
        BasicInfo.put('Status', null);
        BasicInfo.put('StartDate', null);
        BasicInfo.put('EndDate', null);
        BasicInfo.put('RuleFromRecdID', cloneMembershipTier.Id);
        BasicInfo.put('IncentiveRuleNo', updateIncentiveRule.Name);
        
        Action.put('Reward', null);
        Action.put('PointMultiplier', null);
        Action.put('PointAmount', null);
        Action.put('MaxTimes', null);
        Action.put('MaxPoint', null);
        
        jsonDict.put('BasicInfo', BasicInfo);
        jsonDict.put('Action', Action);
        jsonDict.put('Condition', null);
        updateIncentiveRule.RecordID__c = cloneMembershipTier.Id;
        updateIncentiveRule.JSON__c = JSON.serialize(jsonDict);
        update updateIncentiveRule;
        
        cloneMembershipTier.IncentiveRuleNo__c = updateIncentiveRule.Id;
        update cloneMembershipTier;
         
        return cloneMembershipTier.Id;
    }
}