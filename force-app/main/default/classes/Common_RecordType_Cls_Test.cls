@isTest
public class Common_RecordType_Cls_Test {
	static testmethod void Common_RecordType_Cls_Test(){
        Test.startTest();
            Id RecordId = Common_RecordType_Cls.getRtId('AccountMemberAccount');
            string rtname = Common_RecordType_Cls.getRTName(RecordId);
            RecordType objRT1 = Common_RecordType_Cls.getRt(RecordId);
            RecordType objRT2 = Common_RecordType_Cls.getRtRec('AccountMemberAccount');
            List<RecordType> ListRT1 = Common_RecordType_Cls.getRtList('Account');
            set<string> newStringSet = new set<string>();
            newStringSet.add('MemberAccount');
            newStringSet.add('DummyMemberAccount');
            set<Id> SetId = Common_RecordType_Cls.getRtIdSet('Account', newStringSet);
            Common_RecordType_Cls.getRtDevName(RecordId);
        Test.stopTest();
    }
}