@isTest
public class PointRules_Ctrl_Test {
    
    static testMethod void PointRules_Ctrl_Test(){

        TestClassHelper.createRuleSysConDefinition();
        TestClassHelper.createRulesSetting();
        Campaign campaign = TestClassHelper.createCampaign();
        Reward__c reward = TestClassHelper.createReward();
        TestClassHelper.createFieldType();
        TestClassHelper.createSpecificDateSetting();
        
        User userA = TestClassHelper.createUser();
        System.runAs(userA){
            
            Test.startTest();
            String recordJson = PointRules_Ctrl.apex_getRecordById('Campaign', campaign.Id);
            PointRules_Ctrl.apex_getBasicInfotxnCreate('txnCreate');
            String jsonString = '{"Action":{"Reward":null,"RefObjAccField":null,"RefObj":null,"PointTo":"Campaign","PointMultiplier":null,"PointGroup":null,"PointBase":"Net Spending","PointAmount":200,"MaxTimes":null,"MaxPoint":null,"ActionType":"FixPoint","ActionTo":"Self","ActionLevel":"Txn","ActionCon":null},"Condition":{"SysCon":"OneOff","RelatedTo":"Txn","Logic":"1 AND ( 2 OR 3 )","Detail":[{"Operator":"=","ObjectRelation":"Txn","ObjectName":"Transaction__c","ObjectLevel":"Header","No":1,"FieldValueString":"Mon","FieldValue":["Mon"],"FieldType":"Date","FieldName":"transactionday__c","rulesSettingName":"Transaction(Transaction)"},{"Operator":"=","ObjectRelation":"Shop","ObjectName":"Shop__c","ObjectLevel":"Header","No":2,"FieldValueString":"Shatin","FieldValue":["Shatin"],"FieldType":"String","FieldName":"District__c","rulesSettingName":"Shop(Transaction)"},{"Operator":"=","ObjectRelation":"Shop","ObjectName":"Shop__c","ObjectLevel":"Header","No":3,"FieldValueString":"Tai Po","FieldValue":["Tai Po"],"FieldType":"String","FieldName":"District__c","rulesSettingName":"Shop(Transaction)"}],"ConditionType":"OneOff","Amount":null},"BasicInfo":{"RuleFromObj":"Campaign","IncentiveRuleNo":"IR00000034","RuleName":"Monday Bonus","RuleFromRecdID":"701p0000000HTUQAA4","Status":"Active","StartDate":"2018-07-01","EndDate":"2018-11-30","TriggerPoint":{"TriggerSysCon":"txnCreate","TriggerObj":"Transaction__c","TriggerAction":"Create","TriggerField":null,"TriggerFieldValue":[]}},"Targeting":{"SpecificMemberOnly":false,"SpecificMemberList":[],"Logic":"1","Detail":[{"Operator":"=","ObjectRelation":"Account","ObjectName":"Account","No":1,"FieldValueString":"Male","FieldValue":["Male"],"FieldType":"String","FieldName":"Gender__pc","rulesSettingName":"Member"}]}}';
            PointRules_Ctrl.apex_createIncentiveRule(jsonString, campaign.Id, 'Campaign', campaign.Name, campaign.Status);
            PointRules_Ctrl.apex_getIncentiveRule(campaign.Id);
            PointRules_Ctrl.apex_getObjectField2('Campaign', 'Targeting');
            PointRules_Ctrl.apex_getAllReward();
            PointRules_Ctrl.apex_getConditionObjectField();
            PointRules_Ctrl.apex_getConditionType();
            PointRules_Ctrl.apex_getFieldTypeList();
            PointRules_Ctrl.apex_getCampaignMemberList(campaign.Id);
            PointRules_Ctrl.apex_getSpecificDateSetting();
            PointRules_Ctrl.apex_addConditionDetail();
            PointRules_Ctrl.apex_addTargetDetail();
            PointRules_Ctrl.apex_getObjectField('Campaign');
            Test.stopTest();
            
        }
        
    }

}