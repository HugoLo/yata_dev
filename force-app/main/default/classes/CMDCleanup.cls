public class CMDCleanup implements Schedulable, Database.Batchable<sObject> {

    public integer cleanup_last_n_days = 3;
    
    public CMDCleanup() {
        
    }

    public CMDCleanup(integer last_n_days) {
        cleanup_last_n_days = last_n_days;
    }

    /////Schedulable
    public void execute(SchedulableContext sc) {
        CMDCleanup c = new CMDCleanup();
        database.executebatch(c);
    }
    //////Schedulable
    
    /////Batchable
    public Database.QueryLocator start(Database.BatchableContext BC) {
        date d = system.today().addDays(-cleanup_last_n_days);
        return Database.getQueryLocator([SELECT Id FROM CMDQueue__c 
                                         WHERE (Status__c='Completed' OR Status__c='Merged')
                                         AND DateTime__c < :d]);
    }
    public void execute(Database.BatchableContext BC, List<sObject> batch) {
        delete batch;
    }
    
    public void finish(Database.BatchableContext BC) {
    }

    //////Batchable
}