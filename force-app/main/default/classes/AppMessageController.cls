public with sharing class AppMessageController {

    @AuraEnabled
    public static String apex_getAppMessageList(Map<String, String> parameter){
        String appMessageList = Common_HerokuIntegration_Cls.getAppMessageList(parameter);
        
        Map<String, Schema.SObjectField> appMessageFieldMap = CommonController.apex_getAllFieldset('InAppMessage__c');
		Map<String,Object> appMessageMap = (Map<String,Object>) JSON.deserializeUntyped(appMessageList);
        list<object> Messages = (list<object>)appMessageMap.get('Messages');
        for(Object message : Messages){
            Map<String, Object> messageMap = (Map<String, Object>) message;
            CommonController.convertFieldValue(appMessageFieldMap, messageMap);
        }
        return JSON.serialize(appMessageMap);
    } 
    
    @AuraEnabled
    public static String apex_getFieldset(String objectName, String fieldSetName){
        return CommonController.apex_getFieldset(objectName, fieldSetName);
    }
    /*
    @AuraEnabled
    public static String apex_getRecordType(String objectName){
        return CommonController.apex_getRecordType(objectName);
    }
    */
    @AuraEnabled
    public static String apex_getPicklistFieldValues(String objectName, String pickListFieldName){   
        return CommonController.apex_getPicklistFieldValues(objectName, pickListFieldName);
    }
    
    @AuraEnabled
    public static Integer apex_getNoRecordDisplay(String name){
        return CommonController.apex_getNoRecordDisplay(name);
    }
}