public class dev_util {

    static public string[] get_field_names(string sobject_name) {
        return new list<string>(schema.describeSObjects(new string[]{sobject_name})[0].fields.getMap().keyset());
    }
    
    static public string get_select_all_soql(string sobject_name) {
     	string[] field_names = get_field_names(sobject_name);
        return 'SELECT ' + string.join(field_names, ',') + ' FROM ' + sobject_name;
    }

    static public sobject getSObject(string sobject_name, id object_id) {
        string soql = get_select_all_soql(sobject_name) + ' WHERE id=:object_id';
        return database.query(soql);
    }
    
    static public account clone_account(id account_id) {
        account a = (account)getSObject('account', account_id);
        a.id = null;
        a.pgid__c = null;
        a.PersonMobilePhone = '1' + a.PersonMobilePhone;
        insert a;
        return a;
    }
    
}