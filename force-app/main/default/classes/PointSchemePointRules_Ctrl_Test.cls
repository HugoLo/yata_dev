@isTest
public class PointSchemePointRules_Ctrl_Test {

    static testMethod void PointSchemePointRules_Ctrl_Test(){
		User userA = TestClassHelper.createUser();
        System.runAs(userA){
            
            Membership__c membership = TestClassHelper.createMembership();
            MembershipTier__c membershipTier = TestClassHelper.createMembershipTier(membership);
            PointsScheme__c pointsScheme = TestClassHelper.createPointsScheme(membership, membershipTier);
            IncentiveRule__c incentiveRule = TestClassHelper.createIncentiveRule_PointScheme(pointsScheme);
            PointSchemePointRules_Ctrl.apex_getIncentiveRule(pointsScheme.Id);
            PointSchemePointRules_Ctrl.apex_getIncentiveRuleData(pointsScheme.Id);
            PointSchemePointRules_Ctrl.apex_getRecordById(pointsScheme.Id);
            PointSchemePointRules_Ctrl.apex_getPicklistFieldValues('IncentiveRule__c', 'Status__c');
            pointsScheme.IncentiveRuleNo__c = incentiveRule.Id;
            update pointsScheme;
			PointSchemePointRules_Ctrl.apex_saveJson(pointsScheme.Id, '', 'Active', '2019-01-10', '2019-01-13', pointsScheme.RuleCode__c, 'A'); 
             
            
            PointsScheme__c pointsScheme2 = TestClassHelper.createPointsScheme(membership, membershipTier);
            IncentiveRule__c incentiveRule2 = TestClassHelper.createIncentiveRule_PointScheme(pointsScheme2);
            pointsScheme2.IncentiveRuleNo__c = incentiveRule2.Id;
            update pointsScheme2;
            PointSchemePointRules_Ctrl.apex_saveJson(pointsScheme.Id, '', 'Active', '2019-01-10', '2019-01-13', pointsScheme.RuleCode__c, 'A'); 
            
            pointsScheme2.StartDate__c = Date.valueOf('2019-01-14');
            pointsScheme2.EndDate__c = Date.valueOf('2019-01-18');
            update pointsScheme2;
            PointSchemePointRules_Ctrl.apex_saveJson(pointsScheme.Id, '', 'Active', '2019-01-10', '2019-01-13', pointsScheme.RuleCode__c, 'A'); 
            
            pointsScheme2.StartDate__c = Date.valueOf('2019-01-13');
            pointsScheme2.EndDate__c = null;
            update pointsScheme2;
            PointSchemePointRules_Ctrl.apex_saveJson(pointsScheme.Id, '', 'Active', '2019-01-10', '2019-01-13', pointsScheme.RuleCode__c, 'A');
            
        }        
    } 
}