/********************************************************************************************
*                           Class Name: Common_Member_Cls
* Desciption: Class that provides common function for all classes to use
* 
* Version History:
* 2018-08-17 Created by Gordon Ho (Introv Limited)
*                                   
*-------------------------------------------------------------------------------------------*
*1.setDefaultValAll(List<Account> MemberList)
*   To assign default value(address and member's age) to member
*
*2.setDefaultValActive(List<Account> MemberList)
*   To update value when account is active:
*
*3.inactiveLoginFlag(List<Account> MemberList)
*   To check mobile contain inactive mobile key word, then inactive member.
*
********************************************************************************************/
public class Common_Member_Cls {
    
    public static void setDefaultValAll(List<Account> MemberList){
        
        Date today = Common_Utilities_Cls.getToday();
            
        for(account acct: MemberList){
            
            //To update mobile key:
            acct.PersonMobilePhone=acct.PersonMobilePhone==null ? null : acct.PersonMobilePhone.deleteWhitespace();	//remove all empty space of mobile no.
            acct.MobileKey__c = acct.CountryCode__pc+acct.PersonMobilePhone;
            
            //To update birthdate and age:
            if(acct.DateofBirthMM__pc !=null && acct.DateofBirthYYYY__pc != null){
                acct.SysDOB__pc = date.newinstance(acct.DateofBirthYYYY__pc.intValue(), acct.DateofBirthMM__pc.intValue(), 1);
                
                if(today.month()>=acct.DateofBirthMM__pc){
                    acct.Age__pc = today.year() - acct.DateofBirthYYYY__pc;
                }
                else{
                    acct.Age__pc = today.year() - acct.DateofBirthYYYY__pc - 1;
                }
            }
            
            //To update primary address:
           if((!String.isEmpty(acct.AppAddressLine1__pc) || !String.isEmpty(acct.AppAddressLine2__pc) || !String.isEmpty(acct.AppCountry__pc) || 
               !String.isEmpty(acct.AppDistrict__pc) || !String.isEmpty(acct.AppRegion__pc)) && acct.PostOptOut__pc == false){
                   acct.PrimaryAddressLine1__pc = acct.AppAddressLine1__pc;
                   acct.PrimaryAddressLine2__pc = acct.AppAddressLine2__pc;
                   acct.PrimaryCountry__pc = acct.AppCountry__pc;
                   acct.PrimaryDistrict__pc = acct.AppDistrict__pc;
                   acct.PrimaryRegion__pc = acct.AppRegion__pc;
            }
            else if(!String.isEmpty(acct.CreditCardAddressLine1__pc) || !String.isEmpty(acct.CreditCardAddressLine2__pc) || !String.isEmpty(acct.CreditCardCountry__pc) || 
                    !String.isEmpty(acct.CreditCardDistrict__pc) || !String.isEmpty(acct.CreditCardRegion__pc)){
                        acct.PrimaryAddressLine1__pc = acct.CreditCardAddressLine1__pc;
                   		acct.PrimaryAddressLine2__pc = acct.CreditCardAddressLine2__pc;
                   		acct.PrimaryCountry__pc = acct.CreditCardCountry__pc;
                   		acct.PrimaryDistrict__pc = acct.CreditCardDistrict__pc;
                   		acct.PrimaryRegion__pc = acct.CreditCardRegion__pc;
                    }
            else if(!String.isEmpty(acct.BBClubAddressLine1__pc) || !String.isEmpty(acct.BBClubAddressLine2__pc) || !String.isEmpty(acct.BBClubCountry__pc) || 
                    !String.isEmpty(acct.BBClubDistrict__pc) || !String.isEmpty(acct.BBClubRegion__pc)){
                        acct.PrimaryAddressLine1__pc = acct.BBClubAddressLine1__pc;
                   		acct.PrimaryAddressLine2__pc = acct.BBClubAddressLine2__pc;
                   		acct.PrimaryCountry__pc = acct.BBClubCountry__pc;
                   		acct.PrimaryDistrict__pc = acct.BBClubDistrict__pc;
                   		acct.PrimaryRegion__pc = acct.BBClubRegion__pc;
                    }
            else if((!String.isEmpty(acct.AppAddressLine1__pc) || !String.isEmpty(acct.AppAddressLine2__pc) || !String.isEmpty(acct.AppCountry__pc) || 
               !String.isEmpty(acct.AppDistrict__pc) || !String.isEmpty(acct.AppRegion__pc)) && acct.PostOptOut__pc == true){
                        acct.PrimaryAddressLine1__pc = acct.AppAddressLine1__pc;
                   		acct.PrimaryAddressLine2__pc = acct.AppAddressLine2__pc;
                   		acct.PrimaryCountry__pc = acct.AppCountry__pc;
                   		acct.PrimaryDistrict__pc = acct.AppDistrict__pc;
                   		acct.PrimaryRegion__pc = acct.AppRegion__pc;
                    }
        
    	}
    }
    public static void setDefaultValActive(List<Account> MemberList){
        
        Date today = Common_Utilities_Cls.getToday();
        DateTime now = DateTime.newInstance(today, DateTime.now().time());
        
        //Obtain membership ID
        List<Membership__c> l_membership = new List<Membership__c>();
        l_membership =[SELECT Id, Name, EffectiveFrom__c, EffectiveTo__c  FROM Membership__c 
                       WHERE EffectiveFrom__c<=:today AND (EffectiveTo__c =NULL OR EffectiveTo__c>=:today) ORDER BY EffectiveTo__c DESC NULLS FIRST];
        ID membershipID;
        if(l_membership.size()>0){
            membershipID = l_membership[0].Id;
            system.debug('membershipID equal='+membershipID);
        
            //Obtain membership Tier ID
            List<MembershipTier__c> l_membershipTier = new List<MembershipTier__c>();
            l_membershipTier = [SELECT ID, Name, EffectiveFrom__c, EffectiveTo__c, Membership__c, Sequence__c FROM MembershipTier__c
                               WHERE Membership__c =:membershipID AND EffectiveFrom__c<=:today AND (EffectiveTo__c =NULL OR EffectiveTo__c>=:today)
                               ORDER BY EffectiveTo__c DESC NULLS FIRST, Sequence__c ASC NULLS FIRST];
            ID membershipTierID;
            if(l_membershipTier.size()>0){
                membershipTierID = l_membershipTier[0].Id;
                system.debug('membershipTierID equal='+membershipTierID);
                
                for(account acct: MemberList){
                    
                    if(acct.DateJoin__pc == null){
                        acct.DateJoin__pc = today;
                    }
                    
                    if(acct.Membership__pc == null || acct.MembershipTier__pc == null){                 
                        acct.Membership__pc = membershipID;
                        acct.MembershipTier__pc = membershipTierID;
                    }
                    if(acct.MembershipAgeYear__pc == null){
                        acct.MembershipAgeYear__pc = 0;
                    }
                    acct.EffectiveFrom__pc = now;
                    acct.EffectiveTo__pc = null; //cross check with tier
                    acct.InactiveMobile__pc = false;
                    
                }
            }                  
        }      
    }
    
    public static void inactiveLoginFlag(List<Account> MemberList){

        //Getting inactive key from custom setting.
        LoyaltySetting__c inactiveKey = LoyaltySetting__c.getInstance('InactiveMobileKeyword');

            String inactiveKeyWord =inactiveKey.Text__c;
            
            for(account acct: MemberList){
            
                /**
                //Check if mobile contain inactive key word.
                if(!String.isEmpty(acct.PersonMobilePhone) && acct.PersonMobilePhone.contains(inactiveKeyWord)){
        if(inactiveKey!=null && !String.isEmpty(inactiveKey.Text__c) ){                    
                    //Update inactive flag to true
                    acct.InactiveMobile__pc = true;
                }
                }**/
                
                
                    //Update inactive flag to true
                    acct.InactiveMobile__pc = true;

            }
        
        
    }
    
    
}