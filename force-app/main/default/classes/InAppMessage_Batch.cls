global class InAppMessage_Batch implements Database.Batchable<sObject>, Database.AllowsCallouts {
    private String query;
	
	global InAppMessage_Batch() {
        
		List<CronTrigger> l_ct = new List<CronTrigger>();
        l_ct = [SELECT Id,CronJobDetailId,CronJobDetail.Name,NextFireTime,PreviousFireTime,State,StartTime,EndTime,CronExpression,TimeZoneSidKey,OwnerId,TimesTriggered 
                FROM CronTrigger
                WHERE CronJobDetail.Name like '%InAppMessage_Sched%' AND NextFireTime = null];
        if(l_ct!=null && l_ct.size()>0){
            System.abortJob(l_ct[0].id);
        }
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator([SELECT Id
                                         FROM InAppMessage__c  
                                         WHERE ((SyncStatus__c = 'Open') OR (SyncStatus__c = 'Failed' AND (RetryTimes__c <= 3 OR RetryTimes__c = NULL))) 
                                         ORDER BY CreatedDate, Member__c ASC
                                        ]);
	}

   	global void execute(Database.BatchableContext BC, List<InAppMessage__c> scope) {
        
        String successCode = '1';
        Set<Id> s_inappmessage = new Set<Id>();
        for(InAppMessage__c inappmessage : scope){
            s_inappmessage.add(inappmessage.Id);
        }
        
        Map<Id, InAppMessage__c> m_inappmessage = new Map<Id, InAppMessage__c>([SELECT Id, Member__c, Name, RedirectURL__c, PushNotification__c,
                                                ContentC__c, ContentE__c, 
                                                TitleC__c, TitleE__c, 
                                                SyncStatus__c, RetryTimes__c, 
                                                SystemLog__c, SyncTime__c, Status__c, CreatedDate, TemplateCode__c
                                                 FROM InAppMessage__c  
                                                 WHERE ((SyncStatus__c = 'Open') OR (SyncStatus__c = 'Failed' AND (RetryTimes__c <= 3 OR RetryTimes__c = NULL))) 
                                                 AND Id IN :s_inappmessage
                                                FOR UPDATE
                                                ]);
        
        Map<Id, InAppMessage__c> m_failed_inappmessage = new Map<Id, InAppMessage__c>();
        Map<Id, InAppMessage__c> m_validated_inappmessage = new Map<Id, InAppMessage__c>();
        List<Common_HerokuIntegration_Cls.AppMessage> l_inappmessage = new List<Common_HerokuIntegration_Cls.AppMessage>();
        Common_HerokuIntegration_Cls.AppMessageList inappmessage_Wrapper = new Common_HerokuIntegration_Cls.AppMessageList();
        for(InAppMessage__c inappmessage : m_inappmessage.values()){
            Boolean isCorrect = true;
            String SystemLog = '';
            if(inappmessage.Member__c == null){
                isCorrect = false;
                SystemLog += 'Missing MemberId\n';
            }
            if(String.isBlank(inappmessage.ContentC__c) ){
                isCorrect = false;
                SystemLog += 'Missing Content (C)\n';
            }
            if(String.isBlank(inappmessage.ContentE__c) ){
                isCorrect = false;
                SystemLog += 'Missing Content (E)\n';
            }
            if(String.isBlank(inappmessage.TitleC__c) ){
                isCorrect = false;
                SystemLog += 'Title (C)\n';
            }
            if(String.isBlank(inappmessage.TitleE__c) ){
                isCorrect = false;
                SystemLog += 'Title (E)';
            }
            
            if(!isCorrect){
                inappmessage.SyncStatus__c = 'On Hold';
                inappmessage.RetryTimes__c = inappmessage.RetryTimes__c != null ? inappmessage.RetryTimes__c+1 : 1;   
                inappmessage.SystemLog__c = SystemLog;
                m_failed_inappmessage.put(inappmessage.Id, inappmessage);
            }
            else{
                Common_HerokuIntegration_Cls.AppMessage appmessage = new Common_HerokuIntegration_Cls.AppMessage();
                appmessage.ContentC = inappmessage.ContentC__c;
                appmessage.ContentE = inappmessage.ContentE__c;
                appmessage.TitleC = inappmessage.TitleC__c;
                appmessage.TitleE = inappmessage.TitleE__c;
                appmessage.Status = inappmessage.Status__c;
                appmessage.MemberSFId = inappmessage.Member__c;
                appmessage.RedirectURL = inappmessage.RedirectURL__c;
                appmessage.Name = inappmessage.Name;
                appmessage.CreatedDate = inappmessage.CreatedDate;
                appmessage.TemplateCode = inappmessage.TemplateCode__c;
                appmessage.RecordId = inappmessage.Id;
                appmessage.NeedPush = inappmessage.PushNotification__c;
                l_inappmessage.add(appmessage);
                m_validated_inappmessage.put(inappmessage.Id, inappmessage);
            }      
        }
        inappmessage_Wrapper.appMessageList = l_inappmessage;
        
        if(l_inappmessage.size() > 0){
            
            Common_HerokuIntegration_Cls.InAppMessageResponse adjResponse = Common_HerokuIntegration_Cls.batchInAppMessage(JSON.serialize(inappmessage_Wrapper));
            if(adjResponse != null ){
                
                //All Success:
                if(adjResponse.ReturnCode == successCode){
                    for(InAppMessage__c inappmessage : m_validated_inappmessage.values()){
                        inappmessage.SyncTime__c = system.now();
                        inappmessage.RetryTimes__c = inappmessage.RetryTimes__c != null ? (inappmessage.RetryTimes__c+1) : 1;
                        inappmessage.SyncStatus__c = 'Completed';
                        inappmessage.SystemLog__c = null;
                    }
                    Database.update(m_validated_inappmessage.values(), false);
                }
                //Partial Success / All Failed:
                else{
                    if(adjResponse.ReturnDetails != null && adjResponse.ReturnDetails.size() > 0 ){
                        for(Common_HerokuIntegration_Cls.InAppMessageResponseDetail dt : adjResponse.ReturnDetails){
                            m_validated_inappmessage.get(dt.RecordId).SyncTime__c = system.now();
                            m_validated_inappmessage.get(dt.RecordId).RetryTimes__c = m_validated_inappmessage.get(dt.RecordId).RetryTimes__c != null ? (m_InAppMessage.get(dt.RecordId).RetryTimes__c+1) : 1;
                            if(dt.ReturnCode == successCode){
                                m_validated_inappmessage.get(dt.RecordId).SyncStatus__c = 'Completed';
                                m_validated_inappmessage.get(dt.RecordId).SystemLog__c = null;
                            }else{
                                m_validated_inappmessage.get(dt.RecordId).SyncStatus__c = 'Failed';
                                m_validated_inappmessage.get(dt.RecordId).SystemLog__c = dt.ReturnMessage;
                            }
                        }
                    }else{
                        for(InAppMessage__c inappmessage : m_validated_inappmessage.values()){
                            inappmessage.SyncTime__c = system.now();
                            inappmessage.SyncStatus__c = 'Failed';
                            inappmessage.RetryTimes__c = inappmessage.RetryTimes__c != null ? (inappmessage.RetryTimes__c+1) : 1;
                            inappmessage.SystemLog__c ='Connect to Heroku failed.';
                        }
                    }
                    Database.update(m_validated_inappmessage.values(), false);
                }
            }
        }
        
        if(m_failed_inappmessage.size() > 0){
            Database.update(m_failed_inappmessage.values(), false);
        }
      

	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
    
	
}