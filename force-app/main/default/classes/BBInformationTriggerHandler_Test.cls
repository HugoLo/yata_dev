@isTest
public class BBInformationTriggerHandler_Test {
    
	static testMethod void BBInformationTriggerHandler_Test(){
        
        
        User userA = TestClassHelper.createUser();
        System.runAs(userA){
            
            TestClassHelper.createObjectControl('Account');
            TestClassHelper.createObjectControl('BBInformation__c');
            //TestClassHelper.createObjectControl('Subscription__c');
            
            Account account = TestClassHelper.createMember();
            account.PGID__c = 123456;
            update account;
            
            Subscription__c subscription = TestClassHelper.createSubscription(account);
            BBInformation__c BBInformation = TestClassHelper.createBBInformation(account, subscription);
            BBInformation.Member__c = null;
            update BBInformation;
            
            LogShipment__c logShipment = [SELECT Id, RetryTimes__c, Source__c, Status__c, ActionType__c, Detail__c
                                             FROM LogShipment__c 
                                             WHERE Object__c = 'BBInformation__c' 
                                             LIMIT 1];
           
            
            String jsonString = logShipment.Detail__c;
            Map<String, Object> meta = (Map<String, Object>) JSON.deserializeUntyped(jsonString);
            meta.remove('Id');
            jsonString = JSON.serialize( meta );
            TestClassHelper.createLogShipment('Insert', 'BBInformation__c', jsonString);
            meta.put('Id', BBInformation.Id);
            meta.put('PGID__c', 123456);
            jsonString = JSON.serialize( meta );
            TestClassHelper.createLogShipment('Update', 'BBInformation__c', jsonString);
            
            
        	update BBInformation;
            delete BBInformation;
            
        }
        
    }
    
}