@isTest
public class ContentTriggerHandler_Test {
	
	static testMethod void ContentTriggerHandler_Test(){
        
        
        User userA = TestClassHelper.createUser();
        System.runAs(userA){
            
            TestClassHelper.createObjectControl('FixContent__c');
            TestClassHelper.createObjectControl('Content__c');
            
            FixContent__c FixContent = TestClassHelper.createFixContent();
            update FixContent;
            
            
            Content__c Content = TestClassHelper.createContent(FixContent);
        	update Content;
            
            delete Content;
            delete FixContent;
            
            
        }
        
    }
    
}