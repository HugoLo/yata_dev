public class BBInformationTriggerHandler extends TriggerHandler{
    //private static final String ObjName = 'BBInformation__c';
	private static final String ObjName = Trigger.isDelete ? Trigger.Old.getSObjectType().getDescribe().getName() : Trigger.New.getSObjectType().getDescribe().getName();
        
    public BBInformationTriggerHandler(){}
    
    protected override void beforeInsert() {
        system.debug('@@@beforeInsert');
        
        List<BBInformation__c> l_BBInformation = (List<BBInformation__c>)Trigger.new;
        system.debug('@@@l_BBInformation '+l_BBInformation);
        Set<Double> s_MemberPGID = new Set<Double>();
        Set<Double> s_SubscriptionPGID = new Set<Double>();
        for(BBInformation__c BBInformation : l_BBInformation){
            Double MemberPGID = BBInformation.MemberPGID__c;
            Id MemberId = BBInformation.Member__c;
            Id SubscriptionId = BBInformation.Subscription__c;
            Double SubscriptionPGID = BBInformation.SubscriptionPGID__c;
            
            if(MemberPGID != null && MemberId == null){
                s_MemberPGID.add(MemberPGID);
            }
            if(SubscriptionPGID != null && SubscriptionId == null){
                s_SubscriptionPGID.add(SubscriptionPGID);
            }
            
        }
        
        system.debug('@@@s_MemberPGID '+s_MemberPGID);
        system.debug('@@@s_SubscriptionPGID '+s_SubscriptionPGID);
        if(s_MemberPGID.size() > 0){
            
            List<Account> l_account = [SELECT Id, PGID__c 
                                       FROM Account 
                                       WHERE PGID__c IN :s_MemberPGID];
            system.debug('@@@l_account '+l_account);
            
            Map<Double, Id> m_MemberId = new Map<Double, Id>();
            for(Account account : l_account){
                Double PGID = account.PGID__c;
                Id memberId = account.Id;
                if(PGID != null){
                    m_MemberId.put(PGID, memberId);    
                }
            }
            
            system.debug('@@@m_MemberId '+m_MemberId);
            for(BBInformation__c BBInformation : l_BBInformation){
                Double MemberPGID = BBInformation.MemberPGID__c;
                Boolean contains = m_MemberId.containsKey(MemberPGID);
                if(contains){
                    Id MemberId = m_MemberId.get(MemberPGID);
                    BBInformation.Member__c = MemberId;
                }
            }
        }
        
        if(s_SubscriptionPGID.size() > 0){
            
            List<Subscription__c> l_subscription = [SELECT Id, PGID__c 
                                       FROM Subscription__c 
                                       WHERE PGID__c IN :s_SubscriptionPGID];
            system.debug('@@@l_subscription '+l_subscription);
            
            Map<Double, Id> m_SubscriptionId = new Map<Double, Id>();
            for(Subscription__c subscription : l_subscription){
                Double PGID = subscription.PGID__c;
                Id subscriptionId = subscription.Id;
                if(PGID != null){
                    m_SubscriptionId.put(PGID, subscriptionId);    
                }
            }
            
            system.debug('@@@m_SubscriptionId '+m_SubscriptionId);
            for(BBInformation__c BBInformation : l_BBInformation){
                Double SubscriptionPGID = BBInformation.SubscriptionPGID__c;
                Boolean contains = m_SubscriptionId.containsKey(SubscriptionPGID);
                if(contains){
                    Id SubscriptionId = m_SubscriptionId.get(SubscriptionPGID);
                    BBInformation.Subscription__c = SubscriptionId;
                }
            }
        }
        
        
  	}
    
    protected override void beforeUpdate () {
        system.debug('@@@beforeUpdate');
        
        List<BBInformation__c> l_BBInformation = (List<BBInformation__c>)Trigger.new;
        system.debug('@@@l_BBInformation '+l_BBInformation);
        Set<Double> s_MemberPGID = new Set<Double>();
        for(BBInformation__c BBInformation : l_BBInformation){
            Double MemberPGID = BBInformation.MemberPGID__c;
            Id MemberId = BBInformation.Member__c;
            
            if(MemberPGID != null && MemberId == null){
                s_MemberPGID.add(MemberPGID);
            }
            
        }
        
        system.debug('@@@s_MemberPGID '+s_MemberPGID);
        if(s_MemberPGID.size() > 0){
            
            List<Account> l_account = [SELECT Id, PGID__c 
                                       FROM Account 
                                       WHERE PGID__c IN :s_MemberPGID];
            system.debug('@@@l_account '+l_account);
            
            Map<Double, Id> m_MemberId = new Map<Double, Id>();
            for(Account account : l_account){
                Double PGID = account.PGID__c;
                Id memberId = account.Id;
                if(PGID != null){
                    m_MemberId.put(PGID, memberId);    
                }
            }
            
            system.debug('@@@m_MemberId '+m_MemberId);
            for(BBInformation__c BBInformation : l_BBInformation){
                Double MemberPGID = BBInformation.MemberPGID__c;
                Boolean contains = m_MemberId.containsKey(MemberPGID);
                if(contains){
                    Id MemberId = m_MemberId.get(MemberPGID);
                    BBInformation.Member__c = MemberId;
                }
            }
        }
  	}
    
    /*
    protected override void beforeDelete () {
        system.debug('@@@beforeDelete');
  	}
    */    
    protected override void afterInsert () {
        system.debug('@@@BBInfo afterInsert');
        if(Common_Security_Cls.toRunTriggerMethod('BBInformationTriggerHandler','afterInsert')){
            
            //Create Log shipment records:
        	if(Common_Utilities_Cls.needLogShipment(ObjName)){
	            LogShipmentHandler.createLogShipment(ObjName, Trigger.new);   
            }
    	}
    }
    
    protected override void afterDelete () {
        if(Common_Security_Cls.toRunTriggerMethod('BBInformationTriggerHandler','afterDelete')){
        	//Create Log shipment records:
            if(Common_Utilities_Cls.needLogShipment(ObjName)){
	        	LogShipmentHandler.createDeleteLogShipment(ObjName, Trigger.old);
            }   
        }
  	}
    
        protected override void afterUpdate () {
        system.debug('@@@Account afterUpdate');
        if(Common_Security_Cls.toRunTriggerMethod('BBInformationTriggerHandler','afterUpdate')){
            
            //Create Log shipment records:
            if(Common_Utilities_Cls.needLogShipment(ObjName)){
	        	LogShipmentHandler.updateLogShipment(ObjName, Trigger.new, Trigger.oldMap);
            }
        }
    }
    /*
    protected override void afterUndelete  () {
		system.debug('@@@afterUndelete');        
  	}
	*/
}