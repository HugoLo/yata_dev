trigger CampaignAfterTrigger on Campaign (after insert, after update) {
    set<id> campaignIds = new set<id>();
    for(Campaign c : trigger.new) {
        if(c.Status=='Active') {
            if(trigger.isInsert || (trigger.isUpdate && trigger.oldMap.get(c.Id).Status!='Active'))
                campaignIds.add(c.id);
        }
    }
    for(id cid : campaignIds) {
        UpdateCampaignMemberList.run(cid);
    }
}