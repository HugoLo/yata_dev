trigger SubscriptionTrigger on Subscription__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    if(Common_Security_Cls.isUserTriggerOn()){
        new SubscriptionTriggerHandler().run();    
    }
}