trigger CampaignTrigger on Campaign (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    if(Common_Security_Cls.isUserTriggerOn()){
        new CampaignTriggerHandler().run();    
    }
}